-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 24, 2015 at 03:20 PM
-- Server version: 5.5.44
-- PHP Version: 5.3.10-1ubuntu3.19

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `easamoveadmin_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `easamoveadmin_tbl_admin`
--

CREATE TABLE IF NOT EXISTS `easamoveadmin_tbl_admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(50) NOT NULL,
  `admin_email` varchar(200) NOT NULL,
  `admin_password` varchar(250) NOT NULL,
  `admin_role` varchar(30) NOT NULL,
  `admin_access` smallint(6) NOT NULL DEFAULT '0',
  `admin_verifykey` varchar(100) NOT NULL,
  `admin_verified` tinyint(4) NOT NULL,
  `admin_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `admin_created_by` int(11) NOT NULL,
  `admin_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `admin_modified_by` int(11) NOT NULL,
  `admin_is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `easamoveadmin_tbl_admin`
--

INSERT INTO `easamoveadmin_tbl_admin` (`admin_id`, `admin_name`, `admin_email`, `admin_password`, `admin_role`, `admin_access`, `admin_verifykey`, `admin_verified`, `admin_created_on`, `admin_created_by`, `admin_modified_on`, `admin_modified_by`, `admin_is_deleted`) VALUES
(1, 'admin', 'admin@easasoft.com', 'password', 'Super Admin', 1, '', 1, '2015-09-24 07:46:30', 0, '0000-00-00 00:00:00', 0, 0),
(6, 'praba', 'praba.rayan@gmail.com', 'praba@123', 'manager', 3, '', 1, '2015-09-24 07:44:14', 1, '0000-00-00 00:00:00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `easamoveadmin_tbl_agent`
--

CREATE TABLE IF NOT EXISTS `easamoveadmin_tbl_agent` (
  `agent_id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_name` varchar(250) NOT NULL,
  `agent_profilename` varchar(250) NOT NULL,
  `agent_type` int(11) NOT NULL,
  `agent_email` varchar(100) NOT NULL,
  `agent_telephone` varchar(20) NOT NULL,
  `agent_fax` varchar(20) NOT NULL,
  `agent_postalcode` varchar(50) NOT NULL,
  `agent_street` varchar(200) NOT NULL,
  `agent_city` varchar(100) NOT NULL,
  `agent_country` varchar(100) NOT NULL,
  `agent_facebookurl` varchar(300) NOT NULL,
  `agent_twitterurl` varchar(300) NOT NULL,
  `agent_linkedinurl` varchar(300) NOT NULL,
  `agent_logo` varchar(300) NOT NULL,
  `agent_aboutus` text NOT NULL,
  `agent_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `agent_created_by` int(11) NOT NULL,
  `agent_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `agent_modified_by` int(11) NOT NULL,
  `agent_is_deleted` int(11) NOT NULL,
  `agent_lat` double NOT NULL,
  `agent_long` double NOT NULL,
  PRIMARY KEY (`agent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `easamoveadmin_tbl_agent`
--

INSERT INTO `easamoveadmin_tbl_agent` (`agent_id`, `agent_name`, `agent_profilename`, `agent_type`, `agent_email`, `agent_telephone`, `agent_fax`, `agent_postalcode`, `agent_street`, `agent_city`, `agent_country`, `agent_facebookurl`, `agent_twitterurl`, `agent_linkedinurl`, `agent_logo`, `agent_aboutus`, `agent_created_on`, `agent_created_by`, `agent_modified_on`, `agent_modified_by`, `agent_is_deleted`, `agent_lat`, `agent_long`) VALUES
(1, 'agent1', 'agent1', 0, 'fdsfdsf@vcvcx.cc', '3231231234', '4343432424', 'NSW 2150', '212 George Street, Parramatta NSW 2150, Australia', ' Parramatta NSW 2150', ' Australia', '', '', '', '1_2.jpg', 'some test data', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0, -33.81737601124859, 151.01654052734375),
(2, 'agent', 'agent1', 0, 'fdsfdsf@vcvcx.cc', '3231231234', '4343432424', 'NSW 2150', '54 Aldington Road, Kemps Creek NSW 2178, Australia', ' Kemps Creek NSW 2178', ' Australia', '', '', '', 'agent_1441775194.jpg', 'some test data', '2015-09-09 05:06:34', 1, '0000-00-00 00:00:00', 1, 0, -33.83106633658368, 150.81192016601562);

-- --------------------------------------------------------

--
-- Table structure for table `easamoveadmin_tbl_agenttype`
--

CREATE TABLE IF NOT EXISTS `easamoveadmin_tbl_agenttype` (
  `agent_id` int(11) NOT NULL AUTO_INCREMENT,
  `agent_name` varchar(250) NOT NULL,
  `agent_desc` text NOT NULL,
  `agent_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `agent_created_by` int(11) NOT NULL,
  `agent_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `agent_modified_by` int(11) NOT NULL,
  `agent_is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`agent_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `easamoveadmin_tbl_agenttype`
--

INSERT INTO `easamoveadmin_tbl_agenttype` (`agent_id`, `agent_name`, `agent_desc`, `agent_created_on`, `agent_created_by`, `agent_modified_on`, `agent_modified_by`, `agent_is_deleted`) VALUES
(1, 'letting agent', 'test', '2015-09-16 07:20:37', 1, '0000-00-00 00:00:00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `easamoveadmin_tbl_alerttype`
--

CREATE TABLE IF NOT EXISTS `easamoveadmin_tbl_alerttype` (
  `alert_id` int(11) NOT NULL AUTO_INCREMENT,
  `alert_name` varchar(250) NOT NULL,
  `alert_desc` text NOT NULL,
  `alert_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `alert_created_by` int(11) NOT NULL,
  `alert_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `alert_modified_by` int(11) NOT NULL,
  `alert_is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`alert_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `easamoveadmin_tbl_alerttype`
--

INSERT INTO `easamoveadmin_tbl_alerttype` (`alert_id`, `alert_name`, `alert_desc`, `alert_created_on`, `alert_created_by`, `alert_modified_on`, `alert_modified_by`, `alert_is_deleted`) VALUES
(1, 'test', 'test', '2015-09-15 07:02:26', 1, '0000-00-00 00:00:00', 1, 0),
(2, 'test1', 'vvcxcxczxdc', '2015-09-15 09:56:35', 1, '0000-00-00 00:00:00', 1, 0),
(3, 'dsfsdf#$%#$$&%45$#$($(@#($@#)$)#@$(@#$*@#*$@#*$*@$@*#$*&', '', '2015-09-02 06:30:05', 1, '0000-00-00 00:00:00', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `easamoveadmin_tbl_category`
--

CREATE TABLE IF NOT EXISTS `easamoveadmin_tbl_category` (
  `category_id` int(11) NOT NULL AUTO_INCREMENT,
  `category_name` varchar(250) NOT NULL,
  `category_desc` text NOT NULL,
  `category_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `category_created_by` int(11) NOT NULL,
  `category_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `category_modified_by` int(11) NOT NULL,
  `category_is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `easamoveadmin_tbl_category`
--

INSERT INTO `easamoveadmin_tbl_category` (`category_id`, `category_name`, `category_desc`, `category_created_on`, `category_created_by`, `category_modified_on`, `category_modified_by`, `category_is_deleted`) VALUES
(1, 'commercial', 'test', '2015-09-16 07:52:34', 1, '0000-00-00 00:00:00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `easamoveadmin_tbl_centralheatingtype`
--

CREATE TABLE IF NOT EXISTS `easamoveadmin_tbl_centralheatingtype` (
  `centralheating_id` int(11) NOT NULL AUTO_INCREMENT,
  `centralheating_name` varchar(250) NOT NULL,
  `centralheating_desc` text NOT NULL,
  `centralheating_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `centralheating_created_by` int(11) NOT NULL,
  `centralheating_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `centralheating_modified_by` int(11) NOT NULL,
  `centralheating_is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`centralheating_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `easamoveadmin_tbl_centralheatingtype`
--

INSERT INTO `easamoveadmin_tbl_centralheatingtype` (`centralheating_id`, `centralheating_name`, `centralheating_desc`, `centralheating_created_on`, `centralheating_created_by`, `centralheating_modified_on`, `centralheating_modified_by`, `centralheating_is_deleted`) VALUES
(1, 'center room', 'test', '2015-09-16 07:36:14', 1, '0000-00-00 00:00:00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `easamoveadmin_tbl_city`
--

CREATE TABLE IF NOT EXISTS `easamoveadmin_tbl_city` (
  `city_id` int(11) NOT NULL AUTO_INCREMENT,
  `city_name` varchar(200) NOT NULL,
  `city_code` varchar(20) NOT NULL,
  `state_id` int(11) NOT NULL,
  `city_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `city_created_by` int(11) NOT NULL,
  `city_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `city_modified_by` int(11) NOT NULL,
  `city_is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`city_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `easamoveadmin_tbl_city`
--

INSERT INTO `easamoveadmin_tbl_city` (`city_id`, `city_name`, `city_code`, `state_id`, `city_created_on`, `city_created_by`, `city_modified_on`, `city_modified_by`, `city_is_deleted`) VALUES
(1, 'test1', 'ADC', 0, '2015-09-02 05:44:43', 1, '0000-00-00 00:00:00', 1, 1),
(2, 'test', 'new ', 3, '2015-09-02 05:44:20', 1, '0000-00-00 00:00:00', 1, 0),
(3, 'ddd', 'ddd', 5, '2015-09-02 09:52:49', 1, '0000-00-00 00:00:00', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `easamoveadmin_tbl_country`
--

CREATE TABLE IF NOT EXISTS `easamoveadmin_tbl_country` (
  `country_id` int(11) NOT NULL AUTO_INCREMENT,
  `country_code` varchar(10) NOT NULL,
  `country_name` varchar(200) NOT NULL,
  `country_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `country_created_by` int(11) NOT NULL,
  `country_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `country_modified_by` int(11) NOT NULL,
  `country_is_deleted` smallint(6) NOT NULL,
  PRIMARY KEY (`country_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `easamoveadmin_tbl_country`
--

INSERT INTO `easamoveadmin_tbl_country` (`country_id`, `country_code`, `country_name`, `country_created_on`, `country_created_by`, `country_modified_on`, `country_modified_by`, `country_is_deleted`) VALUES
(1, 'fasdfds', 'test24252', '2015-09-01 07:35:25', 1, '0000-00-00 00:00:00', 1, 1),
(3, 'USA', 'America', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0),
(5, 'UK', 'United Kingdom', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0),
(6, 'IN', 'India', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0),
(7, 'fasdfds', 'test', '2015-09-01 07:37:27', 1, '0000-00-00 00:00:00', 1, 1),
(8, 'fasdfds', 'dssdsdsd', '2015-09-01 07:37:22', 1, '0000-00-00 00:00:00', 1, 1),
(9, 'fasdfds', 'test', '2015-09-01 07:36:32', 1, '0000-00-00 00:00:00', 1, 1),
(10, '', '', '2015-09-02 09:34:46', 1, '0000-00-00 00:00:00', 1, 1),
(11, 'ffff', 'dddd', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `easamoveadmin_tbl_decorativecondition`
--

CREATE TABLE IF NOT EXISTS `easamoveadmin_tbl_decorativecondition` (
  `condition_id` int(11) NOT NULL AUTO_INCREMENT,
  `condition_name` varchar(250) NOT NULL,
  `condition_desc` text NOT NULL,
  `condition_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `condition_created_by` int(11) NOT NULL,
  `condition_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `condition_modified_by` int(11) NOT NULL,
  `condition_is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`condition_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `easamoveadmin_tbl_decorativecondition`
--

INSERT INTO `easamoveadmin_tbl_decorativecondition` (`condition_id`, `condition_name`, `condition_desc`, `condition_created_on`, `condition_created_by`, `condition_modified_on`, `condition_modified_by`, `condition_is_deleted`) VALUES
(1, 'test', 'test', '2015-09-15 10:17:47', 1, '0000-00-00 00:00:00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `easamoveadmin_tbl_enquirytype`
--

CREATE TABLE IF NOT EXISTS `easamoveadmin_tbl_enquirytype` (
  `enquiry_id` int(11) NOT NULL AUTO_INCREMENT,
  `enquiry_name` varchar(250) NOT NULL,
  `enquiry_desc` text NOT NULL,
  `enquiry_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `enquiry_created_by` int(11) NOT NULL,
  `enquiry_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `enquiry_modified_by` int(11) NOT NULL,
  `enquiry_is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`enquiry_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `easamoveadmin_tbl_enquirytype`
--

INSERT INTO `easamoveadmin_tbl_enquirytype` (`enquiry_id`, `enquiry_name`, `enquiry_desc`, `enquiry_created_on`, `enquiry_created_by`, `enquiry_modified_on`, `enquiry_modified_by`, `enquiry_is_deleted`) VALUES
(1, 'Request view', 'test', '2015-09-15 06:53:16', 1, '0000-00-00 00:00:00', 1, 0),
(2, 'fsdfsdfd', '', '2015-09-04 10:41:28', 1, '0000-00-00 00:00:00', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `easamoveadmin_tbl_features`
--

CREATE TABLE IF NOT EXISTS `easamoveadmin_tbl_features` (
  `features_id` int(11) NOT NULL AUTO_INCREMENT,
  `features_name` varchar(250) NOT NULL,
  `features_comments` text NOT NULL,
  `features_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `features_created_by` int(11) NOT NULL,
  `features_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `features_modified_by` int(11) NOT NULL,
  `features_is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`features_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `easamoveadmin_tbl_features`
--

INSERT INTO `easamoveadmin_tbl_features` (`features_id`, `features_name`, `features_comments`, `features_created_on`, `features_created_by`, `features_modified_on`, `features_modified_by`, `features_is_deleted`) VALUES
(1, 'Wood floors', 'Wood floors', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0),
(2, 'Waterfront', 'Waterfront', '2015-09-02 13:29:34', 1, '0000-00-00 00:00:00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `easamoveadmin_tbl_feedbackcategory`
--

CREATE TABLE IF NOT EXISTS `easamoveadmin_tbl_feedbackcategory` (
  `feedbackcategory_id` int(11) NOT NULL AUTO_INCREMENT,
  `feedbackcategory_name` varchar(250) NOT NULL,
  `feedbackcategory_desc` text NOT NULL,
  `feedbackcategory_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `feedbackcategory_created_by` int(11) NOT NULL,
  `feedbackcategory_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `feedbackcategory_modified_by` int(11) NOT NULL,
  `feedbackcategory_is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`feedbackcategory_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `easamoveadmin_tbl_feedbackcategory`
--

INSERT INTO `easamoveadmin_tbl_feedbackcategory` (`feedbackcategory_id`, `feedbackcategory_name`, `feedbackcategory_desc`, `feedbackcategory_created_on`, `feedbackcategory_created_by`, `feedbackcategory_modified_on`, `feedbackcategory_modified_by`, `feedbackcategory_is_deleted`) VALUES
(1, 'Suggestions for Improvement', 'test', '2015-09-16 10:09:49', 1, '0000-00-00 00:00:00', 1, 0),
(2, 'House Prices', '', '2015-09-03 10:40:04', 1, '0000-00-00 00:00:00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `easamoveadmin_tbl_feedbacksubcategory`
--

CREATE TABLE IF NOT EXISTS `easamoveadmin_tbl_feedbacksubcategory` (
  `feedbacksubcategory_id` int(11) NOT NULL AUTO_INCREMENT,
  `feedbackcategory_id` int(11) NOT NULL,
  `feedbacksubcategory_name` varchar(250) NOT NULL,
  `feedbacksubcategory_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `feedbacksubcategory_created_by` int(11) NOT NULL,
  `feedbacksubcategory_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `feedbacksubcategory_modified_by` int(11) NOT NULL,
  `feedbacksubcategory_is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`feedbacksubcategory_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `easamoveadmin_tbl_feedbacksubcategory`
--

INSERT INTO `easamoveadmin_tbl_feedbacksubcategory` (`feedbacksubcategory_id`, `feedbackcategory_id`, `feedbacksubcategory_name`, `feedbacksubcategory_created_on`, `feedbacksubcategory_created_by`, `feedbacksubcategory_modified_on`, `feedbacksubcategory_modified_by`, `feedbacksubcategory_is_deleted`) VALUES
(1, 2, 'test12', '2015-09-03 11:04:13', 1, '0000-00-00 00:00:00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `easamoveadmin_tbl_furnishes`
--

CREATE TABLE IF NOT EXISTS `easamoveadmin_tbl_furnishes` (
  `furnishes_id` int(11) NOT NULL AUTO_INCREMENT,
  `furnishes_name` varchar(250) NOT NULL,
  `furnishes_desc` text NOT NULL,
  `furnishes_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `furnishes_created_by` int(11) NOT NULL,
  `furnishes_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `furnishes_modified_by` int(11) NOT NULL,
  `furnishes_is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`furnishes_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `easamoveadmin_tbl_furnishes`
--

INSERT INTO `easamoveadmin_tbl_furnishes` (`furnishes_id`, `furnishes_name`, `furnishes_desc`, `furnishes_created_on`, `furnishes_created_by`, `furnishes_modified_on`, `furnishes_modified_by`, `furnishes_is_deleted`) VALUES
(1, 'Furnished/Unfurnished', 'tset', '2015-09-16 09:32:21', 1, '0000-00-00 00:00:00', 1, 0),
(2, 'Furnished/Unfurnished', '', '2015-09-03 07:03:01', 1, '0000-00-00 00:00:00', 1, 1),
(3, 'Unfurnished', '', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0),
(4, 'Furnished/Unfurnishede', '', '2015-09-03 07:03:39', 1, '0000-00-00 00:00:00', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `easamoveadmin_tbl_listingstatus`
--

CREATE TABLE IF NOT EXISTS `easamoveadmin_tbl_listingstatus` (
  `listingstatus_id` int(11) NOT NULL AUTO_INCREMENT,
  `listingstatus_name` varchar(250) NOT NULL,
  `isforsale` int(11) NOT NULL,
  `listingstatus_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `listingstatus_created_by` int(11) NOT NULL,
  `listingstatus_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `listingstatus_modified_by` int(11) NOT NULL,
  `listingstatus_is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`listingstatus_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `easamoveadmin_tbl_listingstatus`
--

INSERT INTO `easamoveadmin_tbl_listingstatus` (`listingstatus_id`, `listingstatus_name`, `isforsale`, `listingstatus_created_on`, `listingstatus_created_by`, `listingstatus_modified_on`, `listingstatus_modified_by`, `listingstatus_is_deleted`) VALUES
(1, 'test', 0, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0),
(2, 'ress44', 0, '2015-09-02 12:41:09', 1, '0000-00-00 00:00:00', 1, 0),
(3, 'test', 0, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0),
(4, 'test1', 1, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `easamoveadmin_tbl_natureoferrors`
--

CREATE TABLE IF NOT EXISTS `easamoveadmin_tbl_natureoferrors` (
  `natureoferrors_id` int(11) NOT NULL AUTO_INCREMENT,
  `natureoferrors_name` varchar(450) NOT NULL,
  `natureoferrors_desc` text NOT NULL,
  `natureoferrors_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `natureoferrors_created_by` int(11) NOT NULL,
  `natureoferrors_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `natureoferrors_modified_by` int(11) NOT NULL,
  `natureoferrors_is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`natureoferrors_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `easamoveadmin_tbl_natureoferrors`
--

INSERT INTO `easamoveadmin_tbl_natureoferrors` (`natureoferrors_id`, `natureoferrors_name`, `natureoferrors_desc`, `natureoferrors_created_on`, `natureoferrors_created_by`, `natureoferrors_modified_on`, `natureoferrors_modified_by`, `natureoferrors_is_deleted`) VALUES
(1, 'This property has been sold or is no longer on the market', 'test', '2015-09-16 09:57:48', 1, '0000-00-00 00:00:00', 1, 0),
(2, 'The price is wrong', '', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `easamoveadmin_tbl_parkingtype`
--

CREATE TABLE IF NOT EXISTS `easamoveadmin_tbl_parkingtype` (
  `parking_id` int(11) NOT NULL AUTO_INCREMENT,
  `parking_name` varchar(250) NOT NULL,
  `parking_desc` text NOT NULL,
  `parking_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `parking_created_by` int(11) NOT NULL,
  `parking_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `parking_modified_by` int(11) NOT NULL,
  `parking_is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`parking_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `easamoveadmin_tbl_parkingtype`
--

INSERT INTO `easamoveadmin_tbl_parkingtype` (`parking_id`, `parking_name`, `parking_desc`, `parking_created_on`, `parking_created_by`, `parking_modified_on`, `parking_modified_by`, `parking_is_deleted`) VALUES
(1, 'outdoor parking', 'sdsds', '2015-09-16 07:12:13', 1, '0000-00-00 00:00:00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `easamoveadmin_tbl_pricemodifier`
--

CREATE TABLE IF NOT EXISTS `easamoveadmin_tbl_pricemodifier` (
  `pricemodifier_id` int(11) NOT NULL AUTO_INCREMENT,
  `pricemodifier_name` varchar(250) NOT NULL,
  `pricemodifier_desc` text NOT NULL,
  `pricemodifier_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `pricemodifier_created_by` int(11) NOT NULL,
  `pricemodifier_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `pricemodifier_modified_by` int(11) NOT NULL,
  `pricemodifier_is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`pricemodifier_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `easamoveadmin_tbl_pricemodifier`
--

INSERT INTO `easamoveadmin_tbl_pricemodifier` (`pricemodifier_id`, `pricemodifier_name`, `pricemodifier_desc`, `pricemodifier_created_on`, `pricemodifier_created_by`, `pricemodifier_modified_on`, `pricemodifier_modified_by`, `pricemodifier_is_deleted`) VALUES
(1, 'Shared ownership', 'Shared ow@#$#$#$%@#%#$%#%#$%($*#@*$#$(@nership\r\nShared ownership\r\nShared ownership\r\nShared ownershipShared ownership', '2015-09-16 09:13:47', 1, '0000-00-00 00:00:00', 1, 0),
(2, 'Shared equity', '', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0),
(3, 'Sale by tenter(price not dislay)', '', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `easamoveadmin_tbl_propertyrelationships`
--

CREATE TABLE IF NOT EXISTS `easamoveadmin_tbl_propertyrelationships` (
  `propertyrelationships_id` int(11) NOT NULL AUTO_INCREMENT,
  `propertyrelationships_name` varchar(250) NOT NULL,
  `propertyrelationships_desc` text NOT NULL,
  `propertyrelationships_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `propertyrelationships_created_by` int(11) NOT NULL,
  `propertyrelationships_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `propertyrelationships_modified_by` int(11) NOT NULL,
  `propertyrelationships_is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`propertyrelationships_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `easamoveadmin_tbl_propertyrelationships`
--

INSERT INTO `easamoveadmin_tbl_propertyrelationships` (`propertyrelationships_id`, `propertyrelationships_name`, `propertyrelationships_desc`, `propertyrelationships_created_on`, `propertyrelationships_created_by`, `propertyrelationships_modified_on`, `propertyrelationships_modified_by`, `propertyrelationships_is_deleted`) VALUES
(1, 'I am the owner of the property', 'test', '2015-09-16 10:06:05', 1, '0000-00-00 00:00:00', 1, 0),
(2, 'I''ve seen the property advertised differently elsewhere', '', '2015-09-03 10:24:04', 1, '0000-00-00 00:00:00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `easamoveadmin_tbl_propertytype`
--

CREATE TABLE IF NOT EXISTS `easamoveadmin_tbl_propertytype` (
  `propertytype_id` int(11) NOT NULL AUTO_INCREMENT,
  `propertytype_name` varchar(250) NOT NULL,
  `propertytype_desc` text NOT NULL,
  `propertytype_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `propertytype_created_by` int(11) NOT NULL,
  `propertytype_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `propertytype_modified_by` int(11) NOT NULL,
  `propertytype_is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`propertytype_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `easamoveadmin_tbl_propertytype`
--

INSERT INTO `easamoveadmin_tbl_propertytype` (`propertytype_id`, `propertytype_name`, `propertytype_desc`, `propertytype_created_on`, `propertytype_created_by`, `propertytype_modified_on`, `propertytype_modified_by`, `propertytype_is_deleted`) VALUES
(1, 'Land', 'Details Land type', '2015-09-16 08:56:05', 1, '0000-00-00 00:00:00', 1, 0),
(2, 'Land1', 'Detail Land type 2', '2015-09-16 08:56:17', 1, '0000-00-00 00:00:00', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `easamoveadmin_tbl_rentalfrequency`
--

CREATE TABLE IF NOT EXISTS `easamoveadmin_tbl_rentalfrequency` (
  `rentalfrequency_id` int(11) NOT NULL AUTO_INCREMENT,
  `rentalfrequency_name` varchar(250) NOT NULL,
  `rentalfrequency_desc` text NOT NULL,
  `rentalfrequency_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `rentalfrequency_created_by` int(11) NOT NULL,
  `rentalfrequency_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `rentalfrequency_modified_by` int(11) NOT NULL,
  `rentalfrequency_is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`rentalfrequency_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `easamoveadmin_tbl_rentalfrequency`
--

INSERT INTO `easamoveadmin_tbl_rentalfrequency` (`rentalfrequency_id`, `rentalfrequency_name`, `rentalfrequency_desc`, `rentalfrequency_created_on`, `rentalfrequency_created_by`, `rentalfrequency_modified_on`, `rentalfrequency_modified_by`, `rentalfrequency_is_deleted`) VALUES
(1, 'Weekly', 'Weekly Frequency', '2015-09-16 09:23:36', 1, '0000-00-00 00:00:00', 1, 0),
(2, 'Monthly', '', '2015-09-03 06:29:57', 1, '0000-00-00 00:00:00', 1, 0),
(3, 'new', '', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `easamoveadmin_tbl_sitestatus`
--

CREATE TABLE IF NOT EXISTS `easamoveadmin_tbl_sitestatus` (
  `sitestatus_id` int(11) NOT NULL AUTO_INCREMENT,
  `sitestatus_name` varchar(250) NOT NULL,
  `sitestatus_desc` text NOT NULL,
  `sitestatus_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `sitestatus_created_by` int(11) NOT NULL,
  `sitestatus_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `sitestatus_modified_by` int(11) NOT NULL,
  `sitestatus_is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`sitestatus_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `easamoveadmin_tbl_sitestatus`
--

INSERT INTO `easamoveadmin_tbl_sitestatus` (`sitestatus_id`, `sitestatus_name`, `sitestatus_desc`, `sitestatus_created_on`, `sitestatus_created_by`, `sitestatus_modified_on`, `sitestatus_modified_by`, `sitestatus_is_deleted`) VALUES
(1, 'Public', 'Public to user', '2015-09-16 07:45:36', 1, '0000-00-00 00:00:00', 1, 0),
(2, 'Private', '', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0),
(3, 'Deleted', '', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `easamoveadmin_tbl_state`
--

CREATE TABLE IF NOT EXISTS `easamoveadmin_tbl_state` (
  `state_id` int(11) NOT NULL AUTO_INCREMENT,
  `state_name` varchar(200) NOT NULL,
  `country_id` int(11) NOT NULL,
  `state_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `state_created_by` int(11) NOT NULL,
  `state_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `state_modified_by` int(11) NOT NULL,
  `state_is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`state_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `easamoveadmin_tbl_state`
--

INSERT INTO `easamoveadmin_tbl_state` (`state_id`, `state_name`, `country_id`, `state_created_on`, `state_created_by`, `state_modified_on`, `state_modified_by`, `state_is_deleted`) VALUES
(1, 'Los Angels', 6, '2015-09-01 13:32:09', 1, '0000-00-00 00:00:00', 1, 1),
(2, 'testqqq', 5, '2015-09-02 05:44:59', 1, '0000-00-00 00:00:00', 1, 0),
(3, 'Harrow', 5, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0),
(4, 'wembly', 5, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0),
(5, 'San fransico', 3, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0),
(6, 'Tamilnadu', 6, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0),
(7, 'Kerala', 6, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0),
(8, 'Andra predesh', 6, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0),
(9, 'Maharastra', 6, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0),
(10, 'Karnadaka', 6, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0),
(11, 'Uttara Pradesh', 6, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0),
(12, 'Assam', 6, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0),
(13, 'Manipur', 6, '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `easamoveadmin_tbl_tenures`
--

CREATE TABLE IF NOT EXISTS `easamoveadmin_tbl_tenures` (
  `tenures_id` int(11) NOT NULL AUTO_INCREMENT,
  `tenures_name` varchar(250) NOT NULL,
  `tenures_desc` text NOT NULL,
  `tenures_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `tenures_created_by` int(11) NOT NULL,
  `tenures_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `tenures_modified_by` int(11) NOT NULL,
  `tenures_is_deleted` int(11) NOT NULL,
  PRIMARY KEY (`tenures_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `easamoveadmin_tbl_tenures`
--

INSERT INTO `easamoveadmin_tbl_tenures` (`tenures_id`, `tenures_name`, `tenures_desc`, `tenures_created_on`, `tenures_created_by`, `tenures_modified_on`, `tenures_modified_by`, `tenures_is_deleted`) VALUES
(1, 'Share of freehold', 'testkbhjvj', '2015-09-16 09:28:59', 1, '0000-00-00 00:00:00', 1, 0),
(2, 'Leasehold', '', '0000-00-00 00:00:00', 1, '0000-00-00 00:00:00', 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `pro_tbl_admin`
--

CREATE TABLE IF NOT EXISTS `pro_tbl_admin` (
  `admin_id` int(11) NOT NULL AUTO_INCREMENT,
  `admin_name` varchar(250) NOT NULL,
  `admin_email` varchar(200) NOT NULL,
  `admin_password` varchar(250) NOT NULL,
  `admin_created_on` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `admin_created_by` int(11) NOT NULL,
  `admin_modified_on` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `admin_modified_by` int(11) NOT NULL,
  `admin_is_deleted` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`admin_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `pro_tbl_admin`
--

INSERT INTO `pro_tbl_admin` (`admin_id`, `admin_name`, `admin_email`, `admin_password`, `admin_created_on`, `admin_created_by`, `admin_modified_on`, `admin_modified_by`, `admin_is_deleted`) VALUES
(1, 'Admin', 'admin@easasoft.com', 'password', '2015-09-16 11:25:07', 0, '0000-00-00 00:00:00', 0, 0);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
