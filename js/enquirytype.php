<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>


   <!-- main content -->
      <section id="main-content" class="main-content">
        <section class="surround">

        <div class="container-fluid property_listing master">
          <div class="row">
              <div class="col-xs-12">
                <div class="panel">
                  <div class="panel-heading">
                    

                    <div class="row">
                      <div class="col-sm-12">
                        <h3>Enquiry Type</h3>
                      </div>
                    </div>
<input type="hidden" id="checkEnquiry">
<?php if ($access != 3) { ?>
                    <form method="post" id="enquiryType-form" >
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-horizontal">


                        <input type="hidden" name="id" value="0"/>

                          <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="Type">Enquiry Type<span style="color: #F00"> *</span></label>
                            <div class="col-sm-4 col-md-3">
                              <input type="text"  tabindex="1" id="Type" name="Type" maxlength="30" class="form-control input" onkeypress="return onlyAlphabets(event,this);" />
                            </div>
                          </div>



                    
                          <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2" for="Type">Enquiry Description</label>
                            <div class="col-sm-4 col-md-3">
                              <textarea id="Desc" tabindex="2" name="Desc" maxlength="60" class="form-control input"></textarea>
                            </div>
                          </div>


                          <div class="form-group">
                            <div class="col-md-offset-5 col-sm-offset-3 col-sm-3 save-cancel">
                              <button tabindex="3" type="button" class="btn btn-primary submit">Save</button>
                              <button tabindex="4" type="button" class="btn btn-danger reset">Cancel</button>
                            </div>                            
                          </div>

                        </div>                        
                      </div>
                    </div>
                    </form>
<?php } ?>

              
                  
                  </div>
                  <div class="">
                    <div class="table-responsive">

<table id="datatb" class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>#</th>                            
                            <th>Enquiry Type</th>
                            <th>Enquiry Description</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <!-- ko if:(Items().length>0) -->

                    <!--/ko -->
                   <tbody class="data-append">
                        <tr>
                            <td colspan="5" class="tac" style="height: 100px; vertical-align: middle;">Loading
                            </td>
                        </tr>
                    </tbody>

                </table>


                    </div>
                  </div>
                </div>
              </div>    
          </div>
        </div>
          
        </section>        
      </section>




<script>
    var base_url = '<?php echo base_url(); ?>';
    var cont = 'EnquiryType';


    $(document).ready(function () {


        getEnquiryType(base_url, cont);
 $(".reset").click(function(){
$('.error').remove();
$('.submit').prop('disabled', false);
    });

    });


$('.input').on('keypress',function(e)  {
 if (e.which != 13  ) {

$('.error').remove();

} else {
submit();
  
}



});



    $('.submit').on('click', function () {
       submit();
    });



function submit() {
   
        if ($("input[name='Type']").val() == '') {
          $('.error').remove();
            $("input[name='Type']").after('<span class="error">Please fill this field</span>');
        } else if ($("input[name='Desc']").val() == '') {
          $('.error').remove();
            $("input[name='Desc']").after('<span class="error">Please fill this field</span>');
        } else if(checkEnquiry() == 1){
      $('.error').remove();
      $("#Type").after('<span class="error">Enquiry type already exist</span>');

        } else {

$('.submit').text('Processing');

          $('.error').remove();
            var datas = $('#enquiryType-form').serialize();
            ajaxcalls(base_url, cont, datas);
//            getEnquiryType(base_url, cont);
            $("input[name='id']").val('0');
            $('#enquiryType-form')[0].reset();


        }





// setTimeout(function () {
// if ($('#checkEnquiry').val() == 1) { 
// $('.error').remove();
// $("#Type").after('<span class="error">Enquiry type already exist</span>');

// } else {


          



// }


// }, 2000);


// } 


}


    function edit(id) {
//        console.log($('#'+id).find("td:eq(1)").text());
        $("input[name='id']").val(id);
        $("input[name='Type']").val($('#' + id).find("td:eq(1)").text());
//        $("input[name='Desc']").val($('#' + id).find("td:eq(2)").text());
$(window).scrollTop(0);
        document.getElementById("Desc").value =$('#' + id).find("td:eq(2)").text();


    }
    function deleteEnquiryType(id) {
if (confirm('Are you sure you want to Delete?')) { 
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>master/" + cont + "/deleteEnquirytype",
            data: "id=" + id,
            success: function (data) {
                if (data) {
                    window.location.reload();
                        $('#' + id).remove();
                        alert('Deletes successfully');
                }
            }
        });
        }
    }
    
    // $("input[name='Type']").on('keyup', function (e) {
    //  if($("input[name='id']").val()==0&& e.which != 13 ){
    // $('.error').remove();

    function checkEnquiry(){
      // if($("input[name='id']").val()==0){
        var returnval;
        $.ajax({
            type: "POST",
            async: false,
            url: "<?php echo base_url(); ?>master/" + cont + "/checkEnquiry",
            data: "code=" +$("input[name='Type']").val() + "&id=" + $("input[name='id']").val(),
            dataType:'html',
            success: function (data) {
                if(data==1){
                  returnval = data;

                }else{
                  $('#checkEnquiry').val('');
                    returnval = data;
                }
            }
        });
return returnval;

      // }
      }

    //     }
    // });
</script>