/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


function addagentdb(base_url, cont, datas) {
    $('.submit').html('<i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> Sending mail');
    $('#loader').show();
    $.ajax({
        type: "POST",
        url: base_url + "agent/" + cont + "/addagentdb",
        data: datas,
        success: function (data) {
            $('#loader').hide();
            $('.submit').text('Send Mail');
            $('#messageadd').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#adde', function () {
window.location.href = base_url+"agent/agent/agentdb";
            });
            // window.location.reload();
           
        }

    });


}


function loadagentdb(base_url,cont) {
$.ajax({
    type: "POST",
    url: base_url + 'agent/' + cont + '/loadagentdb', 
    success: function(data) {
    $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');

            $("#datatb_filter").after("<div class='col-lg-5 col-md-4 text-right'><button type='button' class='dark btn btn-primary lg' disabled>Send Mail</button></div>");
            
            $("select[name='datatb_length']").addClass('form-control');
    }
});

}









// Insert approval entry

function ajaxapproval(base_url, cont, datas) {
    $('.submit').text('Sending...');
    $('#loader').show();
    $.ajax({
        type: "POST",
        url: base_url + "agent/" + cont + "/addapproval",
        data: 'datas=' + datas,
        success: function (data) {
            $('#loader').hide();
            $('.submit').text('Send Mail');
            $('#mailsent').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#mailsentbtn', function () {
window.location.href = base_url+"agent/agent/approval";
            });
            // window.location.reload();
           
        }

    });


}



function getappuser(base_url,cont) {

$.ajax({
type: "POST",
url: base_url + "agent/" + cont + "/loaduser",
success: function(data) {
     $('.data-append').html(data);
     var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
}


});


}






//Get agent branch list using database

function getagentBranch(base_url,cont,agent_id){
    // console.log(agent_id);
    $.ajax({

    type: "POST",
    url: base_url + "agent/" + cont + "/getagentbranch",
    data: "id=" + agent_id,
 success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }

    });
}


// for agent user
function getagentuser(base_url,cont,agent_id) {

   
$.ajax({

 type: "POST",
 url: base_url + "agent/" + cont + "/getagentuser",
 data: "id=" + agent_id,
 success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }

});

}

function ajaxuseragent(base_url, cont, formData) {
    $.ajax({
        type: "POST",
        url: base_url + "agent/" + cont + "/addagentuser",
        mimeType: "multipart/form-data",
        data: formData,
        success: function (data) {
            // alert('Saved Successfully');
            $('#messageadd').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#adde', function () {
            window.location.reload();
             window.location.href = base_url + "/agent/" + cont + "/agentUser";
         });
        }
    });
}





function getfeedback(base_url,cont) {

$.ajax({

 type: "POST",
 url: base_url + "AgentRequirements/" + cont + "/get_feedback",
 success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }

});

}


function getcontact(base_url,cont) {

$.ajax({

 type: "POST",
 url: base_url + "AgentRequirements/" + cont + "/getcontact",
 success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }

});

}





function ajaxcalls(base_url, cont, datas) {
$('.submit').html('<i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> Save');
$('#loader').show();
    $.ajax({
        type: "POST",
        url: base_url + "master/" + cont + "/insert",
        mimeType: "multipart/form-data",
        data: datas,
        success: function (data) {
            $('#loader').hide();
            $('.submit').text('Save');
            // console.log(data);
            // alert('Saved Successfully');
            $('#messageadd').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#adde', function () {
            window.location.reload();
        });
        }
    });
}
function getCountry(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "master/" + cont + "/getCountry",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}
function getState(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "master/" + cont + "/getState",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}
function getCity(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "master/" + cont + "/getCity",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getEnquiryType(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "master/" + cont + "/getEnquirytype",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}






function getAgentMember(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "master/" + cont + "/getAgentMember",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}








function ajaxMembers(base_url, cont, datas) {
    $('.submit').html('<i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> Save');
    $('#loader').show();
    console.log(datas);
    $.ajax({
        type: "POST",
        url: base_url + "master/" + cont + "/insert",
        data: datas,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            $('#loader').hide();
$('.submit').text('Save');
            // alert('Saved Successfully');
            $('#messageadd').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#adde', function () {
            $("input[name='id']").val('0');
            $('#agentmember-form')[0].reset();
            window.location.href= base_url + "/master/agentMember";
        });
        }
    });
}











function getAlertType(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "master/" + cont + "/getAlerttype",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}
function getDecorativeCondition(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "master/" + cont + "/getDecorativecondition",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getParkingType(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "master/" + cont + "/getParkingtype",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getAgentType(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "master/" + cont + "/getAgenttype",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}
function getCentralheatingType(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "master/" + cont + "/getCentralheatingType",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

//property master

function ajaxcallsproperty(base_url, cont, datas) {
    $('.submit').html('<i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> Save');
    $('#loader').show();
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/insert",
        data: datas,
        success: function (data) {
            $('#loader').hide();
            $('.submit').text('Save');
            // alert('Saved Successfully');
            $('#messageadd').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#adde', function () {
            window.location.reload();
           
       });
        }
    });
}


function getSitestatus(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getSitestatus",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getListingstatus(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getListingstatus",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}
function getCategory(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getCategory",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}
function getFeatures(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getFeatures",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getPropertytype(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getPropertytype",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getPricemodifier(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getPricemodifier",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getRentalfrequencies(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getRentalfrequencies",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}


function getTenures(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getTenures",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getTransaction(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getTransaction",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getBuildingcondition(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getBuildingcondition",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}
function getLettingtype(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getLettingtype",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getConsiderinfo(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getConsiderinfo",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}
function getFurnishes(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "featuremaster/" + cont + "/getFurnishes",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getNatureoferrors(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getNatureoferrors",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}
function getPropertyrelationships(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getPropertyrelationships",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getFeedbackcategory(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getFeedbackcategory",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getFeedbacksubcategory(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "propertymaster/" + cont + "/getFeedbacksubcategory",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}


//admin


function ajaxadmin(base_url, cont, datas) {
    $('.submit').html('<i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> Save');
    $('#loader').show();
    $.ajax({
        type: "POST",
        url: base_url + "/" + cont + "/insert",
        data: datas,
        success: function (data) {
            $('#ldr').remove();
            $('#loader').hide();
            $('.submit').text('Save');
            // alert('Saved Successfully');
            $('#messageadd').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#adde', function () {
            window.location.reload();
        });
        }
    });
}


function ajaxadminUsersUpdate(base_url, cont, datas) {
    $('.submit').html('<i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> Saving');
    $('#loader').show();
    $.ajax({
        type: "POST",
        url: base_url + "/" + cont + "/updateUsers",
        data: datas,
        success: function (data) {
            $('.submit').text('Save');
            $('#loader').hide();
            if(data === "2"){
                alert("your verification mail has been sent to your email id, please click that link and verify superadmin");
                window.location.href = base_url+"Account/logout";
            }else{
                $('#messageadd').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#adde', function () {
                // alert('Saved Successfully');
            window.location.reload();
            });
            }
            
            
        }
    });
}




function selfajaxadmin(base_url, cont, datas) {
    $('.submit').html('<i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> Save');
    $('#loader').show();
    $.ajax({
        type: "POST",
        url: base_url + "/" + cont + "/selfupdateAdminuser",
        data: datas,
        success: function (data) {
            $('#loader').hide();
            $('.submit').text('Save');
            // alert('Saved Successfully');
            $('#messageadd').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#adde', function () {
            window.location.reload();
        });
        }
    });
}


function getAdminuser(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "/" + cont + "/getAdminuser",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}
function randanstring() {
    var chars = "ABCDEFGHIJKLMNOPQRSTUVWXTZabcdefghiklmnopqrstuvwxyz";
    var string_length = 8;
    var randomstring = '';
    for (var i = 0; i < string_length; i++) {
        var rnum = Math.floor(Math.random() * chars.length);
        randomstring += chars.substring(rnum, rnum + 1);
    }
    return randomstring;
}
$('.btn-danger').click(function () {
    $("input[name='id']").val('0');
    $('form')[0].reset();
    var password = randanstring();
    $("#Password").val(password);
});



//    data: 'datas='+datas+'&data2='+datas2 ,
function ajaxAgents(base_url, cont, datas) {
    $('.submit').html('<i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> Save');
    $('#loader').show();
    str = JSON.stringify(datas);
str = JSON.stringify(datas, null, 4); // (Optional) beautiful indented output.
console.log(str); // Logs output to dev tools console.
    $.ajax({
        type: "POST",
        url: base_url + "agent/" + cont + "/insert",
        data: datas,
        mimeType: "multipart/form-data",
        contentType: false,
        cache: false,
        processData: false,
        success: function (data) {
            $('#loader').hide();
$('.submit').text('Save');
            // alert('Saved Successfully ');
            $('#messageadd').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#adde', function () {
            $("input[name='id']").val('0');
            $('#add-to-listing-partial-form')[0].reset();
            // window.location.reload();
            window.location.href=base_url+"agent/agent";
        });
        }
    });
}
function getAgent(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "agent/" + cont + "/getAgent",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}


function ajaxcallsfeatures(base_url, cont, datas) {
    $('.submit').html('<i class="fa fa-spinner fa-pulse fa-1x fa-fw"></i> Save');
    $('#loader').show();
    $.ajax({
        type: "POST",
        url: base_url + "featuremaster/" + cont + "/insert",
        data: datas,
        success: function (data) {
            $('#loader').hide();
            $('.submit').text('Save');
            // alert('Saved Successfully');
            $('#messageadd').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#adde', function () {
            window.location.reload();
        });
        }
    });
}

function getOutsidespace(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "featuremaster/" + cont + "/getOutsidespace",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getParking(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "featuremaster/" + cont + "/getParking",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}


function getHeatingtype(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "featuremaster/" + cont + "/getHeatingtype",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}



function getSpecialfeature(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "featuremaster/" + cont + "/getSpecialfeature",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}


function getAccessibility(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "featuremaster/" + cont + "/getAccessibility",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}

function getContentmaster(base_url, cont) {
    $.ajax({
        type: "POST",
        url: base_url + "featuremaster/" + cont + "/getContentmaster",
        success: function (data) {
            $('.data-append').html(data);
            var $table = $("#datatb").DataTable({
                bRetrieve: true
            });
            $("input[type='search']").addClass('form-control');
            $("select[name='datatb_length']").addClass('form-control');
        }
    });
}


function onlyAlphabets(evt, t) {
    var keyCode = (evt.which) ? evt.which : evt.keyCode
    if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && keyCode != 32&& keyCode != 8 && keyCode != 9)
        return false;
    return true;
}