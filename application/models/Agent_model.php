<?php

class Agent_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }








public function getUserdata($id) {

        $query = $this->db->query("SELECT * FROM pro_tbl_agentuser where agentuser_id='$id' and agentuser_is_deleted =0");
        return $query->result();
    }



public function updateAgentuser($data, $aid) {
    $array = array("agentuser_access !=" => 1, "agentuser_id" => $aid);
$this->db->where($array);
$query = $this->db->update('pro_tbl_agentuser', $data);

return $query;

}


//for country
    public function addAgent($data,$key,$deleted = NULL,$hash) {
        $query = $this->db->insert('easamoveadmin_tbl_agent', $data);
         // $key = $this->generateRandomString(16);
            $pass = $this->generateRandomString(8);
            $data = array(
                'agentuser_name' => $this->input->post('Forename'),
                'agentuser_email' => $this->input->post('Email'),
                'agentuser_password' => $pass,
                'agentuser_mobile' => $this->input->post('Phone'),
                'agentuser_userrole' => 'admin',
                'agentuser_access' => 1,
                'agent_id' => $this->db->insert_id(),
                'agentuser_created_on' => date('y-m-d H:i:s'),
                'agentuser_created_by' => 1,
                'agentuser_verifykey' => $key
            );

            

            $query = $this->db->insert('pro_tbl_agentuser', $data);
        
        if(!empty($hash)) { 

            $this->db->query("UPDATE easamoveadmin_tbl_user_approval  SET status = 1 WHERE hash='".$hash."'");
        }

//mail function
            // $this->load->library('email');
            // $mydata['name'] = $this->input->post('Forename');

            // $mydata['link'] = base_url() . 'settings/verify/' . $key;

            // $message = $this->load->view('mail_template/welcome_message', $mydata, true);
            // $this->email->from('info@easasoft.com', 'Easasoft');

            // $this->email->to($this->input->post('Email'));

            // $this->email->subject('Welcome to Easamove');

            // $this->email->message($message);

            // $this->email->send();
        return $query;
    }
    public function getAgent($id=0){
        $whr='';
        if($id!=''&&$id!=0){
            $whr="and a.agent_id=$id";
        }
        $query = $this->db->query("SELECT a.*,b.agent_name as type FROM easamoveadmin_tbl_agent a left join easamoveadmin_tbl_agenttype b on (a.agent_type=b.agent_id) where a.agent_is_deleted=0 and a.agent_head_office=0 $whr");
        // echo $this->db->last_query();
        // exit();
        return $query->result();
    }

    public function getAgentEdit($id){
        $query = $this->db->query("SELECT a.*,b.agent_name as type FROM easamoveadmin_tbl_agent a left join easamoveadmin_tbl_agenttype b on (a.agent_type=b.agent_id) where a.agent_is_deleted=0 and a.agent_id=$id");
        // echo $this->db->last_query();
        // exit();
        return $query->result();
    }
    public function updateAgent($data,$id) {
        $this->db->where('agent_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_agent', $data);
        $key = $this->generateRandomString(16);
            $pass = $this->generateRandomString(8);
            $data = array(
                'agentuser_name' => $this->input->post('Forename'),
                'agentuser_email' => $this->input->post('Email'),
                // 'agentuser_password' => $pass,
                'agentuser_mobile' => $this->input->post('Phone'),
                'agentuser_userrole' => 'admin',
                'agentuser_access' => 1,
                'agent_id' => $id,
                'agentuser_created_on' => date('Y-m-d H:i:s'),
                'agentuser_created_by' => 1
                // 'agentuser_verifykey' => $key
            );
            $this->db->where('agent_id',$id);
            $query = $this->db->update('pro_tbl_agentuser', $data);

            $this->load->library('email');

            $config['charset'] = 'iso-8859-1';
            $config['wordwrap'] = TRUE;
            $config['mailtype'] = 'html';
            $this->email->initialize($config);
            $mydata['name'] = $this->input->post('Forename');

            $mydata['link'] = base_url() . 'settings/verify/' . $key;

            $message = $this->load->view('mail_template/welcome_message', $mydata, true);
            $this->email->from('info@easasoft.com', 'Easasoft');

            $this->email->to($this->input->post('Email'));

            $this->email->subject('Welcome to Easamove');

            $this->email->message($message);

            $this->email->send();
        return $query;
    }
    public function deleteAgent($data,$id){
         $this->db->where('agent_id',$id);
         $query = $this->db->query("update pro_tbl_agentuser set agentuser_is_deleted=1 where agent_id='$id'");
        $query = $this->db->update('easamoveadmin_tbl_agent', $data);
        echo $query;
    }
    
    public function verify($key) {
        $query = $this->db->query("SELECT * , pro_tbl_agentuser.agentuser_password
FROM easamoveadmin_tbl_agent
LEFT JOIN pro_tbl_agentuser ON easamoveadmin_tbl_agent.agent_id = pro_tbl_agentuser.agent_id
WHERE agent_is_deleted =0
AND agent_key ='$key'");
        if ($query->num_rows() > 0) {
            $this->db->where('agent_key', $key);
            $data = array(
                'agent_verified' => 1
            );
             $this->db->update('easamoveadmin_tbl_agent', $data);
            return $query->result();
        } else {
            return FALSE;
        }
    }
    function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    //Get agent user by agent id
    public function agentUser() {
        $agentId = $this->input->post('id');
        $query = $this->db->query("SELECT * FROM pro_tbl_agentuser where agentuser_is_deleted=0 and agent_id='$agentId' and agentuser_access!=1");
        return $query->result();
    }

    //get agent branch list
    public function getAgentBranch(){
        $agentId = $this->input->post('id');
        $query = $this->db->query("SELECT a.*,b.agent_name as type FROM easamoveadmin_tbl_agent a left join easamoveadmin_tbl_agenttype b on (a.agent_type=b.agent_id) where a.agent_is_deleted=0 and a.agent_head_office!=0 and a.agent_head_office='$agentId'");
        // echo $this->db->last_query();
        // exit();
        return $query->result();
    }

}
?>