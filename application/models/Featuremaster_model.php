<?php

class Featuremaster_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
//for Outside space
    public function addOutsidespace($data) {
        $query = $this->db->insert('easamoveadmin_tbl_outsidespace', $data);
        return $query;
    }
    public function getOutsidespace(){
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_outsidespace where outsidespace_is_deleted=0 order by outsidespace_id desc');
        return $query->result();
    }
    public function updateOutsidespace($data,$id) {
        $this->db->where('outsidespace_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_outsidespace', $data);
        return $query;
    }
    public function deleteOutsidespace($data,$id){
         $this->db->where('outsidespace_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_outsidespace', $data);
        echo $query;
    }
    
    //for Parking
    public function addParking($data) {
        $query = $this->db->insert('easamoveadmin_tbl_parking', $data);
        return $query;
    }
    public function getParking(){
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_parking where parking_is_deleted=0 order by parking_id desc');
        return $query->result();
    }
    public function updateParking($data,$id) {
        $this->db->where('parking_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_parking', $data);
        return $query;
    }
    public function deleteParking($data,$id){
         $this->db->where('parking_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_parking', $data);
        echo $query;
    }
    
    //for Heating type
    public function addHeatingtype($data) {
        $query = $this->db->insert('easamoveadmin_tbl_heatingtype', $data);
        return $query;
    }
    public function getHeatingtype(){
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_heatingtype where heatingtype_is_deleted=0 order by heatingtype_id desc');
        return $query->result();
    }
    public function updateHeatingtype($data,$id) {
        $this->db->where('heatingtype_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_heatingtype', $data);
        return $query;
    }
    public function deleteHeatingtype($data,$id){
         $this->db->where('heatingtype_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_heatingtype', $data);
        echo $query;
    }
    
    //for Special Feature
    public function addSpecialfeature($data) {
        $query = $this->db->insert('easamoveadmin_tbl_specialfeature', $data);
        return $query;
    }
    public function getSpecialfeature(){
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_specialfeature where specialfeature_is_deleted=0 order by specialfeature_id desc');
        return $query->result();
    }
    public function updateSpecialfeature($data,$id) {
        $this->db->where('specialfeature_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_specialfeature', $data);
        return $query;
    }
    public function deleteSpecialfeature($data,$id){
         $this->db->where('specialfeature_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_specialfeature', $data);
        echo $query;
    }
    //for Furnishes
     public function addFurnishes($data) {
        $query = $this->db->insert('easamoveadmin_tbl_furnishes', $data);
        return $query;
    }
    public function getFurnishes(){
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_furnishes where furnishes_is_deleted=0 order by furnishes_id desc');
        return $query->result();
    }
    public function updateFurnishes($data,$id) {
        $this->db->where('furnishes_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_furnishes', $data);
        return $query;
    }
    public function deleteFurnishes($data,$id){
         $this->db->where('furnishes_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_furnishes', $data);
        echo $query;
    }
    
    
    //for Accessibility
     public function addAccessibility($data) {
        $query = $this->db->insert('easamoveadmin_tbl_accessibility', $data);
        return $query;
    }
    public function getAccessibility(){
        $query = $this->db->query('SELECT * FROM  easamoveadmin_tbl_accessibility where accessibility_is_deleted=0 order by accessibility_id desc');
        return $query->result();
    }
    public function updateAccessibility($data,$id) {
        $this->db->where('accessibility_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_accessibility', $data);
        return $query;
    }
    public function deleteAccessibility($data,$id){
         $this->db->where('accessibility_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_accessibility', $data);
        echo $query;
    }
    
      //for Content
     public function addContentmaster($data) {
        $query = $this->db->insert('easamoveadmin_tbl_content', $data);
        return $query;
    }
    public function getContentmaster(){
        $query = $this->db->query('SELECT * FROM  easamoveadmin_tbl_content where content_is_deleted=0 order by content_id desc');
        return $query->result();
    }
    public function updateContentmaster($data,$id) {
        $this->db->where('content_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_content', $data);
        return $query;
    }
    public function deleteContentmaster($data,$id){
         $this->db->where('content_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_content', $data);
        echo $query;
    }
}
?>