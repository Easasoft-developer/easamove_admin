<?php

class Dashboard_model extends CI_Model {

    function __construct() {
        parent::__construct();
        
    }

//for country
    public function agent_count() {
//        $query = $this->db->insert('easamoveadmin_tbl_country', $data);
       $query = $this->db->query("SELECT *  from easamoveadmin_tbl_agent where agent_is_deleted=0");
        return $query->num_rows();
    }

    //for central heating type
    public function admin_count() {

       $query = $this->db->query("SELECT * from easamoveadmin_tbl_admin where admin_is_deleted=0");
        return $query->num_rows();
    }

    public function getAdminuser($id) {

        $query = $this->db->query("SELECT * from easamoveadmin_tbl_admin where admin_is_deleted=0 and admin_id=$id order by admin_id desc");
        return $query->result();
    }
}
?>