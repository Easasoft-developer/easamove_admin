<?php

class Propertymaster_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }
//for site status
    public function addSitestatus($data) {
        $query = $this->db->insert('easamoveadmin_tbl_sitestatus', $data);
        return $query;
    }
    public function getSitestatus(){
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_sitestatus where sitestatus_is_deleted=0 order by sitestatus_id desc');
        return $query->result();
    }
    public function updateSitestatus($data,$id) {
        $this->db->where('sitestatus_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_sitestatus', $data);
        return $query;
    }
    public function deleteSitestatus($data,$id){
         $this->db->where('sitestatus_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_sitestatus', $data);
        echo $query;
    }
    
    //for lisitingstatus
    public function addListingstatus($data) {
        $query = $this->db->insert('easamoveadmin_tbl_listingstatus', $data);
        return $query;
    }
    public function getListingstatus(){
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_listingstatus where listingstatus_is_deleted=0 order by listingstatus_id desc');
        return $query->result();
    }
    public function updateListingstatus($data,$id) {
        $this->db->where('listingstatus_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_listingstatus', $data);
        return $query;
    }
    public function deleteListingstatus($data,$id){
         $this->db->where('listingstatus_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_listingstatus', $data);
        echo $query;
    }
    
    //for Category
    public function addCategory($data) {
        $query = $this->db->insert('easamoveadmin_tbl_category', $data);
        return $query;
    }
    public function getCategory(){
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_category where category_is_deleted=0 order by category_id desc');
        return $query->result();
    }
    public function updateCategory($data,$id) {
        $this->db->where('category_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_category', $data);
        return $query;
    }
    public function deleteCategory($data,$id){
         $this->db->where('category_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_category', $data);
        echo $query;
    }
    
    
    
    //for Features
    public function addFeatures($data) {
        $query = $this->db->insert('easamoveadmin_tbl_features', $data);
        return $query;
    }
    public function getFeatures(){
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_features where features_is_deleted=0 order by features_id desc');
        return $query->result();
    }
    public function updateFeatures($data,$id) {
        $this->db->where('features_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_features', $data);
        return $query;
    }
    public function deleteFeatures($data,$id){
         $this->db->where('features_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_features', $data);
        echo $query;
    }
    
    
     
    //for Propertytype
    public function addPropertytype($data) {
        $query = $this->db->insert('easamoveadmin_tbl_propertytype', $data);
        return $query;
    }
    public function getPropertytype(){
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_propertytype where propertytype_is_deleted=0 order by propertytype_id desc');
        return $query->result();
    }
    public function updatePropertytype($data,$id) {
        $this->db->where('propertytype_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_propertytype', $data);
        return $query;
    }
    public function deletePropertytype($data,$id){
         $this->db->where('propertytype_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_propertytype', $data);
        echo $query;
    }
    
     //for Pricemodifier
    public function addPricemodifier($data) {
        $query = $this->db->insert('easamoveadmin_tbl_pricemodifier', $data);
        return $query;
    }
    public function getPricemodifier(){
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_pricemodifier where pricemodifier_is_deleted=0 order by pricemodifier_id desc');
        return $query->result();
    }
    public function updatePricemodifier($data,$id) {
        $this->db->where('pricemodifier_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_pricemodifier', $data);
        return $query;
    }
    public function deletePricemodifier($data,$id){
         $this->db->where('pricemodifier_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_pricemodifier', $data);
        echo $query;
    }
    
    
     //for Rentalfrequencies
    public function addRentalfrequencies($data) {
        $query = $this->db->insert('easamoveadmin_tbl_rentalfrequency', $data);
        return $query;
    }
    public function getRentalfrequencies(){
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_rentalfrequency where rentalfrequency_is_deleted=0 order by rentalfrequency_id desc');
        return $query->result();
    }
    public function updateRentalfrequencies($data,$id) {
        $this->db->where('rentalfrequency_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_rentalfrequency', $data);
        return $query;
    }
    public function deleteRentalfrequencies($data,$id){
         $this->db->where('rentalfrequency_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_rentalfrequency', $data);
        echo $query;
    }
    
    //for getTenures
     public function addTenures($data) {
        $query = $this->db->insert('easamoveadmin_tbl_tenures', $data);
        return $query;
    }
    public function getTenures(){
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_tenures where tenures_is_deleted=0 order by tenures_id desc');
        return $query->result();
    }
    public function updateTenures($data,$id) {
        $this->db->where('tenures_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_tenures', $data);
        return $query;
    }
    public function deleteTenures($data,$id){
         $this->db->where('tenures_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_tenures', $data);
        echo $query;
    }
    
    
    
    
    
    //for Natureoferrors
    public function addNatureoferrors($data) {
        $query = $this->db->insert('easamoveadmin_tbl_natureoferrors', $data);
        return $query;
    }
    public function getNatureoferrors(){
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_natureoferrors where natureoferrors_is_deleted=0 order by natureoferrors_id desc');
        return $query->result();
    }
    public function updateNatureoferrors($data,$id) {
        $this->db->where('natureoferrors_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_natureoferrors', $data);
        return $query;
    }
    public function deleteNatureoferrors($data,$id){
         $this->db->where('natureoferrors_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_natureoferrors', $data);
        echo $query;
    }
    
    //for Natureoferrors
    public function addPropertyrelationships($data) {
        $query = $this->db->insert('easamoveadmin_tbl_propertyrelationships', $data);
        return $query;
    }
    public function getPropertyrelationships(){
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_propertyrelationships where propertyrelationships_is_deleted=0 order by propertyrelationships_id desc');
        return $query->result();
    }
    public function updatePropertyrelationships($data,$id) {
        $this->db->where('propertyrelationships_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_propertyrelationships', $data);
        return $query;
    }
    public function deletePropertyrelationships($data,$id){
         $this->db->where('propertyrelationships_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_propertyrelationships', $data);
        echo $query;
    }
    
    //for Feedbackcategory
    
    public function addFeedbackcategory($data) {
        $query = $this->db->insert('easamoveadmin_tbl_feedbackcategory', $data);
        return $query;
    }
    public function getFeedbackcategory(){ 
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_feedbackcategory where feedbackcategory_is_deleted=0 order by outsidespace_id desc');
        return $query->result();
    }
    public function updateFeedbackcategory($data,$id) {
        $this->db->where('feedbackcategory_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_feedbackcategory', $data);
        return $query;
    }
    public function deleteFeedbackcategory($data,$id){
         $this->db->where('feedbackcategory_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_feedbackcategory', $data);
        echo $query;
    }
    //for Feedbacksubcategory
    
    public function addFeedbacksubcategory($data) {
        $query = $this->db->insert('easamoveadmin_tbl_feedbacksubcategory', $data);
        return $query;
    }
    public function getFeedbacksubcategory(){
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_feedbacksubcategory a left join easamoveadmin_tbl_feedbackcategory b on (a.feedbackcategory_id=b.feedbackcategory_id) where feedbacksubcategory_is_deleted=0');
        return $query->result();
    }
    public function updateFeedbacksubcategory($data,$id) {
        $this->db->where('feedbacksubcategory_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_feedbacksubcategory', $data);
        return $query;
    }
    public function deleteFeedbacksubcategory($data,$id){
         $this->db->where('feedbacksubcategory_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_feedbacksubcategory', $data);
        echo $query;
    }
    
    //for transaction
    
    public function addTransaction($data) {
        $query = $this->db->insert('easamoveadmin_tbl_transactiontype', $data);
        return $query;
    }
    public function getTransaction(){
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_transactiontype where  transactiontype_is_deleted=0 order by transactiontype_id desc');
        return $query->result();
    }
    public function updateTransaction($data,$id) {
        $this->db->where('transactiontype_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_transactiontype', $data);
        return $query;
    }
    public function deleteTransaction($data,$id){
         $this->db->where('transactiontype_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_transactiontype', $data);
        echo $query;
    }
     //for building condition
    
    public function addBuildingcondition($data) {
        $query = $this->db->insert('easamoveadmin_tbl_buildcondition', $data);
        return $query;
    }
    public function getBuildingcondition(){
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_buildcondition where  buildcondition_is_deleted=0 order by buildcondition_id desc');
        return $query->result();
    }
    public function updateBuildingcondition($data,$id) {
        $this->db->where('buildcondition_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_buildcondition', $data);
        return $query;
    }
    public function deleteBuildingcondition($data,$id){
         $this->db->where('buildcondition_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_buildcondition', $data);
        echo $query;
    }
    
    //for Letting type
    
    public function addLettingtype($data) {
        $query = $this->db->insert('easamoveadmin_tbl_lettingtype', $data);
        return $query;
    }
    public function getLettingtype(){
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_lettingtype where  lettingtype_is_deleted=0 order by lettingtype_id desc');
        return $query->result();
    }
    public function updateLettingtype($data,$id) {
        $this->db->where('lettingtype_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_lettingtype', $data);
        return $query;
    }
    public function deleteLettingtype($data,$id){
         $this->db->where('lettingtype_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_lettingtype', $data);
        echo $query;
    }
    
    //for consider info
    
    public function addConsiderinfo($data) {
        $query = $this->db->insert('easamoveadmin_tbl_considerinfo', $data);
        return $query;
    }
    public function getConsiderinfo(){
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_considerinfo where  considerinfo_is_deleted=0 order by considerinfo_id desc');
        return $query->result();
    }
    public function updateConsiderinfo($data,$id) {
        $this->db->where('considerinfo_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_considerinfo', $data);
        return $query;
    }
    public function deleteConsiderinfo($data,$id){
         $this->db->where('considerinfo_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_considerinfo', $data);
        echo $query;
    }
}