<?php

class Admin_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

//for country
    public function login($data) {
//        $query = $this->db->insert('easamoveadmin_tbl_country', $data);
        $query = $this->db->get_where('easamoveadmin_tbl_admin', $data);
        if($query->num_rows() > 0){
            $dataverify = array(
                'admin_email' => $this->input->post('UserName'),
                'admin_password' => $this->input->post('Password'),   
                'admin_verified' => 1,         
                'admin_is_deleted'=>0
                
            );
            $query1 = $this->db->get_where('easamoveadmin_tbl_admin', $dataverify);
            // echo $this->db->last_query();
            // exit();
            if($query1->num_rows() > 0){
                return $query1->result();
            }else{
                return 1;
            }

        }else{
            return $query->result();
        }        
    }

    //for central heating type
    public function addAdminuser($data) {

        $query = $this->db->insert('easamoveadmin_tbl_admin', $data);
        return $query;
    }


       public function anotherupdateAdminuser($data,$id){

        $this->db->where('admin_id', $id);
      $query = $this->db->update('easamoveadmin_tbl_admin',$data);
      return $query;

 
    }


    public function getAdminuser() {

        $query = $this->db->query("SELECT * from easamoveadmin_tbl_admin where admin_is_deleted=0 and admin_access!=1 order by admin_id desc");
        return $query->result();
    }


    public function selfupdateAdminuser($data, $id) {
    $this->db->where('admin_id', $id);
        $query = $this->db->update('easamoveadmin_tbl_admin', $data);
        return $query;

    }


    public function updateAdminuser($data, $id) {
        $this->db->where('admin_id', $id);
        $query = $this->db->update('easamoveadmin_tbl_admin', $data);
        return $query;
    }

    public function resetPassword($data, $email) {
        $this->db->where('admin_email', $email);
        $query = $this->db->update('easamoveadmin_tbl_admin', $data);
        return $query;
    }

    public function verify($key) {
        $query = $this->db->query("SELECT * from easamoveadmin_tbl_admin where admin_is_deleted=0 and admin_verifykey='$key' and admin_verified=0");
        if ($query->num_rows() > 0) {
            $this->db->where('admin_verifykey', $key);
            $data = array(
                'admin_verified' => 1
            );
             $this->db->update('easamoveadmin_tbl_admin', $data);
            return $query->result();
        } else {
            return FALSE;
        }
    }

    public function deleteAdminuser($data, $id) {
        $query = $this->db->query("SELECT * from easamoveadmin_tbl_admin where admin_is_deleted=0 and admin_access=1 and admin_id=$id");
        if ($query->num_rows() == 0) {
            $this->db->where('admin_id', $id);
            $query = $this->db->update('easamoveadmin_tbl_admin', $data);
            echo 1;
        } else {
            echo 'Permission Denied';
        }
    }

    public function getUser($email) {
        $query = $this->db->query("SELECT * from easamoveadmin_tbl_admin where admin_is_deleted=0 and admin_email='$email'");
        return $query->result();
    }

    public function mgetUser($id) {
 $query = $this->db->query("SELECT * from easamoveadmin_tbl_admin where admin_is_deleted=0 and admin_id='$id'");
        return $query->result();

    }

    public function updateUsersAdmin($data, $id){
        $this->db->where('admin_id', $id);
        $query = $this->db->update('easamoveadmin_tbl_admin', $data);
        if($this->db->affected_rows()){
            return 1;
        }
    }

}
