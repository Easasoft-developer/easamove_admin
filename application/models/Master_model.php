<?php

class Master_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }



public function addagentdb($data) {

if(empty($data['id']) ) {
    $query = $this->db->query("INSERT INTO easamoveadmin_tbl_agentdb SET serial='".$data['name']."',company='".$data['company']."',branch='".$data['branch']."',address='".$data['address']."',contact='".$data['contact']."',phone='".$data['phone']."',email='".$data['email']."',website='".$data['website']."',deleted='0'");
}

else {


    $query = $this->db->query("UPDATE easamoveadmin_tbl_agentdb SET serial='".$data['name']."',company='".$data['company']."',branch='".$data['branch']."',address='".$data['address']."',contact='".$data['contact']."',phone='".$data['phone']."',email='".$data['email']."',website='".$data['website']."',deleted='0' WHERE id=".$data['id']);
}
echo $query;
}


public function loadagentdb() {

    $query = $this->db->query("SELECT * FROM `easamoveadmin_tbl_agentdb` WHERE deleted = 0");
    return $query->result();
}




public function deleteagentdb($id) {

 $query = $this->db->query("UPDATE `easamoveadmin_tbl_agentdb` SET deleted = 1 WHERE id =".$id);
    return $query;

}


public function userapproval() {

$query = $this->db->query("SELECT * FROM easamoveadmin_tbl_agentdb where approved = 0 and deleted = 0 and sent = 1 ORDER by contact asc");

return $query->result();
   } 


public function selectfewagentdb($id) {


$qry = $this->db->query("SELECT * FROM `easamoveadmin_tbl_agentdb` WHERE id in (".$id.")");

return $qry->result_array();


}


public function sendmail() {




}


public function adduserapproval($id,$salt) {

    $query = $this->db->query("UPDATE easamoveadmin_tbl_agentdb SET sent= 1,hash='".$salt."',status=2 WHERE id=".$id);
    
}


public function checkhash($hash) {

$query = $this->db->query("SELECT * FROM easamoveadmin_tbl_agentdb WHERE hash ='".$hash."' and approved != 1");

return $query->row_array();

}




public function approveit($hash) {

$id  = $this->db->query("SELECT agent_id from easamoveadmin_tbl_agent WHERE hash ='".$hash."'");

$this->db->query("UPDATE easamoveadmin_tbl_agentdb SET approved = 1, agent_id='".$id."' where  hash='".$hash."'");

$query = $this->db->query("UPDATE easamoveadmin_tbl_agent SET agent_is_deleted =0  WHERE hash ='".$hash."'");

 return $query; 
}



public function deleteapproval($hash) {

$query1 =  $this->db->query("UPDATE  easamoveadmin_tbl_agentdb SET hash = '' where hash='".$hash."'");

$query2 = $this->db->query("UPDATE  easamoveadmin_tbl_agent SET agent_is_deleted = 1 where hash='".$hash."'");

$key = $query2->row['agentuser_verifykey'];

$query3 = $this->db->query("UPDATE  pro_tbl_agentuser SET agentuser_is_deleted = 1 where agentuser_verifykey='".$key."'");

return $query3; 

}





public function agentUser() {

    $query = $this->db->query("SELECT * FROM pro_tbl_agentuser where agentuser_is_deleted=0 and agentuser_access!=1");
    return $query->result();
}



public function deleteuseragent($id) {

$data  = array('agentuser_is_deleted'=>1);
    $this->db->where("agentuser_id",$id);
    $query = $this->db->update("pro_tbl_agentuser",$data);
    
    return $query; 
}









//for country
    public function addCountry($data) {
        $query = $this->db->insert('easamoveadmin_tbl_country', $data);
        return $query;
    }

    public function getCountry() {
        $query = $this->db->query('SELECT * FROM easamoveadmin_tbl_country where country_is_deleted=0 order by country_id desc');
        return $query->result();
    }

    public function updateCountry($data, $id) {
        $this->db->where('country_id', $id);
        $query = $this->db->update('easamoveadmin_tbl_country', $data);
        return $query;
    }

    public function deleteCountry($data, $id) {
         $this->db->where('country_id', $id);
        $query = $this->db->update('easamoveadmin_tbl_country', $data);
         $res =  $this->db->query("SELECT state_id FROM easamoveadmin_tbl_state where country_id =".$id." and state_is_deleted=0");
        $query1 = $this->db->query('update easamoveadmin_tbl_state set state_is_deleted =1 WHERE country_id =' . $id.'');
     
       foreach ($res->result() as $key => $value) {
        $a[] = $value->state_id;
        }

    $result = implode(',',$a);
if(!empty($result)) {

       $query2 = $this->db->query('update easamoveadmin_tbl_city set city_is_deleted = 1 WHERE state_id in ('.$result .')');
   }
        echo $query;
    }

//for state
    public function addState($data) {
        $query = $this->db->insert('easamoveadmin_tbl_state', $data);
        return $query;
    }

    public function getState($id = 0) {
        $whr = '';
        if ($id > 0) {
            $whr = "and a.country_id=$id";
        }
        $query = $this->db->query("SELECT a.state_id,a.state_name,b.country_name FROM easamoveadmin_tbl_state a left join easamoveadmin_tbl_country b on (a.country_id=b.country_id) where state_is_deleted=0 $whr order by state_id desc");
        return $query->result();
    }

    public function updateState($data, $id) {
        $this->db->where('state_id', $id);
        $query = $this->db->update('easamoveadmin_tbl_state', $data);
        return $query;
    }

    public function deleteState($data, $id) {
        $this->db->where('state_id', $id);
        $query = $this->db->update('easamoveadmin_tbl_state', $data);
        $query1 = $this->db->query('update easamoveadmin_tbl_city set city_is_deleted = 1 where state_id ='. $id.'');
        echo $query;
        return $query1;
    }

    public function getHeadoffice() {
        $query = $this->db->query("SELECT agent_id,agent_name FROM easamoveadmin_tbl_agent where agent_office_type=1");
        return $query->result();
    }

//for city
    public function addCity($data) {
        $query = $this->db->insert('easamoveadmin_tbl_city', $data);
        return $query;
    }

    public function getCity($id = 0) {
        $whr = '';
        if ($id > 0) {
            $whr = "and a.country_id=$id";
        }
        $query = $this->db->query("SELECT a.city_id,a.city_name,a.city_code,c.country_name,b.state_name,b.state_id,c.country_id FROM easamoveadmin_tbl_city a left join easamoveadmin_tbl_state b on (a.state_id=b.state_id) left join easamoveadmin_tbl_country c on (b.country_id=c.country_id) where city_is_deleted=0 $whr order by city_id desc");
        return $query->result();
    }

    public function updateCity($data, $id) {
        $this->db->where('city_id', $id);
        $query = $this->db->update('easamoveadmin_tbl_city', $data);
        return $query;
    }

    public function deleteCity($data, $id) {
        $this->db->where('city_id', $id);
        $query = $this->db->update('easamoveadmin_tbl_city', $data);
        echo $query;
    }

    //for enquiry type
    public function addEnquirytype($data) {
        $query = $this->db->insert('easamoveadmin_tbl_enquirytype', $data);
        return $query;
    }

    public function getEnquirytype() {

        $query = $this->db->query("SELECT * from easamoveadmin_tbl_enquirytype where enquiry_is_deleted=0 order by enquiry_id desc");
        return $query->result();
    }

    public function updateEnquirytype($data, $id) {
        $this->db->where('enquiry_id', $id);
        $query = $this->db->update('easamoveadmin_tbl_enquirytype', $data);
        return $query;
    }

    public function deleteEnquirytype($data, $id) {
        $this->db->where('enquiry_id', $id);
        $query = $this->db->update('easamoveadmin_tbl_enquirytype', $data);
        echo $query;
    }

    //for alert type
    public function addAlerttype($data) {
        $query = $this->db->insert('easamoveadmin_tbl_alerttype', $data);
        return $query;
    }

    public function getAlerttype() {

        $query = $this->db->query("SELECT * from easamoveadmin_tbl_alerttype where alert_is_deleted=0 order by alert_id desc");
        return $query->result();
    }

    public function updateAlerttype($data, $id) {
        $this->db->where('alert_id', $id);
        $query = $this->db->update('easamoveadmin_tbl_alerttype', $data);
        return $query;
    }

    public function deleteAlerttype($data, $id) {
        $this->db->where('alert_id', $id);
        $query = $this->db->update('easamoveadmin_tbl_alerttype', $data);
        echo $query;
    }

    //for decorative condition
    public function addDecorativecondition($data) {
        $query = $this->db->insert('easamoveadmin_tbl_decorativecondition', $data);
        return $query;
    }

    public function getDecorativecondition() {

        $query = $this->db->query("SELECT * from easamoveadmin_tbl_decorativecondition where condition_is_deleted=0 order by condition_id desc");
        return $query->result();
    }

    public function updateDecorativecondition($data, $id) {
        $this->db->where('condition_id', $id);
        $query = $this->db->update('easamoveadmin_tbl_decorativecondition', $data);
        return $query;
    }

    public function deleteDecorativecondition($data, $id) {
        $this->db->where('condition_id', $id);
        $query = $this->db->update('easamoveadmin_tbl_decorativecondition', $data);
        echo $query;
    }

    //for parking type
    public function addParkingtype($data) {
        $query = $this->db->insert('easamoveadmin_tbl_parkingtype', $data);
        return $query;
    }

    public function getParkingtype() {

        $query = $this->db->query("SELECT * from easamoveadmin_tbl_parkingtype where parking_is_deleted=0 order by parking_id desc");
        return $query->result();
    }

    public function updateParkingtype($data, $id) {
        $this->db->where('parking_id', $id);
        $query = $this->db->update('easamoveadmin_tbl_parkingtype', $data);
        return $query;
    }

    public function deleteParkingtype($data, $id) {
        $this->db->where('parking_id', $id);
        $query = $this->db->update('easamoveadmin_tbl_parkingtype', $data);
        echo $query;
    }
















// For agent member 
    //upload image here
   public function addMemeber($name,$website) {
       if (isset($_FILES['memberLogo'])) {
           if ($this->upload_files($_FILES['memberLogo'],$name,$website) === false) {
               return 0;
           } else {
               return 1;
           }

       }
   }

   //calling this function to move image and save in 'charity_gallery' 
   private function upload_files($files,$name,$web) {
      //print_r($files);exit();
       $config = array(
           'upload_path' => 'agent_member/',
           'allowed_types' => 'jpg|gif|png|jpeg|JPEG|JPG',
           'overwrite' => 1,
           'max_width' => '2048',
           'max_height' => '2048'
       );

       $this->load->library('upload', $config);

       $_FILES['memberLogo']['name'] = $files['name'];
       $_FILES['memberLogo']['type'] = $files['type'];
       $_FILES['memberLogo']['tmp_name'] = $files['tmp_name'];
       $_FILES['memberLogo']['error'] = $files['error'];
       $_FILES['memberLogo']['size'] = $files['size'];
       $fileName = $files['name'];
//        echo $fileName='gallery_img_'.time().'.'.$files['type'][$key];

       $config['file_name'] = str_replace(' ', '_', $fileName);
       $imgName = str_replace(' ', '_', $fileName);
       $this->upload->initialize($config);
       //echo "INSERT INTO `easamoveadmin_tbl_agentmembers` (member_name,member_website,member_created_by,member_logo) VALUES ('$name','$web','1','$imgName')";
       
       if ($this->upload->do_upload('memberLogo')) {
           $this->upload->data();
           //echo "INSERT INTO `easamoveadmin_tbl_agentmembers` (member_name,member_website,member_created_by,member_logo) VALUES ('$name','$web','1','$imgName')";
           $query = $this->db->query("INSERT INTO `easamoveadmin_tbl_agentmembers` (member_name,member_website,member_created_by,member_logo) VALUES ('$name','$web','1','$imgName')");
           return true;
       } else {
           return false;
       }

       
   }

    public function addAgentMember($values) {
        // print_r($values);

        $query = $this->db->insert('easamoveadmin_tbl_agentmembers', $values);
        return $query;
    }


    public function updateAgentMember($values, $id) {

        $this->db->where('member_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_agentmembers',$values);
        return $query;
    }


    public function getAgentMember() {

        $query = $this->db->query("SELECT * from easamoveadmin_tbl_agentmembers where member_is_deleted=0 order by member_id desc");
        return $query->result(); 

    }


    public function deleteAgentMember($values,$id) {

        $this->db->where('member_id',$id);
        $query = $this->db->update('easamoveadmin_tbl_agentmembers',$values);
        return $query;
    }






//for member in

    public function agent_member_in() {

      $query = $this->db->query("SELECT * FROM `easamoveadmin_tbl_agentmembers` where member_is_deleted=0");
        return $query->result();  
          }



function get_mem($get_mem = '') {

     if($get_mem != null ) { $get_mem = "where agent_id = ".$get_mem;} 
$query = $this->db->query("SELECT * FROM `easamoveadmin_tbl_agent` ".$get_mem." ORDER BY `easamoveadmin_tbl_agent`.`agent_id` ASC");
        return $query->result();

}






















    //for Agent type
    public function addAgenttype($data) {
        $query = $this->db->insert('easamoveadmin_tbl_agenttype', $data);
        return $query;
    }

    public function getAgenttype() {

        $query = $this->db->query("SELECT * from easamoveadmin_tbl_agenttype where agent_is_deleted=0 order by agent_id desc");
        return $query->result();
    }

    public function updateAgenttype($data, $id) {
        $this->db->where('agent_id', $id);
        $query = $this->db->update('easamoveadmin_tbl_agenttype', $data);
        return $query;
    }

    public function deleteAgenttype($data, $id) {
        $this->db->where('agent_id', $id);
        $query = $this->db->update('easamoveadmin_tbl_agenttype', $data);
        echo $query;
    }

    //for central heating type
    public function addCentralheatingtype($data) {
        $query = $this->db->insert('easamoveadmin_tbl_centralheatingtype', $data);
        return $query;
    }

    public function getCentralheatingtype() {

        $query = $this->db->query("SELECT * from easamoveadmin_tbl_centralheatingtype where centralheating_is_deleted=0 order by centralheating_id desc");
        return $query->result();
    }

    public function updateCentralheatingtype($data, $id) {
        $this->db->where('centralheating_id', $id);
        $query = $this->db->update('easamoveadmin_tbl_centralheatingtype', $data);
        return $query;
    }

    public function deleteCentralheatingtype($data, $id) {
        $this->db->where('centralheating_id', $id);
        $query = $this->db->update('easamoveadmin_tbl_centralheatingtype', $data);
        echo $query;
    }

}

?>