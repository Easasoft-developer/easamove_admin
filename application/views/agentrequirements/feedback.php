<?php $actualurl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>

<nav class="subnav" role="navigation">
        <div class="container-fixed">

        <div class="row">
          <div class="col-sm-12">
            <ul class="list-inline">
               <li <?php echo (base_url('AgentRequirements/contact')==$actualurl)?'class=active':''; ?>><a href="<?php echo base_url();?>AgentRequirements/contact">Contact</a></li>
              <li <?php echo (base_url('AgentRequirements/feedback')==$actualurl)?'class=active':''; ?>><a href="<?php echo base_url();?>AgentRequirements/feedback">Feedback</a></li>
            
          </ul>
          </div>
        </div>       
        
          
        </div>
      </nav>




<section id="main-content" class="main-content">
        <section class="surround">

        <div class="container-fluid property_listing master">
          <div class="row">
              <div class="col-xs-12">
                <div class="panel">
                  <div class="panel-heading">
                    


                    <div class="row">
                      <div class="col-sm-12">
                        <h3>Feedback</h3>
                      </div>
                    </div>

              
                  
                  </div>

                  <div class="">
                    <div class="table-responsive">



 <table id="datatb" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Feedback name</th>
                                <th>Feedback email</th>
                                <th>Feedback phone</th>
                                <th>Feedback title</th>
                                <th>Feedback categories</th>
                                <th>Feedback comments</th>
                            </tr>
                        </thead>

                       <tbody class="data-append">
                        <tr>
                            <td colspan="5" class="tac" style="height: 100px; vertical-align: middle;">Loading
                            </td>
                        </tr>
                    </tbody>

                    </table>



                    </div>
                  </div>

                  
                </div>
              </div>    
          </div>
        </div>
          
        </section>        
      </section>




      <script>
    var base_url = '<?php echo base_url(); ?>';
    var cont = 'feedback';


    $(document).ready(function () {


        getfeedback(base_url, cont);


    });


    // function edit(id) {

    //     $("input[name='id']").val(id);
    //     $("input[name='Type']").val($('#' + id).find("td:eq(1)").text());
        
    //     document.getElementById("Desc").value = $('#' + id).find("td:eq(2)").text();
        
    // }
    // function deleteOutsidespace(id) {
    //     if (confirm('Are you sure you want to Delete?')) {
    //         $.ajax({
    //             type: "POST",
    //             url: "<?php echo base_url(); ?>featuremaster/" + cont + "/deleteOutsidespace",
    //             data: "id=" + id,
    //             success: function (data) {
    //                 if (data) {
    //                     $('#' + id).remove();
    //                     window.location.reload();
    //                 }
    //             }
    //         });
    //     }
    // }
    // $("input[name='Type']").on('keyup', function () {
    //     $('.error').remove();
    //     if ($("input[name='id']").val() == 0) {
    //         $.ajax({
    //             type: "POST",
    //             url: "<?php echo base_url(); ?>featuremaster/" + cont + "/checkOutsidespace",
    //             data: "type=" + $("input[name='Type']").val(),
    //             dataType: 'html',
    //             success: function (data) {
    //                 if (data == 1) {
    //                     $("input[name='Type']").after('<span class="error">Outside space already exist</span>');
    //                     $('.submit').prop('disabled', true);
    //                 } else {
    //                     $("input[name='Type']").after('<span class="error"></span>');
    //                     $('.submit').prop('disabled', false);
    //                 }
    //             }
    //         });
    //     }
    // });
</script>