<?php 
$actualurl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

?>


<nav class="subnav" role="navigation">
        <div class="container">

        <div class="row">
          <div class="col-sm-12">
            <ul class="list-inline">
               <li <?php echo (base_url('adminUser') == $actualurl) ? 'class=active' : ''; ?>>
            <a href="<?php echo base_url(); ?>adminUser">Manage User</a>
            <span></span>
        </li>

   
        <li <?php echo (base_url('myprofile') == $actualurl) ? 'class=active' : ''; ?>>
            <a href="<?php echo base_url(); ?>account/myprofile">My Profile</a>
            <span></span>
        </li>

        <li class="active">
            <a href="<?php echo base_url(); ?>account/changePassword">Change Password</a>
            <span></span>
        </li>

          </ul>
          </div>
        </div>       
        
          
        </div>
      </nav>







<section id="main-content" class="main-content">
        <section class="surround">

        <div class="container-fluid property_listing master">
          <div class="row">
              <div class="col-xs-12">
                <div class="panel">
                  <div class="panel-heading">
                    


                    <div class="row">
                      <div class="col-sm-12">
                        <h3>Change Password</h3>
                      </div>
                    </div>

                    <form method="post" id="changepassword"> 
                    <div class="row">

                      <div class="col-sm-12">
                        <div class="form-horizontal">
                     

             

                          <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="name">New Password</label>
                            <div class="col-sm-4 col-md-3">
                    <input type="password" id="newpassword" name="newpassword" maxlength="20" class="form-control" />
                            </div>
                          </div>

                    

  <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="Email">Confirm Password</label>
                            <div class="col-sm-4 col-md-3">
                        <input type="password" id="cpassword" name="cpassword" maxlength="20" class="form-control" />
                            </div>
                          </div>


                        


                          <div class="form-group">
                            <div class="col-md-offset-5 col-sm-offset-3 col-sm-3 save-cancel">
                              <button type="button" class="btn btn-default submit">Save</button>
                              <button type="button" class="btn btn-default btn-danger reset">Cancel</button>
                            </div>                            
                          </div>


                        </div>                        
                      </div>


                    </div>
                    </form>


               
                  
                  </div>
                 
                </div>
              </div>    
          </div>
        </div>
          
        </section>        
      </section>






<script>
    var base_url = '<?php echo base_url(); ?>';
    var cont = 'AdminUser';

    $(document).ready(function () {
    

    $(".reset").on("click",function() { 
$('.error').remove();
    });

$("input").on("keypress",function(e) {  
if(e.which == 13){
e.preventDefault();
$('.submit').click();
}
else {
$('.error').remove();
  
}

});

    });


    $('.submit').on('click', function () {
        $('.error').remove();
        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

//        alert(echeck);

        if ($("input[name='newpassword']").val() == '') {
            $("input[name='newpassword']").after('<span class="error">Please fill this field</span>');
            $("input[name='newpassword']").focus();
        } else if ($("input[name='cpassword']").val() == '') {
            $("input[name='cpassword']").after('<span class="error">Please fill this field</span>');
            $("input[name='cpassword']").focus();
        } else if ($("input[name='newpassword']").val() != $("input[name='cpassword']").val()) {
            $("input[name='cpassword']").after('<span class="error">Password mismatch</span>');
            $("input[name='cpassword']").focus();

        } else {

            var datas = $('#changepassword').serialize();
            $.ajax({
                type: "POST",
                url: base_url + "/" + cont + "/resetPassword",
                data: datas,
                success: function (data) {
                    alert('Password Changed Successfully, Please login again');
                    window.location.href='<?php echo base_url('Account/logout');?>'
                }
            });

            $('#changepassword')[0].reset();

        }
    });


</script>
