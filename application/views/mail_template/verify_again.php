
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Easamove</title>
    <meta http-equiv='content-Control' content='no-cache' />
    <meta http-equiv='Cache-Control' content='no-cache' />
    <meta http-equiv='Expires' content='0' />
    <meta http-equiv="content-type" content="text/html; charset=iso-8859-1" />
    <meta http-equiv="content-language" content="en" />
    <meta name="robots" content="noindex, follow" />
  </head>


  <body leftmargin="0" topmargin="0" marginwidth="0" marginheight="0" border="1" style="font-family: Arial,Helvetica Neue,Helvetica,sans-serif;font-size:14px;color:#333;">

    <table width="650" border="0" cellspacing="0" cellpadding="0" background="http://<?php echo $_SERVER['SERVER_NAME']; ?>/images/mailer-header.jpg" height="181" style="margin:auto;padding:20px;text-align:center;color:white;">
      <tr>
        <td style="padding-top:20px;">
          <img width="253" src="<?php echo $_SERVER['SERVER_NAME']; ?>/images/logo1.png">
        </td>
      </tr>
<tr>
        <td><h3 style="font-size:24px;margin: 15px 0px 10px;"><b>Welcome <?php echo $name; ?></b></h3></td>
      </tr>
      <tr>
        <td><img src="<?php echo $_SERVER['SERVER_NAME']; ?>/images/waves.png"></td>
      </tr>
    </table>

    <table width="650" border="0" cellspacing="0" cellpadding="0" style=" border-right: 1px solid #ccc;  border-left: 1px solid #ccc; margin:0 auto; height:auto;font-size:13px; font-family: Arial,Helvetica Neue,Helvetica,sans-serif;padding: 20px;">


 <tr>
        <td><h5 style="font-size:14px;margin: 10px 0px;"><b>Hello <?php echo $name; ?>,</b></h5></td>
      </tr>
    <tr>
        <td style="line-height:1.5;padding-bottom:5px;">Thank you for signing up with us. To verify your registration, please click on the link below (or copy and paste the URL into
        your browser) <a href="<?php echo $link;?>" style="color:#ff9000;"><?php echo $link;?></a>.</td>
    </tr>

    <tr>
        <td style="line-height:1.5;padding-bottom:5px;padding-top:10px;">Best Regards,</td>
      </tr>

      <tr>
        <td style="line-height:1.5;padding-bottom:10px;">Easamove team</td>
      </tr>
      
    </table>


    <table width="650" border="0" cellspacing="0" cellpadding="0" style=" font-family: Arial,Helvetica Neue,Helvetica,sans-serif; font-size:13px; margin:0 auto; height:auto; background:#efefef;border:1px solid #ccc;border-top:0;padding: 10px 20px;">
      <tr>
        <td style="padding:10px 0px;">
          <img width="135" src="<?php echo $_SERVER['SERVER_NAME']; ?>/images/logo-footer.png">
        </td>
        <td style=" font-size:12px; padding:10px 0px;" align="right">
          © <?php echo date('Y'); ?> Easamove Property.  All rights reserved.
        </td>
        <td style="padding:10px 0px;" align="right">
          <ul style="list-style:none;margin: 0px;">
            <li style="float:left;margin-left: 0px;"><a href="" style="padding: 0px 5px;display: block;"><img width="16" src="<?php echo $_SERVER['SERVER_NAME']; ?>/images/facebook.png"></a></li>
            <li style="float:left;margin-left: 0px;"><a href="" style="padding: 0px 5px;display: block;"><img width="16" src="<?php echo $_SERVER['SERVER_NAME']; ?>/images/twitter.png"></a></li>
            <li style="float:left;margin-left: 0px;"><a href="" style="padding: 0px 5px;display: block;"><img width="18" src="<?php echo $_SERVER['SERVER_NAME']; ?>/images/google-plus.png"></a></li>
            <li style="float:left;margin-left: 0px;"><a href="" style="display: block;"><img width="24" src="<?php echo $_SERVER['SERVER_NAME']; ?>/images/linkedin.png"></a></li>
            <li style="float:left;margin-left: 0px;"><a href="" style="padding: 0px 5px;display: block;"><img width="20" src="<?php echo $_SERVER['SERVER_NAME']; ?>/images/youtube.png"></a></li>
          </ul>
        </td>
      </tr>
    </table>

    </table>
  </body>
</html>