<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Easamove Admin</title>
</head>
<body>
<table width ="550px" border="0" cellspacing="0" cellpadding="0" style=" border-top: 1px solid #f2f2f2;  border-right: 1px solid #f2f2f2;  border-left: 1px solid #f2f2f2;font-family:'Open Sans'; font-size:14px; margin:0 auto; height:auto; background-color:#fffff6;">
	<tr valign="top" style="background-color:#f2f2f2;text-align: center;">
    	<td width="10%" style=" padding-bottom: 5px; padding-top: 5px;"><img src="" alt="EASAMOVE"></td>

        <!-- <td style="padding:20px 0 20px 15px; font-size: 24px;"></td> -->
    </tr>
    <!-- <tr valign="top" style="background-color:#f2f2f2;text-align: center;">
        <td width="10%" style=" padding-bottom: 10px;"><span style="padding:20px 0 5px 15px; font-size: 15px; font-family:'MyriadSetPro-Text';letter-spacing: 5px; color:#777;">ONLINE WORKSHOP ON FILMMAKING</span></td>
    </tr> -->
    <table width ="550px" border="0" cellspacing="0" cellpadding="0" style=" border-right: 1px solid #f2f2f2;  border-left: 1px solid #f2f2f2; margin:0 auto; height:auto; background-color:#ffffff; font-family:'Open Sans'; padding:0 15px 0 15px;">
   		
        <tr>
        	<td style=" font-size:14px; font-weight:400; color:#555353; padding:15px 0 0 0;">Hello <?php echo $name; ?><br /><br />
            Thank you for signing up with us. To verify your registration, please click on the link below.</td>
        </tr>
        
        <tr>
        	<td style=" font-size:14px; font-weight:400; color:#555353; padding:15px 0 0 0;">
             <a style="color:#2a98d7" href="<?php echo $link;?>">Verify Registration</a></td>
        </tr>
        
         <tr>
             <td>
                 <h1 style="font-size:15px; color:#333; font-weight:500; margin:26px 0 0 0px;">Thanks,</h1>
                 <h2 style="font-size:14px; color:#444; font-weight:500; margin:0 0 0 0px; padding:3px 0 24px 0">Easamove</h2>
             </td>
          </tr>
   </table>
   
   <table width ="550px" border="0" cellspacing="0" cellpadding="0" style="  font-family:'Open Sans'; font-size:14px; margin:0 auto; height:auto; background-color:#f7fbfe;border-left:1px solid #cadce6;border-right:1px solid #cadce6;border-bottom:1px solid #cadce6;">
        <tr valign="top" style="background-color:#fff; ">
            <td style="text-align:center;border-top:1px solid #333;">
              <a href="" style="padding:2px 3px;"><img src="http://easasoft.com/onlinestudies/images/facebook1.png"></a>
              <a href="" style="padding:2px 3px;"><img src="http://easasoft.com/onlinestudies/images/youtube1.png"></a>
              <a href="" style="padding:2px 3px;"><img src="http://easasoft.com/onlinestudies/images/twitter1.png"></a>
            </td>
        </tr>
        <tr valign="top" style="background-color:#fff; ">
            <td><h5 style="text-align:center;font-size:12px; color:#718591; font-weight:400; margin: 0 10px; padding: 5px 0;">Copyright © <?php echo date('Y'); ?> -easamove.co.in. All rights reserved
            
            </h5></td>
        </tr>
        
    </table>
</table>
</body>
</html>
