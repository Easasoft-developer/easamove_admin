<?php error_reporting(0);
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
//print_r($agent);
?>


<!-- main content -->
      
      <section class="surround"> 

<input type="hidden" id="echeck" value="0">
<form id="add-to-listing-partial-form" name="add-to-listing-partial-form" class="horizontal-form" method="post"  enctype="multipart/form-data">
        <div class="container-fixed add-listing">
  
<!-- Basic info -->
        <div class="panel">  

          <div class="panel-heading">
            <div class="row">
              <div class="col-sm-6">
                <h3 >New Agent</h3>
              </div>
              <div class="col-sm-6">
                <div class="save-cancel">
                  <button type="button" class="btn btn-default submit">Save</button>
                  <a href="<?php echo base_url(); ?>agent/agent" class="btn btn-danger">Cancel</a>
                </div>
              </div>
            </div>
          </div>   

 <div class="row">
                <div class="col-md-12 pl5 pr0">
                    <div class="alert alert-success alert-dismissable DN" >
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                        <strong>Success!</strong> 
                    </div>
                </div>
            </div>

            <input type="hidden" name="id" value="<?php echo isset($agent[0]->agent_id) ? $agent[0]->agent_id : '0'; ?>"/>
          <div class="panel-title">
            <div class="row">
              <div id="headings" class="col-sm-6">
                Basic Infomation
              </div>
              <div class="col-sm-6 hidden-xs">
                Drag the marker to correct location
              </div>
            </div>
          </div>
          

          <div class="panel-body">
            <!-- <form class=""> -->
            <div class="row">
              <div class="col-sm-6 p0">

            
              <div class="row">
                <div class="form-group col-sm-6">
                  <label for="agentrefno">Agent Reference No<span style="color: #F00"> *</span></label>                                                  
                  <input type="text" id="agentrefno" name="agentrefno" class="form-control input"  maxlength="10"  value="<?php echo isset($agent[0]->agent_refno) ? $agent[0]->agent_refno : ''; ?>" >                              
                </div>

                <div class="form-group col-sm-6">
                  <label for="p-name">Company Name<span style="color: #F00"> *</span></label>                                                  
                  <input type="text" id="cName" name="cName" class="form-control input"  maxlength="50"  value="<?php echo isset($agent[0]->agent_name) ? $agent[0]->agent_name : ''; ?>" placeholder="e.g. Goren Gates"/>                              
                </div>
</div>
<div class="row">
                <div class="form-group col-sm-6">
                  <label for="vatregcty">VAT Registration Number</label>
                  <input type="text" id="vatregno" name="vatregno" class="form-control input"  maxlength="10"  value="<?php echo isset($agent[0]->vat_reg_no) ? $agent[0]->vat_reg_no : ''; ?>"/>                
                </div>

                <div class="form-group col-sm-6">
                  <label for="vatregcty">VAT Registration Country</label>
                  <input type="text" id="vatregcty" name="vatregcty" class="form-control input" maxlength="30" value="<?php echo isset($agent[0]->vat_reg_country) ? $agent[0]->vat_reg_country : ''; ?>" />                                
                </div>
</div>
<div class="row">
                <div class="form-group col-sm-6">
                  <label for="nobranch">Number of Branch</label>                                                  
                  <input type="text" id="nobranch" name="nobranch" class="form-control input" maxlength="3" value="<?php echo isset($agent[0]->num_of_branch) ? $agent[0]->num_of_branch : ''; ?>" />                                
                </div>

                <div class="form-group col-sm-6">
                  <label for="defcur">Default Currency<span style="color: #F00"> *</span></label>                                                  
                  <input type="text" id="defcur" name="defcur" class="form-control input" maxlength="3" value="<?php echo isset($agent[0]->default_currency) ? $agent[0]->default_currency : 'INR'; ?>" />                                
                </div>
</div>
<div class="row">
                <div class="form-group col-md-12 size">
                  <label for="invoicedue">Invoice Due Days<span style="color: #F00"> *</span></label>                                                  
                  <input type="text" id="invoicedue" name="invoicedue" class="form-control input" maxlength="3" value="<?php echo isset($agent[0]->invoice_due_day) ? $agent[0]->invoice_due_day : ''; ?>" />                                
                </div>
</div>
<div class="row">
                <div class="form-group col-sm-6">
                  <label for="workstart">Start Time</label>                                                  
                  <input type="text" id="workstart" name="workstart" class="form-control input" maxlength="3" value="<?php echo isset($agent[0]->work_start_time) ? $agent[0]->work_start_time : ''; ?>" readonly/>                                
                </div>

                <div class="form-group col-sm-6">
                  <label for="workend">End Time</label>                                                  
                  <input type="text" id="workend" name="workend" class="form-control input" maxlength="3" value="<?php echo isset($agent[0]->work_end_time) ? $agent[0]->work_end_time : ''; ?>" readonly/>
                </div>

                <div class="form-group col-md-12 size">
                  <label for="AgentTypeId">Agent Type<span style="color: #F00"> *</span></label>                                                  
                  <select id="AgentTypeId" name="AgentTypeId" class="form-control">
                    <option value="0">- Select -</option>
                                        <?php
                                        $check = '';
                                        foreach ($agenttype as $key => $type) {
                                            if ($agent[0]->agent_type == $type->agent_id) {
                                                $check = 'selected="selected"';
                                            }
                                            echo "<option $check value=" . $type->agent_id . ">" . $type->agent_name . "</option>";
                                        }
                                        ?>
                  </select>
                </div>
</div>
<div class="row">

<div class="form-group col-sm-6">
                  <label for="Address_Street">Number<span style="color: #F00"> *</span></label>                                                  
                  <input type="text" name="Number" id="Number" maxlength="200" value="<?php echo isset($agent[0]->number) ? $agent[0]->number : ''; ?>" class="form-control input" />                                
                </div>

                <div class="form-group col-sm-6">
                  <label for="Address_Street">Street<span style="color: #F00"> *</span></label>                                                  
                  <input type="text" name="AddressStreet" id="Address_Street" maxlength="200" value="<?php echo isset($agent[0]->street) ? $agent[0]->street : ''; ?>" class="form-control input" />                                
                </div>
</div>
<div class="row">
                <div class="form-group col-sm-6">
                  <label for="Address_CityName">City<span style="color: #F00"> *</span></label>                                                  
                  <input type="text" name="AddressCityName" id="Address_CityName" maxlength="50" value="<?php echo isset($agent[0]->town) ? $agent[0]->town : ''; ?>" class="form-control input" />                                
                   <input type="hidden" name="Latitude" id="Latitude" value="<?php echo isset($agent[0]->agent_lat) ? $agent[0]->agent_lat : '0'; ?>"/>
                   <input type="hidden" name="Longitude" id="Longitude" value="<?php echo isset($agent[0]->agent_long) ? $agent[0]->agent_long : '0'; ?>"/>
                   </div>

 <div class="form-group col-sm-6">
                  <label for="Address_Street">County</label>                                                  
                  <input type="text" name="County" id="County" maxlength="200" value="<?php echo isset($agent[0]->county) ? $agent[0]->county : ''; ?>" class="form-control input" />                                
                </div>

</div>

                <div class="row">

                <div class="form-group col-md-12">
                  <label for="AddressZip">Post Code<span style="color: #F00"> *</span></label>                                                  
                   <input type="text" name="AddressZip" id="Address_Zip" maxlength="10" value="<?php echo isset($agent[0]->post_code) ? $agent[0]->post_code : ''; ?>" placeholder="e.g. W4 2LT rather than W42LT" class="form-control input" />
                </div>

                

                <!-- <div class="form-group col-sm-6">
                  <label for="Address_County">Country<span style="color: #F00"> *</span></label>                                                  
                   <input type="text" name="AddressCounty" id="Address_County" maxlength="50" value="<?php echo isset($agent[0]->country) ? $agent[0]->country : ''; ?>" class="form-control input" />          
                </div> -->

</div>
              </div>

              <div class="col-sm-6 p0">

              <h5 class="visible-xs"><strong>Drag the marker to correct location</strong></h5>

           <!--     <form class="form-inline">
                                <div class="form-group">
                                    <label class="sr-only" for=""></label>
                                    <div class="input-group">

                                        <input id="pac-input" class="form-control input-sm" type="text" placeholder="Search Box">
                                        <div class="input-group-addon"><a type="button" style="cursor:pointer;color:#777;" value="find" onClick="getcode()">Find</a></div>
                                    </div>
                                </div>

                            </form> -->
                
                                    <div style="height: 570px;margin: 15px 0px;" id="map">

                            </div>
              </div>

              
         
                 
                
              </div>

              
            </div>
            <!-- </form> -->
          </div>
          
        

        <!-- contact info -->
        <div class="panel">      

          <div class="panel-title">
            <div class="row">
              <div class="col-sm-6 p0">
                Contact Information
              </div>
              <div class="col-md-6 p0 hidden-sm hidden-xs">
                Agent Logo
              </div>
            </div>
          </div>
          

          <div class="panel-body">
            <!-- <form class=""> -->
            <div class="row">
              <div class="col-md-6 p0 col-xs-12">
<?php // print_r($agent[0]);

 ?>
              
              <div class="row">
            <div class="col-md-12 p0 form-group">
              <div class="row gndr">
                  <div class="col-md-2">
                    <label for="title">Title<span style="color: #F00"> *</span></label>                                                  
                    <select id="title" name="title" class="form-control">
                     <option value="0" >----</option>
                    <option value="MR" <?php echo (isset($agent[0]->title) && $agent[0]->title=='MR') ? 'selected': ''; ?>>Mr</option>
                     <option value="MRS" <?php echo (isset($agent[0]->title) && $agent[0]->title=='MRS') ? 'selected': ''; ?>>Mrs</option>
                   </select>

                </div>

                <div class="col-md-5">
                  <label for="Surname">Forename<span style="color: #F00"> *</span></label>   
                      <input type="text" name="Forename" id="Forename" maxlength="30" onkeypress="return keyPress(event);" value="<?php echo isset($agent[0]->forename) ? $agent[0]->forename : ''; ?>" class="form-control input" />                                
                                                                               
               <!-- <input type="text" name="Surname" id="Surname" maxlength="30" onkeypress="return keyPress(event);" value="<?php echo isset($agent[0]->surname) ? $agent[0]->surname : ''; ?>" class="form-control" />                                 -->
                </div>

                <div class="col-md-5">
                  <label for="p-name">Surname<span style="color: #F00"> *</span></label>                                                
                  <!-- <input id="p-name" type="text" class="form-control"> -->
                   <input type="text" name="Surname" id="Surname" maxlength="30" onkeypress="return keyPress(event);" value="<?php echo isset($agent[0]->surname) ? $agent[0]->surname : ''; ?>" class="form-control input" />                                

                </div>
              </div>
            </div>
</div>
<div class="row">

                <div class="form-group col-sm-6">
                  <label for="p-name">Email address<span style="color: #F00"> *</span></label>                                               
                  <!-- <input id="p-name" type="text" class="form-control"> -->
                   <input type="text" id="Email" name="Email" maxlength="30" value="<?php echo isset($agent[0]->email_id) ? $agent[0]->email_id : ''; ?>" class="form-control input" />

                </div>

                <div class="form-group col-sm-6">
                  <label for="p-name">Phone Number<span style="color: #F00"> *</span></label>                                                  
                  <!-- <input id="p-name" type="text" class="form-control"> -->
                  <input type="text" id="Phone" name="Phone" maxlength="15" value="<?php echo isset($agent[0]->phone_no) ? $agent[0]->phone_no : ''; ?>" class="form-control input"/>

                </div>
                </div>
<div class="row">
                <div class="form-group col-sm-6">
                  <label for="p-name">Fax</label>                                                  
                  <!-- <input id="p-name" type="text" class="form-control"> -->
                    <input type="text" id="Fax" name="Fax" onkeypress="return isNumber(event)" maxlength="10" value="<?php echo isset($agent[0]->fax) ? $agent[0]->fax : ''; ?>" class="form-control input" />

                </div>

                <div class="form-group col-sm-6">
                  <label for="p-name">Website<span style="color: #F00"> *</span></label>                                                  
                  <!-- <input id="p-name" type="text" class="form-control"> -->
                    <input type="text" id="Website" name="Website" maxlength="100" value="<?php echo isset($agent[0]->website) ? $agent[0]->website : ''; ?>" class="form-control input" />
                </div>

              </div>
                
              </div>

              <div class="col-md-6 col-xs-12">

              <h5 class="visible-xs visible-sm"><strong>Agent Logo</strong></h5>
                
                <figure>
                  <img src="<?php echo base_url(); ?>images/<?php echo isset($agent[0]->agent_logo) ? $agent[0]->agent_logo : 'noimage.jpeg'; ?>" id="preview"  class="img-responsive" alt="" width="175">
                  <p class="help-block">We support png, jpeg and jpg files</p class="help-block">
                </figure>

                  <label class="btn btn-primary">Click to Add Photos

                    <input type="file" name="agentImage" class="DN upload" id="Photo_Upload_Control"/>
                    <input type="hidden" name="imageedit" id="imageedit" value="<?php echo isset($agent[0]->agent_logo) ? $agent[0]->agent_logo : ''; ?>"/>
                        
                <!-- <a href="#" id="upload_photo" class="btn btn-default">Click to Add Photos</a> -->
                                
</label>


              </div>

            </div>
            <!-- </form> -->
          </div>
          
        </div> 





<!-- Member in -->
       <div class="panel media document">
          <div class="panel-title">
            Member in
          </div>

          <div class="panel-body">            
            <div class="row">
            <?php
            if(isset($agent_m[0])) {
            $mem_id_no = explode(',', $agent_m[0]->member_id);
            }
            else { $mem_id_no = array(); }
            foreach ($get_member as $mem_key => $mem_val) {  ?>
            <div class="col-md-2">
              <div class="form-group">
                <label class="checkbox-inline">
                    <input name="mem[]" <?php if(in_array($mem_val->member_id, $mem_id_no)) {  if(isset($agent[0]->agent_id)) { echo 'checked'; } }    ?> value="<?php echo $mem_val->member_id;?>" type="checkbox" >
                    <img class='img-responsive' src="<?php echo base_url(); ?>images/agent_member/<?php echo $mem_val->member_logo; ?>" style="height: 50px;">
                    <span><?php echo $mem_val->member_name;   ?></span>
                </label>
              </div>
            </div>   
            <?php } ?>
            </div>            
          </div>
        </div>     























<!-- About -->
        <div class="panel media document">
          <div class="panel-title">
            About <span style="color: #F00"> *</span>
          </div>

          <div class="panel-body">            
            <div class="row">
              <div class="col-sm-12">
                <!-- <label for="full-describe">Summary</label> -->
                <textarea rows="10" cols="4" id="About" maxlength="4000" name="About" class="form-control"><?php echo isset($agent[0]->agent_aboutus) ? $agent[0]->agent_aboutus : ''; ?></textarea>

                    <!-- <textarea id="full-describe" class="form-control" rows="3" placeholder="Just start to write"></textarea> -->
                    <p id="help-block" class="help-block">Characters remaining: 4000 / 4000</p>                    
              </div>              
            </div>            
            
          </div>

          <div class="panel-title">
            <div class="save-cancel">
              <a href="#" class="btn btn-default submit">Save</a>
              <a href="" class="btn btn-default">Cancel</a>
            </div>
          </div>


        </div>          


          
</div> 


        </div>   
        </form>
        </div>

        
      </section>        
      




<script type="text/javascript" src="http://maps.googleapis.com/maps/api/js?libraries=places&sensor=false"></script>
<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>-->
<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
<link rel="stylesheet" href="<?php echo base_url(); ?>js/jquery.ui.timepicker.css?v=0.3.3" type="text/css" />  
<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>js/jquery.ui.timepicker.js?v=0.3.3"></script>
<!-- jQuery -->
<script type="text/javascript">


$(document).ready(function() {
    var text_max = 4000;
    var text_length = $('#About').val().length;
      
        var text_remaining = text_max - text_length;

        $('#help-block').html('Characters remaining ' + text_remaining + ' / 4000');
$(document).on('keydown', '#About', function() {
        var text_length = $('#About').val().length;
      
        var text_remaining = text_max - text_length;

        $('#help-block').html('Characters remaining ' + text_remaining + ' / 4000' );
    });






map();
function map() {

    var latp = document.getElementById("Latitude").value;
    var longp = document.getElementById("Longitude").value;
    if (latp == 0) {
        var lat = 51.573094;
        var lng = -0.371677;
    } else {
        var lat = latp;
        var lng = longp;
    }
    var latlng = new google.maps.LatLng(lat, lng),
            image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';
    var mapOptions = {
        center: new google.maps.LatLng(lat, lng),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    },
    map = new google.maps.Map(document.getElementById('map'), mapOptions),
            marker = new google.maps.Marker({
                position: latlng,
                map: map,
                icon: image,
                draggable: true
            });
    var input = document.getElementById('Address_Street');
    
    var geocoder = new google.maps.Geocoder();
    google.maps.event.addListener(marker, 'dragend', function () {
        var latLng = marker.getPosition();
        document.getElementById("Latitude").value = latLng.lat();
        document.getElementById("Longitude").value = latLng.lng();
        geocodePosition(marker.getPosition());
    });
    function geocodePosition(pos) {
        geocoder.geocode({
            latLng: pos
        }, function (responses) {
            if (responses && responses.length > 0) {

                console.log(responses[0].formatted_address);
                var add = responses[0].formatted_address;


                 var str = add.split(',');
                                                    var india = str[str.length - 1];
                                                    var city = str[str.length - 2];
                                                    var street = str;
                                                    // $('#Address_Zip').val();
                                                    // $('#Address_Street').val(street.toString());
                                                    // $('#Address_CityName').val(city);
                                                    // $('#Address_County').val(india);

            } else {
//              updateMarkerAddress('Cannot determine address at this location.');
            }
        });
    }
    $("#Address_Street,#Address_Zip,#Address_CityName,#Address_County").change(function (e) {
        console.log("Hello");
        //$(document).keypress(function (e) {
            // if (e.which == 13) {
                // infowindow.close();
                var firstResult = "";
                if($("#Address_Street").val() != "" && $("#Address_CityName").val() != "" && $("#Address_Zip").val() != ""){
                    console.log("harrow");
                    firstResult = $("#Address_Street").val() + ',' + $("#Address_CityName").val() + ',' + $("#Address_County").val() + ','+ $("#Address_Zip").val();
                }
                console.log(firstResult);        
                geocoder.geocode({"address": firstResult}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var lat = results[0].geometry.location.lat(),
                                lng = results[0].geometry.location.lng(),
                                placeName = results[0].address_components[0].long_name,
                                latlng = new google.maps.LatLng(lat, lng);
                        document.getElementById("Latitude").value = lat;
                        document.getElementById("Longitude").value = lng;
                        moveMarker(placeName, latlng);
                        $("#pac-input").val(firstResult);
                        map.setCenter(results[0].geometry.location);
                        map.setZoom(17);
                    }
                });
            // }
        //});
    });
    function moveMarker(placeName, latlng) {
        marker.setIcon(image);
        marker.setPosition(latlng);
        // infowindow.setContent(placeName);
        // infowindow.open(map, marker);
    }
}









});











                                        var base_url = '<?php echo base_url(); ?>';
                                        var cont = 'Agent';
                                        
                                        var markers = [];

                                        // var map;
                                        // initialize();
                                        $(document).ready(function () {
//        initAutocomplete();
                                        });
                                        $('.submit').on('click', function () {
                                            $('form#add-to-listing-partial-form').submit();
                                        });
$('.input').keypress(function (e) {
  if (e.which == 13) {
    $('form#add-to-listing-partial-form').submit();
    // return false;    //<---- Add this line
  }
});


                                        $('#add-to-listing-partial-form').on('submit', function (e) {
                                            var emailPattern = /(?![0-9])^[a-z0-9%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
                                            var num = /^(?!\s)[0-9 ]+/;
                                            // var mobilePattern = /^(?:(?:\+?[0-9]{2}|0|0[0-9]{2}|\+|\+(?:[.-]\s*)?)?(?:\(\s*([2-9][02-9]|[2-9][02-9]1|[2-9][02-9][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/;
                                            var mobilePattern = /^([0-9]{3})[\s]{0,1}([0-9]{4})([\s]{0,1})([0-9]{4})$/;
                                            var faxpattern = /^\+?[0-9]{6,}$/;
                                            var website=/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i;
                                            var postcode = /^(?![0-9])[a-z0-9]{2,5}\s[a-z0-9]{3}$/i;
                                            $('.error').remove();
                                            if ($("input[name='agentrefno']").val() == '') {
                                                $("input[name='agentrefno']").after('<span class="error">Please fill this field</span>');
                                                $("input[name='agentrefno']").focus();
                                            } else if ($("input[name='cName']").val() == '') {
                                                $("input[name='cName']").after('<span class="error">Please fill this field</span>');
                                                $("input[name='cName']").focus();
                                            } else if ($("input[name='defcur']").val() == '') {
                                                $("input[name='defcur']").after('<span class="error">Please fill this field</span>');
                                                $("input[name='defcur']").focus();
                                            }



                                            


                                             else if ($("input[name='invoicedue']").val() == '') {
                                                $("input[name='invoicedue']").after('<span class="error">Please fill this field</span>');
                                                $("input[name='invoicedue']").focus();
                                            } else if ($("select[name='AgentTypeId']").val() == '0') {
                                                $("select[name='AgentTypeId']").after('<span class="error">Please fill this field</span>');
                                                $("select[name='AgentTypeId']").focus();
                                            } 

                                            else if ($("input[name='Number']").val() == '') {
                                                $("input[name='Number']").after('<span class="error">Please fill this field</span>');
                                                $("input[name='Number']").focus();
                                            }
                                            else if(!num.test($("input[name='Number']").val()))  {
                                                $("input[name='Number']").after('<span class="error">Please fill valid number</span>');
                                                $("input[name='Number']").focus();
                                            }


                                            else if ($("input[name='AddressStreet']").val() == '') {
                                                $("input[name='AddressStreet']").after('<span class="error">Please fill this field</span>');
                                                $("input[name='AddressStreet']").focus();
                                            } 
                                            // else if ($("input[name='County']").val() == '') {
                                            //     $("input[name='County']").after('<span class="error">Please fill this field</span>');
                                            //     $("input[name='County']").focus();
                                            // }

                                            else if ($("input[name='AddressZip']").val() == '') {
                                                $("input[name='AddressZip']").after('<span class="error">Please fill this field</span>');
                                                $("input[name='AddressZip']").focus();
                                            } 
                                             else if (!postcode.test($("input[name='AddressZip']").val()) ) {
                                                $("input[name='AddressZip']").after('<span class="error">Please enter valid postcode</span>');
                                                $("input[name='AddressZip']").focus();
                                            } 

                                            else if ($("input[name='AddressCityName']").val() == '') {
                                                $("input[name='AddressCityName']").after('<span class="error">Please fill this field</span>');
                                                $("input[name='AddressCityName']").focus();
                                            } 

                                            // else if ($("input[name='AddressCounty']").val() == '') {
                                            //     $("input[name='AddressCounty']").after('<span class="error">Please fill this field</span>');
                                            //     $("input[name='AddressCounty']").focus();
                                            
                                            // }

                                            else if ($("select[name='title']").val() == '0') {
                                                $(".gndr").append('<div class="col-sm-10"><span class="error">Please fill this field</span></div>');
                                                $("select[name='title']").focus();

                                            }  else if ($("input[name='Forename']").val() == '') {
                                                $("input[name='Forename']").after('<span class="error">Please fill this field</span>');
                                                $("input[name='Forename']").focus();
                                            } else if ($("input[name='Surname']").val() == '') {
                                                $("input[name='Surname']").after('<span class="error">Please fill this field</span>');
                                                $("input[name='Surname']").focus();
                                            } else if ($("input[name='Email']").val() == '') {
                                                $("input[name='Email']").after('<span class="error">Please fill this field</span>');
                                                $("input[name='Email']").focus();
                                            } else if (!emailPattern.test($("input[name='Email']").val())) {
                                                $("input[name='Email']").after('<span class="error">Please enter valid email address</span>');
                                                $("input[name='Email']").focus();   
                                            } else if ($("input[name='Phone']").val() == '') {
                                                $("input[name='Phone']").after('<span class="error">Please fill this field</span>');
                                                $("input[name='Phone']").focus();
                                            } else if (!mobilePattern.test($("input[name='Phone']").val())) {
                                                $("input[name='Phone']").after('<span class="error">Please enter valid phone number</span>');
                                                $("input[name='Phone']").focus();
                                            }  else if(!faxpattern.test($("input[name='Fax']").val()) && $("input[name='Fax']").val().length > 0) {
                                              
                                                $("input[name='Fax']").after('<span class="error">Please enter valid fax number</span>');
                                                $("input[name='Fax']").focus();
                                            
                                            }else if ($("input[name='Website']").val() == '') {
                                                $("input[name='Website']").after('<span class="error">Please enter Website address</span>');
                                                $("input[name='Website']").focus();
                                            } 
                                              else if (!/^(http:\/\/|https:\/\/|ftp:\/\/|)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test($("input[name='Website']").val())) {

                                                 $("input[name='Website']").after('<span class="error">Please enter valid Website address</span>');
                                                $("input[name='Website']").focus();
                                                }
                                            else if ($("input[name='agentImage']").val() == '' && $('#imageedit').val() == '') {
                                                $("#preview").after('<span class="error">Please add image</span>');
                                                $("#preview").focus();
                                            } else if ($("#About").val() == '') {
                                                $("#About").after('<span class="error">Please fill this field</span>');
                                                $("#About").focus();
                                            } else if ($('#Latitude').val() == 0 || $('#Longitude').val() == 0) {
                                                $("input[name='AddressZip']").after('<span class="error">Please locate your address in the map</span>');
                                                $("input[name='AddressZip']").focus();
                                            } else {

var formData = new FormData(this);
           checkemail($("input[name='Email']").val());
            setTimeout(function () {
                if ($("#echeck").val() == 1 ) {
              $('.error').remove();
                    
                    $("input[name='Email']").after('<span class="error">email address already registered</span>');
                $("input[name='Email']").focus();
                } else {
$(".submit").attr("disabled","disabled");
                        // $('.submit').prop('disabled', true);
                  var sel = $('input[type=checkbox]:checked').map(function(_, el) {
                   return $(el).val();
                  }).get();
    
                 ajaxAgents(base_url, cont, formData );
                    
                }
            }, 500);






                                            


                                            }
                                            e.preventDefault();
                                        });


                                        function readURL(input) {

                                            if (input.files && input.files[0]) {
                                                var extension = input.files[0].name.substring(input.files[0].name.lastIndexOf('.'));
                                                console.log('here image ' + extension);
                                                if (extension == '.jpeg' || extension == '.png' || extension == '.jpg') {
                                                    var reader = new FileReader();

                                                    reader.onload = function (e) {
                                                        $('#preview').attr('src', e.target.result);
                                                    }

                                                    reader.readAsDataURL(input.files[0]);
                                                } else {
                                                    $("#preview").after('<span class="error">Please choose valid file format</span>');
                                                    $("#preview").focus();
                                                }
                                            }
                                        }

                                        $("#Photo_Upload_Control").change(function () {
                                            $('.error').remove();
                                            readURL(this);

                                        });
//                $('#loadmap').on('click', function () {
//                    $('#myModal').modal();
//                    initialize();
//                });
                                        function isNumber(evt) {
                                            evt = (evt) ? evt : window.event;
                                            var charCode = (evt.which) ? evt.which : evt.keyCode;
                                            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                                                return false;
                                            }
                                            return true;
                                        }
                                        function keyPress(e) {
                                            var key, x = e || window.event;
                                            key = (x.keyCode || x.which);
                                            if (!(key == 32 || key == 9 || (key>=97 && key<=122)|| (key>=65 && key<=90))) {
                                                return false;
                                            }
                                            return true;
                                        }

//                                         var geocoder = new google.maps.Geocoder();
//                                         function getcode() {
                                            
//                                             var address = $('#pac-input').val();
//                                             console.log('hre3'+address);
//                                             geocoder.geocode({address: address}, function (results, status) {
//                                                 console.log('hre1'+results);
//                                                 if (status == google.maps.GeocoderStatus.OK)
//                                                 {
                                                    
//                                                     map.setCenter(results[0].geometry.location);
//                                                     deleteMarkers();
//                                                     var marker = new google.maps.Marker({
//                                                         position: results[0].geometry.location,
//                                                         title: 'Point A',
//                                                         map: map,
//                                                         draggable: true
//                                                     });
//                                                      markers.push(marker);
//                                                     geocodePosition(results[0].geometry.location)
//                                                     updateMarkerPosition(marker.getPosition());
//                                                     geocodePosition(marker.getPosition());

//                                                     google.maps.event.addListener(marker, 'dragend', function () {
// //                                                        updateMarkerStatus('Drag ended');
//                                                         geocodePosition(marker.getPosition());
//                                                         updateMarkerPosition(marker.getPosition());
//                                                     });


//                                                 }
//                                                 else
//                                                 {
//                                                     alert("Geocode was not successful for the following reason: " + status);
//                                                 }
//                                             });

//                                         }
                                       
//                                         function updateMarkerPosition(latLng) {
                                                
//                                             document.getElementById("Latitude").value = latLng.lat();
//                                             document.getElementById("Longitude").value = latLng.lng();
                                           
//                                         }

                                        
//                                         function initialize() {
//                                             var latLng = new google.maps.LatLng(51.5072, 0.1275);
//                                             map = new google.maps.Map(document.getElementById('map'), {
//                                                 zoom: 8,
//                                                 center: latLng,
//                                                 mapTypeId: google.maps.MapTypeId.ROADMAP
//                                             });
//                                             var marker = new google.maps.Marker({
//                                                 position: latLng,
//                                                 title: 'Point A',
//                                                 map: map,
//                                                 draggable: true
//                                             });
//                                          markers.push(marker);

//                                             // Update current position info.
//                                             updateMarkerPosition(latLng);
// //                                            geocodePosition(latLng);
//                                             var input = document.getElementById('pac-input');
//                                             var autocomplete = new google.maps.places.Autocomplete(input, {
//                                                 types: ["geocode"]
//                                             });

//                                             autocomplete.bindTo('bounds', map);
//                                             // Add dragging event listeners.
//                                             google.maps.event.addListener(marker, 'dragstart', function () {
// //                                                updateMarkerAddress('Dragging...');
//                                                 updateMarkerPosition(marker.getPosition());
//                                             });

//                                             google.maps.event.addListener(marker, 'drag', function () {
// //                                                updateMarkerStatus('Dragging...');
//                                                 updateMarkerPosition(marker.getPosition());
//                                             });

//                                             google.maps.event.addListener(marker, 'dragend', function () {
// //                                                updateMarkerStatus('Drag ended');
//                                                 geocodePosition(marker.getPosition());
//                                                 updateMarkerPosition(marker.getPosition());
//                                             });
//                                             google.maps.event.addListener(autocomplete, 'place_changed', function () {
                                               
//                                                 var place = autocomplete.getPlace();
//                                                 if (place.geometry.viewport) {
//                                                     map.fitBounds(place.geometry.viewport);
//                                                 } else {
//                                                     map.setCenter(place.geometry.location);
//                                                     map.setZoom(17);
//                                                 }


//                                                 moveMarker(place.name, place.geometry.location);
//                                                 function moveMarker(placeName, latlng) {
// //                                                    marker.setIcon(image);
                                                    
//                                                     marker.setPosition(latlng);
                                                    
// //                                                    infowindow.setContent(placeName);
// //                                                    infowindow.open(map, marker);
//                                                 }
//                                             });
//                                         }
// // Sets the map on all markers in the array.
function setMapOnAll(map) {
  for (var i = 0; i < markers.length; i++) {
    markers[i].setMap(map);
  }
}

// Removes the markers from the map, but keeps them in the array.
function clearMarkers() {
  setMapOnAll(null);
}


// Deletes all markers in the array by removing references to them.
function deleteMarkers() {
  clearMarkers();
  markers = [];
}
    
                                       
                                        $("#pac-input").focusin(function () {
                                            $(document).keypress(function (e) {
                                                if (e.which == 13) {
//                    infowindow.close();       
                                                                    deleteMarkers();
                                                    var firstResult = $(".pac-container .pac-item:first").text();

                                                    var geocoder = new google.maps.Geocoder();
                                                    geocoder.geocode({"address": firstResult}, function (results, status) {
                                                        if (status == google.maps.GeocoderStatus.OK) {
                                                            var lat = results[0].geometry.location.lat(),
                                                                    lng = results[0].geometry.location.lng(),
                                                                    placeName = results[0].address_components[0].long_name,
                                                                    latlng = new google.maps.LatLng(lat, lng);
                                                            document.getElementById("Latitude").value = lat;
                                                            document.getElementById("Longitude").value = lng;

                                                            moveMarker(placeName, latlng);
                                                            $("#pac-input").val(firstResult);
                                                            
                                                        }
                                                    });
                                                }
                                            });
                                        });
                                        $("input[name='agentrefno']").on('keyup', function () {
                                            // $('.error').remove();
                                            if ( ($("input[name='id']").val() == 0) && ($('#agentrefno').val() == '') )  {
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>agent/agent/checkref",
                                                    data: "ref=" + $("input[name='agentrefno']").val(),
                                                    dataType: 'html',
                                                    success: function (data) {
                                                        if (data == 1) {
                                                            $("input[name='agentrefno']").after('<span class="error">Refernce number already exist</span>');
                                                            $('.submit').prop('disabled', true);
                                                        } else {
                                                            $("input[name='agentrefno']").after('<span class="error"></span>');
                                                            $('.submit').prop('disabled', false);
                                                        }
                                                    }
                                                });
                                            }
                                        });




  function checkemail(email) {

        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>/agent/agent/checkemail",
            data: "email=" + email,
            dataType: "html",
            success: function (data) {
                var id = $("input[name='id']").val();

                if ((data == 1)&&(id == 0)) {
                  $("#echeck").val('1');
                }
                else { $("#echeck").val('0');
                 }
            }
        });

    }











function map() {
    var latp = document.getElementById("Latitude").value;
    var longp = document.getElementById("Longitude").value;
    if (latp == 0) {
        var lat = 51.573094;
        var lng = -0.371677;
    } else {
        var lat = latp;
        var lng = longp;
    }
    var latlng = new google.maps.LatLng(lat, lng),
            image = 'http://www.google.com/intl/en_us/mapfiles/ms/micons/blue-dot.png';
    var mapOptions = {
        center: new google.maps.LatLng(lat, lng),
        zoom: 15,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    },
    map = new google.maps.Map(document.getElementById('map'), mapOptions),
            marker = new google.maps.Marker({
                position: latlng,
                map: map,
                icon: image,
                draggable: true
            });
    var input = document.getElementById('Address_Zip');
    
    var geocoder = new google.maps.Geocoder();
    google.maps.event.addListener(marker, 'dragend', function () {
        var latLng = marker.getPosition();
        document.getElementById("Latitude").value = latLng.lat();
        document.getElementById("Longitude").value = latLng.lng();
        geocodePosition(marker.getPosition());
    });
    function geocodePosition(pos) {
        geocoder.geocode({
            latLng: pos
        }, function (responses) {
            if (responses && responses.length > 0) {

                console.log(responses[0].formatted_address);
            } else {
//              updateMarkerAddress('Cannot determine address at this location.');
            }
        });
    }
    $("#Address_Zip,#Address_CityName,#County").change(function (e) {
        console.log("Hello");
        //$(document).keypress(function (e) {
            // if (e.which == 13) {
                // infowindow.close();
                var firstResult = "";
                if($("#Address_Zip").val() != "" && $("#Address_CityName").val() != "" && $("#County").val() != ""){
                    console.log("harrow");
                    firstResult = $("#Address_Zip").val() + ',' + $("#Address_CityName").val() + ',' + $("#County").val();
                }
                console.log(firstResult);
                geocoder.geocode({"address": firstResult}, function (results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        var lat = results[0].geometry.location.lat(),
                                lng = results[0].geometry.location.lng(),
                                placeName = results[0].address_components[0].long_name,
                                latlng = new google.maps.LatLng(lat, lng);
                        document.getElementById("Latitude").value = lat;
                        document.getElementById("Longitude").value = lng;
                        moveMarker(placeName, latlng);
                        $("#pac-input").val(firstResult);
                        map.setCenter(results[0].geometry.location);
                        map.setZoom(17);
                    }
                });
            // }
        //});
    });
    function moveMarker(placeName, latlng) {
        marker.setIcon(image);
        marker.setPosition(latlng);
        // infowindow.setContent(placeName);
        // infowindow.open(map, marker);
    }
}
map();















function datamail($val) {

  var datamail = $val;
  alert(datamail);
}

   $(document).ready(function () {

// if ($("input[name='id']").val() != 0) {
// 	$('#Email').prop( "readonly", true );
// }
                                       

$('.input').keypress(function(){

 var emailPattern = /(?![0-9])^[a-z0-9%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;

                                            var mobilePattern = /^(?:(?:\+?[0-9]{2}|0|0[0-9]{2}|\+|\+[0-9]{2}\s*(?:[.-]\s*)?)?(?:\(\s*([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9])\s*\)|([2-9]1[02-9]|[2-9][02-8]1|[2-9][02-8][02-9]))\s*(?:[.-]\s*)?)?([2-9]1[02-9]|[2-9][02-9]1|[2-9][02-9]{2})\s*(?:[.-]\s*)?([0-9]{4})(?:\s*(?:#|x\.?|ext\.?|extension)\s*(\d+))?$/;
                                            var faxpattern = /^\+?[0-9]{6,}$/;
                                            var website=/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i;
                                            


if ( $('#agentrefno').val() != ''){
    $(this).next(".error").text('');
    
} 



});
 

                                           $("#invoicedue").keypress(function (e) {
                                         if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                                         return false;
                                          }
                                          });
                                            $('#workstart').timepicker({
                                                showPeriod: true,
                                                showLeadingZero: true,
                                            });

                                            $('#workend').timepicker({
                                                showPeriod: true,
                                                showLeadingZero: true,
                                            });
                                        });
</script>
