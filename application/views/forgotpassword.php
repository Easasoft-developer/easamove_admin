<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
﻿<!DOCTYPE html>

<html>
    <head>
        <meta name="viewport" content="width=device-width" />
        <title>Forgot Password</title>
        <link href="<?php echo base_url(); ?>/css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>/css/bootstrap-custom.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>css/responsive.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
    </head>
    <body class="forgot">
        <div class="row">

        <div class="" >
            <div class="col-md-offset-4 col-md-3 forgot"  style="">
                <?php if ($this->session->flashdata('forgot')) { ?>
                    <div class="alert alert-success"> <?= $this->session->flashdata('forgot') ?> </div>
                <?php } ?>
                <form role="form" method="post" action="<?php echo base_url('Account/forgotpassword'); ?>">
                    <div class="form-group">
                        <h2>Forgot Password</h2>
                    </div>
                    <div class="form-group">
                        <label for="UserName">Email address</label>
                        <input type="text" placeholder="Enter email" name="UserEmail"  id="UserEmail" class="form-control">
                    </div>
                    
                    
                    <button class="btn btn-warning btn-sm" type="submit">Submit</button>
                    <a class="btn btn-warning btn-sm" href="<?php echo base_url(); ?>">Sign-in</a>
                </form>
            </div>
</div>

        </div>

    </body>
</html>
