<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in th */
//print_r($data);
$actualurl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
?>



<nav class="subnav" role="navigation">
        <div class="container">

        <div class="row">
          <div class="col-sm-12">
            <ul class="list-inline">
               <li <?php echo (base_url('adminUser')==$actualurl) ? 'class=active':''; ?>><a href="<?php echo base_url();?>adminUser">Manage User</a></li>
              <li <?php echo (base_url('account/myprofile')==$actualurl) ? 'class=active':''.base_url('myprofile') ; ?>><a href="<?php echo base_url();?>account/myprofile">My Profile</a></li>
             <li <?php echo (base_url('changePassword')==$actualurl)?'class=active':''; ?>><a href="<?php echo base_url();?>account/changePassword">Change Password</a></li>
          </ul>
          </div>
        </div>       
        
          
        </div>
      </nav>


<section id="main-content" class="main-content">
        <section class="surround">

        <div class="container-fluid property_listing master">
          <div class="row">
              <div class="col-xs-12">
                <div class="panel">
                  <div class="panel-heading">
                    


                    <div class="row">
                      <div class="col-sm-12">
                        <h3>Profile</h3>
                      </div>
                    </div>

                    <form method="post" id="admin-user"> 
                    <div class="row">



                <input type="hidden" name="id" value="<?php echo $data[0]->admin_id; ?>"/>
                <input type="hidden" id="echeck"/>
                <input type="hidden" value="<?php echo $data[0]->admin_access; ?>" name="permission" >

                      <div class="col-sm-12">
                        <div class="form-horizontal">
                     

             

                          <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="name">Name</label>
                            <div class="col-sm-4 col-md-3">
                    <input type="text" id="name" name="name" onkeypress="return onlyAlphabets(event,this);" class="form-control" value="<?php echo $data[0]->admin_name; ?>"/>
                            </div>
                          </div>

                    

  <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="Email">Email Id</label>
                            <div class="col-sm-4 col-md-3">
                     <input type="email" id="Email" name="Email" class="form-control" <?php echo ($data[0]->admin_access == 1) ? '' : 'readonly' ;?> value="<?php echo $data[0]->admin_email; ?>"/>
                            </div>
                          </div>



<div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="Email">Role</label>
                            <div class="col-sm-4 col-md-3">
                     <input type="text" id="Email" name="role" class="form-control" <?php echo ($data[0]->admin_access == 1) ? '' : 'readonly' ;?> value="<?php echo $data[0]->admin_role; ?>"/>
                            </div>
                          </div>
                        


                          <div class="form-group">
                            <div class="col-md-offset-5 col-sm-offset-3 col-sm-3 save-cancel">
                              <button type="button" class="btn btn-default submit">Save</button>
                              <!-- <button type="button" class="btn btn-default btn-danger reset">Cancel</button> -->
                            </div>                            
                          </div>


                        </div>                        
                      </div>


                    </div>
                    </form>


               
                  
                  </div>
                 
                </div>
              </div>    
          </div>
        </div>
          
        </section>        
      </section>




<script>
    var base_url = '<?php echo base_url(); ?>';
    var cont = 'AdminUser';

    $(document).ready(function () {


      $("input").on("keypress",function(e) { 

        if(e.which == 13) {
e.preventDefault();
$(".submit").click();
      } else {
$('.error').remove();
        
      }

      });

    });
    $('.submit').on('click', function () {
        $('.error').remove();
        var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;

//        alert(echeck);

        if ($("input[name='name']").val() == '') {
            $("input[name='name']").after('<span class="error">Please fill this field</span>');
            $("input[name='name']").focus();
        } else if($("input[name='Email']").val() == ''){
            $("input[name='Email']").after('<span class="error">Please fill this field</span>');
            $("input[name='Email']").focus();
        } else if(!emailPattern.test($("input[name='Email']").val())){
            $("input[name='Email']").after('<span class="error">Please fill this field</span>');
            $("input[name='Email']").focus();
        } else if($("input[name='role']").val() == ''){
            $("input[name='role']").after('<span class="error">Please fill this field</span>');
            $("input[name='role']").focus();
        } else {
checkemail($("input[name='Email']").val());

          setTimeout(function() {

if ($("#echeck").val() == 1) {

$('.error').remove();
                    
                    $("input[name='Email']").after('<span class="error">email address already registered</span>');
                $("input[name='Email']").focus();

 } 
  else {

 var datas = $('#admin-user').serialize();
                    ajaxadminUsersUpdate(base_url, cont, datas);
                    // window.location.href='<?php echo base_url('account/myprofile');?>';


  }


           


           },2000);

                    

        }
    });




      function checkemail(email) {

        $.ajax({
            type: "POST",
            url: base_url + cont + "/checkemail",
            data: "email=" + email + "&id=" + $("input[name='id']").val(),
            dataType: "html",
            success: function (data) {
                var id = $("input[name='id']").val();

                if ((data == 1)&&(id != 0)) {
                  $('.error').remove();
                    $('#echeck').val(data);
                }
                else { 
                  $('.error').remove();
                  $('#echeck').val(''); }
            }
        });

    }
   


</script>
