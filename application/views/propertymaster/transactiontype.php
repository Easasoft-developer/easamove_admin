<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>




<section id="main-content" class="main-content">
        <section class="surround">

        <div class="container-fluid property_listing master">
          <div class="row">
              <div class="col-xs-12">
                <div class="panel">
                  <div class="panel-heading">
                    


                    <div class="row">
                      <div class="col-sm-12">
                        <h3>Property Transaction</h3>
                      </div>
                    </div>
                    <input type="hidden" id="checkTransaction">
<?php if ($access != 3) { ?>

                    <form method="post" id="transaction-form"> 
                    <div class="row">

                      <div class="col-sm-12">
                        <div class="form-horizontal">
                     
             
             <input type="hidden" name="id" value="0"/>
                   <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="Type">Transaction Type<span style="color: #F00"> *</span></label>
                            <div class="col-sm-4 col-md-3">
                        <input  onkeypress="return onlyAlphabets(event,this);"type="text" id="Type" name="Type" maxlength="30"  class="form-control input"/>
                            </div>
                          </div>

           
                        
                <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="for">Transaction For<span style="color: #F00"> *</span></label>
                            <div class="col-sm-4 col-md-3">
                       <!-- <textarea id="Desc" name="Desc" class="form-control"></textarea> -->
                       <select id="for" name="for" class="form-control">
                                <option value="1">Sale</option>
                                <option value="2">Rent</option>
                            </select>
                            </div>
                          </div>



                <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="Desc">Transaction Description</label>
                            <div class="col-sm-4 col-md-3">
                        <textarea maxlength="60" id="Desc" name="Desc" class="form-control"></textarea>
                            </div>
                          </div>




                          <div class="form-group">
                            <div class="col-md-offset-5 col-sm-offset-3 col-sm-3 save-cancel">
                              <button type="button" class="btn btn-default submit">Save</button>
                              <button type="button" class="btn btn-default btn-danger reset">Cancel</button>
                            </div>                            
                          </div>


                        </div>                        
                      </div>


                    </div>
                    </form>

<?php } ?>
               
                  
                  </div>

                  <div class="">
                    <div class="table-responsive">


<div class="preventdel" style="display: none;"></div>
 <table id="datatb" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Transaction Type</th>
                                 <th>Transaction For</th>
                                <th>Transaction Description</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>

                        <tbody class="data-append">
                        <tr>
                            <td colspan="5" class="tac" style="height: 100px; vertical-align: middle;">Loading
                            </td>
                        </tr>
                    </tbody>

                    </table>


                    </div>
                  </div>

                  
                </div>
              </div>    
          </div>
        </div>
          
        </section>        
      </section>






<script>
    var base_url = '<?php echo base_url(); ?>';
    var cont = 'TransactionType';


    $(document).ready(function () {

 $(".reset").click(function(){
$('.error').remove();
    });
        getTransaction(base_url, cont);


    });



$('.input').on('keypress',function(e)  {

 if (e.which != 13  ) {

$('.error').remove();

} else {
e.preventDefault();
submit();
  
}




});
    $('.submit').on('click', function () {
        submit();

    });



function submit() {
    if ($("input[name='Type']").val() == '') {
            $('.error').remove();
            $("input[name='Type']").after('<span class="error">Please fill this field</span>');
        $("input[name='Type']").focus();
        } else if ($("input[name='Desc']").val() == '') {
            $('.error').remove();
            $("input[name='Desc']").after('<span class="error">Please fill this field</span>');
            $("input[name='Desc']").focus();
        } else if(checkTransaction() ==1){
$('.error').remove();
$("input[name='Type']").after('<span class="error">Transaction type already exist</span>');
$("input[name='Type']").focus();

} else {


  $('.error').remove();
            var datas = $('#transaction-form').serialize();
            ajaxcallsproperty(base_url, cont, datas);
            getTransaction(base_url, cont);
            $("input[name='id']").val('0');
            $('#transaction-form')[0].reset();

// }

// },2000);


          






        }
}








    function edit(id) {
//        console.log($('#'+id).find("td:eq(1)").text());
        $("input[name='id']").val(id);
        $("input[name='Type']").val($('#' + id).find("td:eq(1)").text());
        
        document.getElementById("Desc").value = $('#' + id).find("td:eq(3)").text();
        $(window).scrollTop(0);
         $('#for option').filter(function() { 
    return ($(this).text() == $('#' + id).find("td:eq(2)").text()); //To select Blue
}).prop('selected', true);
    }

function df() {
    $(window).scrollTop(0);
 $('.preventdel').text('This is in use, cannot delete');
  $('.preventdel').removeAttr('style');
        setTimeout(function() { 
$('.preventdel').css("display","none");
        },3000);
    }

    function deleteTransaction(id) {
        // if (confirm('Are you sure you want to Delete?')) {
              $('#confirm').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delete', function () {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>propertymaster/" + cont + "/deleteTransaction",
                data: "id=" + id,
                success: function (data) {
                    if (data) {
                            $('#message').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delet', function () {
                        window.location.reload();
                        $('#' + id).remove();
                    });
                        // alert('Deletes successfully');
                    }
                }
            });
        });
    }
    

    function checkTransaction() {
        

        var returnval;
            $.ajax({
                type: "POST",
                async: false,
                url: "<?php echo base_url(); ?>propertymaster/" + cont + "/checkTransaction",
                data: "type=" + $("input[name='Type']").val() + "&desc=" + $('#for').val() + "&id=" + $('input[name="id"]').val(),
                dataType: 'html',
                success: function (data) {

                    if (data == 1) {
                        returnval = data;
                    } else {

                        returnval = data;
                    }
                }
            });
        

return returnval;
    }
</script>
