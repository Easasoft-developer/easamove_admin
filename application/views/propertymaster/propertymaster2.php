<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 $actualurl="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

?>
<div class="row" id="property-masters-page-grouptwo">
    <div class="row title-container">
        <div class="col-md-12">
            <h2 class="pl20 mt10 fwb mb20">Property Masters</h2>
        </div>
    </div>
    <div class="tab-container-x x1 mb20 pl5 pr5 PR -T34">
        <ul class="tabs">
            
        </ul>
        <div class="tab-content1 p0">
            <div class="tab-pane mt20 active">
                <div id="natureOfError-tab"></div>
            </div>
            <div class="tab-pane mt20 active">
                <div id="propertyRelationship-tab"></div>
            </div>
            <div class="tab-pane mt20 active">
                <div id="feedbackCategory-tab"></div>
            </div>
            <div class="tab-pane mt20 active">
                <div id="feedbackSubCategory-tab"></div>
            </div>
        </div>
    </div>
</div>