<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="col-md-9">
     <h3 class="masters_title">Feedback Category</h3>
<form method="post" id="feedbackCategory-form" >
    <div class="">
        <div class="row mb10 mt10">
            <div class="col-md-12 pl5 pr0">
                 <input type="hidden" name="id" value="0"/>
                <div class="form-group tar clearfix mb5">
                    <label class="col-sm-4 control-label" for="Type">Feedback Category Type</label>
                    <div class="col-sm-3">
                        <input type="text" id="Type" name="Type" class="form-control" />
                    </div>
                </div>
                 <div class="form-group tar clearfix mb5">
                    <label class="col-sm-4 control-label" for="Desc">Feedback Category Description</label>
                    <div class="col-sm-3">
                        <textarea id="Desc" name="Desc" class="form-control"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 mt10 tar">
                        <button class="btn btn-danger"  type="button"><i class="fa fa-times pr5"></i>Cancel</button>
                        <button class="btn btn-primary submit"  type="button"><i class="fa fa-save pr5"></i>Save</button>
                    </div>                    
                </div>
               
            </div>
        </div>        
        <div class="row">
            <div class="col-md-12">
                <table id="datatb" class="table table-bordered thumbnail-style mt10">
                    <thead>
                        <tr>
                            <th>#</th>                            
                            <th>Feedback Category Type</th>
                            <th>Feedback Category Description</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    
                    <!-- ko if:(Items().length===0) -->
                    <tbody class="data-append">
                        <tr>
                            <td colspan="4" class="tac" style="height: 100px; vertical-align: middle;">No FeedbackCategory found ! Please refine your filter or <a href="#">Click here to cancel</a>
                            </td>
                        </tr>
                    </tbody>
                    
                   
                </table>
            </div>
        </div>
    </div>
</form>
</div>
<script>
    var base_url = '<?php echo base_url(); ?>';
    var cont = 'FeedbackCategory';

    
    $(document).ready(function () {
        
         
        getFeedbackcategory(base_url, cont);


    });
    $('.submit').on('click', function () {
         $('.error').remove();
        if ($("input[name='Type']").val() == '') {
            $("input[name='Type']").after('<span class="error">Please fill this field</span>');
        } else if ($("input[name='Desc']").val() == '') {
            $("input[name='Desc']").after('<span class="error">Please fill this field</span>');
        } else{
        var datas = $('#feedbackCategory-form').serialize();
        ajaxcallsproperty(base_url, cont, datas);
        getFeedbackcategory(base_url, cont);
        $("input[name='id']").val('0');
        $('#feedbackCategory-form')[0].reset();
    }
    });
    function edit(id) {
//        console.log($('#'+id).find("td:eq(1)").text());
        $("input[name='id']").val(id);
        $("input[name='Type']").val($('#' + id).find("td:eq(1)").text());
        document.getElementById("Desc").value =$('#' + id).find("td:eq(2)").text();
    }
    function deleteFeedbackcategory(id) {
if (confirm('Are you sure you want to Delete?')) { 
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>propertymaster/" + cont + "/deleteFeedbackcategory",
            data: "id=" + id,
            success: function (data) {
                if (data) {
                    $('#' + id).remove();
                }
            }
        });
        }
    }
</script>