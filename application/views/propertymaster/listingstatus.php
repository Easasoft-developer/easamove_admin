<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>

<section id="main-content" class="main-content">
        <section class="surround">

        <div class="container-fluid property_listing master">
          <div class="row">
              <div class="col-xs-12">
                <div class="panel">
                  <div class="panel-heading">
                    


                    <div class="row">
                      <div class="col-sm-12">
                        <h3>Listing Status</h3>
                      </div>
                    </div>
<input type="hidden" id="checkListingstatus">
<?php if ($access != 3) { ?>
                    <form method="post" id="listingStatus-form"> 
                    <div class="row">

                      <div class="col-sm-12">
                        <div class="form-horizontal">
                     
             
             <input type="hidden" name="id" value="0"/>
                   <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="Status">Status<span style="color: #F00"> *</span></label>
                            <div class="col-sm-4 col-md-3">
                     <input tabindex="1" type="text" id="Status" name="Status" maxlength="30" class="form-control input" />
                            </div>
                          </div>

           
                        <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " ></label>
                            <div class="col-sm-4 col-md-3">
                       <label><input tabindex="2" type="radio" id="IsForSale" name="IsForSale" value="1" checked> Sale</label>&nbsp;
                        
                            <label><input tabindex="3" id="IsForRent" type="radio" name="IsForSale" value="0"> Rent</label>
                            </div>
                          </div>


                          <div class="form-group">
                            <div class="col-md-offset-5 col-sm-offset-3 col-sm-3 save-cancel">
                              <button tabindex="4" type="button" class="btn btn-default submit">Save</button>
                              <button tabindex="5" type="button" class="btn btn-default btn-danger reset">Cancel</button>
                            </div>                            
                          </div>


                        </div>                        
                      </div>


                    </div>
                    </form>


               <?php } ?>
                  
                  </div>

                  <div class="">
                    <div class="table-responsive">
<div class="preventdel" style="display: none;"></div>
 <table id="datatb" class="table table-striped table-hover">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Status</th>
                                <th>Listing for</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>

                        <tbody class="data-append">
                            <tr>
                                <td colspan="5" class="tac" style="height: 100px; vertical-align: middle;">Loading
                                </td>
                            </tr>
                        </tbody>

                    </table>



                    </div>
                  </div>

                  
                </div>
              </div>    
          </div>
        </div>
          
        </section>        
      </section>







<script>
    var base_url = '<?php echo base_url(); ?>';
    var cont = 'ListingStatus';


    $(document).ready(function () {
 $(".reset").click(function(){
$('.error').remove();
 $('.submit').prop('disabled', false);
    });


$("#Status").keypress(function (e) {
  if (e.which != 8 && e.which != 0 && !(e.which < 48 || e.which > 57)) {
   return false;
    }
     });



        getListingstatus(base_url, cont);


    });




$('.input').on('keypress',function(e)  {

 if (e.which != 13  ) {

$('.error').remove();

} else {
e.preventDefault();
submit();
  
}




});




    $('.submit').on('click', function () { submit();   });



function submit() {
  
        if ($("input[name='Status']").val() == '') {
            $('.error').remove();
            $("input[name='Status']").after('<span class="error">Please fill this field</span>');
            $("input[name='Status']").focus();
        } else if(checkListingstatus() ==1){

$('.error').remove();
   $("input[name='Status']").after('<span class="error">Listing status already exist</span>');
   $("input[name='Status']").focus();
} else {
// setTimeout(function() { 



// if ($('#checkListingstatus').val() == 1) { 
// $('.error').remove();
//    $("input[name='Status']").after('<span class="error">Listing status already exist</span>');

// } else {


            $('.error').remove();
            var datas = $('#listingStatus-form').serialize();
            ajaxcallsproperty(base_url, cont, datas);
            getListingstatus(base_url, cont);
            $("input[name='id']").val('0');
            $('#listingStatus-form')[0].reset();
// }




// },2000);




        }


}


    function edit(id) {
//        console.log($('#'+id).find("td:eq(1)").text());
        $("input[name='id']").val(id);
        $("input[name='Status']").val($('#' + id).find("td:eq(1)").text());
        $(window).scrollTop(0);
        var temp = $('#' + id).find("td:eq(2)").text();
        if (temp == 'Sale') {
            $('#IsForSale').prop('checked', true);
        } else if (temp == 'Rent') {
            $('#IsForRent').prop('checked', true);
        }


    }

function df() {
  $(window).scrollTop(0);
 $('.preventdel').text('This is in use, cannot delete');
  $('.preventdel').removeAttr('style');
        setTimeout(function() { 
$('.preventdel').css("display","none");
        },3000);
    }

    function deleteListingstatus(id) {
        // if (confirm('Are you sure you want to Delete?')) {
                 $('#confirm').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delete', function () {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>propertymaster/" + cont + "/deleteListingstatus",
                data: "id=" + id,
                success: function (data) {
                    if (data) {
                          $('#message').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delet', function () {
                        window.location.reload();
                        $('#' + id).remove();
                      });
                        // alert('Deletes successfully');
                    }
                }
            });
        });
    }
    // $("input[name='Status']").on('keyup', function () {
        // $('.error').remove();

var desc = 1;
$("input[name='IsForSale']").on('change',function(){

 var favorite = [];
            $.each($("input[name='IsForSale']:checked"), function(){            
                favorite.push($(this).val());
            });
             desc = favorite.join(", ");


});


function checkListingstatus() {
        // if ($("input[name='id']").val() == 0 ) {
          var returnval;
            $.ajax({
                type: "POST",
                async: false,
                url: "<?php echo base_url(); ?>propertymaster/" + cont + "/checkListingstatus",
                data: "status=" + $("input[name='Status']").val()  + "&desc=" + desc + "&id=" + $("input[name='id']").val(),
                dataType: 'html',
                success: function (data) {
                    if (data == 1) {
                      returnval = data;
                    } else {
                 returnval = data;
                    }

                }

            });
        // }
return returnval;
}


    // });
</script>