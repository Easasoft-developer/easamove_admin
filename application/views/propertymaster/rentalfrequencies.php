<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<div class="col-md-9">
     <h3 class="masters_title">Rental Frequency</h3>
<form method="post" id="rentalFrequency-form" >
    <div class="">
        <div class="row mb10 mt10">
            <div class="col-md-12 pl5 pr0">
                  <input type="hidden" name="id" value="0"/>
                <div class="form-group tar clearfix mb5">
                    <label class="col-sm-4 control-label" for="FrequencyType">Frequency Type</label>
                    <div class="col-sm-3">
                      
                        <input type="text" id="FrequencyType" name="FrequencyType" class="form-control"/>
                    </div>
                </div>
                  <div class="form-group tar clearfix mb5">
                    <label class="col-sm-4 control-label" for="FrequencyDesc">Frequency Description</label>
                    <div class="col-sm-3">
                      
                        <textarea id="FrequencyDesc" name="FrequencyDesc" class="form-control"></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 mt10 tar">
                        <button class="btn btn-danger" type="button"><i class="fa fa-times pr5"></i>Cancel</button>
                        <button class="btn btn-primary submit" type="button"><i class="fa fa-save pr5"></i>Save</button>
                    </div>                    
                </div>
                
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table id="datatb" class="table table-bordered thumbnail-style mt10">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Frequency Type</th>
                            <th>Frequency Description</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    
                  <tbody class="data-append">
                        <tr>
                            <td colspan="5" class="tac" style="height: 100px; vertical-align: middle;">Loading
                            </td>
                        </tr>
                    </tbody>
                    
                </table>
            </div>
        </div>
    </div>
</form>
</div>
<script>
    var base_url = '<?php echo base_url(); ?>';
    var cont = 'RentalFrequencies';

    
    $(document).ready(function () {
        
         
        getRentalfrequencies(base_url, cont);


    });
    $('.submit').on('click', function () {
         $('.error').remove();
        if ($("input[name='FrequencyType']").val() == '') {
            $("input[name='FrequencyType']").after('<span class="error">Please fill this field</span>');
        } else if ($("input[name='FrequencyDesc']").val() == '') {
            $("input[name='FrequencyDesc']").after('<span class="error">Please fill this field</span>');
        } else{
        var datas = $('#rentalFrequency-form').serialize();
        ajaxcallsproperty(base_url, cont, datas);
        getRentalfrequencies(base_url, cont);
        $("input[name='id']").val('0');
        $('#rentalFrequency-form')[0].reset();
    }
    });
    function edit(id) {
//        console.log($('#'+id).find("td:eq(1)").text());
        $("input[name='id']").val(id);
        $("input[name='FrequencyType']").val($('#' + id).find("td:eq(1)").text());
        document.getElementById("FrequencyDesc").value =$('#' + id).find("td:eq(2)").text();
    }
    function deleteRentalfrequencies(id) {
if (confirm('Are you sure you want to Delete?')) { 
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>propertymaster/" + cont + "/deleteRentalfrequencies",
            data: "id=" + id,
            success: function (data) {
                if (data) {
                    $('#' + id).remove();
                    $("input[name='id']").val('0');
                }
            }
        });
        }
    }
</script>