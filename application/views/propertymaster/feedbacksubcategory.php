<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<div class="col-md-9">
     <h3 class="masters_title">Feedback Sub Category</h3>
<form method="post" id="feedbackSubCategory-form">
    <div class="">
        <div class="row mb10 mt10">
            <div class="col-md-12 pl5 pr0">
                <input type="hidden" name="id" value="0"/>
                <div class="form-group tar clearfix mb5">
                    <label class="col-sm-4 control-label" for="Code">FeedbackCategory</label>
                    <div class="col-sm-3">
                        <select class="form-control" name="FeedbackCategoryId" id="FeedbackCategoryId" >
                            <option value="0">- Select -</option>
                            <?php
                            if (isset($feedbackcate)) {
                                foreach ($feedbackcate as $fb) {
                                    echo '<option value="' . $fb->feedbackcategory_id . '">' . $fb->feedbackcategory_name . '</option>';
                                }
                            }
                            ?>

                        </select>
                    </div>
                </div>
                <div class="form-group tar clearfix mb5">
                    <label class="col-sm-4 control-label" for="Type">Type</label>
                    <div class="col-sm-3">
                        <input type="text" id="Type" name="Type" class="form-control" />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 mt10 tar">
                        <button class="btn btn-danger"  type="button"><i class="fa fa-times pr5"></i>Cancel</button>
                        <button class="btn btn-primary submit" type="button"><i class="fa fa-save pr5"></i>Save</button>
                    </div>                    
                </div>
                
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table id="datatb" class="table table-bordered thumbnail-style mt10">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Type</th>
                            <th>FeedbackCategory</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                   
                    <!-- ko if:(Items().length===0) -->
                    <tbody class="data-append">
                        <tr>
                            <td colspan="5" class="tac" style="height: 100px; vertical-align: middle;">No FeedbackCategoryType found ! Please refine your filter or <a href="#">Click here to cancel</a>
                            </td>
                        </tr>
                    </tbody>
                   
                </table>
            </div>
        </div>
    </div>
</form>
</div>
<script>
    var base_url = '<?php echo base_url(); ?>';

    var cont = 'FeedbackSubcategory';
    $(document).ready(function () {


        getFeedbacksubcategory(base_url, cont);


    });
    $('.submit').on('click', function () {
         $('.error').remove();
        if ($("select[name='FeedbackCategoryId']").val() == 0) {
            $("select[name='FeedbackCategoryId']").after('<span class="error">Please fill this field</span>');
        } else if ($("input[name='Type']").val() == '') {
            $("input[name='Type']").after('<span class="error">Please fill this field</span>');
        } else {
            var datas = $('#feedbackSubCategory-form').serialize();
            ajaxcallsproperty(base_url, cont, datas);
            getFeedbacksubcategory(base_url, cont);
            $("input[name='id']").val('0');
            $('#feedbackSubCategory-form')[0].reset();
        }
    });
    function edit(id) {
//        console.log($('#'+id).find("td:eq(1)").text());
        $("input[name='id']").val(id);
        $("input[name='Type']").val($('#' + id).find("td:eq(1)").text());
        $("#FeedbackCategoryId option:selected").text($('#' + id).find("td:eq(2)").text());

    }
    function deleteFeedbacksubcategory(id) {
if (confirm('Are you sure you want to Delete?')) { 
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>propertymaster/" + cont + "/deleteFeedbacksubcategory",
            data: "id=" + id,
            success: function (data) {
                if (data) {
                    $('#' + id).remove();
                }
            }
        });
        }
    }

</script>