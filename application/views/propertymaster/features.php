<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="col-md-9">
    <h3 class="masters_title">Features</h3>
    <form method="post" id="features-form">
        <div class="">
            <div class="row mb10 mt10">
                <div class="col-md-12 pl5 pr0">
                    <input type="hidden" name="id" value="0"/>
                    <div class="form-group tar clearfix mb5">
                        <label class="col-sm-4 control-label" for="FeatureType">Feature Type</label>
                        <div class="col-sm-3">

                            <input type="text" id="FeatureType" name="FeatureType" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group tar clearfix mb5">
                        <label class="col-sm-4 control-label" for="Comments">Comments</label>
                        <div class="col-sm-3">
                            <input type="text" id="Comments" name="Comments" class="form-control" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mt10 tar">
                            <button class="btn btn-danger" type="button"><i class="fa fa-times pr5"></i>Cancel</button>
                            <button class="btn btn-primary submit" type="button"><i class="fa fa-save pr5"></i>Save</button>
                        </div>                    
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <table id="datatb" class="table table-bordered thumbnail-style mt10">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>FeatureType</th>
                                <th>Comments</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>

                        <tbody class="data-append">
                        <tr>
                            <td colspan="5" class="tac" style="height: 100px; vertical-align: middle;">Loading
                            </td>
                        </tr>
                    </tbody>

                    </table>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    var base_url = '<?php echo base_url(); ?>';
    var cont = 'Features';


    $(document).ready(function () {


        getFeatures(base_url, cont);


    });
    $('.submit').on('click', function () {
        $('.error').remove();
        if ($("input[name='FeatureType']").val() == '') {
            $("input[name='FeatureType']").after('<span class="error">Please fill this field</span>');
        } else if ($("input[name='Comments']").val() == '') {
            $("input[name='Comments']").after('<span class="error">Please fill this field</span>');
        } else {
            var datas = $('#features-form').serialize();
            ajaxcallsproperty(base_url, cont, datas);
            getFeatures(base_url, cont);
            $("input[name='id']").val('0');
            $('#features-form')[0].reset();
        }
    });
    function edit(id) {
//        console.log($('#'+id).find("td:eq(1)").text());
        $("input[name='id']").val(id);
        $("input[name='FeatureType']").val($('#' + id).find("td:eq(1)").text());
        $("input[name='Comments']").val($('#' + id).find("td:eq(2)").text());
    }
    function deleteFeatures(id) {
        if (confirm('Are you sure you want to Delete?')) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>propertymaster/" + cont + "/deleteFeatures",
                data: "id=" + id,
                success: function (data) {
                    if (data) {
                        $('#' + id).remove();
                         window.location.reload();
                    }
                }
            });
        }
    }
    $("input[name='FeatureType']").on('keyup', function () {
        $('.error').remove();
        if ($("input[name='id']").val() == 0) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>propertymaster/" + cont + "/checkFeatureType",
                data: "type=" + $("input[name='FeatureType']").val(),
                dataType: 'html',
                success: function (data) {
                    if (data == 1) {
                        $("input[name='FeatureType']").after('<span class="error">feature type already exist</span>');
                        $('.submit').prop('disabled', true);
                    } else {
                        $("input[name='FeatureType']").after('<span class="error"></span>');
                        $('.submit').prop('disabled', false);
                    }
                }
            });
        }
    });
</script>