<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>






<section id="main-content" class="main-content">
        <section class="surround">

        <div class="container-fluid property_listing master">
          <div class="row">
              <div class="col-xs-12">
                <div class="panel">
                  <div class="panel-heading">
                    


                    <div class="row">
                      <div class="col-sm-12">
                        <h3>Site Status</h3>
                      </div>
                    </div>

<input type="hidden" id="checkSitestatus">
                    <?php if ($access != 3) { ?>
                    <form method="post" id="sitestatus-form"> 
                    <div class="row">

                      <div class="col-sm-12">
                        <div class="form-horizontal">
                     

             
 <input type="hidden" name="id" value="0"/>




       <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="Status">Status<span style="color: #F00"> *</span></label>
                            <div class="col-sm-4 col-md-3">
                    <input tabindex="1" type="text" id="Status" name="Status" maxlength="30" class="form-control input" onkeypress="return onlyAlphabets(event,this);" />
                            </div>
                          </div>

           
<div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="StatusDesc">Description</label>
                            <div class="col-sm-4 col-md-3">
                     <textarea tabindex="2" maxlength="60" id="StatusDesc" name="StatusDesc" class="form-control"></textarea>
                            </div>
                          </div>


                               


                          <div class="form-group">
                            <div class="col-md-offset-5 col-sm-offset-3 col-sm-3 save-cancel">
                              <button  tabindex="3" type="button" class="btn btn-default submit">Save</button>
                              <button tabindex="4" type="button" class="btn btn-default btn-danger reset">Cancel</button>
                            </div>                            
                          </div>


                        </div>                        
                      </div>


                    </div>
                    </form>

<?php } ?>
               
                  
                  </div>
                  <div class="">
                  <div class="preventdel" style="display: none;"></div>
                    <div class="table-responsive">



    <table id="datatb" class="table table-striped table-hover">
                   <!--  <colgroup>
                        <col width="7%">
                        <col width="15%">
                        <col width="64%">
                        <col width="7%">
                        <col width="7%">
                    </colgroup> -->
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Status</th>
                            <th>Description</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    
                    <tbody class="data-append">
                        <tr>
                            <td colspan="5" class="tac" style="height: 100px; vertical-align: middle;">Loading
                            </td>
                        </tr>
                    </tbody>
                   
                </table>






                    </div>
                  </div>
                </div>
              </div>    
          </div>
        </div>
          
        </section>        
      </section>





<script>
    var base_url = '<?php echo base_url(); ?>';
    var cont = 'SiteStatus';
 
    
    $(document).ready(function () {
        
         
            getSitestatus(base_url, cont);
 $(".reset").click(function(){
$('.submit').prop('disabled', false);$('.error').remove();
    });

    });


$('.input').on('keypress',function(e)  {
 if (e.which != 13  ) {

$('.error').remove();

} else {
submit();
  
}



});




    $('.submit').on('click', function (e) {
    e.preventDefault();
submit();


    });



    function submit() {


        if ($("input[name='Status']").val() == '') {
            $('.error').remove();
            $("input[name='Status']").after('<span class="error">Please fill this field</span>');
            $("input[name='Status']").focus();
        } else if(/[0-9]/g.test($("input[name='Status']").val())){
            $('.error').remove();
            $("input[name='Status']").after('<span class="error">Please enter valid detail</span>');
            $("input[name='Status']").focus();
        }
        else  if ($("input[name='StatusDesc']").val() == '') {
            $('.error').remove();
            $("input[name='StatusDesc']").after('<span class="error">Please fill this field</span>');
            $("input[name='StatusDesc']").focus();
        } else if(checkSitestatus() == 1){
$('.error').remove();
   $("input[name='Status']").after('<span class="error">Property status already exist</span>');
   $("input[name='Status']").focus();

} else {
// setTimeout(function () {
// if ($('#checkSitestatus').val() == 1) { 
// $('.error').remove();
//    $("input[name='Status']").after('<span class="error">Property status already exist</span>');

// } else {


            $('.error').remove();
        var datas = $('#sitestatus-form').serialize();
        ajaxcallsproperty(base_url, cont, datas);
        getSitestatus(base_url, cont);
        $("input[name='id']").val('0');
        $('#sitestatus-form')[0].reset();
    

// }


// }, 2000);






    }


    }








    function edit(id) {
//        console.log($('#'+id).find("td:eq(1)").text());
        $("input[name='id']").val(id);
        $("input[name='Status']").val($('#' + id).find("td:eq(1)").text());
        $(window).scrollTop(0);
        document.getElementById("StatusDesc").value =$('#' + id).find("td:eq(2)").text();

       
    }

function df() {
    $(window).scrollTop(0);
 $('.preventdel').text('This is in use, cannot delete');
  $('.preventdel').removeAttr('style');
        setTimeout(function() { 
$('.preventdel').css("display","none");
        },3000);
    }


    function deleteSitestatus(id) {
// if (confirm('Are you sure you want to Delete?')) { 
       $('#confirm').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delete', function () {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>propertymaster/" + cont + "/deleteSitestatus",
            data: "id=" + id,
            success: function (data) {
                if (data) {
                     $('#message').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delet', function () {
                 window.location.reload();
                        $('#' + id).remove();
});
                        // alert('Deletes successfully');
                } 
            }
        });
        });
    }





function checkSitestatus() {
var returnval;
    // $('.error').remove();
    // if($("input[name='id']").val()==0 ){
        $.ajax({
            type: "POST",
            async:false,
            url: "<?php echo base_url(); ?>propertymaster/" + cont + "/checkSitestatus",
            data: "status=" +$("input[name='Status']").val() + "&id=" + $("input[name='id']").val(),
            dataType:'html',
            success: function (data) {
                if(data==1){
                    returnval = data;
                    // $('#checkSitestatus').val(data);
                }else{
                    
                    returnval = data;
                    // $('#checkSitestatus').val('');
                }
            }
        });
        // }
        return returnval;

}



</script>