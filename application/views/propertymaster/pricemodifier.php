<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>







<section id="main-content" class="main-content">
        <section class="surround">

        <div class="container-fluid property_listing master">
          <div class="row">
              <div class="col-xs-12">
                <div class="panel">
                  <div class="panel-heading">
                    


                    <div class="row">
                      <div class="col-sm-12">
                        <h3>Price Modifier</h3>
                      </div>
                    </div>
                    <input type="hidden" id="checkModifier">
                    <?php if ($access != 3) { ?>
                    <form method="post" id="priceModifier-form"> 
                    <div class="row">

                      <div class="col-sm-12">
                        <div class="form-horizontal">
                     

             
 <input type="hidden" name="id" value="0"/>




       <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="Modifier">Modifier Name<span style="color: #F00"> *</span></label>
                            <div class="col-sm-4 col-md-3">
                    <input type="text" id="Modifier" name="Modifier" maxlength="30" class="form-control input" onkeypress="return onlyAlphabets(event,this);" />
                            </div>
                          </div>

           
<div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " ></label>
                            <div class="col-sm-4 col-md-3">
                       <label><input type="radio" id="IsForSale" name="ModifierDesc" value="Sale" checked>Sale</label>
                        
                            <label><input id="IsForRent" type="radio" name="ModifierDesc" value="Rent">Rent</label>
                            </div>
                          </div>



                               


                          <div class="form-group">
                            <div class="col-md-offset-5 col-sm-offset-3 col-sm-3 save-cancel">
                              <button type="button" class="btn btn-default submit">Save</button>
                              <button type="button" class="btn btn-default btn-danger reset">Cancel</button>
                            </div>                            
                          </div>


                        </div>                        
                      </div>


                    </div>
                    </form>

<?php } ?>
               
                  
                  </div>
                  <div class="">
                    <div class="table-responsive">

<div class="preventdel" style="display: none;"></div>

    <table id="datatb" class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Modifier Name</th>
                            <th>Modifier for</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    
                    <tbody class="data-append">
                        <tr>
                            <td colspan="5" class="tac" style="height: 100px; vertical-align: middle;">Loading
                            </td>
                        </tr>
                    </tbody>
                   
                </table>






                    </div>
                  </div>
                </div>
              </div>    
          </div>
        </div>
          
        </section>        
      </section>






<script>
    var base_url = '<?php echo base_url(); ?>';
    var cont = 'PriceModifier';

    
    $(document).ready(function () {
        
         
        getPricemodifier(base_url, cont);
$(".reset").click(function(){
$('.error').remove();
 $('.submit').prop('disabled', false);
    });

    });



$('.input').on('keypress',function(e)  {

 if (e.which != 13  ) {

$('.error').remove();

} else {
e.preventDefault();
submit();
  
}




});



    $('.submit').on('click', function () {
         
submit();
    });



    function submit(){

        if ($("input[name='Modifier']").val() == '') {
            $('.error').remove();
            $("input[name='Modifier']").after('<span class="error">Please fill this field</span>');
       $("input[name='Modifier']").focus();
        // } else if ($("input[name='ModifierDesc']").val() == '') {
        //     $("input[name='ModifierDesc']").after('<span class="error">Please fill this field</span>');
       
        } else if(checkModifier() ==1){


 $('.error').remove();
 $("input[name='Modifier']").after('<span class="error">Price modifier already exist</span>');
                       $("input[name='Modifier']").focus();

}
else {

 $('.error').remove();
        var datas = $('#priceModifier-form').serialize();
        ajaxcallsproperty(base_url, cont, datas);
        getPricemodifier(base_url, cont);
        $("input[name='id']").val('0');
        $('#priceModifier-form')[0].reset();





           










    }

    }


    function edit(id) {
//        console.log($('#'+id).find("td:eq(1)").text());
  $(window).scrollTop(0);
        $("input[name='id']").val(id);
        $("input[name='Modifier']").val($('#' + id).find("td:eq(1)").text());
         // document.getElementById("ModifierDesc").value =$('#' + id).find("td:eq(2)").text();
         var temp = $('#' + id).find("td:eq(2)").text();
        if (temp == 'Sale') {
            $('#IsForSale').prop('checked', true);
        } else if (temp == 'Rent') {
            $('#IsForRent').prop('checked', true);
        }

    }


function df() {
    $(window).scrollTop(0);
 $('.preventdel').text('This is in use, cannot delete');
  $('.preventdel').removeAttr('style');
        setTimeout(function() { 
$('.preventdel').css("display","none");
        },3000);
    }



    function deletePricemodifier(id) {
// if (confirm('Are you sure you want to Delete?')) { 
         $('#confirm').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delete', function () {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>propertymaster/" + cont + "/deletePricemodifier",
            data: "id=" + id,
            success: function (data) {
                if (data) {
                    $('#message').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delet', function () {
                    window.location.reload();
                        $('#' + id).remove();
                    });

                        // alert('Deletes successfully');
                }
            }
        });
        });
    }
    


var desc = "Sale";
$("input[name='ModifierDesc']").on('change',function(){

 var favorite = [];
            $.each($("input[name='ModifierDesc']:checked"), function(){            
                favorite.push($(this).val());
            });
             desc = favorite.join(", ");


});

    function checkModifier() {
        
        var returnval;
            $.ajax({
                type: "POST",
                async: false,
                url: "<?php echo base_url(); ?>propertymaster/" + cont + "/checkModifier",
                data: "type=" + $("input[name='Modifier']").val() + "&desc=" + desc + "&id=" + $("input[name='id']").val(),
                dataType: 'html',
                success: function (data) {
                    if (data == 1) {
                        returnval = data;

                    } else {
                       returnval = data;
                    }
                }
            });
        
        return returnval;
    }
</script>