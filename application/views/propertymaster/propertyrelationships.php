<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>
<div class="col-md-9">
     <h3 class="masters_title">Property Relationship</h3>
<form method="post" id="propertyRelationship-form">
    <div class="">
        <div class="row mb10 mt10">
            <div class="col-md-12 pl5 pr0">
                <input type="hidden" name="id" value="0"/>
                <div class="form-group tar clearfix mb5">
                    <label class="col-sm-4 control-label" for="Relationship">Relationship</label>
                    <div class="col-sm-3">
                        <input type="text" id="Relationship" name="Relationship" class="form-control" />
                    </div>
                </div>
                <div class="form-group tar clearfix mb5">
                    <label class="col-sm-4 control-label" for="RelationshipDesc">Relationship</label>
                    <div class="col-sm-3">
                        <textarea id="RelationshipDesc" name="RelationshipDesc" class="form-control" ></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 mt10 tar">
                        <button class="btn btn-danger" type="button"><i class="fa fa-times pr5"></i>Cancel</button>
                        <button class="btn btn-primary submit" type="button"><i class="fa fa-save pr5"></i>Save</button>
                    </div>                    
                </div>
                
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table id="datatb" class="table table-bordered thumbnail-style mt10">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Relationship Type</th>
                            <th>Relationship Description</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                   
                    <!-- ko if:(Items().length===0) -->
                    <tbody class="data-append">
                        <tr>
                            <td colspan="5" class="tac" style="height: 100px; vertical-align: middle;">No relationship found ! Please refine your filter or <a href="#">Click here to cancel</a>
                            </td>
                        </tr>
                    </tbody>
                   
                </table>
            </div>
        </div>
    </div>
</form>
</div>
<script>
    var base_url = '<?php echo base_url(); ?>';
    var cont = 'PropertyRelationships';

    
    $(document).ready(function () {
        
         
        getPropertyrelationships(base_url, cont);


    });
    $('.submit').on('click', function () {
         $('.error').remove();
        if ($("input[name='Relationship']").val() == '') {
            $("input[name='Relationship']").after('<span class="error">Please fill this field</span>');
        } else if ($("input[name='RelationshipDesc']").val() == '') {
            $("input[name='RelationshipDesc']").after('<span class="error">Please fill this field</span>');
        } else{
        var datas = $('#propertyRelationship-form').serialize();
        ajaxcallsproperty(base_url, cont, datas);
        getPropertyrelationships(base_url, cont);
        $("input[name='id']").val('0');
        $('#propertyRelationship-form')[0].reset();
    }
    });
    function edit(id) {
//        console.log($('#'+id).find("td:eq(1)").text());
        $("input[name='id']").val(id);
        $("input[name='Relationship']").val($('#' + id).find("td:eq(1)").text());
        document.getElementById("RelationshipDesc").value =$('#' + id).find("td:eq(2)").text();
    }
    function deletePropertyrelationships(id) {
if (confirm('Are you sure you want to Delete?')) { 
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>propertymaster/" + cont + "/deletePropertyrelationships",
            data: "id=" + id,
            success: function (data) {
                if (data) {
                    $('#' + id).remove();
                }
            }
        });
        }
    }
</script>
