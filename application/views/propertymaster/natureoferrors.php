<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="col-md-9">
     <h3 class="masters_title">Nature of Errors</h3>
<form method="get" id="natureOfError-form" >
    <div class="">
        <div class="row mb10 mt10">
            <div class="col-md-12 pl5 pr0">
                <input type="hidden" name="id" value="0"/>
                <div class="form-group tar clearfix mb5">
                    <label class="col-sm-4 control-label" for="Error">Error</label>
                    <div class="col-sm-3">
                        <input type="text" id="Error" name="Error" class="form-control" />
                    </div>
                </div>
                <div class="form-group tar clearfix mb5">
                    <label class="col-sm-4 control-label" for="Desc">Error</label>
                    <div class="col-sm-3">
                        <textarea id="Desc" name="Desc" class="form-control" ></textarea>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 mt10 tar">
                        <button class="btn btn-danger" type="button"><i class="fa fa-times pr5"></i>Cancel</button>
                        <button class="btn btn-primary submit"  type="button"><i class="fa fa-save pr5"></i>Save</button>
                    </div>                    
                </div>
                
            </div>
        </div>
        <div class="row">
            <div class="col-md-12" >
                <table id="datatb" class="table table-bordered thumbnail-style mt10">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Error Type</th>
                            <th>Error Description</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    
                    <!-- ko if:(Items().length===0) -->
                    <tbody class="data-append">
                        <tr>
                            <td colspan="5" class="tac" style="height: 100px; vertical-align: middle;">No Natureoferror found ! Please refine your filter or <a href="#">Click here to cancel</a>
                            </td>
                        </tr>
                    </tbody>
                   
                </table>
            </div>
        </div>
    </div>
</form>
</div>
<script>
    var base_url = '<?php echo base_url(); ?>';
    var cont = 'NatureofErrors';

    
    $(document).ready(function () {
        
         
        getNatureoferrors(base_url, cont);


    });
    $('.submit').on('click', function () {
         $('.error').remove();
        if ($("input[name='Error']").val() == '') {
            $("input[name='Error']").after('<span class="error">Please fill this field</span>');
        } else if ($("input[name='Desc']").val() == '') {
            $("input[name='Desc']").after('<span class="error">Please fill this field</span>');
        } else{
        var datas = $('#natureOfError-form').serialize();
        ajaxcallsproperty(base_url, cont, datas);
        getNatureoferrors(base_url, cont);
        $("input[name='id']").val('0');
        $('#natureOfError-form')[0].reset();
    }
    });
    function edit(id) {
//        console.log($('#'+id).find("td:eq(1)").text());
        $("input[name='id']").val(id);
        $("input[name='Error']").val($('#' + id).find("td:eq(1)").text());
        document.getElementById("Desc").value =$('#' + id).find("td:eq(2)").text();
    }
    function deleteNatureoferrors(id) {
if (confirm('Are you sure you want to Delete?')) { 
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>propertymaster/" + cont + "/deleteNatureoferrors",
            data: "id=" + id,
            success: function (data) {
                if (data) {
                    $('#' + id).remove();
                }
            }
        });
        }
    }
</script>

