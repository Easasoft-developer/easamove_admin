<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="col-md-9">
     <h3 class="masters_title">Decorative Condition</h3>
<form method="post" id="decorativecondition">
    <div class="">
        <div class="row mb10 mt10">
            <div class="col-md-12 pl5 pr0">    
                <input type="hidden" name="id" value="0"/>
                <div class="form-group tar clearfix mb5">
                    <label class="col-sm-4 control-label" for="Code">Condition Type</label>
                    <div class="col-sm-3">
                        <input type="text" id="ConditionType" name="ConditionType" class="form-control" />
                    </div>
                </div> 
                <div class="form-group tar clearfix mb5">
                    <label class="col-sm-4 control-label" for="Code">Condition Description</label>
                    <div class="col-sm-3">
                        <textarea id="ConditionDesc" name="ConditionDesc" class="form-control"></textarea>
                    </div>
                </div> 
                <div class="row">
                    <div class="col-md-6 mt10 tar">
                        <button class="btn btn-danger" type="button"><i class="fa fa-times pr5"></i>Cancel</button>
                        <button class="btn btn-primary submit" type="button"><i class="fa fa-save pr5"></i>Save</button>
                    </div>                    
                </div>
                
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table id="datatb" class="table table-bordered thumbnail-style mt10">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Condition Type</th>
                             <th>Condition Description</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    
                    <tbody class="data-append">
                        <tr>
                            <td colspan="5" class="tac" style="height: 100px; vertical-align: middle;">Loading
                            </td>
                        </tr>
                    </tbody>
                    
                </table>
            </div>
        </div>
    </div>
</form>
</div>
<script>
    var base_url = '<?php echo base_url(); ?>';
    var cont = 'DecorativeCondition';

    
    $(document).ready(function () {
        
         
        getDecorativeCondition(base_url, cont);


    });
    $('.submit').on('click', function () {
        $('.error').remove();
        if ($("input[name='ConditionType']").val() == '') {
            $("input[name='ConditionType']").after('<span class="error">Please fill this field</span>');
        } else if ($("input[name='ConditionDesc']").val() == '') {
            $("input[name='ConditionDesc']").after('<span class="error">Please fill this field</span>');
        } else {
        var datas = $('#decorativecondition').serialize();
        ajaxcalls(base_url, cont, datas);
        getDecorativeCondition(base_url, cont);
        $("input[name='id']").val('0');
        $('#decorativecondition')[0].reset();
    }
    });
    function edit(id) {
//        console.log($('#'+id).find("td:eq(1)").text());
        $("input[name='id']").val(id);
        $("input[name='ConditionType']").val($('#' + id).find("td:eq(1)").text());
//        $("input[name='ConditionDesc']").val($('#' + id).find("td:eq(2)").text());
        document.getElementById("ConditionDesc").value =$('#' + id).find("td:eq(2)").text();
       
    }
    function deleteDecorativecondition(id) {
if (confirm('Are you sure you want to Delete?')) { 
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>master/" + cont + "/deleteDecorativecondition",
            data: "id=" + id,
            success: function (data) {
                if (data) {
                    $('#' + id).remove();
                }
            }
        });
        }
    }
</script>
