<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>



 <!-- main content -->
      <section id="main-content" class="main-content">
        <section class="surround">

        <div class="container-fluid property_listing master">
          <div class="row">
              <div class="col-xs-12">
                <div class="panel">
                  <div class="panel-heading">
                    

                    <div class="row">
                      <div class="col-sm-12">
                        <h3>City Master</h3>
                      </div>
                    </div>
                    <input type="hidden" id="checkCity">
<?php if ($access != 3) { ?>
        <form method="Post" id="city-form" >
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-horizontal">
                        <input type="hidden" name="id" value="0"/>
                          <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="Code">Country Name<span style="color: #F00"> *</span></label>
                          
                        <div class="col-sm-4 col-sm-3">
                            <select tabindex="1" class="form-control input" name="CountryId" id="CountryId">
                                <option value="0">- Select -</option>
                                <?php
                                if (isset($country)) {
                                    foreach ($country as $cnty) {
                                        echo '<option value="' . $cnty->country_id . '">' . $cnty->country_name . '</option>';
                                    }
                                }
                                ?>          
                            </select>
                        </div>
                        </div>


                         

                     <div class="form-group">  
                        <label class="control-label col-md-offset-3 col-sm-3 col-md-2" for="State">State Name<span style="color: #F00"> *</span></label>
                        <div class="col-sm-4 col-md-3">
                            <select tabindex="2" class="form-control input" name="StateId" id="StateId">
                                <option value="0">- Select -</option>
                                <?php
                                // if (isset($state)) {
                                //     foreach ($state as $sts) {
                                //         echo '<option value="' . $sts->state_id . '">' . $sts->state_name . '</option>';
                                //     }
                                // }
                                ?>           
                            </select>
                        </div>
                    </div>









                 <div class="form-group">
                        <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="Code">City Code<span style="color: #F00"> *</span></label>
                        <div class="col-sm-3">

                            <input tabindex="3" type="text" id="Code" name="Code" onkeypress="return onlyAlphabets(event,this);" maxlength="30" class="form-control input" />
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="Name">City Name<span style="color: #F00"> *</span></label>
                        <div class="col-sm-3">
                            <input tabindex="4" type="text" id="Name" name="Name" onkeypress="return onlyAlphabets(event,this);" maxlength="30" class="form-control input" />

                        </div>
                    </div>


                          <div class="form-group">
                            <div class="col-md-offset-5 col-sm-offset-3 col-sm-3 save-cancel">
                              <button tabindex="5" type="button" class="btn btn-default submit">Save</button>
                              <button tabindex="6" type="button" class="btn btn-danger reset">Cancel</button>
                            </div>                            
                          </div>

                           </div>
                        </div>                        
                      </div>
                    <!-- </div> -->
                    </form>


                 <?php } ?>
                  
                  </div>
                  <div class="">
                    <div class="table-responsive">
                    <table id="datatb" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                                <th>City Code</th>
                                <th>City Name</th>
                                <th>Country Name</th>
                                <th>State Name</th>
                                <th>Edit</th>
                                <th>Delete</th>
                        </tr>

                        </thead>
                       

                <tbody class="data-append">
                        <tr>
                            <td colspan="5" class="tac" style="height: 100px; vertical-align: middle;">Loading
                            </td>
                        </tr>
                    </tbody>

                       <!--  <tfoot>
                          <tr>
                            
                            <td colspan="3"> 
                            <div class="btn-group">
                              <button class="btn btn-default btn-sm active" type="button">10</button>
                              <button class="btn btn-default btn-sm" type="button">20</button>
                              <button class="btn btn-default btn-sm" type="button">30</button>
                              <button class="btn btn-default btn-sm" type="button">40</button>
                            </div> Items
                            </td>
                            <td colspan="11">
                              <ul class="pagination pagination-sm">
                                <li class="disabled"><a href="#">Prev</a></li>
                                <li class="active"><a href="#">1</a></li>
                                <li><a href="#">2</a></li>
                                <li><a href="#">3</a></li>
                                <li><a href="#">4</a></li>
                                <li><a href="#">5</a></li>
                                <li><a href="#">Next</a></li>
                              </ul>
                            </td>
                          </tr>
                        </tfoot> -->
                      </table>
                    </div>
                  </div>
                </div>
              </div>    
          </div>
        </div>
          
        </section>        
      </section>



































































































<script>
    var base_url = '<?php echo base_url(); ?>';
    var cont = 'City';

    $('#CountryId').on('change', function () {
        var cid = $(this).val();

        // if(cid > 0) {
        $('#StateId').after("<i id='ldr' class='fa fa-refresh fa-spin'></i>");
        $('#StateId').prop('disabled','true');
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>master/" + cont + "/getCountry",
            data: "id=" + cid,
            success: function (data) {
                $('#ldr').remove();
                $('#StateId').removeAttr("disabled");
                $('#StateId').html(data);
            }
        });
// }


    });
    $(document).ready(function () {


        getCity(base_url, cont);

 $(".reset").click(function(){
$('.error').remove();
$('.submit').prop('disabled', false);
    });



$('#StateId,#CountryId').on('change',function() {

$('.error').remove();
}); 

$('.input').on('keypress',function(e)  {
 if (e.which != 13  ) {

$('.error').remove();

} else {
submit();
  
}



});

    });





    $('.submit').on('click', function () {
        
submit();
      

    });



function submit() {

      if ($("select[name='CountryId']").val() == 0) {
        $('.error').remove();
            $("select[name='CountryId']").after('<span class="error">Please fill this field</span>');
            $("select[name='CountryId']").focus();
        } else if ($("select[name='StateId']").val() == 0) {
            $('.error').remove();
            $("select[name='StateId']").after('<span class="error">Please fill this field</span>');
            $("select[name='StateId']").focus();
        } else if ($("input[name='Code']").val() == '') {
            $('.error').remove();
            $("input[name='Code']").after('<span class="error">Please fill this field</span>');
            $("input[name='Code']").focus();
        } else if ($("input[name='Name']").val() == '') {
            $('.error').remove();
            $("input[name='Name']").after('<span class="error">Please fill this field</span>');
            $("input[name='Name']").focus();
        } else if(checkCity() == 1){
$('.error').remove();
$("input[name='Code']").after('<span class="error">City code already exist</span>');
$("input[name='Code']").focus();
} else {
$('.submit').text('Processing');
$('.error').remove();
            var datas = $('#city-form').serialize();
            ajaxcalls(base_url, cont, datas);
//            getCity(base_url, cont);
            $("input[name='id']").val('0');
            $('#city-form')[0].reset();
       
        }
}

    function edit(id, sid, cid) {
//        console.log($('#'+id).find("td:eq(1)").text());
        $("input[name='id']").val(id);
        $("input[name='Code']").val($('#' + id).find("td:eq(1)").text());
        $("input[name='Name']").val($('#' + id).find("td:eq(2)").text());
$(window).scrollTop(0);
//        $("#CountryId").val($('#' + id).find("td:eq(3)").text());
//        var country = $('#' + id).find("td:eq(3)").text();
//        var state = $('#' + id).find("td:eq(4)").text();
        $('#CountryId option').filter(function () {
            return ($(this).text() == $('#' + id).find("td:eq(3)").text()); //To select Blue
        }).prop('selected', true);
        
       $('#StateId').after("<i id='ldr' class='fa fa-refresh fa-spin'></i>");
        $('#StateId').prop('disabled','true');
        var cid = $('#CountryId').val();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>master/" + cont + "/getCountry",
            data: "id=" + cid,
            success: function (data) {
                $('#ldr').remove();
                $('#StateId').removeAttr("disabled");
                $('#StateId').html(data);

                 $('#StateId option').filter(function () {
            return ($(this).text() == $('#' + id).find("td:eq(4)").text()); //To select Blue
        }).prop('selected', true);

            }
        });

    // setTimeout(function(){
        
       
    // },1000);
//        $("#CountryId").val(cid);
//        $("#StateId").val(sid);
//        $("#StateId").val($('#' + id).find("td:eq(4)").text());

    }



    function deleteCity(id) {
        // if (confirm('Are you sure you want to Delete?')) {
             $('#confirm').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delete', function () {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>master/" + cont + "/deleteCity",
                data: "id=" + id,
                success: function (data) {
                    if (data) {
                              $('#message').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delet', function () {
                        window.location.reload();
                        $('#' + id).remove();
                    });
                        // alert('Deletes successfully');
                    }
                }
            });
        });
    }
    

    
    function checkCity() {
var returndata;
        $.ajax({
            type: "POST",
            async:false,
            url: "<?php echo base_url(); ?>master/" + cont + "/checkCity",
            data: "code=" +$("input[name='Code']").val() + "&id="+ $("input[name='id']").val(),
            dataType:'html',
            success: function (data) {
                if(data==1){
                	returndata = data;
                    
                }else{
                    
                    returndata = data;
                }
            }
        });

    return returndata;
    }


    //     }
    // });
</script>