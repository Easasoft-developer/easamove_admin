<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>



 <!-- main content -->
      <section id="main-content" class="main-content">
        <section class="surround">

        <div class="container-fluid property_listing master">
          <div class="row">
              <div class="col-xs-12">
                <div class="panel">
                  <div class="panel-heading">
                    

                    <div class="row">
                      <div class="col-sm-12">
                        <h3>Agent Members</h3>
                      </div>
                    </div>
<?php if ($access != 3) { ?>
    <form method="post" id="agentmember-form" enctype="multipart/form-data" >
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-horizontal">

                <input type="hidden" name="id" value="0"/>
                          <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="Code">Member Name<span style="color: #F00"> *</span></label>
                            <div class="col-sm-4 col-md-3">
                              <input  tabindex="1" onkeypress="return onlyAlphabets(event,this);" type="text" id="Type" name="member_name" maxlength="30" class="form-control input" />
                            </div>
                          </div>



                          <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2" for="Desc">Website<span style="color: #F00"> *</span></label>
                            <div class="col-sm-4 col-md-3">
                              <input tabindex="2" maxlength="40" type="url" id="Desc" name="member_website" class="form-control input" />
                            </div>
                          </div>



                           <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2" for="Logo">Logo<span style="color: #F00"> *</span></label>
                            <div class="col-sm-4 col-md-3">
                             <img  src="" width="100" style="display: none;" />
                              <input  tabindex="3" type="file" id="Logo" accept="image/jpg,image/png,image/jpeg"  name="memberLogo" data-logo="0" class="form-control input" />
                             
                               <input type="hidden" name="imageedit" id="imageedit" value=""/>
                               <input type="hidden" name="imageeditlogo" id="imageeditlogo" value=""/>
                            </div>
                          </div>



                          <div class="form-group">
                            <div class="col-md-offset-5 col-sm-offset-3 col-sm-3 save-cancel">
                              <button tabindex="4" type="button" class="btn btn-primary submit">Save</button>
                              <button tabindex="5" class="btn btn-danger reset" type="button">Cancel</button>
                            </div>                            
                          </div>


                        </div>                        
                      </div>
                    </div>
</form>
<?php } ?>
                    
                  
                  </div>
                  <div class="">
                    <div class="table-responsive">
                    <table id="datatb" class="table table-striped table-hover">
                    <colgroup>
                      <col width="5%">
                      <col width="20%">
                      <col width="30%">
                      <col width="30%">
                      <col width="7.5%">
                      <col width="7.5%">
                    </colgroup>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Member Name</th>
                            <th>Website</th>
                            <th>Logo</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <!-- ko if:(Items().length>0) -->
                    
                   <tbody class="data-append">
                        <tr>
                            <td colspan="5" class="tac" style="height: 100px; vertical-align: middle;">Loading
                            </td>
                        </tr>
                    </tbody>
                   <!--/ko -->
                </table> 
                    </div>
                  </div>
                </div>
              </div>    
          </div>
        </div>
          
        </section>        
      </section>



<script src="<?php echo base_url(); ?>js/jquery.dataTables.js"></script> 
<script src="<?php echo base_url(); ?>js/easamove-admin.js"></script> 

<script>
    var base_url = '<?php echo base_url(); ?>';
    var cont = 'AgentMember';
    var extension;

    
    $(document).ready(function () {
        
        getAgentMember(base_url, cont);
 $(".reset").click(function(){
$('.error').remove();
ultrasafe(2);
    });
    });


$('.btn-danger').on('click', function(e) {
  $("#agentmember-form img").attr("style", "display:none");
});



   $('.input').keypress(function (e) {
   
  // if (e.which == 13 && !$('.submit').is('[disabled]')) {
    if (e.which == 13 ) {
      e.preventDefault();
    submit();
    
  }
});



function ultrasafe(ch) {

if(ch == 1 ) {
b = ch;
} 
else if(ch == 2) {

  b = '';
}

else {
if(typeof(b) != "undefined" && b !== null) {
return b;
}
}

}






 // $('.submit').on('click', function () {
 //   $('form#agentmember-form').submit();
 //                                    });

  function readURL(input) {


                                            if (input.files && input.files[0]) {
                                                extension = input.files[0].name.substring(input.files[0].name.lastIndexOf('.'));
                                                extension = extension.toLowerCase();
                                                // console.log('here image ' + extension +  ' ' + input.files[0]);
                                                if (extension == '.jpeg' || extension == '.png' || extension == '.jpg') {
                                                    var reader = new FileReader();

                                                    reader.onload = function (e) {
                                                        $('#Logo').attr('src', e.target.result);
                                                    }

                                                    reader.readAsDataURL(input.files[0]);
                                                } else {
                                                  var input = $("#Logo");


                                            input.replaceWith(input.val('').clone(true));
                                                  console.log($('#Logo').val());
                                                  // $('#Logo').empty();
                                                          // $('#Logo').attr('src', null);
                                                    $("#Logo").after('<span class="error">Please choose valid file format</span>');
                                                    $("#Logo").focus();
                                                
                                                    ultrasafe(1);

                                                }

                                                if(extension == '' && ($("input[name='id']").val() != 0)) { 
// console.log('not set');
                                                }

                                            }
                                            else {
                                             // console.log('not set new');
                                           }
                                        }



                                    $("#Logo").change(function () {
                                            $('.error').remove();
                                            readURL(this);

                                        });

      $('.submit').on('click', function (e) {

          e.preventDefault();
          submit();
   });



      function submit() {

        // console.log($('input[name="memberLogo"]').attr('data-logo'));
        var url = $("input[name='member_website']").val();
        // alert(url);
         var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
         var mobilePattern = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
           var faxpattern = /^\+?[0-9]{6,}$/;
         var website=/\b(?:(?:https?|ftp):\/\/|www\.)[-a-z0-9+&@#\/%?=~_|!:,.;]*[-a-z0-9+&@#\/%=~_|]/i;
            $('.error').remove();
           if ($("input[name='member_name']").val() == '')  {
        $("input[name='member_name']").after('<span class="error">Please fill this field</span>');
        $("input[name='member_name']").focus();
        } 

        else if ($("input[name='member_website']").val() == '') {
            $("input[name='member_website']").after('<span class="error">Please fill this field</span>');
            $("input[name='member_website']").focus();
        } 
else if (/^(http:\/\/|https:\/\/|ftp:\/\/|)[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/i.test(url) == 0)

{
 $("input[name='member_website']").after('<span class="error">Invalid web address</span>');
$("input[name='member_website']").focus();
}

else if($('#imageeditlogo').val() == ""){
      if ( $("input[name='memberLogo']").val() == '' ) {
          
           $("input[name='memberLogo']").after('<span class="error">Please upload logo</span>');        
      $("input[name='memberLogo']").focus();
      }else{
        $('#imageeditlogo').val('1');
        // return;
        // $('form#agentmember-form').submit();
        submit();

      }
}

else if(ultrasafe() == 1) {
$('.error').remove();
 $("#Logo").after('<span class="error">Please choose valid file format</span>');
}

       else {
        $('.submit').text('Processing');
             // var formData = new FormData(test);
           // formData =  $('#agentmember-form').serialize();
           $('.error').remove();
           newton = $('form')[0];
            me = new FormData(newton);
           

              ajaxMembers(base_url, cont, me);
              }

      }

            









var imgname;

    function edit(id) {
ultrasafe(2);
 
        $("input[name='id']").val(id);
        $("input[name='Type']").val($('#' + id).find("td:eq(1)").text());
        $("input[name='Desc']").val($('#' + id).find("td:eq(2)").text());
        $("input[name='Logo']").val($('#' + id).find("td:eq(3)").text());
        $(window).scrollTop(0);
        document.getElementById("Type").value =$('#' + id).find("td:eq(1)").text();
         document.getElementById("Desc").value =$('#' + id).find("td:eq(2)").text();  
         document.getElementById("imageedit").value =$('#' + id).find("td:eq(3)").text();
 imgname = $('#' + id).find("td:eq(3)").text();

$('#agentmember-form img').attr("src","<?php echo base_url(); ?>images/agent_member/"+imgname);
 
 $("#agentmember-form img").removeAttr("style");


 

 $('#imageeditlogo').val('1');
         
    }



           $(document).on('change', 'input[type="file"]' ,function() {

                if ($('#Logo').get(0).files.length != 0)  {

      $("#agentmember-form img").attr("style", "display:none");

}
else {


  $('#agentmember-form img').attr("src","<?php echo base_url(); ?>images/agent_member/"+imgname);
}

});          






    function deleteAgentMember(id) {
// if (confirm('Are you sure you want to Delete?')) { 
       $('#confirm').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delete', function () {
        $.ajax({
            type: "POST",

            url: "<?php echo base_url(); ?>master/" + cont + "/deleteAgentMember",
            data: "id=" + id,
            success: function (data) {
                if (data) {
                   $('#message').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delet', function () {
                    window.location.reload();
                        $('#' + id).remove();
                        $("input[name='id']").val('0');

                      });

                        // alert('Deletes successfully');


                }
            }
        });
        });
    }
    $("input[name='Type']").on('keyup', function (e) {
     if($("input[name='id']").val()==0  && e.which != 13){
    $('.error').remove();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>master/" + cont + "/checkMember",
            data: "type=" +$("input[name='Type']").val(),
            dataType:'html',
            success: function (data) {
                if(data==1){
                     $("input[name='Type']").after('<span class="error">Alert type already exist</span>');
                     // $('.submit').prop('disabled', true);
                }else{
                    $("input[name='Type']").after('<span class="error"></span>');
                     // $('.submit').prop('disabled', false);
                }
            }
        });
        }
    });
</script>