<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>



 <!-- main content -->
      <section id="main-content" class="main-content">
        <section class="surround">

        <div class="container-fluid property_listing master">
          <div class="row">
              <div class="col-xs-12">
                <div class="panel">
                  <div class="panel-heading">
                    

                    <div class="row">
                      <div class="col-sm-12">
                        <h3>Alert Type</h3>
                      </div>
                    </div>
<input type="hidden" id="checkAlert">
<?php if ($access != 3) { ?>
    <form method="post" id="alerttype-form" >
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-horizontal">

                <input type="hidden" name="id" value="0"/>
                          <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="Code">Alert Type<span style="color: #F00"> *</span></label>
                            <div class="col-sm-4 col-md-3">
                              <input type="text" id="Type" tabindex="1" name="Type" maxlength="30" class="form-control input" onkeypress="return onlyAlphabets(event,this);"/>
                            </div>
                          </div>



                          <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2" for="Desc">Alert Description</label>
                            <div class="col-sm-4 col-md-3">
                              <textarea maxlength="60" tabindex="2" id="Desc" name="Desc" class="form-control input"></textarea>
                            </div>
                          </div>


                          <div class="form-group">
                            <div class="col-md-offset-5 col-sm-offset-3 col-sm-3 save-cancel">
                              <button tabindex="3" type="button" class="btn btn-primary submit">Save</button>
                              <button tabindex="4" class="btn btn-danger reset" type="button">Cancel</button>
                            </div>                            
                          </div>


                        </div>                        
                      </div>
                    </div>
</form>
<?php } ?>
                    
                  
                  </div>
                  <div class="">
                    <div class="table-responsive">
                    <table id="datatb" class="table table-striped table-hover">
                    <!-- <colgroup>
                      <col width="5%">
                      <col width="20%">
                      <col width="55%">
                      <col width="10%">
                      <col width="10%">
                    </colgroup> -->
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Alert Type</th>
                            <th>Alert Description</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <!-- ko if:(Items().length>0) -->
                    
                   <tbody class="data-append">
                        <tr>
                            <td colspan="5" class="tac" style="height: 100px; vertical-align: middle;">Loading
                            </td>
                        </tr>
                    </tbody>
                   <!--/ko -->
                </table> 
                    </div>
                  </div>
                </div>
              </div>    
          </div>
        </div>
          
        </section>        
      </section>





<script>
    var base_url = '<?php echo base_url(); ?>';
    var cont = 'AlertType';

    
    $(document).ready(function () {
        
         
        getAlertType(base_url, cont);
 $(".reset").click(function(){
$('.error').remove();
// $('.submit').prop('disabled', false);
    });

    });





$('.input').on('keypress',function(e)  {
 if (e.which != 13  ) {

$('.error').remove();

} else {
submit();
  
}



});


    $('.submit').on('click', function () {
      submit()
     
    });


function submit() {

    
        if ($("input[name='Type']").val() == '') {
          $('.error').remove();
            $("input[name='Type']").after('<span class="error">Please fill this field</span>');
            $("input[name='Type']").focus();
        } else if ($("input[name='Desc']").val() == '') {
          $('.error').remove();
            $("input[name='Desc']").after('<span class="error">Please fill this field</span>');
        $("input[name='Desc']").focus();
        } else if(checkAlert() == 1){

          $('.error').remove();
$("#Type").after('<span class="error">Alert type already exist</span>');
$("#Type").focus();
        }
        else {
$('.submit').text('Processing');

          $('.error').remove();
        var datas = $('#alerttype-form').serialize();
        ajaxcalls(base_url, cont, datas);
//        getAlertType(base_url, cont);
        $("input[name='id']").val('0');
        $('#alerttype-form')[0].reset();
    

    }


}



    function edit(id) {
//        console.log($('#'+id).find("td:eq(1)").text());
        $("input[name='id']").val(id);
        $("input[name='Type']").val($('#' + id).find("td:eq(1)").text());
        $(window).scrollTop(0);
//         $("input[name='Desc']").val($('#' + id).find("td:eq(2)").text());
         document.getElementById("Desc").value =$('#' + id).find("td:eq(2)").text();

       
    }
    function deleteAlert(id) {
// if (confirm('Are you sure you want to Delete?')) { 
         $('#confirm').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delete', function () {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>master/" + cont + "/deleteAlerttype",
            data: "id=" + id,
            success: function (data) {
                if (data) {
                   $('#message').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delet', function () {
                   
                    window.location.reload();
                        $('#' + id).remove();
                        $("input[name='id']").val('0');
});
                        // alert('Deletes successfully');
                }
            }
        });
        });
    }



function checkAlert() {

  var returnval;
        $.ajax({
            type: "POST",
            async: false,
            url: "<?php echo base_url(); ?>master/" + cont + "/checkAlert",
            data: "type=" +$("input[name='Type']").val() + "&id=" + $("input[name='id']").val() ,
            dataType:'html',
            success: function (data) {
                if(data==1){
                returnval = data;
                }else{
                 returnval = data;
                }
            }
        });

        return returnval;
      
}

  
</script>