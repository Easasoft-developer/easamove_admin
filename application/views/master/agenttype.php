<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>


      <!-- main content -->
      <section id="main-content" class="main-content">
        <section class="surround">

        <div class="container-fluid property_listing master">
          <div class="row">
              <div class="col-xs-12">
                <div class="panel">
                  <div class="panel-heading">
                    

                    <div class="row">
                      <div class="col-sm-12">
                        <h3>Agent Type</h3>
                      </div>
                    </div>

<input type="hidden" id="checkAgent">
<form method="post" id="agenttype-form">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-horizontal">

                    <input type="hidden" name="id" value="0"/>

                          <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="Type">Agent Type</label>
                            <div class="col-sm-4 col-md-3">
                              <input type="text" id="Type" name="Type" maxlength="30" class="form-control" />
                            </div>
                          </div>




                          <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2" for="Desc">Agent Description</label>
                            <div class="col-sm-4 col-md-3">
                              <textarea id="Desc" name="Desc" class="form-control"></textarea>
                            </div>
                          </div>

                          <div class="form-group">
                            <div class="col-md-offset-5 col-sm-offset-3 col-sm-3 save-cancel">
                              <button type="button" class="btn btn-primary submit">Save</button>
                              <button class="btn btn-danger reset" type="button">Cancel</button>
                            </div>                            
                          </div>



                        </div>                        
                      </div>
                    </div>
</form>




                   
                  
                  </div>
                  <div class="">
                    <div class="table-responsive">
                    <table id="datatb" class="table table-striped table-hover ">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Agent Type</th>
                            <th>Agent Description</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    <!-- ko if:(Items().length>0) -->
                    
                    <tbody class="data-append">
                        <tr>
                            <td colspan="5" class="tac" style="height: 100px; vertical-align: middle;">Loading
                            </td>
                        </tr>
                    </tbody>
                   
                </table>
                    </div>
                  </div>
                </div>
              </div>    
          </div>
        </div>
          
        </section>        
      </section>









<script>
    var base_url = '<?php echo base_url(); ?>';
    var cont = 'AgentType';

    
    $(document).ready(function () {
        
         
        getAgentType(base_url, cont);
 $(".reset").click(function(){
$('.error').remove();
    });

    });



    $('.submit').on('click', function () {
         $('.error').remove();
        if ($("input[name='Type']").val() == '') {
            $("input[name='Type']").after('<span class="error">Please fill this field</span>');
        } else if ($("input[name='Desc']").val() == '') {
            $("input[name='Desc']").after('<span class="error">Please fill this field</span>');
        } else {
checkAgent();

console.log(checkAgent());
setTimeout(function() {  


if ($('#checkAgent').val() == 1) { 
$('.error').remove();
$("#Type").after('<span class="error">Agent type already exist</span>');

} else {
  alert();
        var datas = $('#agenttype-form').serialize();
        ajaxcalls(base_url, cont, datas);
//        getAgentType(base_url, cont);
        $("input[name='id']").val('0');
        $('#agenttype-form')[0].reset();

      }

},500);
    }
    });



    function edit(id) {
//        console.log($('#'+id).find("td:eq(1)").text());
        $("input[name='id']").val(id);
        $("input[name='Type']").val($('#' + id).find("td:eq(1)").text());
document.getElementById("Desc").value =$('#' + id).find("td:eq(2)").text();
       
    }
    function deleteAgent(id) {
if (confirm('Are you sure you want to Delete?')) { 
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>master/" + cont + "/deleteAgenttype",
            data: "id=" + id,
            success: function (data) {
                if (data) {
                    $('#' + id).remove();
                    $("input[name='id']").val('0');
                    window.location.reload();
                }
            }
        });
        }
    }

     function checkAgent() {
    $('.error').remove();
    // if($("input[name='id']").val()==0){
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>master/" + cont + "/checkAgent",
            data: "type=" +$("input[name='Type']").val() + "&id=" + $("input[name='id']").val(),
            dataType:'html',
            success: function (data) {
                if(data==1){
                  $('#checkAgent').val(data);
                     // $("input[name='Type']").after('<span class="error">Agent type already exist</span>');
                     // $('.submit').prop('disabled', true);
                }else{
                  $('#checkAgent').val(data+'gg');
                    // $("input[name='Type']").after('<span class="error"></span>');
                     // $('.submit').prop('disabled', false);
                }
            }
        });
        // }
    }
</script>