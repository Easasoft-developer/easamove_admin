<section id="main-content" class="main-content">
        <section class="surround">

        <div class="container-fluid property_listing master">
          <div class="row">
              <div class="col-xs-12">
                <div class="panel">
                  <div class="panel-heading">
                    

                    <div class="row">
                      <div class="col-sm-12">
                        <h3>State Master</h3>
                      </div>
                    </div>

<input name="checkState" type="hidden" value="">
<?php if ($access != 3) { ?>
<form method="" id="state-form">
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-horizontal">
                            <input type="hidden" name="id" value="0"/>
                          <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="Code">Country Name<span style="color: #F00"> *</span></label>
                           
                    <div class="col-sm-4 col-md-3">
                        <select class="form-control" name="CountryId" id="CountryId">
                            <option value="0">- Select -</option>
                            <?php
                            if (isset($country)) {
                                foreach ($country as $cnty) {
                                    echo '<option value="' . $cnty->country_id . '">' . $cnty->country_name . '</option>';
                                }
                            }
                            ?>

                        </select>
                    </div>


                          </div>

                          <div class="form-group">                          
                            
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2" for="Name">State Name<span style="color: #F00"> *</span></label>
                            <div class="col-sm-4 col-md-3">
                              
                              <input type="text" id="Name" name="Name" onkeypress="return onlyAlphabets(event,this);" maxlength="30" class="form-control input"/>
                            </div>
                          </div>

                          <div class="form-group">
                            <div class="col-md-offset-5 col-sm-offset-3 col-sm-3 save-cancel">
                             <button tabindex="3" type="button" id="submit" class="btn btn-default submit">Save</button>
                             <button tabindex="4" type="button" class="btn btn-danger reset">Cancel</button>
                
                            </div>                            
                          </div>


                        </div>                        
                      </div>
                    </div>
</form>
<?php } ?>
                
                  
                  </div>
                  <div class="">
                    <div class="table-responsive">
                    <table id="datatb" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>State Name</th>
                            <th>Country Name</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                 
                  <tbody class="data-append">
                        <tr>
                            <td colspan="5" class="tac" style="height: 100px; vertical-align: middle;">Loading
                            </td>
                        </tr>
                    </tbody>



                        
                      </table>
                    </div>
                  </div>
                </div>
              </div>    
          </div>
        </div>
          
        </section>        
      </section>





<script>
    var base_url = '<?php echo base_url(); ?>';

    var cont = 'State';
    $(document).ready(function () {


        getState(base_url, cont);

 $(".reset").click(function(){
$('.error').remove();
$('.submit').prop('disabled', false);
    });

    });




$('.input').on('keypress',function(e)  {
 if (e.which != 13  ) {

$('.error').remove();

} else {
submit();
  
}



});




    $('.submit').on('click', function () { submit();  });


    function submit() {

 if ($("select[name='CountryId']").val() == 0) {
          $('.error').remove();
            $("select[name='CountryId']").after('<span class="error">Please fill this field</span>');
            $("select[name='CountryId']").focus();
        }  
        else if ($("input[name='Name']").val() == '') {
          $('.error').remove();
            $("input[name='Name']").after('<span class="error">Please fill this field</span>');
            $("input[name='Name']").focus();
        } else if(checkState() == 1){
          $('.error').remove();
$("input[name='Name']").after('<span class="error">state already exist</span>');
$("input[name='Name']").focus();
}

else {
$('.submit').text('Processing');
$('.error').remove();
            var datas = $('#state-form').serialize();
            ajaxcalls(base_url, cont, datas);
//            getState(base_url, cont);
            $("input[name='id']").val('0');
            $('#state-form')[0].reset();


} 
    


    }



    function edit(id) {
//        console.log($('#'+id).find("td:eq(1)").text());
        $("input[name='id']").val(id);
        $("input[name='Name']").val($('#' + id).find("td:eq(1)").text());
//        $("#CountryId option:selected").text($('#' + id).find("td:eq(2)").text());
$(window).scrollTop(0);
        $('#CountryId option').filter(function() { 
    return ($(this).text() == $('#' + id).find("td:eq(2)").text()); //To select Blue
}).prop('selected', true);



    }
    function deleteState(id) {
  $('#confirm').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delete', function () {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>master/" + cont + "/deleteState",
            data: "id=" + id,
            success: function (data) {
                if (data) {
                   $('#message').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delet', function () {
                    window.location.reload();
                        $('#' + id).remove();
                      });
                        // alert('Deletes successfully');
                }
            }
        });
        });
    }


// $("input[name='Name']").on('keyup', function (e) {
    // $('.error').remove();
    // if($("input[name='id']").val()==0 && e.which != 13)
    // {
    //      $('.submit').prop('disabled', true);
    // }else

   function checkState() {

    var returnval;
    $id = $("input[name='id']").val();
 // if($("input[name='id']").val()==0 ){
        $.ajax({
            type: "POST",
            async: false,
            url: "<?php echo base_url(); ?>master/" + cont + "/checkState",
            data: "name=" +$("input[name='Name']").val()+'&country='+$('#CountryId').val() + "&id=" +  $id,
            dataType:'html',
            success: function (data) {
              
              console.log("data:"+ data);
                if(data==1){
                  returnval = data;
                  }
            }
        });
        return returnval;

        }


     //    checkState(function(id) {
     //  alert(id);
     // });
    // });



// $('#CountryId').on('change', function () {
  
//  if($("input[name='id']").val()==0 ){
//  $('.error').remove();
// $.ajax({
//             type: "POST",
//             url: "<?php echo base_url(); ?>master/" + cont + "/checkState",
//             data: "name=" +$("input[name='Name']").val()+'&country='+$('#CountryId').val(),
//             dataType:'html',
//             success: function (data) {
//                 if(data==1){
//                   $('.error').remove();
//                      $("input[name='Name']").after('<span class="error">State code already exist</span>');
//                      $('.submit').prop('disabled', true);
//                 }else{
//                   $('.error').remove();
//                     $("input[name='Name']").after('<span class="error"></span>');
//                      $('.submit').prop('disabled', false);
//                 }
//             }
//         });
// }
        
// });



</script>