<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>
<div class="col-md-9">
    <h3 class="masters_title">Central Heating Type</h3>
    <form method="get" id="centralHeatingType-form">
        <div class="">
            <div class="row mb10 mt10">
                <div class="col-md-12 pl5 pr0">
                    <input type="hidden" name="id" value="0"/>
                    <div class="form-group tar clearfix mb5">
                        <label class="col-sm-4 control-label" for="HeatingType">Heating Type</label>
                        <div class="col-sm-3">

                            <input type="text" id="HeatingType" name="HeatingType" class="form-control" />
                        </div>
                    </div>
                    <div class="form-group tar clearfix mb5">
                        <label class="col-sm-4 control-label" for="HeatingDesc">Heating Description</label>
                        <div class="col-sm-3">

                            <textarea id="HeatingDesc" name="HeatingDesc" class="form-control"></textarea>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6 mt10 tar">
                            <button class="btn btn-danger"  type="button"><i class="fa fa-times pr5"></i>Cancel</button>
                            <button class="btn btn-primary submit" type="button"><i class="fa fa-save pr5"></i>Save</button>
                        </div>                    
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="col-md-12" >
                    <table id="datatb" class="table table-bordered thumbnail-style mt10">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Heating Type</th>
                                <th>Heating Description</th>
                                <th>Edit</th>
                                <th>Delete</th>
                            </tr>
                        </thead>

                        <tbody class="data-append">
                        <tr>
                            <td colspan="5" class="tac" style="height: 100px; vertical-align: middle;">Loading
                            </td>
                        </tr>
                    </tbody>

                    </table>
                </div>
            </div>
        </div>
    </form>
</div>
<script>
    var base_url = '<?php echo base_url(); ?>';
    var cont = 'CentralheatingType';


    $(document).ready(function () {


        getCentralheatingType(base_url, cont);


    });
    $('.submit').on('click', function () {
        $('.error').remove();
        if ($("input[name='HeatingType']").val() == '') {
            $("input[name='HeatingType']").after('<span class="error">Please fill this field</span>');
        } else if ($("input[name='HeatingDesc']").val() == '') {
            $("input[name='HeatingDesc']").after('<span class="error">Please fill this field</span>');
        } else {
            var datas = $('#centralHeatingType-form').serialize();
            ajaxcalls(base_url, cont, datas);
            getCentralheatingType(base_url, cont);
            $("input[name='id']").val('0');
            $('#centralHeatingType-form')[0].reset();
        }
    });
    function edit(id) {
//        console.log($('#'+id).find("td:eq(1)").text());
        $("input[name='id']").val(id);
        $("input[name='HeatingType']").val($('#' + id).find("td:eq(1)").text());
        document.getElementById("HeatingDesc").value =$('#' + id).find("td:eq(2)").text();

$("body").scrollTop(0);
    }
    function deleteCentralheating(id) {
        if (confirm('Are you sure you want to Delete?')) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>master/" + cont + "/deleteCentralheatingtype",
                data: "id=" + id,
                success: function (data) {
                    if (data) {
                        $('#' + id).remove();
                        $("input[name='id']").val('0');
                    }
                }
            });
        }
    }
</script>