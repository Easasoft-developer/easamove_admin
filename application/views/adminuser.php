<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$actualurl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
?>

<nav class="subnav" role="navigation">
        <div class="container">

        <div class="row">
          <div class="col-sm-12">
            <ul class="list-inline">
               <li <?php echo (base_url('adminUser')==$actualurl)?'class=active':''; ?>><a href="<?php echo base_url();?>adminUser">Manage User</a></li>
              <li <?php echo (base_url('myprofile')==$actualurl)?'class=active':''; ?>><a href="<?php echo base_url();?>account/myprofile">My Profile</a></li>
            <li <?php echo (base_url('changePassword')==$actualurl)?'class=active':''; ?>><a href="<?php echo base_url();?>account/changePassword">Change Password</a></li>
          </ul>
          </div>
        </div>       
        
          
        </div>
      </nav>

<section id="main-content" class="main-content">
        <section class="surround">

        <div class="container-fluid property_listing master">
          <div class="row">
              <div class="col-xs-12">
                <div class="panel">
                  <div class="panel-heading">
                    


                    <div class="row">
                      <div class="col-sm-12">
                        <h3>Manage Users</h3>
                      </div>
                    </div>
<?php if ($access == 1) { ?>
                    <form method="post" id="admin-user"> 
                    <div class="row">



                <input type="hidden" name="id" value="0"/>
                    <input type="hidden" id="echeck"/>

                      <div class="col-sm-12">
                        <div class="form-horizontal">
                     

             

                          <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="name">User Name<span style="color: #F00"> *</span></label>
                            <div class="col-sm-4 col-md-3">
                     <input type="text" id="name" name="name" maxlength="50" class="form-control input" onkeypress="return onlyAlphabets(event,this);" placeholder="(max 50 Characters)"/>
                            </div>
                          </div>

                      <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="Email">User Email Id<span style="color: #F00"> *</span></label>
                            <div class="col-sm-4 col-md-3">
                     <input type="email" id="Email" name="Email" class="form-control input" />
                            </div>
                          </div>


                <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="CanRead">User Password<span style="color: #F00"> *</span></label>
                            <div class="col-sm-4 col-md-3">
                     <input type="text" id="Password" name="Password" maxlength="20" class="form-control input" value="" />
                            </div>
                          </div>






                 <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="CanRead">User Role<span style="color: #F00"> *</span></label>
                            <div class="col-sm-4 col-md-3">
                     <input type="text" id="role" name="role" class="form-control input" maxlength="20" value="" placeholder="(max 20 Characters)"/>
                            </div>
                          </div>







                          <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2" for="CanRead">Admin Access<span style="color: #F00"> *</span></label>
                            <div class="col-sm-4 col-md-3">

                             <label class="checkbox-inline"><input type="checkbox" id="CanAdd" name="permission" value="2" class="input"/>Write only</label>


                                <label class="checkbox-inline"><input type="checkbox" id="CanRead" name="permission" value="3" class="input"/>Read only</label>

                              
            
                            </div>
                          </div>



                          <div class="form-group">
                            <div class="col-md-offset-5 col-sm-offset-3 col-sm-3 save-cancel">
                              <button type="button" class="btn btn-default submit">Save</button>
                              <button type="button" class="btn btn-default btn-danger reset">Cancel</button>
                              
                            </div>                            
                          </div>


                        </div>                        
                      </div>


                    </div>
                    </form>
<?php } ?>

               
                  
                  </div>
                  <div class="">
                    <div class="table-responsive">


  <table id="datatb" class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Name</th>
                            <th>Email Id</th>
                            <th>Role</th>
                            <th>Password</th>
                            <th>Access</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>

                    <tbody class="data-append">
                        <tr>
                            <td colspan="5" class="tac" style="height: 100px; vertical-align: middle;">Loading
                            </td>
                        </tr>
                    </tbody>

                </table>

                    </div>
                  </div>
                </div>
              </div>    
          </div>
        </div>
          
        </section>        
      </section>







<script>
    var base_url = '<?php echo base_url(); ?>';
    var cont = 'AdminUser';

    $(document).ready(function () {
        var password = randanstring();
        $("#Password").val(password);
        getAdminuser(base_url, cont);

    $(".reset").click(function(){
$('.error').remove();
    });

        
    });


    $('.input').on('keypress', function (e) {
   
   if (e.which != 13) {
   $('.error').remove();
	}

  // if (e.which == 13 && !$('.submit').is('[disabled]')) {
    if (e.which == 13 && !$('.submit').is('[disabled]') ) {

    submit();
    // e.preventDefault();
    
  }
});



    $('.submit').on('click', function () {
       submit();
    });


function submit() {

 
        var emailPattern = /^[a-z0-9%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i;
var role = /^(?![0-9])[A-Za-z0-9 _.-]+/;
//        alert(echeck);

        if ($("input[name='name']").val() == '') {
            $('.error').remove();
            $("input[name='name']").after('<span class="error">Please fill this field</span>');
            $("input[name='name']").focus();
        } else if ($("input[name='Email']").val() == '') {
            $('.error').remove();
            $("input[name='Email']").after('<span class="error">Please fill this field</span>');
            $("input[name='Email']").focus();
        } else if (!emailPattern.test($("input[name='Email']").val())) {
            $('.error').remove();
            $("input[name='Email']").after('<span class="error">Please enter valid email address</span>');
            $("input[name='Email']").focus();
        } else if ($("input[name='Password']").val() == '') {
            $('.error').remove();
            $("input[name='Password']").after('<span class="error">Please fill this field</span>');
            $("input[name='Password']").focus();
        } else if ($("input[name='role']").val() == '') {
            $('.error').remove();
            $("input[name='role']").after('<span class="error">Please fill this field</span>');
        $("input[name='role']").focus();
        } else if(!role.test($("input[name='role']").val()) ) {
$('.error').remove();
          $("input[name='role']").after('<span class="error">Please enter valid admin</span>');
$("input[name='role']").focus();
        } else if ($("input[name='permission']:checked").length == 0) {
            $('.error').remove();
            $("input[name='role']").after('<span class="error">Please select one permission</span>');
        $("input[name='role']").focus();
        } else if( checkemail() == 1) {
            $('.error').remove();
            $("input[name='Email']").after('<span class="error">email address already registered</span>');
$("input[name='Email']").focus();
}

else {      

                    $('.save-cancel').append("<i id='ldr' class='fa fa-refresh fa-spin'></i>");
                    $('.error').remove();
                    var datas = $('#admin-user').serialize();

                    ajaxadmin(base_url, cont, datas);
//                    getAdminuser(base_url, cont);
                    $("input[name='id']").val('0');
                    $('#admin-user')[0].reset();
                    var password = randanstring();
                    $("#Password").val(password);




        }

}


    $("input[name='permission']").on('change', function () {
        $("input[name='permission']").not(this).prop('checked', false);
    });

    function edit(id) {
$("body").scrollTop(0);

        $("input[name='id']").val('1'); 
        console.log($("input[name='id']").val());
//        console.log($('#'+id).find("td:eq(1)").text());
        $("input[name='permission']").prop('checked', false);
        $("input[name='id']").val(id);
        $("input[name='name']").val($('#' + id).find("td:eq(1)").text());
        $("input[name='Email']").val($('#' + id).find("td:eq(2)").text());
        $("input[name='role']").val($('#' + id).find("td:eq(3)").text());
        $("input[name='Password']").val($('#' + id).find("td:eq(4)").text());
        var access = $('#' + id).find("td:eq(5)").text();
        if (access == 1) {
            $('#adminedit').prop('checked', true);
        } else if (access == 'Write') {
            $('#CanAdd').prop('checked', true);
        } else if (access == 'Read') {
            $('#CanRead').prop('checked', true);
        }
    }

    function deleteAdminuser(id) {
        // if (confirm('Are you sure you want to Delete?')) {
              $('#confirm').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delete', function () {
            
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>/" + cont + "/deleteAdminuser",
                data: "id=" + id,
                success: function (data) {
                    if (data) {
                        if (data == 1) {

//                             var oTable = $('#datatb').dataTable();
// // to reload
// oTable.api().ajax.reload()
                            $('#' + id).remove();
                            $("input[name='id']").val('0');
                             $('#message').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delet', function () {
                            window.location.reload();
});
                            // alert('Deleted successfully');
                        } else {
                            alert('Permission denied');
                        }
                    }
                }
            });
        });
    }
    function checkemail() {

        $.ajaxPrefilter(function( options, originalOptions, jqXHR ) {
    options.async = false;
});
        var returnval;
        
        $.ajax({
            type: "POST",
            async: false,
            url: "<?php echo base_url(); ?>/" + cont + "/checkemail",
            data: "email=" + $("input[name='Email']").val() + "&id=" + $("input[name='id']").val(),
            dataType: "html",
            success: function (data) {
                

                if ((data == 1)) {
                    returnval = data;
                }
                else { 
                    returnval = data;
                     }
            }
        });


return returnval;
    }


</script>
