<section id="main-content" class="main-content">
        <section class="surround">

        <div class="container-fluid property_listing master">
          <div class="row">
              <div class="col-xs-12">
                <div class="panel">
                  <div class="panel-heading">
                    

                    <div class="row">
                      <div class="col-sm-12">
                        <h3>Country Master</h3>
                      </div>
                    </div>

                    <input id="checkCountry" type="hidden">

<?php if($access != 3) { ?>
                    <form method="post" id="country-form" action=""> 
                    <div class="row">
                      <div class="col-sm-12">
                        <div class="form-horizontal">
                        <input type="hidden" name="id" value="0"/>
                          <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="Code">Country Code<span style="color: #F00"> *</span></label>
                            <div class="col-sm-4 col-md-3">
                   <input type="text" tabindex="1" class="form-control input" placeholder="Country Code" id="Code" name="Code" maxlength="30" onkeypress="return onlyAlphabets(event,this);" class="form-control"  />
                            </div>
                          </div>

                          <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2" for="Name">Country Name<span style="color: #F00"> *</span></label>
                            <div class="col-sm-4 col-md-3">
                              
                 <input type="text" class="form-control input" placeholder="Country Name" id="Name" name="Name" maxlength="30" onkeypress="return onlyAlphabets(event,this);" class="form-control"  tabindex="2" />
                            </div>
                          </div>

                          <div class="form-group">
                            <div class="col-md-offset-5 col-sm-offset-3 col-sm-3 save-cancel">
                              <button tabindex="3" type="button" class="btn btn-default submit">Save</button>
                              <button tabindex="4"  type="button" class="btn btn-default btn-danger reset">Cancel</button>
                            </div>                            
                          </div>
                        </div>                        
                      </div>
                    </div>
                    </form>


               <?php } ?>
                  
                  </div>
                  <div class="">
                    <div class="table-responsive">
                    <table id="datatb" class="table table-striped table-hover">
                        <thead>
                        <tr>
                            <th>#</th>
                            <th>Country Code</th>
                            <th>Country Name</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                        </thead>
                        <tbody class="data-append">
                        
                       <tr>
                            <td colspan="5" class="tac" style="height: 100px; vertical-align: middle;">Loading
                            </td>
                        </tr>
                        
                        </tbody>

                        
                      </table>
                    </div>
                  </div>
                </div>
              </div>    
          </div>
        </div>
          
        </section>        
      </section>





<script>
    var base_url = '<?php echo base_url(); ?>';

    var cont = 'Country';
    $(document).ready(function () {

     getCountry(base_url, cont);
// var cr = '';
  $(".reset").click(function(){
$('.error').remove();
 $('.submit').prop('disabled', false);

    });

    });


// $('#Code').on('keyup', function (e) {
  

//   if (e.which == 13 && !$('.submit').is('[disabled]')) {

//     submit();
    
//   }
//   else { $("input[name='Code']").focus(); }
  
// });



// $('#Name').on('keyup',function(e) {



//   if (e.which != 13  ) {
// $('#Name').nextAll('.error').remove();


// } else {
// 	submit();
// }



// }); 



$('.input').on('keypress',function(e)  {
 if (e.which != 13  ) {

$('.error').remove();

} else {
submit();
  
}



});





    $('.submit').on('click', function () {  submit(); });

    function submit() {
        if ($("input[name='Code']").val() == '') {
          $('.error').remove();
            $("input[name='Code']").after('<span class="error">Please fill this field</span>');
            $("input[name='Code']").focus();
        } else if ($("input[name='Name']").val() == '') {
          $('.error').remove();
            $("input[name='Name']").after('<span class="error">Please fill this field</span>');
            $("input[name='Name']").focus();
        } else {

checkCountry();
     setTimeout(function () {

          if($("#checkCountry").val() == 1 ) {
            $('.error').remove();
$("input[name='Code']").after('<span class="error">Country code already exist</span>');
          } 
          else {
          $('.error').remove();
            var datas = $('#country-form').serialize();
            ajaxcalls(base_url, cont, datas);
//            getCountry(base_url, cont);
            $("input[name='id']").val('0');
            $('#country-form')[0].reset();
          }
        }, 500);


    
}
}

    function edit(id) {
//        console.log($('#'+id).find("td:eq(1)").text());
        $("input[name='id']").val(id);
        $("input[name='Code']").val($('#' + id).find("td:eq(1)").text());
        cr = $("input[name='Code']").val($('#' + id).find("td:eq(1)").text());
        $("input[name='Name']").val($('#' + id).find("td:eq(2)").text());

     $(window).scrollTop(0);

    }
    function deleteCountry(id) {
        if (confirm('Are you sure you want to Delete?')) {
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>master/" + cont + "/deleteCountry",
                data: "id=" + id,
                success: function (data) {
                    if (data) {
                        
                        window.location.reload();
                        $('#' + id).remove();
                        alert('Deletes successfully');


                    }

                }
            });

            
        }

    }

function checkCountry() {
  var cr = '';
     // if($("input[name='id']").val()==0 ){
      $id = $("input[name='id']").val();
      // $cr = $("input[name='Code']").val($('#' + id).find("td:eq(1)").text());
    // $('.error').remove();
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>master/" + cont + "/checkCountry",
            data: "code=" +$("input[name='Code']").val() + "&id="+$id + "&cr="+ cr,
            dataType:'html',
            success: function (data) {
              
                if(data==1){
                  $("#checkCountry").val(data);
                  // $('.error').remove();
                     // $("input[name='Code']").after('<span class="error">Country code already exist</span>');
                     // $('.submit').prop('disabled', true);
                }else{
                  $("#checkCountry").val('');
                    // $("input[name='Code']").after('<span class="error"></span>');
                     // $('.submit').prop('disabled', false);
                }
            }
        });
        // }
    }


</script>



