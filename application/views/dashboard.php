<?php
include("common/header.php");
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$this->load->library('session');
$svar = $this->session->userdata['adminuser'];
$name = $svar['name'];
$access = $svar['access'];
?>

<!-- main content -->
<section class="main-content">
    <section class="surround container-fluid dashb">

      <!--   <div class="row">
            <div class="col-sm-2 col-xs-12">
                <div class="page-header">
                    <h2>Dashboard</h2>
                </div>                
            </div>

            <div class="col-lg-7 col-md-8 col-lg-offset-3 col-md-offset-2 col-xs-12">
                <div class="page-header">
                    <h2 class="gorangates" style="text-transform: capitalize;"><small>Welcome </small><?php echo $adminname[0]->admin_name; ?></h2>
                </div>
            </div>
        </div> -->

        <!-- <div class="row">
            <div class="col-sm-12">
                <div class="border-head">
                    <h4>Status</h4>
                </div>
            </div>
        </div> -->

        <div class="row stats" style="margin-top:20px;">
            <div class="col-lg-3 col-sm-6">
                <section class="panel">
                <a href="<?php echo base_url('agent/agent');?>">
                    <div class="symbol color1">
                        <i class="fa fa-building-o"></i>
                    </div>
                    <div class="value">
                        <h1 class="count"><?php echo $agentcount; ?></h1>
                        <p>No of Agents</p>
                    </div>
                    </a>
                </section>
            </div>
            <div class="col-lg-3 col-sm-6">
                <section class="panel">
                <a href="<?php echo base_url('adminUser');?>">
                    <div class="symbol color2">
                        <i class="fa fa-building"></i>
                    </div>
                    <div class="value">
                        <h1 class="count2"><?php echo $admincount; ?></h1>
                        <p>No of Admin Users</p>
                    </div>
                    </a>
                </section>
            </div>
            <div class="col-lg-3 col-sm-6">
                <section class="panel">
                    <div class="symbol color3">
                        <i class="fa fa-money"></i>
                    </div>
                    <div class="value">
                        <h1 class=" count3">0</h1>
                        <p>Sold House</p>
                    </div>
                </section>
            </div>
            <div class="col-lg-3 col-sm-6">
                <section class="panel">
                    <div class="symbol color4">
                        <i class="fa fa-envelope"></i>
                    </div>
                    <div class="value">
                        <h1 class=" count4">0</h1>
                        <p>No of Email Leads</p>
                    </div>
                </section>
            </div>
        </div> 
        <div class="row">
            <div class="col-lg-4">
                <!--user info table start-->
                <section class="panel">
                    <div class="panel-body">
<!--                        <a href="#" class="task-thumb">
                            <img src="images/avatar.png" alt="">
                        </a>-->
                        <div class="task-progress">
                            <h1><a href="#">Leadership Board</a></h1>
                            
                        </div>
                    </div>
                    <table class="table table-hover personal-task">
                        <thead>
                            <tr>
                                <th>S.No</th>
                                <th>Agent Name</th>
                                <th>Property's</th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php 

          $i = 0;
          foreach ($agent_names->result() as $key => $value) {
            
          echo "<tr><td>".($i+1)."</td><td>$value->agent_name</td><td>$agent_cnt[$i]</td></tr>";
          $i++;
        } ?>

                        </tbody>
                    </table>
                </section>
                <!--user info table end-->
            </div>
            <div class="col-lg-8">
                <!--work progress start-->
                <section class="panel">
                    <div class="panel-body progress-panel">
                        <div class="task-progress">
                            <h1>Property Status</h1>
                            <!--<p>Bhooshan</p>-->
                        </div>
<!--                        <div class="task-option">
                            <select class="styled">
                                <option>Bhooshan</option>
                                <option>Menon</option>
                                <option>Jats</option>
                            </select>
                        </div>-->
                    </div>
                    <table class="table table-hover personal-task">
                        <tbody>
                            <tr>
                                <td>1</td>
                                <td>
                                    Target Sell
                                </td>
                                <td>
                                    <span class="badge bg-important">75%</span>
                                </td>
                            </tr>
                            <tr>
                                <td>2</td>
                                <td>
                                    Product Delivery
                                </td>
                                <td>
                                    <span class="badge bg-success">43%</span>
                                </td>
                            </tr>
                            <tr>
                                <td>3</td>
                                <td>
                                    Payment Collection
                                </td>
                                <td>
                                    <span class="badge bg-info">67%</span>
                                </td>
                            </tr>
                            <tr>
                                <td>4</td>
                                <td>
                                    Work Progress
                                </td>
                                <td>
                                    <span class="badge bg-warning">30%</span>
                                </td>
                            </tr>
                            <tr>
                                <td>5</td>
                                <td>
                                    Delivery Pending
                                </td>
                                <td>
                                    <span class="badge label-primary">15%</span>
                                </td>

                            </tr>
                        </tbody>
                    </table>
                </section>
                <!--work progress end-->
            </div>

        </div>
        <div  class="row">
            <section class="panel">
                <div class="panel-body">
                    <div>
                        <h2>Agent Progress</h2>
                    </div>

                    <div id="linechart_material" style="height: 400px; margin: 0 auto">
                        
                    </div>
            </section>
        </div>

    </section>        
</section>



<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<!--<script src="js/bootstrap.min.js"></script>-->
<script type="text/javascript" src="https://www.google.com/jsapi"></script>


<script type="text/javascript">
    google.load('visualization', '1.1', {packages: ['line']});
    google.setOnLoadCallback(drawChart);

    function drawChart() {

      var data = new google.visualization.DataTable();
      data.addColumn('number', 'Day');
      data.addColumn('number', 'Sale');
      data.addColumn('number', 'Rent');
      data.addColumn('number', 'Email Leads');

      data.addRows([
        <?php echo "
        [1,  $agent_sale[0],  $agent_sale[0], $agent_mail[0] ],
        [2,  $agent_sale[1],  $agent_sale[1], $agent_mail[1]],
        [3,  $agent_sale[2],    $agent_sale[2], $agent_mail[2]],
        [4,  $agent_sale[3],  $agent_sale[3], $agent_mail[3]],
        [5,  $agent_sale[4],  $agent_sale[4], $agent_mail[4]],
        [6,   $agent_sale[5],  $agent_sale[5],  $agent_mail[5]],
        [7,   $agent_sale[6],  $agent_sale[6],  $agent_mail[6]],
        [8,  $agent_sale[7],  $agent_sale[7], $agent_mail[7]],
        [9,  $agent_sale[8],  $agent_sale[8], $agent_mail[8]],
        [10, $agent_sale[9],  $agent_sale[9], $agent_mail[9]],
        [11,  $agent_sale[10],   $agent_sale[10],  $agent_mail[10]],
        [12,  $agent_sale[11],   $agent_sale[11],  $agent_mail[11]],
        [13,  $agent_sale[12],   $agent_sale[12],  $agent_mail[12]],
        [14,  $agent_sale[13],   $agent_sale[13],  $agent_mail[13]] "; ?>

      ]);

      var options = {
        chart: {
          title: '(Easamove - report)',
          subtitle: '(write optional)'
        },
        height: 400
      };

      var chart = new google.charts.Line(document.getElementById('linechart_material'));

      chart.draw(data, options);
    }
  </script>

  <script>
    $( window ).scroll(function() {

   
      if ($(window).scrollTop()) {
        $("header").addClass("border");
      }
      else{
        $("header").removeClass("border");
      }      
    });
  </script>

<?php
include("common/footer.php");
?>

