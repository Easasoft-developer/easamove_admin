<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>






<section id="main-content" class="main-content">
        <section class="surround">

        <div class="container-fluid property_listing master">
          <div class="row">
              <div class="col-xs-12">
                <div class="panel">
                  <div class="panel-heading">
                    


                    <div class="row">
                      <div class="col-sm-12">
                        <h3>Furnishes</h3>
                      </div>
                    </div>

<input type="hidden" id="checkFurnishes">
<?php if ($access != 3) { ?>

                    <form method="post" id="furnish-form"> 
                    <div class="row">

                      <div class="col-sm-12">
                        <div class="form-horizontal">
                     
             
             <input type="hidden" name="id" value="0"/>
                   <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="FurnishType">Furnish Type<span style="color: #F00"> *</span></label>
                            <div class="col-sm-4 col-md-3">
                         <input maxlength="50" onkeypress="return onlyAlphabets(event,this);" type="text" id="FurnishType" name="FurnishType" class="form-control input" />
                            </div>
                          </div>



                <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="FurnishType">Furnish Description</label>
                            <div class="col-sm-4 col-md-3">
                        <textarea maxlength="60" id="FurnishDesc" name="FurnishDesc" class="form-control input"></textarea>
                            </div>
                          </div>





                          <div class="form-group">
                            <div class="col-md-offset-5 col-sm-offset-3 col-sm-3 save-cancel">
                              <button type="button" class="btn btn-default submit">Save</button>
                              <button type="button" class="btn btn-default btn-danger reset">Cancel</button>
                            </div>                            
                          </div>


                        </div>                        
                      </div>


                    </div>
                    </form>
<?php } ?>

               
                  
                  </div>

                  <div class="">
                  <div class="preventdel" style="display: none;"></div>
                    <div class="table-responsive">




  <table id="datatb" class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Furnish Type</th>
                            <th>Furnish Descritpion</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    
                   <tbody class="data-append">
                        <tr>
                            <td colspan="5" class="tac" style="height: 100px; vertical-align: middle;">Loading
                            </td>
                        </tr>
                    </tbody>
                   
                </table>


                    </div>
                  </div>

                  
                </div>
              </div>    
          </div>
        </div>
          
        </section>        
      </section>









<script>
    var base_url = '<?php echo base_url(); ?>';
    var cont = 'Furnishes';

    
    $(document).ready(function () {
        
         
        getFurnishes(base_url, cont);

 $(".reset").click(function(){
$('.error').remove();
    });
    });



$('.input').on('keypress', function (e) {
   
  // if (e.which == 13 && !$('.submit').is('[disabled]')) {
    if (e.which == 13 && !$('.submit').is('[disabled]') ) {
    submit();
    // e.preventDefault();
    
  }
});

    $('.submit').on('click', function () {

         submit();

    });

function submit() {



      if ($("input[name='FurnishType']").val() == '') {
            $('.error').remove();
            $("input[name='FurnishType']").after('<span class="error">Please fill this field</span>');
            $("input[name='FurnishType']").focus();
        } else if ($("input[name='FurnishDesc']").val() == '') {
            $('.error').remove();
            $("input[name='FurnishDesc']").after('<span class="error">Please fill this field</span>');
        $("input[name='FurnishDesc']").focus();
        } else if(checkFurnishes() == 1){
            $('.error').remove();
        
$("input[name='FurnishType']").after('<span class="error">Furnish Type already exist</span>');
$("input[name='FurnishType']").focus(); 
}

else {
  $('.error').remove();
  var datas = $('#furnish-form').serialize();
        ajaxcallsfeatures(base_url, cont, datas);
        getFurnishes(base_url, cont);
        $("input[name='id']").val('0');
        $('#furnish-form')[0].reset();





    }
}




function df() {
    $(window).scrollTop(0);
 $('.preventdel').text('This is in use, cannot delete');
  $('.preventdel').removeAttr('style');
        setTimeout(function() { 
$('.preventdel').css("display","none");
        },3000);
    }




    function edit(id) {
//        console.log($('#'+id).find("td:eq(1)").text());
        $("input[name='id']").val(id);
        $("input[name='FurnishType']").val($('#' + id).find("td:eq(1)").text());
        document.getElementById("FurnishDesc").value =$('#' + id).find("td:eq(2)").text();
        $(window).scrollTop(0);
    }
    function deleteFurnishes(id) {
// if (confirm('Are you sure you want to Delete?')) { 
            $('#confirm').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delete', function () {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>featuremaster/" + cont + "/deleteFurnishes",
            data: "id=" + id,
            success: function (data) {
                if (data) {

                   $('#message').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delet', function () {
                     window.location.reload();
                        $('#' + id).remove();
                      });

                        // alert('Deletes successfully');
                }
            }
        });
        });
    }
    
        
        function checkFurnishes() {
        var returnval;
            $.ajax({
                type: "POST",
                async: false,
                url: "<?php echo base_url(); ?>featuremaster/" + cont + "/checkFurnishes",
                data: "type=" + $("input[name='FurnishType']").val() + "&id=" + $("input[name='id']").val(),
                dataType: 'html',
                success: function (data) {
                    if (data == 1) {

                      returnval = data;
                    } else {
                    returnval = data;
                    }
                }
            });
        
        return returnval;
      }
    
</script>