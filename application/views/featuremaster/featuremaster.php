<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
 $actualurl="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

?>

             <nav class="subnav" role="navigation">
        <div class="container">

        <div class="row">
          <div class="col-sm-12">
            <ul class="list-inline">
            <li <?php echo (base_url('featuremaster/outsideSpace')==$actualurl)?'class=active':''; ?>>
                <a href="<?php echo base_url();?>featuremaster/outsideSpace">Outside space</a>
            </li>
           
            <li <?php echo (base_url('featuremaster/parking')==$actualurl)?'class=active':''; ?>>
                <a href="<?php echo base_url();?>featuremaster/parking">Parking</a>
            </li>
            <li <?php echo (base_url('featuremaster/heatingType')==$actualurl)?'class=active':''; ?>>
                <a href="<?php echo base_url();?>featuremaster/heatingType">Heating type</a>
            </li>
            <li <?php echo (base_url('featuremaster/specialFeature')==$actualurl)?'class=active':''; ?>>
                <a href="<?php echo base_url();?>featuremaster/specialFeature">Special features</a>
            </li>
            <li <?php echo (base_url('featuremaster/furnishes')==$actualurl)?'class=active':''; ?>>
                <a href="<?php echo base_url();?>featuremaster/furnishes">Furnishing</a>
            </li>
            <li <?php echo (base_url('featuremaster/accessibility')==$actualurl)?'class=active':''; ?>>
                <a href="<?php echo base_url();?>featuremaster/accessibility">Accessibility</a>
            </li>
            <li <?php echo (base_url('featuremaster/contentMaster')==$actualurl)?'class=active':''; ?>>
                <a href="<?php echo base_url();?>featuremaster/contentMaster">Content Master</a>
                
            </li>
             </ul>
          </div>
        </div>       
        
          
        </div>
      </nav>
