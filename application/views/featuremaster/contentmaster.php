<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

?>






<section id="main-content" class="main-content">
        <section class="surround">

        <div class="container-fluid property_listing master">
          <div class="row">
              <div class="col-xs-12">
                <div class="panel">
                  <div class="panel-heading">
                    


                    <div class="row">
                      <div class="col-sm-12">
                        <h3>Content Master</h3>
                      </div>
                    </div>
                    <input type="hidden" id="checkContentmaster">

<?php if ($access != 3) { ?>
                    <form method="post" id="content-form"> 
                    <div class="row">

                      <div class="col-sm-12">
                        <div class="form-horizontal">
                     
             
             <input type="hidden" name="id" value="0"/>
                   <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="content">Content Type<span style="color: #F00"> *</span></label>
                            <div class="col-sm-4 col-md-3">
                         <input maxlength="30" onkeypress="return onlyAlphabets(event,this);"type="text" id="content" name="content" class="form-control input" />
                            </div>
                          </div>



                <div class="form-group">                          
                            <label class="control-label col-md-offset-3 col-sm-3 col-md-2 " for="Desc">Content Description</label>
                            <div class="col-sm-4 col-md-3">
                        <textarea maxlength="60" id="Desc" name="Desc" class="form-control input"></textarea>
                            </div>
                          </div>



                          <div class="form-group">
                            <div class="col-md-offset-5 col-sm-offset-3 col-sm-3 save-cancel">
                              <button type="button" class="btn btn-default submit">Save</button>
                              <button type="button" class="btn btn-default btn-danger reset">Cancel</button>
                            </div>                            
                          </div>


                        </div>                        
                      </div>


                    </div>
                    </form>


               <?php } ?>
                  
                  </div>

                  <div class="">
                  <div class="preventdel" style="display: none;"></div>
                    <div class="table-responsive">


<table id="datatb" class="table table-striped table-hover">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Content Type</th>
                            <th>Content Descritpion</th>
                            <th>Edit</th>
                            <th>Delete</th>
                        </tr>
                    </thead>
                    
                    
                   <tbody class="data-append">
                        <tr>
                            <td colspan="5" class="tac" style="height: 100px; vertical-align: middle;">Loading
                            </td>
                        </tr>
                    </tbody>
                   
                </table>

                    </div>
                  </div>

                  
                </div>
              </div>    
          </div>
        </div>
          
        </section>        
      </section>










<script>
    var base_url = '<?php echo base_url(); ?>';
    var cont = 'ContentMaster';

    
    $(document).ready(function () {
        
         
        getContentmaster(base_url, cont);

 $(".reset").click(function(){
$('.error').remove();
    });
    });



$('.input').on('keypress',function(e)  {

 if (e.which != 13  ) {

$('.error').remove();

} else {
e.preventDefault();
submit();
  
}




});


    $('.submit').on('click', function () {
         
submit();
    });


function submit() {

     if ($("input[name='content']").val() == '') {
            $('.error').remove();
            $("input[name='content']").after('<span class="error">Please fill this field</span>');
            $("input[name='content']").focus();
        } else if ($("input[name='Desc']").val() == '') {
            $('.error').remove();
            $("input[name='Desc']").after('<span class="error">Please fill this field</span>');
            $("input[name='Desc']").focus();
        } else if(checkContentmaster() == 1) {

 $('.error').remove();
  $("input[name='content']").after('<span class="error">Content type already exist</span>');
  $("input[name='content']").focus();
                       
}
else {

            $('.error').remove();
        var datas = $('#content-form').serialize();
        ajaxcallsfeatures(base_url, cont, datas);
        getContentmaster(base_url, cont);
        $("input[name='id']").val('0');
        $('#content-form')[0].reset();






    }
}




function df() {
    $(window).scrollTop(0);
 $('.preventdel').text('This is in use, cannot delete');
  $('.preventdel').removeAttr('style');
        setTimeout(function() { 
$('.preventdel').css("display","none");
        },3000);
    }


    function edit(id) {
//        console.log($('#'+id).find("td:eq(1)").text());
        $("input[name='id']").val(id);
        $("input[name='content']").val($('#' + id).find("td:eq(1)").text());
        document.getElementById("Desc").value =$('#' + id).find("td:eq(2)").text();
        $(window).scrollTop(0);
    }
    function deleteContentmaster(id) {
// if (confirm('Are you sure you want to Delete?')) { 
       $('#confirm').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delete', function () {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>featuremaster/" + cont + "/deleteContentmaster",
            data: "id=" + id,
            success: function (data) {
                if (data) {
                    $('#message').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delet', function () {
                     window.location.reload();
                        $('#' + id).remove();
                        // alert('Deletes successfully');
                      });
                }
            }
        });
        });
    }
    

    function checkContentmaster() {
        
        var returnval;
            $.ajax({
                type: "POST",
                async: false,
                url: "<?php echo base_url(); ?>featuremaster/" + cont + "/checkContentmaster",
                data: "type=" + $("input[name='content']").val() + "&id=" + $("input[name='id']").val(),
                dataType: 'html',
                success: function (data) {
                    if (data == 1) {
                       
                       returnval = data;
                    } else {
                       returnval = data;
                    }
                }
            });
        
return returnval;

    }
</script>