<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>


<style> 
@media print
{
    /*footer, header, .sidebar{ display:none; }*/
    table {
    border-collapse: collapse;
    width: 120%;
}

}  
</style>

<script type="text/javascript">
      function printDiv() {
      var divToPrint = document.getElementById('datatb');
      newWin = window.open("");
      newWin.document.write(divToPrint.outerHTML);
      newWin.print();
      newWin.close();
   }
</script>






 <!-- main content -->
      <section id="main-content" class="main-content">
        <section class="surround">



        <div class="container-fluid property_listing">
          <div class="row">
              <div class="col-xs-12">
                <div class="panel">
                  <div class="panel-heading">
                    

                    <div class="row">
                      <div class="col-sm-12">
                        <h3>View Agent</h3>
                      </div>
                    </div>


                    <div class="row sel-sear-btn">
                      
                      <div class="col-md-12 hidden-xs">
                          <div class="btn-group bnpad">
                        
                          <button type="button" onclick="location.href='<?php echo base_url(); ?>agent/agent/agentAdd';" class="btn btn-default Myclr"  <?php echo ($access == 3) ? 'disabled' : ''; ?>><i class="glyphicon glyphicon-plus"></i> <span class="hidden-xs">Add New Agent</span></button>
                          <button id="showmap" type="button" class="btn btn-default"><i class="fa fa-map-marker pr5"></i> <span class="hidden-xs">Show Map</span></button>
                          <button id="hidemap" type="button" class="btn btn-default"><i class="fa fa-map-marker pr5"></i> <span class="hidden-xs">Hide map</span></button>
                          <button type="button" onclick="printpage();" class="btn btn-default"><i class="fa fa-print"></i> <span class="hidden-xs">Print</span></button>
                          <button type="button" onclick="downloadpdf();" class="btn btn-default"><i class="glyphicon glyphicon-download-alt"></i> <span class="hidden-xs">Save as PDF</span></button>
                          <button type="button" onclick="exports()" class="btn btn-default"><i class="fa fa-external-link"></i> <span class="hidden-xs">Export to Excel</span></button>
                        
                          </div>
                      </div>


    <div class="btn-group col-xs-2 hidden-sm hidden-md hidden-lg">
                        <button type="button" class="btn dropdown-toggle pull-right" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                          <i class="fa fa-ellipsis-v fa-lg"></i>
                          <span class="sr-only">Toggle Dropdown</span>
                        </button>
                        <ul class="dropdown-menu">
                          <li><a href="add_property.html"><i class="glyphicon glyphicon-plus"></i> <span>Add New Agent</span></a></li>
                          <!-- <li><a href="#"><i class="fa fa-map"></i> <span>Show Map</span></a></li> -->
                          <li><a href="#" onclick="printpage();"><i class="fa fa-print"></i> <span>Print</span></a></li>
                          <li><a href="#"><i class="glyphicon glyphicon-download-alt"></i> <span>Save as PDF</span></a></li>
                          <li><a href="#"><i class="fa fa-external-link"></i> <span>Export to Excel</span></a></li>
                        </ul>
                      </div>

                      
                      
                    </div>
                    

                  </div>


 <div class="row mb10" id="loadmap" style="margin: 0;">
                <div class="col-md-12" id="map" style="width: 100%; height: 350px; border: solid 1px #CCCCCC;">
                </div>
            </div>

                  <div class="">
                    <div class="table-responsive">


                    <table id="datatb" class="table table-striped table-hover" id="no-more-tables">
                        <colgroup>
                            <col width="3%" />
                            <col width="12%" />
                            <col width="8%" />
                            <col width="10%" />
                            <col width="10%" />
                            <col width="12%" />
                            <col width="10%" />
                            <col width="10%" />
                            <col width="8%" />
                            <col width="5%" />
                            <col width="5%" />
                        </colgroup>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Ref no</th>
                                <th>Name</th>   
                                <th>Post code</th>                     
                                <th>Address</th>
                                <th>Agent type</th>
                                <th>Email</th>
                                <th>Telephone</th>
                                <th>Branches / Users</th>                
                                <th>Edit</th>
                                <th>Delete</th>

                            </tr>
                        </thead>

                        
                        <tbody class="data-append">
                            <tr>
                                <td colspan="14" class="tac" style="height:100px; vertical-align:middle;">
                                  
                                    Loading..
                                </td>
                            </tr>
                        </tbody>

                    </table>

                    </div>
                  </div>
                </div>
              </div>    
          </div>
        </div>
          
        </section>        
      </section>


<script src="http://maps.google.com/maps/api/js?sensor=false" type="text/javascript"></script>

<script>
var base_url = '<?php echo base_url(); ?>';
var cont = 'Agent';
                        $(document).ready(function () {
                            googleMap();
                            $('#hidemap').hide();
                            $('#loadmap').hide();
                            $('#showmap').click(function () {
                                $('#loadmap').show();
                                $('#showmap').hide();
                                $('#hidemap').show();
                                googleMap();
                            });
                            $('#hidemap').click(function () {
                                $('#loadmap').hide();
                                $('#hidemap').hide();
                                $('#showmap').show();
                            });

                        });
                        var locations = new Array();
                        function googleMap() {
//        getAgentlatlong(base_url, cont);
//         var locations = [
//      ['Bondi Beach', -33.890542, 151.274856, 4],
//      ['Coogee Beach', -33.923036, 151.259052, 5],
//      ['Cronulla Beach', -34.028249, 151.157507, 3],
//      ['Manly Beach', -33.80010128657071, 151.28747820854187, 2],
//      ['Maroubra Beach', -33.950198, 151.259302, 1]
//    ];

                            getAgentlatlong(base_url, cont);
                            console.log(locations);

                            var map = new google.maps.Map(document.getElementById('map'), {
                                zoom: 10,
                                center: new google.maps.LatLng(51.5000, 0.1167),
                                mapTypeId: google.maps.MapTypeId.ROADMAP
                            });

                            var infowindow = new google.maps.InfoWindow();

                            var marker, i;
                            var boundsew = new google.maps.LatLngBounds();
                            for (i = 0; i < locations.length; i++) {
                                marker = new google.maps.Marker({
                                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                                    map: map,
                                    icon: base_url + 'images/map-icon.png',
                                });
                                boundsew.extend(marker.getPosition());
                                map.fitBounds(boundsew);
                                var latLng = marker.getPosition(); // returns LatLng object
                                map.setCenter(latLng);
                                
                                google.maps.event.addListener(marker, 'click', (function (marker, i) {
                                    return function () {
//                                        infowindow.setContent(locations[i][0]);
                                        infowindow = new google.maps.InfoWindow({
                                            content: locations[i][0],
                                            maxWidth: 200
                                        });
                                        infowindow.open(map, marker);
                                    }
                                })(marker, i));
                            }

                            style_infowindow();


                        }

                        function style_infowindow() {

                            /* Since this div is in a position prior to .gm-div style-iw.
                             * We use jQuery and create a iwBackground variable,
                             * and took advantage of the existing reference .gm-style-iw for the previous div with .prev().
                             */

                            var iwOuter = $('.gm-style-iw');
                            var iwBackground = iwOuter.prev();
                            console.log("fsdfasd",iwBackground);

                            // Removes background shadow DIV
                            iwBackground.children(':nth-child(2)').css({'display': 'none'});

                            // Removes white background DIV
                            iwBackground.children(':nth-child(4)').css({'display': 'none'});

                            // Moves the infowindow 115px to the right.
                            // iwOuter.parent().parent().css({left: '18px'});
                            // iwOuter.find('div:nth-child(1)').css({'width': '100%', 'max-width': 'unset', 'max-height': 'unset'});

                            // Moves the shadow of the arrow 76px to the left margin.
                            iwBackground.children(':nth-child(1)').css('display', 'none');

                            // Moves the arrow 76px to the left margin.
                            iwBackground.children(':nth-child(3)').attr('style', function (i, s) {
                                return s + 'left: 95px !important;'
                            });

                            iwBackground.children(':nth-child(3)').find('div:nth-child(1)').children(':nth-child(1)').css('display', 'none');

                            iwBackground.children(':nth-child(3)').find('div:nth-child(2)').css({'z-index': '1', 'top': '18px', 'left': '-18px', 'height': '10px', 'width': '16px'});

                            // Changes the desired tail shadow color.
                            iwBackground.children(':nth-child(3)').find('div:nth-child(2)').children(':nth-child(1)').css({'z-index': '-1', 'transform': 'none', 'height': '0px', 'width': '0px', 'border': '8px solid rgb(122, 122, 122)', 'top': '-1px', 'border-bottom': '0', 'border-right-color': 'transparent', 'border-left-color': 'transparent', 'background': 'transparent', 'box-shadow': 'none'});

                            // Reference to the div that groups the close button elements.
                            var iwCloseBtn = iwOuter.next();

                            // Apply the desired effect to the close button
                            iwCloseBtn.css({opacity: '1', width:'15px', height:'15px', right: '40px', top: '26px', border: '1px solid rgb(255, 255, 255)', 'border-radius': '13px', 'box-shadow': '-1px 2px 15px 1px rgb(52, 52, 52)'});

                            // The API automatically applies 0.7 opacity to the button after the mouseout event. This function reverses this event to the desired value.
                            iwCloseBtn.mouseout(function () {
                                $(this).css({opacity: '1'});
                            });
                        }
                        


                        $(document).ready(function () {


                            getAgent(base_url, cont);


                        });
                        function getAgentlatlong(base_url, cont) { 
                            $.ajax({
                                type: "POST",
                                url: base_url + "agent/" + cont + "/getAgentall",
                                dataType: 'json',
                                success: function (data) {
                                    console.log(data);
                                    $(data).each(function (i, res) {
                                        locstret='<div id="content" class="infowindow">' +
                                                        '<p>'+res.agent_name+'</p>' +
                                                        '<img src="'+base_url+'images/'+res.agent_logo+'" class="img-responsive"/>'+
                                                        '</div>';                                       

                                         locations[i] = [locstret, res.agent_lat, res.agent_long];
                                    });
                                }
                            });
                        }




                        function printpage() {
                            //var url = base_url + "properties/property/export";
                            var sel = $('.search-prop').val();
                            var userid = "<?php echo $this->session->userdata['adminuser']['id']; ?>";
                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth() + 1; //January is 0!

                            var yyyy = today.getFullYear();
                            if (dd < 10) {
                                dd = '0' + dd
                            }
                            if (mm < 10) {
                                mm = '0' + mm
                            }
                            var today = yyyy + '-' + mm + '-' + dd;
                            var openurl = base_url + 'images/agentdata_' + userid + '_' + today + '.pdf';
                            $.ajax({
                                url: base_url + "agent/agent/getAgentData",
                                type: "POST",
                                data: 'sel=' + sel,
                                success: function (data) {

                                    window.open(openurl, '_blank');
                                }
                            });
  //                                        window.print();
                        }

                        function downloadpdf() {
                            var sel = $('.search-prop').val();

                            var userid = "<?php echo $this->session->userdata['adminuser']['id']; ?>";
                            var today = new Date();
                            var dd = today.getDate();
                            var mm = today.getMonth() + 1; //January is 0!

                            var yyyy = today.getFullYear();
                            if (dd < 10) {
                                dd = '0' + dd
                            }
                            if (mm < 10) {
                                mm = '0' + mm
                            }
                            var today = yyyy + '-' + mm + '-' + dd;
                            var openurl = base_url + 'images/agentdata_' + userid + '_' + today + '.pdf';
                            console.log(sel);
                            $.ajax({
                                url: base_url + "agent/agent/getAgentData",
                                type: "POST",
                                data: 'sel=' + sel,
                                success: function (data) {
                                    window.open(openurl, '_blank');
                                }
                            });

                        }



                        function exports() {
                            var url = base_url + "agent/" + cont + "/export";
                            window.open(url, '_blank');
                            //window.print();
                        }

                        function deleteAgent(id) {
                            // if (confirm('Are you sure you want to Delete?')) {
                                  $('#confirm').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delete', function () {
                                $.ajax({
                                    type: "POST",
                                    url: "<?php echo base_url(); ?>agent/" + cont + "/deleteAgent",
                                    data: "id=" + id,
                                    success: function (data) {
                                        if (data) {
                                              $('#confirm').modal({ backdrop: 'static', keyboard: false })
        .one('click', '#delete', function () {
                                            $('#' + id).remove();
                                            $("input[name='id']").val('0');
                                            window.location.reload();
                                        });
                                            // alert('Deleted successfully');

                                        }
                                    }
                                });
                            });
                        }
</script>