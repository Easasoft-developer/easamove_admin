<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
?>


<style> 
@media print
{
    /*footer, header, .sidebar{ display:none; }*/
    table {
    border-collapse: collapse;
    width: 120%;
}

}  
</style>

<script type="text/javascript">
      function printDiv() {
      var divToPrint = document.getElementById('datatb');
      newWin = window.open("");
      newWin.document.write(divToPrint.outerHTML);
      newWin.print();
      newWin.close();
   }
</script>






 <!-- main content -->
      <section id="main-content" class="main-content">
        <section class="surround">



        <div class="container-fluid property_listing">
          <div class="row">
              <div class="col-xs-12">
                <div class="panel">
                  <div class="panel-heading">
                    

                    <div class="row">
                      <div class="col-sm-12">
                        <h3>Agent Branch List
                        <a href="javascript:void(0);" class="back_to_search" onclick="window.history.go(-1);">Back to list</a></h3>
                      </div>
                    </div>
                  </div>

                  <div class="">
                    <div class="table-responsive">


                    <table id="datatb" class="table table-striped table-hover" id="no-more-tables">
                        <colgroup>
                            <col width="3%" />
                            <col width="15%" />
                            <col width="20%" />
                            <col width="15%" />
                            <col width="15%" />
                            <col width="10%" />
                            <col width="10%" />
                            <col width="5%" />
                            <col width="5%" />
                        </colgroup>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>                        
                                <th>Address</th>
                                <th>Agent type</th>
                                <th>Email</th>
                                <th>Telephone</th>
                                <th>Branche Users</th>                
                                <th>Edit</th>
                                <th>Delete</th>

                            </tr>
                        </thead>

                        <!-- ko if:(Agents().length===0) -->
                        <tbody class="data-append">
                            <tr>
                                <td colspan="14" class="tac" style="height:100px; vertical-align:middle;">
                                    <!-- No agents found ! Please refine your filter or <a href="#" >Click here to cancel</a> -->
                                    Loading..
                                </td>
                            </tr>
                        </tbody>

                    </table>

                    </div>
                  </div>
                </div>
              </div>    
          </div>
        </div>
          
        </section>        
      </section>



<script>
var base_url = '<?php echo base_url(); ?>';
var cont = 'agent';
var agent_id = '<?php echo $agentId; ?>';

$(document).ready(function (){
  getagentBranch(base_url, cont, agent_id);
});
                        
function deleteAgent(id) {
    if (confirm('Are you sure you want to Delete?')) {
        $.ajax({
            type: "POST",
            url: "<?php echo base_url(); ?>agent/" + cont + "/deleteAgent",
            data: "id=" + id,
            success: function (data) {
                if (data) {
                    $('#' + id).remove();
                    $("input[name='id']").val('0');
                    window.location.reload();
                    alert('Deleted successfully');

                }
            }
        });
    }
}
</script>