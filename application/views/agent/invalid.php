<style type="text/css">
body{
  font-family: "Arial";
}
  .invalidpage{
    position: absolute;
    left: 50%;
    top:50%;
    transform: translate(-50%,-50%);
    padding: 50px;
    box-shadow: 0 0 20px rgba(0,0,0,.125);
  }
  h2{
    color: #D62525;
  }

  h2,p{
   margin-top:  0;
  }
  p{
    margin-bottom: 0;
  }
  span{
    width: 25px;
    height: 25px;
    border:2px solid;
    display: inline-block;
    text-align: center;
  }
</style>

<div class="invalidpage">    
    <h2><span>&times;</span> Invalid request page</h2>
    <p>The page you are requested is not valid</p>
</div>
