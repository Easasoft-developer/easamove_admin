<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$this->load->library('session');
$svar = $this->session->userdata['adminuser'];
$name = $svar['name'];
$access = $svar['access'];


 $actualurl="http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
 
?>

<!DOCTYPE html>
<html>
    <head>
        <meta name="viewport" content="width=device-width" />
        <title>Easa Admin</title>
        <link href="<?php echo base_url(); ?>css/bootstrap.min.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>css/bootstrap-custom.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>css/easa.pro.helpers.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>css/easa.pro.tab.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>css/easa.pro.ui.all.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>css/jquery.dataTables.min.css" rel="stylesheet" />
        <link href="<?php echo base_url(); ?>css/style.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/responsive.css" rel="stylesheet">
        <link href="<?php echo base_url(); ?>css/font-awesome.min.css" rel="stylesheet">
        <!-- <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css"> -->
        <script src="<?php echo base_url(); ?>js/jquery.js"></script>
        <script src="<?php echo base_url(); ?>js/bootstrap.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                var bodyHei = $(window).height();
                var menu = bodyHei - 41;
                $('.side-menu-tab').css({
                    height: menu + 'px'
                });
            });

        </script>
    </head>

      <body>
        <section id="container">
            <header>
                <nav class="menubar navbar navbar-default">
                    <div class="container-fluid">
    
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                            </button>
                            <a   class="navbar-brand">EASASOFT</a>
                        </div>
                        
                       <!--  <div class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">

                                <li <?php echo (  (base_url('dashboard')==$actualurl) )?'class=active':''; ?>>
                                    <a href="<?php echo base_url(); ?>dashboard">Dashboard</a>
                                </li>
                                <?php
                                if ($access == 1) {
                                    ?>
                                    <li <?php echo ( (base_url('adminUser')==$actualurl) || ('account' == $this->uri->segment(1))  )?'class=active':''; ?>>
                                        <a href="<?php echo base_url(); ?>adminUser">User management
                                            
                                        </a>


                                    </li>
                                    <?php
                                }
                                ?>
                                <li <?php echo ( (base_url('master/country')==$actualurl) || ('master' == $this->uri->segment(1)) ) ? 'class=active': ''; ?>>
                                    <a href="<?php echo base_url(); ?>master/country" >Master
                                        
                                    </a>


                                </li>
                                <li <?php echo ((base_url('propertymaster/siteStatus')==$actualurl)  || ('propertymaster' == $this->uri->segment(1)))?'class=active':''; ?>>
                                    <a href="<?php echo base_url(); ?>propertymaster/siteStatus" >Property master
                                        
                                    </a>


                                </li>
                                <li <?php echo ((base_url('featuremaster/outsideSpace')==$actualurl)   || ('featuremaster' == $this->uri->segment(1)))?'class=active':''; ?>>
                                    <a href="<?php echo base_url(); ?>featuremaster/outsideSpace" >Feature master
                                        
                                    </a>


                                </li>
                                <li <?php echo ((base_url('agent/agent')==$actualurl) || ('agent' == $this->uri->segment(1)))?'class=active':''; ?>>
                                    <a href="<?php echo base_url(); ?>agent/agent">Agent
                                        
                                    </a>


                                </li>


<li <?php echo ((base_url('AgentRequirements/contact')==$actualurl) || ('AgentRequirements' == $this->uri->segment(1)))?'class=active':''; ?>>
                                    <a href="<?php echo base_url(); ?>AgentRequirements/contact">Customer Requirements
                                    </a>


                                </li>

                            </ul>        


                            <ul class="nav navbar-nav navbar-right">
                                <li class="dropdown">
                                    <a data-toggle="dropdown" class="dropdown-toggle" href="#"><?php echo $name; ?><b class="caret"></b></a>
                                    <ul role="menu" class="dropdown-menu">
                                        <li style="display: none;">

                                        </li>
                                        <li><a href="<?php echo base_url(); ?>account/myprofile"><i class="fa fa-briefcase"></i>My Profile</a></li>
                                        <li><a href="<?php echo base_url(); ?>account/changePassword"><i class="fa fa-cog"></i>Change Password</a></li>
                                        <li><a href="<?php echo base_url(); ?>Account/logout"><i class="fa fa-sign-out"></i>Logout</a></li>
                                      
                                    </ul>
                                </li>
                            </ul>                    

                        </div> -->
                        <!--/.nav-collapse -->
                    </div>
                </div>
            </header>
