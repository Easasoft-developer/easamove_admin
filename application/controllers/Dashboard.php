<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->library('session');
        $this->load->model('Dashboard_model');

        // Your own constructor code
    }

    public function index() {

        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
            $data['agentcount'] = $this->Dashboard_model->agent_count();
            $data['admincount'] = $this->Dashboard_model->admin_count();
             $data['adminname']  = $this->Dashboard_model->getAdminuser($this->session->userdata['adminuser']['id']);
           
           $data['agent_names'] = $this->db->query("SELECT agent_name,agent_id FROM `easamoveadmin_tbl_agent` where agent_is_deleted = 0 ORDER BY agent_name DESC limit 0,5");

        foreach ($data['agent_names']->result() as $key => $value) {

            $res =  $this->db->query("SELECT COUNT(agent_id) as agent_cnt FROM `pro_tbl_property` where property_is_deleted = 0 and agent_id = '$value->agent_id'");
               foreach ($res->result() as $keys => $values) {
                
                $data['agent_cnt'][] = $values->agent_cnt;
               }
        }

           
           $loop =1;

while ($loop <= 14) {

 $date = date('Y-m-d',strtotime("-$loop days"));

$sale = $this->db->query("SELECT count(*) as sale_cnt FROM `pro_tbl_property` WHERE property_created_on LIKE '$date%' AND property_lstatus =3 AND property_is_deleted =0");


$rent = $this->db->query("SELECT count(*) as rent_cnt FROM `pro_tbl_property` WHERE property_created_on LIKE '$date%' AND property_lstatus =2 AND property_is_deleted =0
    "); 

$mail = $this->db->query("SELECT COUNT( * ) AS mail_cnt FROM  `easamoveadmin_tbl_agent` WHERE agent_created_on LIKE  '$date%' AND agent_is_deleted =0");



$loop++;    


foreach ($sale->result() as $key1 => $value1) {
    
    $data['agent_sale'][] = $value1->sale_cnt;
}
foreach ($rent->result() as $key2 => $value2) {
$data['agent_rent'][] = $value2->rent_cnt;
    
}


foreach ($mail->result() as $key3 => $value3) {
    $data['agent_mail'][] = $value3->mail_cnt;
}



}


//              $data['agenttype'] = $this->Master_model->getAgenttype();
            $this->load->view('dashboard', $data);
        } else {

            $this->load->view('login');
        }
    }

    public function getExpense() {
        $result_set = $this->db->query("SELECT count(agent_id) as agent,date_format(agent_created_on, '%M-%y') as date
      from  easamoveadmin_tbl_agent where agent_is_deleted=0
GROUP by date_format(agent_created_on, '%M-%y')");

        $data[0] = array('date', 'agent');

        foreach ($result_set->result() as $key => $row) {
//            $name ='invoice';
            $date = $row->date;
            $pos = $row->agent;

            //I don't think there's supposed to be a comma after this square bracket
            $dates = array($date, (int) $pos);
            $data[$key + 1] = $dates;
//            
        }
        echo json_encode($data);
    }

}
