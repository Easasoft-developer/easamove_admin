<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class CentralheatingType extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Master_model');
 $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
         if (isset($this->session->userdata['adminuser']['logged_in'])&& $this->session->userdata['adminuser']['logged_in']== TRUE) {
        $this->load->view('common/header');
        $this->load->view('master/master');
        $this->load->view('master/centralheatingtype');
        $this->load->view('common/footer');
         } else {
            
        $this->load->view('login');
        }
    }

    public function insert() {
        
        if($this->input->post('id')==0){
            $data = array(
            'centralheating_name' => $this->input->post('HeatingType'),
                'centralheating_desc' => $this->input->post('HeatingDesc'),
            'centralheating_created_on' => date('yy-mm-dd H:i:s'),
            'centralheating_created_by' => 1
        );
        $result = $this->Master_model->addCentralheatingtype($data);
        }else{
            $id=$this->input->post('id');
            $data = array(
            'centralheating_name' => $this->input->post('HeatingType'),
                'centralheating_desc' => $this->input->post('HeatingDesc'),
            'centralheating_modified_on' => date('yy-mm-dd H:i:s'),
            'centralheating_modified_by' => 1
        );
            $result = $this->Master_model->updateCentralheatingtype($data,$id);
        }
        return $result;
    }

    public function getCentralheatingtype() {
        $result = $this->Master_model->getCentralheatingtype();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="'.$res->centralheating_id.'">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->centralheating_name . '</td>
                                <td>' . $res->centralheating_desc . '</td>
                            <td><a title="Edit" href="javascript:;" onclick="edit('.$res->centralheating_id.')"><span class="glyphicon glyphicon-pencil"></span></i></td>
                              <td><a  title="Delete" href="javascript:;" onclick="deleteCentralheating('.$res->centralheating_id.')"><span class="glyphicon glyphicon-trash"></span></a></td>
                        </tr>';
        }
        echo $html;
    }
    public function deleteCentralheatingtype(){
         $id=$this->input->post('id');
          $data = array(
            'centralheating_is_deleted' => 1,
            'centralheating_modified_on' => date('yy-mm-dd H:i:s'),
            'centralheating_modified_by' => 1
        );
          $result = $this->Master_model->deleteCentralheatingtype($data,$id);
         echo $result;
    }

}
