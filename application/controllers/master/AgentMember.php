<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AgentMember extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Master_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {

        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
             $svar = $this->session->userdata['adminuser'];
           $data['access'] = $svar['access'];

            $this->load->view('common/header');
            $this->load->view('master/master');
            $this->load->view('master/agentmember',$data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }








    public function insert() {

    
        $config = array(
            'upload_path' => 'images/agent_member/',
            'allowed_types' => 'jpg|gif|png|jpeg',
            'overwrite' => 1,
//            'file_name' =>$newFileName,
        );
        if(!empty($_FILES['memberLogo']['name'])){
        $this->load->library('upload', $config);
//           print_r($_FILES);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('memberLogo')) {
            $error = array('error' => $this->upload->display_errors());
        } else {
            $data1 = $this->upload->data();

            $file_path = $data1['file_path'];
            $file = $data1['full_path'];
            $file_ext = $data1['file_ext'];

            echo $final_file_name = $this->input->post('member_name') . '_' . time() . '' . $file_ext;
            $rename = rename($file, $file_path . $final_file_name);
        }
        }else{
            echo $final_file_name=$this->input->post('imageedit');
        }

// here is the renaming functon

        if ($this->input->post('id') == 0) {
            // $key = $this->generateRandomString(16);
            $values = array(
                'member_name' => $this->input->post('member_name'),
                'member_website' => $this->input->post('member_website'),                
                'member_logo' => $final_file_name,
                'member_created_on' => date('yy-mm-dd H:i:s'),
                'member_created_by' => 1,
                'member_is_deleted' => 0,
            );
            $result = $this->Master_model->addAgentMember($values);

        } else {
            $id = $this->input->post('id');
            $values = array(
                'member_name' => $this->input->post('member_name'),
                'member_website' => $this->input->post('member_website'),                
                'member_logo' => $final_file_name,
                'member_modified_on' => date('y-m-d H:i:s'),
                'member_modified_by' => 1,
            );
            $result = $this->Master_model->updateAgentMember($values, $id);
        }
        return $result;
    	

    }







    public function getAgentMember() {

        $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];
        $result = $this->Master_model->getAgentMember();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->member_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->member_name . '</td>
                                <td>' . $res->member_website . '</td>
                                <td>' . $res->member_logo . '</td>';

            if ($access == 1 || $access == 2) {
                $html.='<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->member_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a></td>';
            } else {
                $html.='<td>-</td>';
            }
            if ($access == 1 || $access == 2) {
                $html.='<td><a  title="Delete" href="javascript:;" onclick="deleteAgentMember(' . $res->member_id . ')"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';
            } else {
                $html.='<td>-</td>';
            }
            $html.='</tr>';
        }
        echo $html;
    }

    public function deleteAgentMember() {
        $id = $this->input->post('id');

        $values = array(
            'member_is_deleted' => 1,
            'member_modified_on' => date('yy-mm-dd H:i:s'),
            'member_modified_by' => 1
        );
        $result = $this->Master_model->deleteAgentMember($values, $id);
        echo $result;
    }

    public function checkMember() {
        $id = $this->input->post('type');
        $query = $this->db->query("select * from easamoveadmin_tbl_agentmembers where  REPLACE(lower(member_name),' ','')=REPLACE(lower('$id'),' ','') and member_is_deleted=0");
        if ($query->num_rows() > 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

}

