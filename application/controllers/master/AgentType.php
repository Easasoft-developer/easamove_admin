<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AgentType extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Master_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {

        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
            $this->load->view('common/header');
            $this->load->view('master/master');
            $this->load->view('master/agenttype');
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'agent_name' => $this->input->post('Type'),
                'agent_desc' => $this->input->post('Desc'),
                'agent_created_on' => date('yy-mm-dd H:i:s'),
                'agent_created_by' => 1
            );
            $result = $this->Master_model->addAgenttype($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'agent_name' => $this->input->post('Type'),
                'agent_desc' => $this->input->post('Desc'),
                'agent_modified_on' => date('yy-mm-dd H:i:s'),
                'agent_modified_by' => 1
            );
            $result = $this->Master_model->updateAgenttype($data, $id);
        }
        return $result;
    }

    public function getAgenttype() {
        $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];
        $result = $this->Master_model->getAgenttype();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->agent_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->agent_name . '</td>
                                <td>' . $res->agent_desc . '</td>';

            if ($access == 1 || $access == 2) {
                $html.='<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->agent_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a></td>';
            } else {
                $html.='<td>-</td>';
            }
            if ($access == 1) {
                $html.='<td><a  title="Delete" href="javascript:;" onclick="deleteAgent(' . $res->agent_id . ')"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';
            } else {
                $html.='<td>-</td>';
            }
            $html.='</tr>';
        }
        echo $html;
    }

    public function deleteAgenttype() {
        $id = $this->input->post('id');
        $data = array(
            'agent_is_deleted' => 1,
            'agent_modified_on' => date('yy-mm-dd H:i:s'),
            'agent_modified_by' => 1
        );
        $result = $this->Master_model->deleteAgenttype($data, $id);
        echo $result;
    }

    public function checkAgent() {
        $id = $this->input->post('type');
        $query = $this->db->query("select * from easamoveadmin_tbl_agenttype where  REPLACE(lower(agent_name),' ','')=REPLACE(lower('$id'),' ','') and agent_is_deleted=0");
        if ($query->num_rows() > 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

}
