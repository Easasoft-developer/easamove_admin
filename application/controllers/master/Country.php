<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Country extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Master_model');
$this->load->library('session');
        // Your own constructor code
    }

    public function index() {
                if (isset($this->session->userdata['adminuser']['logged_in'])&& $this->session->userdata['adminuser']['logged_in']== TRUE) {

        $svar = $this->session->userdata['adminuser'];
        $data['access'] = $svar['access'];

        $this->load->view('common/header');
        $this->load->view('master/master');
        $this->load->view('master/country',$data);
        $this->load->view('common/footer');
        } else {
            
        $this->load->view('login');
        }
    }

    public function insert() {
        
        if($this->input->post('id')==0){
            $data = array(
            'country_code' => $this->input->post('Code'),
            'country_name' => $this->input->post('Name'),
            'country_created_on' => date('yy-mm-dd H:i:s'),
            'country_created_by' => 1
        );
        $result = $this->Master_model->addCountry($data);
        }else{
            $id=$this->input->post('id');
            $data = array(
            'country_code' => $this->input->post('Code'),
            'country_name' => $this->input->post('Name'),
            'country_modified_on' => date('yy-mm-dd H:i:s'),
            'country_modified_by' => 1
        );
            $result = $this->Master_model->updateCountry($data,$id);
        }
        return $result;
    }

    public function getCountry() {
        $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];
        $result = $this->Master_model->getCountry();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="'.$res->country_id.'">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->country_code . '</td>
                            <td>' . $res->country_name . '</td>';
            if ($access == 1 || $access == 2) {
                $html.='<td><a title="Edit" href="javascript:;" onclick="edit('.$res->country_id.')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></td>';
            } else {
                $html.='<td>-</td>';
            }
            if ($access == 1 || $access == 2) {
                $html.='<td><a  title="Delete" href="javascript:;" onclick="deleteCountry('.$res->country_id.')"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></td>';
            } else {
                $html.='<td>-</td>';
            }
            $html.='</tr>';

        }
        echo $html;
    }
    public function deleteCountry(){
         $id=$this->input->post('id');
          $data = array(
            'country_is_deleted' => 1,
            'country_modified_on' => date('yy-mm-dd H:i:s'),
            'country_modified_by' => 1
        );
          $result = $this->Master_model->deleteCountry($data,$id);
         echo $result;
    }
    public function checkCountry(){
        $id=trim($this->input->post('code'));
        $oid = $this->input->post('id');
        // $cr = $this->input->post('cr');
if ($oid == 0) {
    
         $query=$this->db->query("select * from easamoveadmin_tbl_country where lower(country_code)=lower('$id') and country_is_deleted=0");
} else {

$query=$this->db->query("select * from easamoveadmin_tbl_country where country_code ='$id' and country_id != $oid  and country_is_deleted=0");
}

        
        if ($query->num_rows() > 0) {
            echo 1;
        }else{
            echo 0;
        }
    }

}
