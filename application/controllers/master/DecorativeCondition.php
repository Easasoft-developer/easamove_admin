<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class DecorativeCondition extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Master_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {

            $this->load->view('common/header');
            $this->load->view('master/master');
            $this->load->view('master/decorativecondition');
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'condition_name' => $this->input->post('ConditionType'),
                'condition_desc' => $this->input->post('ConditionDesc'),
                'condition_created_on' => date('yy-mm-dd H:i:s'),
                'condition_created_by' => 1
            );
            $result = $this->Master_model->addDecorativecondition($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'condition_name' => $this->input->post('ConditionType'),
                'condition_desc' => $this->input->post('ConditionDesc'),
                'condition_modified_on' => date('yy-mm-dd H:i:s'),
                'condition_modified_by' => 1
            );
            $result = $this->Master_model->updateDecorativecondition($data, $id);
        }
        return $result;
    }

    public function getDecorativecondition() {
        $result = $this->Master_model->getDecorativecondition();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->condition_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->condition_name . '</td>
                            <td>' . $res->condition_desc . '</td>
                            <td><a title="Edit" href="javascript:;" onclick="edit(' . $res->condition_id . ')"><span class="glyphicon glyphicon-pencil"></span></i></td>
                              <td><a  title="Delete" href="javascript:;" onclick="deleteDecorativecondition(' . $res->condition_id . ')"><span class="glyphicon glyphicon-trash"></span></a></td>
                        </tr>';
        }
        echo $html;
    }

    public function deleteDecorativecondition() {
        $id = $this->input->post('id');
        $data = array(
            'condition_is_deleted' => 1,
            'condition_modified_on' => date('yy-mm-dd H:i:s'),
            'condition_modified_by' => 1
        );
        $result = $this->Master_model->deleteDecorativecondition($data, $id);
        echo $result;
    }

}

?>