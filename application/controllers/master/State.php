<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class State extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Master_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {


            $svar = $this->session->userdata['adminuser'];
           $data['access'] = $svar['access'];
            $data['country'] = $this->Master_model->getCountry();
            $this->load->view('common/header');
            $this->load->view('master/master');
            $this->load->view('master/state', $data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'state_name' => $this->input->post('Name'),
                'country_id' => $this->input->post('CountryId'),
                'state_created_on' => date('yy-mm-dd H:i:s'),
                'state_created_by' => 1
            );
            $result = $this->Master_model->addState($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'state_name' => $this->input->post('Name'),
                'country_id' => $this->input->post('CountryId'),
                'state_modified_on' => date('yy-mm-dd H:i:s'),
                'state_modified_by' => 1
            );
            $result = $this->Master_model->updateState($data, $id);
        }
        return $result;
    }

    public function getState() {
         $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];
        $result = $this->Master_model->getState();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->state_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->state_name . '</td>
                            <td>' . $res->country_name . '</td>';
            if ($access == 1 || $access == 2) {
                $html.='<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->state_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></td>';
            } else {
                $html.='<td>-</td>';
            }
            if ($access == 1|| $access == 2) {
                $html.='<td><a  title="Delete" href="javascript:;" onclick="deleteState(' . $res->state_id . ')"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';
            } else {
                $html.='<td>-</td>';
            }
            $html.='</tr>';
        }
        echo $html;
    }

    public function deleteState() {
        $id = $this->input->post('id');
        $data = array(
            'state_is_deleted' => 1,
            'state_modified_on' => date('yy-mm-dd H:i:s'),
            'state_modified_by' => 1
        );
        $result = $this->Master_model->deleteState($data, $id);
        echo $result;
    }
    public function checkState(){
         $name=trim($this->input->post('name'));
         $country=trim($this->input->post('country'));
         $id = $this->input->post('id');

         if ($id == 0) {
             
         
         $query=$this->db->query("select * from easamoveadmin_tbl_state where REPLACE(lower(state_name),' ','')=REPLACE(lower('$name'),' ','') and country_id='$country' and state_is_deleted=0");
        } else {


            $query = $this->db->get_where('easamoveadmin_tbl_state',array("state_name" => $name, "country_id" =>$country, "state_id !=" => $id,"state_is_deleted" => 0 ));

            // print_r($this->db->get_where('easamoveadmin_tbl_state',array("state_name" => $name, "country_id" =>$country, "state_id !=" => $id,"state_is_deleted" => 0 )));
            // exit();
        }

        if ($query->num_rows() > 0) {
            echo 1;
        }else{
            echo 0;
        }
    }
}

?>