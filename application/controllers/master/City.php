<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class City extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Master_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
  $svar = $this->session->userdata['adminuser'];
           $data['access'] = $svar['access'];
            $data['country'] = $this->Master_model->getCountry();
            $data['state'] = $this->Master_model->getState();
            $this->load->view('common/header');
            $this->load->view('master/master');
            $this->load->view('master/city', $data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'city_name' => $this->input->post('Name'),
                'city_code' => $this->input->post('Code'),
                'state_id' => $this->input->post('StateId'),
                'city_created_on' => date('yy-mm-dd H:i:s'),
                'city_created_by' => 1
            );
            $result = $this->Master_model->addCity($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'city_name' => $this->input->post('Name'),
                'city_code' => $this->input->post('Code'),
                'state_id' => $this->input->post('StateId'),
                'city_modified_on' => date('yy-mm-dd H:i:s'),
                'city_modified_by' => 1
            );
            $result = $this->Master_model->updateCity($data, $id);
        }
        return $result;
    }

    public function getCity() {
        $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];
        $result = $this->Master_model->getCity();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->city_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->city_code . '</td>
                            <td>' . $res->city_name . '</td>
                            <td>' . $res->country_name . '</td>
                            <td>' . $res->state_name . '</td>';
            if ($access == 1 || $access == 2) {
                $html.='<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->city_id . ',' . $res->state_id . ',' . $res->country_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a></td>';
            } else {
                $html.='<td>-</td>';
            }
            if ($access == 1 || $access == 2) {
                $html.='<td><a  title="Delete" href="javascript:;" onclick="deleteCity(' . $res->city_id . ')"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';
            } else {
                $html.='<td>-</td>';
            }
            $html.='</tr>';
        }
        echo $html;
    }

    public function deleteCity() {
        $id = $this->input->post('id');
        $data = array(
            'city_is_deleted' => 1,
            'city_modified_on' => date('yy-mm-dd H:i:s'),
            'city_modified_by' => 1
        );
        $result = $this->Master_model->deleteCity($data, $id);
        echo $result;
    }

    public function getCountry() {
        $id = $this->input->post('id');
        $result = $this->Master_model->getState($id);
        $html = '';
        if($id > 0 ) {
        
         $html.='<option value="0">--select--</option>';
        foreach ($result as $key => $res) {
            $html.='<option value="' . $res->state_id . '">' . $res->state_name . '</option>';
        }
        }
        else {
       $html.='<option value="0">--select--</option>';
    }
        echo $html;
    }

    public function checkCity() {
        $code = trim($this->input->post('code'));
        $id = $this->input->post('id');
        if ($id == 0) {
                   
        $query = $this->db->query("select * from easamoveadmin_tbl_city where lower(city_code)=lower('$code') and city_is_deleted=0");
        } else {

        $query = $this->db->query("select * from easamoveadmin_tbl_city where lower(city_code)=lower('$code') and city_id != $id and city_is_deleted=0");
        }

        if ($query->num_rows() > 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

}

?>