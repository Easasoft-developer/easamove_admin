<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ParkingType extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Master_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {

            $this->load->view('common/header');
            $this->load->view('master/master');
            $this->load->view('master/parkingtype');
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'parking_name' => $this->input->post('Type'),
                'parking_desc' => $this->input->post('Desc'),
                'parking_created_on' => date('yy-mm-dd H:i:s'),
                'parking_created_by' => 1
            );
            $result = $this->Master_model->addParkingtype($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'parking_name' => $this->input->post('Type'),
                'parking_desc' => $this->input->post('Desc'),
                'parking_modified_on' => date('yy-mm-dd H:i:s'),
                'parking_modified_by' => 1
            );
            $result = $this->Master_model->updateParkingtype($data, $id);
        }
        return $result;
    }

    public function getParkingtype() {
        $result = $this->Master_model->getParkingtype();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->parking_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->parking_name . '</td>
                            <td>' . $res->parking_desc . '</td>
                            <td><a title="Edit" href="javascript:;" onclick="edit(' . $res->parking_id . ')"><span class="glyphicon glyphicon-pencil"></span></i></td>
                              <td><a  title="Delete" href="javascript:;" onclick="deleteParking(' . $res->parking_id . ')"><span class="glyphicon glyphicon-trash"></span></a></td>
                        </tr>';
        }
        echo $html;
    }

    public function deleteParkingtype() {
        $id = $this->input->post('id');
        $data = array(
            'parking_is_deleted' => 1,
            'parking_modified_on' => date('yy-mm-dd H:i:s'),
            'parking_modified_by' => 1
        );
        $result = $this->Master_model->deleteParkingtype($data, $id);
        echo $result;
    }

}
