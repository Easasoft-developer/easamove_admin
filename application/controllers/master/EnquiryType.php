<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class EnquiryType extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->library('session');
        $this->load->helper('url');
        $this->load->model('Master_model');
        
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
  $svar = $this->session->userdata['adminuser'];
           $data['access'] = $svar['access'];
            $this->load->view('common/header');
            $this->load->view('master/master');
            $this->load->view('master/enquirytype',$data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'enquiry_name' => $this->input->post('Type'),
                'enquiry_desc' => $this->input->post('Desc'),
                'enquiry_created_on' => date('yy-mm-dd H:i:s'),
                'enquiry_created_by' => 1
            );
            $result = $this->Master_model->addEnquirytype($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'enquiry_name' => $this->input->post('Type'),
                'enquiry_desc' => $this->input->post('Desc'),
                'enquiry_modified_on' => date('yy-mm-dd H:i:s'),
                'enquiry_modified_by' => 1
            );
            $result = $this->Master_model->updateEnquirytype($data, $id);
        }
        return $result;
    }

    public function getEnquirytype() {
        $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];
        $result = $this->Master_model->getEnquirytype();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->enquiry_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->enquiry_name . '</td>
                            <td>' . $res->enquiry_desc . '</td>';
            if ($access == 1 || $access == 2) {
                $html.='<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->enquiry_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a></td>';
            } else {
                $html.='<td>-</td>';
            }
            if ($access == 1 || $access == 2) {
                $html.='<td><a  title="Delete" href="javascript:;" onclick="deleteEnquiryType(' . $res->enquiry_id . ')"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';
            } else {
                $html.='<td>-</td>';
            }
            $html.='</tr>';
        }
        echo $html;
    }

    public function deleteEnquirytype() {
        $id = $this->input->post('id');
        $data = array(
            'enquiry_is_deleted' => 1,
            'enquiry_modified_on' => date('yy-mm-dd H:i:s'),
            'enquiry_modified_by' => 1
        );
        $result = $this->Master_model->deleteEnquirytype($data, $id);
        echo $result;
    }

    public function checkEnquiry() {
        $code = trim($this->input->post('code'));
        $id = $this->input->post('id');
if($id == 0) {
$query = $this->db->query("select * from easamoveadmin_tbl_enquirytype where  REPLACE(lower(enquiry_name),' ','')=REPLACE(lower('$code'),' ','') and enquiry_is_deleted=0");

 } else {
$query = $this->db->query("select * from easamoveadmin_tbl_enquirytype where  REPLACE(lower(enquiry_name),' ','')=REPLACE(lower('$code'),' ','') and enquiry_id != $id and enquiry_is_deleted=0");

 }

        
        if ($query->num_rows() > 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

}
