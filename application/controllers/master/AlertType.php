<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AlertType extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Master_model');
        $this->load->library('session');

        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
            
  $svar = $this->session->userdata['adminuser'];
           $data['access'] = $svar['access'];
            $this->load->view('common/header');
            $this->load->view('master/master');
            $this->load->view('master/alerttype',$data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'alert_name' => $this->input->post('Type'),
                 'alert_desc' => $this->input->post('Desc'),
                'alert_created_on' => date('yy-mm-dd H:i:s'),
                'alert_created_by' => 1
            );
            $result = $this->Master_model->addAlerttype($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'alert_name' => $this->input->post('Type'),
                'alert_desc' => $this->input->post('Desc'),
                'alert_modified_on' => date('yy-mm-dd H:i:s'),
                'alert_modified_by' => 1
            );
            $result = $this->Master_model->updateAlerttype($data, $id);
        }
        return $result;
    }

    public function getAlerttype() {
        $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];
        $result = $this->Master_model->getAlerttype();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->alert_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->alert_name . '</td>
                            <td>' . $res->alert_desc . '</td>';
          if ($access == 1 || $access == 2) {
                      $html.=  '<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->alert_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a></td>';
        } else { 

        $html.='<td>-</td>'; 
        }

            if ($access == 1 || $access == 2) {
                     $html.=  '<td><a  title="Delete" href="javascript:;" onclick="deleteAlert(' . $res->alert_id . ')"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';
                      
        } else { 
            $html.='<td>-</td>'; 
        }

  $html.='</tr>';
        }
        echo $html;
    }

    public function deleteAlerttype() {
        $id = $this->input->post('id');
        $data = array(
            'alert_is_deleted' => 1,
            'alert_modified_on' => date('yy-mm-dd H:i:s'),
            'alert_modified_by' => 1
        );
        $result = $this->Master_model->deleteAlerttype($data, $id);
        echo $result;
    }
 public function checkAlert(){
         $type = trim($this->input->post('type'));
         $id = $this->input->post('id');

 if ($id == 0) {
    $query = $this->db->query("select * from easamoveadmin_tbl_alerttype where  REPLACE(lower(alert_name),' ','')=REPLACE(lower('$type'),' ','') and alert_is_deleted=0");

} else { 

$query = $this->db->query("select * from easamoveadmin_tbl_alerttype where  REPLACE(lower(alert_name),' ','')=REPLACE(lower('$type'),' ','') and alert_id != $id and alert_is_deleted=0");

}
         
        if ($query->num_rows() > 0) {
            echo 1;
        }else{
            echo 0;
        }
    }
}
