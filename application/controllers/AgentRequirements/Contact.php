<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Portal_model');
        $this->load->library('session');

// Your own constructor code
    }


public function index() {
    

            $this->load->view('common/header');            
			$this->load->view('agentrequirements/contact');
            $this->load->view('common/footer');
}





    public function getcontact() {

      
        $svar = $this->session->userdata['adminuser'];
        $result = $this->Portal_model->getcontact();
        $access = $svar['access'];
        $html = '';
        foreach ($result as $key => $res) {
           
            $html.='<tr id="' . $res->contactus_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->contact_name . '</td>
                            <td>' . $res->contact_email . '</td>
                            <td>' . $res->contact_address1. ($res->contact_city != '' ? ", $res->contact_city" : '' ).($res->contact_postcode != '' ? ", $res->contact_postcode" : '' ). '</td>
                            <td>' . $res->contact_subject.'</td>
                            <td>'.$res->contact_message.'</td>';
            //     $html.='<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->fb_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a></td>';
            // } else {
            //     $html.='<td>-</td>';
            // }
            // if ($access == 1) {
            //     $html.='<td><a  title="Delete" href="javascript:;" onclick="deleteOutsidespace(' . $res->fb_id . ')"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';
            // } else {
            //     $html.='<td>-</td>';
            // }
            $html.='</tr>';
        }
        echo $html;
    }




}