<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Feedback extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Portal_model');
        $this->load->library('session');

// Your own constructor code
    }


public function index() {
    
            $this->load->view('common/header');            
			$this->load->view('agentrequirements/feedback');
            $this->load->view('common/footer');
}




    public function get_feedback() {

      
        $svar = $this->session->userdata['adminuser'];
        $result = $this->Portal_model->get_feedback();
        $access = $svar['access'];
        $html = '';
        foreach ($result as $key => $res) {
           
            $html.='<tr id="' . $res->fb_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->feedback_name . '</td>
                            <td>' . $res->feedback_email . '</td>
                            <td>' . $res->feedback_phone.'</td>
                            <td>'.$res->feedback_title.'</td>
                            <td>'.$res->feedback_categories. '</td>
                            <td>'.$res->feedback_comments.'</td>';
            // if ($access == 1 || $access == 2) {
            //     $html.='<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->fb_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a></td>';
            // } else {
            //     $html.='<td>-</td>';
            // }
            // if ($access == 1) {
            //     $html.='<td><a  title="Delete" href="javascript:;" onclick="deleteOutsidespace(' . $res->fb_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-times"></i></button></a></td>';
            // } else {
            //     $html.='<td>-</td>';
            // }
            $html.='</tr>';
        }
        echo $html;
    }






}