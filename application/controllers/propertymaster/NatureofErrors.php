<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class NatureofErrors extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Propertymaster_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {

            $this->load->view('common/header');
            $this->load->view('propertymaster/propertymaster');
            $this->load->view('propertymaster/natureoferrors');
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'natureoferrors_name' => $this->input->post('Error'),
                'natureoferrors_desc' => $this->input->post('Desc'),
                'natureoferrors_created_on' => date('yy-mm-dd H:i:s'),
                'natureoferrors_created_by' => 1
            );
            $result = $this->Propertymaster_model->addNatureoferrors($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'natureoferrors_name' => $this->input->post('Error'),
                'natureoferrors_desc' => $this->input->post('Desc'),
                'natureoferrors_modified_on' => date('yy-mm-dd H:i:s'),
                'natureoferrors_modified_by' => 1
            );
            $result = $this->Propertymaster_model->updateNatureoferrors($data, $id);
        }
        return $result;
    }

    public function getNatureoferrors() {
        $result = $this->Propertymaster_model->getNatureoferrors();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->natureoferrors_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->natureoferrors_name . '</td>
                            <td>' . $res->natureoferrors_desc . '</td>
                            <td><a title="Edit" href="javascript:;" onclick="edit(' . $res->natureoferrors_id . ')"><span class="glyphicon glyphicon-pencil"></span></i></td>
                              <td><a  title="Delete" href="javascript:;" onclick="deleteNatureoferrors(' . $res->natureoferrors_id . ')"><span class="glyphicon glyphicon-trash"></span></a></td>
                        </tr>';
        }
        echo $html;
    }

    public function deleteNatureoferrors() {
        $id = $this->input->post('id');
        $data = array(
            'natureoferrors_is_deleted' => 1,
            'natureoferrors_modified_on' => date('yy-mm-dd H:i:s'),
            'natureoferrors_modified_by' => 1
        );
        $result = $this->Propertymaster_model->deleteNatureoferrors($data, $id);
        echo $result;
    }

}
