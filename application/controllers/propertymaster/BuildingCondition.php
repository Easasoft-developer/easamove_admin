<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class BuildingCondition extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Propertymaster_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
$svar = $this->session->userdata['adminuser'];
           $data['access'] = $svar['access'];
            $this->load->view('common/header');
            $this->load->view('propertymaster/propertymaster');
            $this->load->view('propertymaster/buildingcondition',$data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'buildcondition_name' => $this->input->post('Type'),
                'buildcondition_desc' => $this->input->post('Desc'),
                'buildcondition_created_on' => date('yy-mm-dd H:i:s'),
                'buildcondition_created_by' => 1
            );
            $result = $this->Propertymaster_model->addBuildingcondition($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'buildcondition_name' => $this->input->post('Type'),
                'buildcondition_desc' => $this->input->post('Desc'),
                'buildcondition_modified_on' => date('yy-mm-dd H:i:s'),
                'buildcondition_modified_by' => 1
            );
            $result = $this->Propertymaster_model->updateBuildingcondition($data, $id);
        }
        return $result;
    }

    public function getBuildingcondition() {

         $delc = '';
         $del = $this->db->query("SELECT DISTINCT(property_buildconst)  FROM pro_tbl_property");
        $delchecker = $del->result();
        foreach ($delchecker as $key1 => $value1) {
            $delc[] .= $value1->property_buildconst;
        }

        $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];
        $result = $this->Propertymaster_model->getBuildingcondition();
        $html = '';
        foreach ($result as $key => $res) {
            
            $html.='<tr id="' . $res->buildcondition_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->buildcondition_name . '</td>
                            <td>' . $res->buildcondition_desc . '</td>';
            if ($access == 1 || $access == 2) {
                $html.='<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->buildcondition_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a></td>';
            } else {
                $html.='<td>-</td>';
            }
            if ($access == 1 || $access == 2) {


                $html.='<td><a  title="Delete" href="javascript:;" onclick="';
                
                $html.=   in_array($res->buildcondition_id ,$delc ) ? 'df()' :'deleteBuildingcondition(' . $res->buildcondition_id . ')';

                $html.= '"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';

            } else {
                $html.='<td>-</td>';
            }
            $html.='</tr>';
        }
        echo $html;
    }

    public function deleteBuildingcondition() {
        $id = $this->input->post('id');
        $data = array(
            'buildcondition_is_deleted' => 1,
            'buildcondition_modified_on' => date('yy-mm-dd H:i:s'),
            'buildcondition_modified_by' => 1
        );
        $result = $this->Propertymaster_model->deleteBuildingcondition($data, $id);
        echo $result;
    }
    public function checkBuildingcondition(){
        $type=trim($this->input->post('type'));
        $id = $this->input->post('id');

        if ($id == 0) {
             $query=$this->db->query("select * from easamoveadmin_tbl_buildcondition where  REPLACE(lower(buildcondition_name),' ','')=REPLACE(lower('$type'),' ','') and buildcondition_is_deleted=0");
        } else {

             $query=$this->db->query("select * from easamoveadmin_tbl_buildcondition where  REPLACE(lower(buildcondition_name),' ','')=REPLACE(lower('$type'),' ','') and buildcondition_id != $id  and buildcondition_is_deleted=0");
        }

        if ($query->num_rows() > 0) {
            echo 1;
        }else{
            echo 0;
        }
    }

}
