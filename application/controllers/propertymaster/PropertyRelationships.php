<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PropertyRelationships extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Propertymaster_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {

            $this->load->view('common/header');
            $this->load->view('propertymaster/propertymaster');
            $this->load->view('propertymaster/propertyrelationships');
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'propertyrelationships_name' => $this->input->post('Relationship'),
                'propertyrelationships_desc' => $this->input->post('RelationshipDesc'),
                'propertyrelationships_created_on' => date('yy-mm-dd H:i:s'),
                'propertyrelationships_created_by' => 1
            );
            $result = $this->Propertymaster_model->addPropertyrelationships($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'propertyrelationships_name' => $this->input->post('Relationship'),
                'propertyrelationships_desc' => $this->input->post('RelationshipDesc'),
                'propertyrelationships_modified_on' => date('yy-mm-dd H:i:s'),
                'propertyrelationships_modified_by' => 1
            );
            $result = $this->Propertymaster_model->updatePropertyrelationships($data, $id);
        }
        return $result;
    }

    public function getPropertyrelationships() {
        $result = $this->Propertymaster_model->getPropertyrelationships();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->propertyrelationships_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->propertyrelationships_name . '</td>
                             <td>' . $res->propertyrelationships_desc . '</td>
                            <td><a title="Edit" href="javascript:;" onclick="edit(' . $res->propertyrelationships_id . ')"><span class="glyphicon glyphicon-pencil"></span></i></td>
                              <td><a  title="Delete" href="javascript:;" onclick="deletePropertyrelationships(' . $res->propertyrelationships_id . ')"><span class="glyphicon glyphicon-trash"></span></a></td>
                        </tr>';
        }
        echo $html;
    }

    public function deletePropertyrelationships() {
        $id = $this->input->post('id');
        $data = array(
            'propertyrelationships_is_deleted' => 1,
            'propertyrelationships_modified_on' => date('yy-mm-dd H:i:s'),
            'propertyrelationships_modified_by' => 1
        );
        $result = $this->Propertymaster_model->deletePropertyrelationships($data, $id);
        echo $result;
    }

}
