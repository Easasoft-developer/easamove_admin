<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class TransactionType extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Propertymaster_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
$svar = $this->session->userdata['adminuser'];
           $data['access'] = $svar['access'];
            $this->load->view('common/header');
            $this->load->view('propertymaster/propertymaster');
            $this->load->view('propertymaster/transactiontype',$data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'transactiontype_name' => $this->input->post('Type'),
                'transactiontype_for' => $this->input->post('for'),
                'transactiontype_desc' => $this->input->post('Desc'),
                'transactiontype_created_on' => date('yy-mm-dd H:i:s'),
                'transactiontype_created_by' => 1
            );
            $result = $this->Propertymaster_model->addTransaction($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'transactiontype_name' => $this->input->post('Type'),
                'transactiontype_for' => $this->input->post('for'),
                'transactiontype_desc' => $this->input->post('Desc'),
                'transactiontype_modified_on' => date('yy-mm-dd H:i:s'),
                'transactiontype_modified_by' => 1
            );
            $result = $this->Propertymaster_model->updateTransaction($data, $id);
        }
        return $result;
    }

    public function getTransaction() {
 $delc = '';
         $del = $this->db->query("SELECT DISTINCT(property_ptransact)  FROM pro_tbl_property");
        $delchecker = $del->result();
        foreach ($delchecker as $key1 => $value1) {
            $delc[] .= $value1->property_ptransact;
        }


        $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];
        $result = $this->Propertymaster_model->getTransaction();
        $html = '';
        foreach ($result as $key => $res) {
            $type='';
            if($res->transactiontype_for==1){
                $type='Sale';
            }else{
                $type='Rent';
            }
            $html.='<tr id="' . $res->transactiontype_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->transactiontype_name . '</td>
                            <td>' . $type . '</td>
                            <td>' . $res->transactiontype_desc . '</td>';
            if ($access == 1 || $access == 2) {
                $html.='<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->transactiontype_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a></td>';
            } else {
                $html.='<td>-</td>';
            }
            if ($access == 1 || $access == 2) {

                $html.='<td><a  title="Delete" href="javascript:;" onclick="';
                
                $html.=   in_array($res->transactiontype_id ,$delc ) ? 'df()' :'deleteTransaction(' . $res->transactiontype_id . ')';

                $html.= '"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';
            } else {
                $html.='<td>-</td>';
            }
            $html.='</tr>';
        }
        echo $html;
    }

    public function deleteTransaction() {
        $id = $this->input->post('id');
        $data = array(
            'transactiontype_is_deleted' => 1,
            'transactiontype_modified_on' => date('yy-mm-dd H:i:s'),
            'transactiontype_modified_by' => 1
        );
        $result = $this->Propertymaster_model->deleteTransaction($data, $id);
        echo $result;
    }
    public function checkTransaction(){
        $type=trim($this->input->post('type'));
        $desc = trim($this->input->post('desc'));
        $id = $this->input->post('id');

         if ($id == 0) {
             $query=$this->db->query("select * from easamoveadmin_tbl_transactiontype where  REPLACE(lower(transactiontype_name),' ','')=REPLACE(lower('$type'),' ','') and transactiontype_for='$desc' and transactiontype_is_deleted=0");
         } else {
            $query=$this->db->query("select * from easamoveadmin_tbl_transactiontype where  REPLACE(lower(transactiontype_name),' ','')=REPLACE(lower('$type'),' ','') and transactiontype_for='$desc' and transactiontype_id != $id and transactiontype_is_deleted=0");
         }



        if ($query->num_rows() > 0) {
            echo 1;
        }else{
            echo 0;
        }
    }

}
