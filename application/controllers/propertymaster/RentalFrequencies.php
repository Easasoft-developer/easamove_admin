<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class RentalFrequencies extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Propertymaster_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {

            $this->load->view('common/header');
            $this->load->view('propertymaster/propertymaster');
            $this->load->view('propertymaster/rentalfrequencies');
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'rentalfrequency_name' => $this->input->post('FrequencyType'),
                'rentalfrequency_desc' => $this->input->post('FrequencyDesc'),
                'rentalfrequency_created_on' => date('yy-mm-dd H:i:s'),
                'rentalfrequency_created_by' => 1
            );
            $result = $this->Propertymaster_model->addRentalfrequencies($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'rentalfrequency_name' => $this->input->post('FrequencyType'),
                'rentalfrequency_desc' => $this->input->post('FrequencyDesc'),
                'rentalfrequency_modified_on' => date('yy-mm-dd H:i:s'),
                'rentalfrequency_modified_by' => 1
            );
            $result = $this->Propertymaster_model->updateRentalfrequencies($data, $id);
        }
        return $result;
    }

    public function getRentalfrequencies() {
        $result = $this->Propertymaster_model->getRentalfrequencies();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->rentalfrequency_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->rentalfrequency_name . '</td>
                            <td>' . $res->rentalfrequency_desc . '</td>
                            <td><a title="Edit" href="javascript:;" onclick="edit(' . $res->rentalfrequency_id . ')"><span class="glyphicon glyphicon-pencil"></span></i></td>
                              <td><a  title="Delete" href="javascript:;" onclick="deleteRentalfrequencies(' . $res->rentalfrequency_id . ')"><span class="glyphicon glyphicon-trash"></span></a></td>
                        </tr>';
        }
        echo $html;
    }

    public function deleteRentalfrequencies() {
        $id = $this->input->post('id');
        $data = array(
            'rentalfrequency_is_deleted' => 1,
            'rentalfrequency_modified_on' => date('yy-mm-dd H:i:s'),
            'rentalfrequency_modified_by' => 1
        );
        $result = $this->Propertymaster_model->deleteRentalfrequencies($data, $id);
        echo $result;
    }

}
?>

