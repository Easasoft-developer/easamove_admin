<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Category extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Propertymaster_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {

            $this->load->view('common/header');
            $this->load->view('propertymaster/propertymaster');
            $this->load->view('propertymaster/category');
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'category_name' => $this->input->post('Name'),
                'category_desc' => $this->input->post('Desc'),
                'category_created_on' => date('yy-mm-dd H:i:s'),
                'category_created_by' => 1
            );
            $result = $this->Propertymaster_model->addCategory($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'category_name' => $this->input->post('Name'),
                'category_desc' => $this->input->post('Desc'),
                'category_modified_on' => date('yy-mm-dd H:i:s'),
                'category_modified_by' => 1
            );
            $result = $this->Propertymaster_model->updateCategory($data, $id);
        }
        return $result;
    }

    public function getCategory() {
        $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];
        $result = $this->Propertymaster_model->getCategory();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->category_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->category_name . '</td>
                            <td>' . $res->category_desc . '</td>';
             if ($access == 1 || $access == 2) {
                $html.='<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->category_id . ')"><span class="glyphicon glyphicon-pencil"></span></i></td>';
            } else {
                $html.='<td>-</td>';
            }
            if ($access == 1) {
                $html.='<td><a  title="Delete" href="javascript:;" onclick="deleteCategory(' . $res->category_id . ')"><span class="glyphicon glyphicon-trash"></span></a></td>';
            } else {
                $html.='<td>-</td>';
            }
            $html.='</tr>';
        }
        echo $html;
    }

    public function deleteCategory() {
        $id = $this->input->post('id');
        $data = array(
            'category_is_deleted' => 1,
            'category_modified_on' => date('yy-mm-dd H:i:s'),
            'category_modified_by' => 1
        );
        $result = $this->Propertymaster_model->deleteCategory($data, $id);
        echo $result;
    }
     public function checkCategory(){
        $id=$this->input->post('name');
         $query=$this->db->query("select * from  easamoveadmin_tbl_category where  REPLACE(lower(category_name),' ','')=REPLACE(lower('$id'),' ','') and category_is_deleted=0");
        if ($query->num_rows() > 0) {
            echo 1;
        }else{
            echo 0;
        }
    }

}
