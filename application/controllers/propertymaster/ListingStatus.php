<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ListingStatus extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Propertymaster_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
 $svar = $this->session->userdata['adminuser'];
           $data['access'] = $svar['access'];
            $this->load->view('common/header');
            $this->load->view('propertymaster/propertymaster');
            $this->load->view('propertymaster/listingstatus',$data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {
        $isforsale = 0;
//        if ($this->input->post('IsForSale') == 'on') {
//            $isforsale = 1;
//        }
        if ($this->input->post('id') == 0) {
            $data = array(
                'listingstatus_name' => $this->input->post('Status'),
                'isforsale' => $this->input->post('IsForSale'),
                'listingstatus_created_on' => date('yy-mm-dd H:i:s'),
                'listingstatus_created_by' => 1
            );
            $result = $this->Propertymaster_model->addListingstatus($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'listingstatus_name' => $this->input->post('Status'),
                'isforsale' =>$this->input->post('IsForSale'),
                'listingstatus_modified_on' => date('yy-mm-dd H:i:s'),
                'listingstatus_modified_by' => 1
            );
            $result = $this->Propertymaster_model->updateListingstatus($data, $id);
        }
        return $result;
    }

    public function getListingstatus() {
        $delc = '';
         $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];
        $del = $this->db->query("SELECT DISTINCT(property_lstatus)  FROM pro_tbl_property");
        $delchecker = $del->result();
        foreach ($delchecker as $key1 => $value1) {
            $delc[] .= $value1->property_lstatus;
        }
        $result = $this->Propertymaster_model->getListingstatus();
        $html = '';
         $for='';
        
        foreach ($result as $key => $res) {
            if($res->isforsale==1){
             $for='Sale';
        }else{
            $for='Rent';
        }
            $html.='<tr id="' . $res->listingstatus_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->listingstatus_name . '</td>
                            <td>' . $for . '</td>';
            if ($access == 1 || $access == 2) {
                $html.='<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->listingstatus_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></td>';
            } else {
                $html.='<td>-</td>';
            }
            if ($access == 1 || $access == 2) {
                  $html.='<td><a  title="Delete" href="javascript:;" onclick="';
                
                $html.=   in_array($res->listingstatus_id ,$delc ) ? 'df()' :'deleteListingstatus(' . $res->listingstatus_id . ')';

                $html.= '"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';
            } else {
                $html.='<td>-</td>';
            }
            $html.='</tr>';
        }
        echo $html;
    }

    public function deleteListingstatus() {
        $id = $this->input->post('id');
        $data = array(
            'listingstatus_is_deleted' => 1,
            'listingstatus_modified_on' => date('yy-mm-dd H:i:s'),
            'listingstatus_modified_by' => 1
        );
        $result = $this->Propertymaster_model->deleteListingstatus($data, $id);
        echo $result;
    }
    public function checkListingstatus(){
        $status=trim($this->input->post('status'));
        $desc =trim($this->input->post('desc'));
        $id = $this->input->post('id');
        if ($id == 0 ) {
            
        $query=$this->db->query("select * from  easamoveadmin_tbl_listingstatus where  REPLACE(lower(listingstatus_name),' ','')=REPLACE(lower('$status'),' ','') and isforsale='$desc' and listingstatus_is_deleted=0");
    }
    else {

$query=$this->db->query("select * from  easamoveadmin_tbl_listingstatus where  REPLACE(lower(listingstatus_name),' ','')=REPLACE(lower('$status'),' ','') and isforsale='$desc' and listingstatus_id != $id and listingstatus_is_deleted=0");

    }

        if ($query->num_rows() > 0) {
            echo 1;
        }else{
            echo 0;
        }
    }

}
