<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SiteStatus extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Propertymaster_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
   $svar = $this->session->userdata['adminuser'];
           $data['access'] = $svar['access'];
            $this->load->view('common/header');
            $this->load->view('propertymaster/propertymaster');
            $this->load->view('propertymaster/sitestatus',$data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'sitestatus_name' => $this->input->post('Status'),
                'sitestatus_desc'=> $this->input->post('StatusDesc'),
                'sitestatus_created_on' => date('yy-mm-dd H:i:s'),
                'sitestatus_created_by' => 1
            );
            $result = $this->Propertymaster_model->addSitestatus($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'sitestatus_name' => $this->input->post('Status'),
                'sitestatus_desc'=> $this->input->post('StatusDesc'),
                'sitestatus_modified_on' => date('yy-mm-dd H:i:s'),
                'sitestatus_modified_by' => 1
            );
            $result = $this->Propertymaster_model->updateSitestatus($data, $id);
        }
        return $result;
    }

    public function getSitestatus() {
        $delc = '';
        $svar = $this->session->userdata['adminuser'];
        $access = $svar['access']; $delc = '';
         $del = $this->db->query("SELECT DISTINCT(property_pstatus)  FROM pro_tbl_property");
        $delchecker = $del->result();
        foreach ($delchecker as $key1 => $value1) {
            $delc[] .= $value1->property_pstatus;
        }
        $result = $this->Propertymaster_model->getSitestatus();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->sitestatus_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->sitestatus_name . '</td>
                                <td>' . $res->sitestatus_desc . '</td>';
             if ($access == 1 || $access == 2) {
                $html.='<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->sitestatus_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></td>';
            } else {
                $html.='<td>-</td>';
            }
            if ($access == 1 || $access == 2) {

                $html.='<td><a  title="Delete" href="javascript:;" onclick="';
                
                $html.=   in_array($res->sitestatus_id ,$delc ) ? 'df()' :'deleteSitestatus(' . $res->sitestatus_id . ')';

                $html.= '"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';


            } else {
                $html.='<td>-</td>';
            }
            $html.='</tr>';
        }
        echo $html;
    }

    public function deleteSitestatus() {
        $id = $this->input->post('id');
        $data = array(
            'sitestatus_is_deleted' => 1,
            'sitestatus_modified_on' => date('yy-mm-dd H:i:s'),
            'sitestatus_modified_by' => 1
        );
        
        $result = $this->Propertymaster_model->deleteSitestatus($data, $id);
        echo $result;
        
        
    }
    public function checkSitestatus(){
        $status=trim($this->input->post('status'));
        $id = $this->input->post('id');

if($id == 0) {


         $query=$this->db->query("select * from easamoveadmin_tbl_sitestatus where  REPLACE(lower(sitestatus_name),' ','')=REPLACE(lower('$status'),' ','') and sitestatus_is_deleted=0");
     }
     else {

         $query=$this->db->query("select * from easamoveadmin_tbl_sitestatus where  REPLACE(lower(sitestatus_name),' ','')=REPLACE(lower('$status'),' ','') and sitestatus_id != $id and sitestatus_is_deleted=0");
     }
        if ($query->num_rows() > 0) {
            echo 1;
        } else {
            echo 0;
        }
    }

}
