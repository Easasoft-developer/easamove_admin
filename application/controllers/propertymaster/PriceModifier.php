<?php

defined('BASEPATH') OR exit('No direct script access allowed');
error_reporting(0);
class PriceModifier extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Propertymaster_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
 $svar = $this->session->userdata['adminuser'];
           $data['access'] = $svar['access'];
            $this->load->view('common/header');
            $this->load->view('propertymaster/propertymaster');
            $this->load->view('propertymaster/pricemodifier',$data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'pricemodifier_name' => $this->input->post('Modifier'),
                'pricemodifier_desc' => $this->input->post('ModifierDesc'),
                'pricemodifier_created_on' => date('yy-mm-dd H:i:s'),
                'pricemodifier_created_by' => 1
            );
            $result = $this->Propertymaster_model->addPricemodifier($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'pricemodifier_name' => $this->input->post('Modifier'),
                'pricemodifier_desc' => $this->input->post('ModifierDesc'),
                'pricemodifier_modified_on' => date('yy-mm-dd H:i:s'),
                'pricemodifier_modified_by' => 1
            );
            $result = $this->Propertymaster_model->updatePricemodifier($data, $id);
        }
        return $result;
    }

    public function getPricemodifier() {
  $delc = '';
         $del = $this->db->query("SELECT DISTINCT(property_pricemod)  FROM pro_tbl_property");
        $delchecker = $del->result();
        foreach ($delchecker as $key1 => $value1) {
            $delc[] .= $value1->property_pricemod;
        }



           $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];
        $result = $this->Propertymaster_model->getPricemodifier();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->pricemodifier_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->pricemodifier_name . '</td>
                            <td>' . $res->pricemodifier_desc . '</td>';
            if ($access == 1 || $access == 2) {
                $html.='<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->pricemodifier_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a></td>';
            } else {
                $html.='<td>-</td>';
            }
            if ($access == 1 || $access == 2) {


 $html.='<td><a  title="Delete" href="javascript:;" onclick="';
                
                $html.=   in_array($res->pricemodifier_id ,$delc ) ? 'df()' :'deletePricemodifier(' . $res->pricemodifier_id . ')';

                $html.= '"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';

            } else {
                $html.='<td>-</td>';
            }
            $html.='</tr>';
        }
        echo $html;
    }

    public function deletePricemodifier() {
        $id = $this->input->post('id');
        $data = array(
            'pricemodifier_is_deleted' => 1,
            'pricemodifier_modified_on' => date('yy-mm-dd H:i:s'),
            'pricemodifier_modified_by' => 1
        );
        $result = $this->Propertymaster_model->deletePricemodifier($data, $id);
        echo $result;
    }
    
    public function checkModifier(){
        $type=trim($this->input->post('type'));
        $desc =trim($this->input->post('desc'));
        $id = $this->input->post('id');


    if ($id == 0) {
    
     $query=$this->db->query("select * from easamoveadmin_tbl_pricemodifier where  REPLACE(lower(pricemodifier_name),' ','')=REPLACE(lower('$type'),' ','') and pricemodifier_desc= '$desc' and pricemodifier_is_deleted=0");

     } else {

     $query=$this->db->query("select * from easamoveadmin_tbl_pricemodifier where  REPLACE(lower(pricemodifier_name),' ','')=REPLACE(lower('$type'),' ','') and pricemodifier_id != $id and pricemodifier_desc= '$desc' and pricemodifier_is_deleted=0");
       }
        



        if ($query->num_rows() > 0) {
            echo 1;
        }else{
            echo 0;
        }
    }
}
?>