<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class PropertyType extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Propertymaster_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
   $svar = $this->session->userdata['adminuser'];
           $data['access'] = $svar['access'];
            $this->load->view('common/header');
            $this->load->view('propertymaster/propertymaster');
            $this->load->view('propertymaster/propertytype',$data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'propertytype_name' => $this->input->post('Type'),
                'propertytype_desc' => $this->input->post('Desc'),
                'propertytype_created_on' => date('yy-mm-dd H:i:s'),
                'propertytype_created_by' => 1
            );
            $result = $this->Propertymaster_model->addPropertytype($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'propertytype_name' => $this->input->post('Type'),
                'propertytype_desc' => $this->input->post('Desc'),
                'propertytype_modified_on' => date('yy-mm-dd H:i:s'),
                'propertytype_modified_by' => 1
            );
            $result = $this->Propertymaster_model->updatePropertytype($data, $id);
        }
        return $result;
    }

    public function getPropertytype() {

$delc = '';


        $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];
        $del = $this->db->query("SELECT DISTINCT(property_ptype)  FROM pro_tbl_property");
        $delchecker = $del->result();
        foreach ($delchecker as $key1 => $value1) {
            $delc[] .= $value1->property_ptype;
        }
        $result = $this->Propertymaster_model->getPropertytype();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->propertytype_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->propertytype_name . '</td>
                            <td>' . $res->propertytype_desc . '</td>';
             if ($access == 1 || $access == 2) {
                $html.='<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->propertytype_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></td>';
            } else {
                $html.='<td>-</td>';
            }
            if ($access == 1 || $access == 2) {

              
                 $html.='<td><a  title="Delete" href="javascript:;" onclick="';
                
                $html.=   in_array($res->propertytype_id ,$delc ) ? 'df()' :'deletePropertytype(' . $res->propertytype_id . ')';

                $html.= '"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';
            } else {
                $html.='<td>-</td>';
            }
            $html.='</tr>';
        }
        echo $html;
    }

    public function deletePropertytype() {
        $id = $this->input->post('id');
        $data = array(
            'propertytype_is_deleted' => 1,
            'propertytype_modified_on' => date('yy-mm-dd H:i:s'),
            'propertytype_modified_by' => 1
        );
        $result = $this->Propertymaster_model->deletePropertytype($data, $id);
        echo $result;
    }
    public function checkPropertytype(){
        $type=trim($this->input->post('type'));
        $id = $this->input->post('id');

        if ($id == 0) {
            $query=$this->db->query("select * from  easamoveadmin_tbl_propertytype where  REPLACE(lower(propertytype_name),' ','')=REPLACE(lower('$type'),' ','') and propertytype_is_deleted=0");
        } else{

            $query=$this->db->query("select * from  easamoveadmin_tbl_propertytype where  REPLACE(lower(propertytype_name),' ','')=REPLACE(lower('$type'),' ','') and propertytype_id != $id  and propertytype_is_deleted=0");
        }
         
        if ($query->num_rows() > 0) {
            echo 1;
        }else{
            echo 0;
        }
    }

}
