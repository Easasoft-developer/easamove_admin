<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class LettingType extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Propertymaster_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
$svar = $this->session->userdata['adminuser'];
           $data['access'] = $svar['access'];
            $this->load->view('common/header');
            $this->load->view('propertymaster/propertymaster');
            $this->load->view('propertymaster/lettingtype',$data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'lettingtype_name' => $this->input->post('Type'),
                'lettingtype_desc' => $this->input->post('Desc'),
                'lettingtype_created_on' => date('yy-mm-dd H:i:s'),
                'lettingtype_created_by' => 1
            );
            $result = $this->Propertymaster_model->addLettingtype($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'lettingtype_name' => $this->input->post('Type'),
                'lettingtype_desc' => $this->input->post('Desc'),
                'lettingtype_modified_on' => date('yy-mm-dd H:i:s'),
                'lettingtype_modified_by' => 1
            );
            $result = $this->Propertymaster_model->updateLettingtype($data, $id);
        }
        return $result;
    }

    public function getLettingtype() {

 $delc = '';
         $del = $this->db->query("SELECT DISTINCT(property_lettype)  FROM pro_tbl_property");
        $delchecker = $del->result();
        foreach ($delchecker as $key1 => $value1) {
            $delc[] .= $value1->property_lettype;
        }

         $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];
        $result = $this->Propertymaster_model->getLettingtype();
        $html = '';
        foreach ($result as $key => $res) {
           
            $html.='<tr id="' . $res->lettingtype_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->lettingtype_name . '</td>
                            <td>' . $res->lettingtype_desc . '</td>';
             if ($access == 1 || $access == 2) {
                $html.='<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->lettingtype_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a></td>';
            } else {
                $html.='<td>-</td>';
            }
            if ($access == 1 || $access == 2) {

                $html.='<td><a  title="Delete" href="javascript:;" onclick="';
                
                $html.=   in_array($res->lettingtype_id ,$delc ) ? 'df()' :'deleteLettingtype(' . $res->lettingtype_id . ')';

                $html.= '"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';
            } else {
                $html.='<td>-</td>';
            }
            $html.='</tr>';
        }
        echo $html;
    }

    public function deleteLettingtype() {
        $id = $this->input->post('id');
        $data = array(
            'lettingtype_is_deleted' => 1,
            'lettingtype_modified_on' => date('yy-mm-dd H:i:s'),
            'lettingtype_modified_by' => 1
        );
        $result = $this->Propertymaster_model->deleteLettingtype($data, $id);
        echo $result;
    }
    public function checkLettingtype(){
        $type=trim($this->input->post('type'));
        $id = $this->input->post('id');

        if ($id == 0) {
  $query=$this->db->query("select * from easamoveadmin_tbl_lettingtype where  REPLACE(lower(lettingtype_name),' ','')=REPLACE(lower('$type'),' ','') and lettingtype_is_deleted=0");
        }
        else {

             $query=$this->db->query("select * from easamoveadmin_tbl_lettingtype where  REPLACE(lower(lettingtype_name),' ','')=REPLACE(lower('$type'),' ','') and lettingtype_id != $id  and lettingtype_is_deleted=0");
        }

        
        if ($query->num_rows() > 0) {
            echo 1;
        }else{
            echo 0;
        }
    }

}
