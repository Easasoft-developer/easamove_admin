<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Tenures extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Propertymaster_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
$svar = $this->session->userdata['adminuser'];
           $data['access'] = $svar['access'];
            $this->load->view('common/header');
            $this->load->view('propertymaster/propertymaster');
            $this->load->view('propertymaster/tenures',$data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'tenures_name' => $this->input->post('Type'),
                'tenures_desc' => $this->input->post('Desc'),
                'tenures_created_on' => date('yy-mm-dd H:i:s'),
                'tenures_created_by' => 1
            );
            $result = $this->Propertymaster_model->addTenures($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'tenures_name' => $this->input->post('Type'),
                'tenures_desc' => $this->input->post('Desc'),
                'tenures_modified_on' => date('yy-mm-dd H:i:s'),
                'tenures_modified_by' => 1
            );
            $result = $this->Propertymaster_model->updateTenures($data, $id);
        }
        return $result;
    }

    public function getTenures() {

        $delc = '';
         $del = $this->db->query("SELECT DISTINCT(property_tenure)  FROM pro_tbl_property");
        $delchecker = $del->result();
        foreach ($delchecker as $key1 => $value1) {
            $delc[] .= $value1->property_tenure;
        }




         $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];
        $result = $this->Propertymaster_model->getTenures();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->tenures_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->tenures_name . '</td>
                            <td>' . $res->tenures_desc . '</td>';
             if ($access == 1 || $access == 2) {
                $html.='<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->tenures_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></td>';
            } else {
                $html.='<td>-</td>';
            }
            if ($access == 1 || $access == 2) {


$html.='<td><a  title="Delete" href="javascript:;" onclick="';
                
                $html.=   in_array($res->tenures_id ,$delc ) ? 'df()' :'deleteTenures(' . $res->tenures_id . ')';

                $html.= '"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';


            } else {
                $html.='<td>-</td>';
            }
            $html.='</tr>';
        }
        echo $html;
    }

    public function deleteTenures() {
        $id = $this->input->post('id');
        $data = array(
            'tenures_is_deleted' => 1,
            'tenures_modified_on' => date('yy-mm-dd H:i:s'),
            'tenures_modified_by' => 1
        );
        $result = $this->Propertymaster_model->deleteTenures($data, $id);
        echo $result;
    }
    public function checkTenure(){
        $type=trim($this->input->post('type'));
        $id = $this->input->post('id');

         if ($id == 0) {
             $query=$this->db->query("select * from easamoveadmin_tbl_tenures where  REPLACE(lower(tenures_name),' ','')=REPLACE(lower('$type'),' ','') and tenures_is_deleted=0");
         } else {

            $query=$this->db->query("select * from easamoveadmin_tbl_tenures where  REPLACE(lower(tenures_name),' ','')=REPLACE(lower('$type'),' ','') and tenures_id != $id and tenures_is_deleted=0");


         }



        if ($query->num_rows() > 0) {
            echo 1;
        }else{
            echo 0;
        }
    }

}
