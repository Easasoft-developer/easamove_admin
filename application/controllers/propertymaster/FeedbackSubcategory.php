<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class FeedbackSubcategory extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Propertymaster_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {

            $data['feedbackcate'] = $this->Propertymaster_model->getFeedbackcategory();
            $this->load->view('common/header');
            $this->load->view('propertymaster/propertymaster');
            $this->load->view('propertymaster/feedbacksubcategory', $data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'feedbacksubcategory_name' => $this->input->post('Type'),
                'feedbackcategory_id' => $this->input->post('FeedbackCategoryId'),
                'feedbacksubcategory_created_on' => date('yy-mm-dd H:i:s'),
                'feedbacksubcategory_created_by' => 1
            );
            $result = $this->Propertymaster_model->addFeedbacksubcategory($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'feedbacksubcategory_name' => $this->input->post('Type'),
                'feedbackcategory_id' => $this->input->post('FeedbackCategoryId'),
                'feedbacksubcategory_modified_on' => date('yy-mm-dd H:i:s'),
                'feedbacksubcategory_modified_by' => 1
            );
            $result = $this->Propertymaster_model->updateFeedbacksubcategory($data, $id);
        }
        return $result;
    }

    public function getFeedbacksubcategory() {
        $result = $this->Propertymaster_model->getFeedbacksubcategory();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->feedbacksubcategory_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->feedbacksubcategory_name . '</td>
                            <td>' . $res->feedbackcategory_name . '</td>
                            <td><a title="Edit" href="javascript:;" onclick="edit(' . $res->feedbacksubcategory_id . ')"><span class="glyphicon glyphicon-pencil"></span></i></td>
                              <td><a  title="Delete" href="javascript:;" onclick="deleteFeedbacksubcategory(' . $res->feedbacksubcategory_id . ')"><span class="glyphicon glyphicon-trash"></span></a></td>
                        </tr>';
        }
        echo $html;
    }

    public function deleteFeedbacksubcategory() {
        $id = $this->input->post('id');
        $data = array(
            'feedbacksubcategory_is_deleted' => 1,
            'feedbacksubcategory_modified_on' => date('yy-mm-dd H:i:s'),
            'feedbacksubcategory_modified_by' => 1
        );
        $result = $this->Propertymaster_model->deleteFeedbacksubcategory($data, $id);
        echo $result;
    }

}

?>