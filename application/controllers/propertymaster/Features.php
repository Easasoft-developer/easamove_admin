<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Features extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Propertymaster_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
                if (isset($this->session->userdata['adminuser']['logged_in'])&& $this->session->userdata['adminuser']['logged_in']== TRUE) {

        $this->load->view('common/header');
        $this->load->view('propertymaster/propertymaster');
        $this->load->view('propertymaster/features');
        $this->load->view('common/footer');
        } else {
            
        $this->load->view('login');
        }
    }

    public function insert() {
       
        if($this->input->post('id')==0){
            $data = array(
            'features_name' => $this->input->post('FeatureType'),
            'features_comments' => $this->input->post('Comments'),
            'features_created_on' => date('yy-mm-dd H:i:s'),
            'features_created_by' => 1
        );
        $result = $this->Propertymaster_model->addFeatures($data);
        }else{
            $id=$this->input->post('id');
            $data = array(
            'features_name' => $this->input->post('FeatureType'),
            'features_comments' => $this->input->post('Comments'),
            'features_modified_on' => date('yy-mm-dd H:i:s'),
            'features_modified_by' => 1
        );
            $result = $this->Propertymaster_model->updateFeatures($data,$id);
        }
        return $result;
    }

    public function getFeatures() {
         $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];
        $result = $this->Propertymaster_model->getFeatures();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="'.$res->features_id.'">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->features_name . '</td>
                                <td>' . $res->features_comments . '</td>';
            if ($access == 1 || $access == 2) {
                $html.='<td><a title="Edit" href="javascript:;" onclick="edit('.$res->features_id.')"><span class="glyphicon glyphicon-pencil"></span></i></td>';
            } else {
                $html.='<td>-</td>';
            }
            if ($access == 1) {
                $html.='<td><a  title="Delete" href="javascript:;" onclick="deleteFeatures('.$res->features_id.')"><span class="glyphicon glyphicon-trash"></span></a></td>';
            } else {
                $html.='<td>-</td>';
            }
            $html.='</tr>';
        }
        echo $html;
    }
    public function deleteFeatures(){
         $id=$this->input->post('id');
          $data = array(
            'features_is_deleted' => 1,
            'features_modified_on' => date('yy-mm-dd H:i:s'),
            'features_modified_by' => 1
        );
          $result = $this->Propertymaster_model->deleteFeatures($data,$id);
         echo $result;
    }
     public function checkFeatureType(){
        $id=$this->input->post('type');
         $query=$this->db->query("select * from easamoveadmin_tbl_features where  REPLACE(lower(features_name),' ','')=REPLACE(lower('$id'),' ','') and features_is_deleted=0");
        if ($query->num_rows() > 0) {
            echo 1;
        }else{
            echo 0;
        }
    }


}
