<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ConsiderInfo extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Propertymaster_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
$svar = $this->session->userdata['adminuser'];
           $data['access'] = $svar['access'];
            $this->load->view('common/header');
            $this->load->view('propertymaster/propertymaster');
            $this->load->view('propertymaster/considerinfo',$data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'considerinfo_name' => $this->input->post('Type'),
                'considerinfo_desc' => $this->input->post('Desc'),
                'considerinfo_created_on' => date('yy-mm-dd H:i:s'),
                'considerinfo_created_by' => 1
            );
            $result = $this->Propertymaster_model->addConsiderinfo($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'considerinfo_name' => $this->input->post('Type'),
                'considerinfo_desc' => $this->input->post('Desc'),
                'considerinfo_modified_on' => date('yy-mm-dd H:i:s'),
                'considerinfo_modified_by' => 1
            );
            $result = $this->Propertymaster_model->updateConsiderinfo($data, $id);
        }
        return $result;
    }

    public function getConsiderinfo() {

        $delc = '';
         $del = $this->db->query("SELECT DISTINCT(property_considerinfo)  FROM pro_tbl_property");
        $delchecker = $del->result();
        foreach ($delchecker as $key1 => $value1) {
            $delc[] .= $value1->property_considerinfo;
        }

        $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];
        $result = $this->Propertymaster_model->getConsiderinfo();
        $html = '';
        foreach ($result as $key => $res) {
           
            $html.='<tr id="' . $res->considerinfo_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->considerinfo_name . '</td>
                            <td>' . $res->considerinfo_desc . '</td>';
             if ($access == 1 || $access == 2) {
                $html.='<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->considerinfo_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a></td>';
            } else {
                $html.='<td>-</td>';
            }
            if ($access == 1 || $access == 2) {

                $html.='<td><a  title="Delete" href="javascript:;" onclick="';
                
                $html.=   in_array($res->considerinfo_id ,$delc ) ? 'df()' :'deleteConsiderinfo(' . $res->considerinfo_id . ')';

                $html.= '"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';

            } else {
                $html.='<td>-</td>';
            }
            $html.='</tr>';
        }
        echo $html;
    }

    public function deleteConsiderinfo() {
        $id = $this->input->post('id');
        $data = array(
            'considerinfo_is_deleted' => 1,
            'considerinfo_modified_on' => date('yy-mm-dd H:i:s'),
            'considerinfo_modified_by' => 1
        );
        $result = $this->Propertymaster_model->deleteConsiderinfo($data, $id);
        echo $result;
    }
    public function checkConsiderinfo(){
        $type=trim($this->input->post('type'));

         if ($id == 0) {
             $query=$this->db->query("select * from easamoveadmin_tbl_considerinfo where  REPLACE(lower(considerinfo_name),' ','')=REPLACE(lower('$type'),' ','') and considerinfo_is_deleted=0");
         } else {

            $query=$this->db->query("select * from easamoveadmin_tbl_considerinfo where  REPLACE(lower(considerinfo_name),' ','')=REPLACE(lower('$type'),' ','') and considerinfo_id != $id and considerinfo_is_deleted=0");

         }


        if ($query->num_rows() > 0) {
            echo 1;
        }else{
            echo 0;
        }
    }

}
