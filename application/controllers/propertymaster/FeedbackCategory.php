<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class FeedbackCategory extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Propertymaster_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {

            $this->load->view('common/header');
            $this->load->view('propertymaster/propertymaster');
            $this->load->view('propertymaster/feedbackcategory');
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'feedbackcategory_name' => $this->input->post('Type'),
                'feedbackcategory_desc' => $this->input->post('Desc'),
                'feedbackcategory_created_on' => date('yy-mm-dd H:i:s'),
                'feedbackcategory_created_by' => 1
            );
            $result = $this->Propertymaster_model->addFeedbackcategory($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'feedbackcategory_name' => $this->input->post('Type'),
                'feedbackcategory_desc' => $this->input->post('Desc'),
                'feedbackcategory_modified_on' => date('yy-mm-dd H:i:s'),
                'feedbackcategory_modified_by' => 1
            );
            $result = $this->Propertymaster_model->updateFeedbackcategory($data, $id);
        }
        return $result;
    }

    public function getFeedbackcategory() {
        $result = $this->Propertymaster_model->getFeedbackcategory();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->feedbackcategory_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->feedbackcategory_name . '</td>
                            <td>' . $res->feedbackcategory_desc . '</td>
                            <td><a title="Edit" href="javascript:;" onclick="edit(' . $res->feedbackcategory_id . ')"><span class="glyphicon glyphicon-pencil"></span></i></td>
                              <td><a  title="Delete" href="javascript:;" onclick="deleteFeedbackcategory(' . $res->feedbackcategory_id . ')"><span class="glyphicon glyphicon-trash"></span></a></td>
                        </tr>';
        }
        echo $html;
    }

    public function deleteFeedbackcategory() {
        $id = $this->input->post('id');
        $data = array(
            'feedbackcategory_is_deleted' => 1,
            'feedbackcategory_modified_on' => date('yy-mm-dd H:i:s'),
            'feedbackcategory_modified_by' => 1
        );
        $result = $this->Propertymaster_model->deleteFeedbackcategory($data, $id);
        echo $result;
    }

}
