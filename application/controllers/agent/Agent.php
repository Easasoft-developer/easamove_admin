<?php 

defined('BASEPATH') OR exit('No direct script access allowed');

class Agent extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper(array('form', 'url'));
        $this->load->model('Agent_model');
        $this->load->model('Master_model');
        $this->load->library('session');

// Your own constructor code
    }


    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
          $svar = $this->session->userdata['adminuser'];
            $data['access'] = $svar['access'];

            $this->load->view('common/header');
            $this->load->view('agent/agentmain');
            $this->load->view('agent/agent',$data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }





 public function agentdb() {
    if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
         $svar = $this->session->userdata['adminuser'];
            $data['access'] = $svar['access'];


            $this->load->view('common/header');
            $this->load->view('agent/agentmain');
            $this->load->view('agent/agentdb',$data);
            $this->load->view('common/footer');

    } else {

            $this->load->view('login');
        }

    }



public function loadagentdb() {


$result = $this->Master_model->loadagentdb();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->id . '">
<td><input class="checkbox1" type="checkbox" name="ids" value="'.$res->id.'" /></td>
                    <td>'.($key + 1).'</td>
                    <td>'.$res->serial.'</td>
                    <td>'.$res->company.'</td>
                    <td>'.$res->branch.'</td>
                    <td>'.$res->address.'</td>
                    <td>'.$res->contact.'</td>
                    <td>'.$res->phone.'</td>
                    <td>'.$res->email.'</td>
                    <td>'.$res->website.'</td>';
           

    //        if($res->status == 2) {
    //     $html.= '<td>'."No".'</td>';            

    //        } else {
    // $html.= '<td>'."No <a title='Approve' href='javascript:;' onclick=\"hashdel('".$res->hash."')\" >(Approve)</a>".'</td>';            

    //        }



           $html .= "<td><button  type='button' class='btn btn-primary btn-xs' onclick=\"edit('".$res->id."')\" ><i class='fa fa-pencil'></i></button></td>";

           $html .= "<td><button  type='button' class='btn btn-danger btn-xs' onclick=\"delhash('".$res->id."')\" ><i class='fa fa-times'></i></button></td>";
            $html.='</tr>';
        }
        echo $html;


}



public function addagentdb() {
if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {


$query = $this->Master_model->addagentdb($_POST);

    echo $query;

 } else {

            $this->load->view('login');
        }

}



public function deleteagentdb() {
if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {


$query = $this->Master_model->deleteagentdb($_POST['data']);

echo $query;

 } else {

            $this->load->view('login');
        }

}


public function editagentdb() {
if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {





 } else {

            $this->load->view('login');
        }
}





 public function approval() {
    if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {

            $this->load->view('common/header');
            $this->load->view('agent/agentmain');
            $this->load->view('agent/approval');
            $this->load->view('common/footer');

    } else {

            $this->load->view('login');
        }

    }







    public function approveit() {

    $query = $this->Master_model->approveit($_POST['data']);

    echo $query;
    }


    public function deleteapproval() {
        // print_r($_POST['data']);
    $query = $this->Master_model->deleteapproval($_POST['data']);

    echo $query;

    }





public function loaduser() {

 $result = $this->Master_model->userapproval();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->id . '">
            
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->contact . '</td>
                                <td>' . $res->email . '</td>
                                <td>' . $res->phone . '</td>';
           if($res->status == 2) {
            $html.= '<td>'."Waiting for approval".'</td>';

           } else {

            $html.= '<td>'."Responce recieved".'</td>';            
           }

           if($res->status == 2) {
            $html.= '<td>'."No".'</td>';
        

           } else {

            // $agent_id = !empty($res->agent_id) ? $res->agent_id : ;



    $html.= '<td>'."<button type='button' class='btn btn-primary btn-xs' onclick=\"hashdel('".$res->hash."')\" >Approve</button>".'</td>';    

           }




           if(empty($res->agent_id)) { 
$html.= '<td>'."No".'</td>';            

} else {
$html.= '<td>'."<a target='_blank' href='".base_url('agent/agent/editAgent/'.$res->id)."'><button type='button' class='btn btn-primary btn-xs'>View</button></a>".'</td>';   

}







           $html .= "<td><button  type='button' class='btn btn-danger btn-xs' onclick=\"delhash('".$res->hash."')\" ><i class='fa fa-times'></i></button></td>";
            $html.='</tr>';
        }
        echo $html;


}


        public function addapproval() {

            $values =  $this->input->post('datas');
        
               $val =  $this->Master_model->selectfewagentdb($values);


            foreach ($val as $key => $column) {
    

            $salt = str_shuffle(md5($column['email']));

            
            $this->Master_model->adduserapproval($column['id'],$salt);
           

            $affected = $this->db->affected_rows();


            $data['name'] = $column['contact'];
            $data['link'] = base_url() . 'agent/agent/agentAdd?salt='.$salt;


            $message = $this->load->view('mail_template/agent_approval', $data, true);

            $this->load->library('email');     

            $this->email->from('info@admin.easamove.co.uk', 'Easamove Admin');
            
            $this->email->to($column['email']);
            $this->email->subject('Agent creation');
            
            $this->email->message($message);    
            
            $this->email->send();            


}


        }





   public function agentUser() {
 if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
            
            $this->load->view('common/header');
            $this->load->view('agent/agentmain');
            $this->load->view('agent/agentuser');
            $this->load->view('common/footer');

            } else {

            $this->load->view('login');
        }
    }


public function getagentuser() {


 if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
$result = $this->Agent_model->agentUser();

    $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];

        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->agentuser_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td style="text-transform: capitalize;">';

      $html.=   $res->agentuser_name;

                   $html.=          '</td>
                            <td>' . $res->agentuser_email . '</td>
                            <td>' . $res->agentuser_mobile . '</td>
                            <td>' . $res->agentuser_userrole . '</td>';
                            // '<td>' . $res->phone_no . '</td>
                            // <td>' . $res->fax . '</td>'
if ($access == 1 || $access == 2) {

                         $html.=    '<td><a title="Edit" href="' . base_url('agent/agent/editagentuser') . '/' . $res->agentuser_id . '"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a></td>';
                         $html.=   '<td><a  title="Delete" href="javascript:;" onclick="deleteAgent(' . $res->agentuser_id . ')"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';
                          } 
                          else 

                          {

       $html.='<td>-</td>';  $html.='<td>-</td>'; 
                          }

                        $html.='</tr>';
        }
        echo $html;


           } else {

            $this->load->view('login');
        }
}


public function editagentuser($get_id) {

    if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
// if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['agentuser']['logged_in'] == TRUE) {
            $get_id = $this->uri->segment(4);
            // $data1['branch'] = $this->Agent_model->getbranchcnt();
            // $data1['users'] = $this->Agent_model->getUsercnt();
            $data['user'] = $this->Agent_model->getUserdata($get_id);
            $this->load->view('common/header');
            $this->load->view('agent/agentmain',$data);
            $this->load->view('agent/editagentuser',$data);
            $this->load->view('common/footer');
        // } else {

        //     $this->load->view('login');
        // }
  } else {

            $this->load->view('login');
        }


}




public function addagentuser() {
if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {

 $id = $this->session->userdata['adminuser']['id'];
        // if ($this->input->post('id') != 0) {

            // $id = $this->input->post('id');
             $aid = $this->input->post('aid');
            $data = array(
                'agentuser_name' => $this->input->post('username'),
                'agentuser_mobile' => $this->input->post('usermobile'),
                'agentuser_userrole' => $this->input->post('userrole'),
                'agentuser_access' => $this->input->post('permission'),
                'agentuser_modified_on' => date('Y-m-d H:i:s'),
                // 'agentuser_modified_by' => $id
            );
            $result = $this->Agent_model->updateAgentuser($data, $aid);
            if($result){
                $this->session->set_flashdata('userAdd', 'User updated successfully');
                // redirect('settings/usermanage');
            }
        // }
        //     print_r($aid);
        // print_r($result);
 } else {

            $this->load->view('login');
        }
}




public function deleteUseragent() {
if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
    $this->load->model('Master_model');
$id  = $this->input->post('id');
    $result = $this->Master_model->deleteuseragent($id);
        $this->db->query("UPDATE pro_tbl_property SET property_is_deleted = 1 WHERE agent_id='$id'");
    echo $result;
} else {

            $this->load->view('login');
        }
    
}






    public function agentAdd() {



if( isset($_GET['salt'] ) ) {

       $data['agenttype'] = $this->Master_model->getAgenttype();

            //  for member in
        $data['get_member'] = $this->Master_model->agent_member_in();
       // member in ends here
        $data['agent_m'] = $this->Master_model->get_mem();

       $verified = $this->Master_model->checkhash($_GET['salt']);

if(isset($verified)) {

$data['name'] = $verified['contact'];$data['email'] = $verified['email'];$data['phone'] = $verified['phone'];
$this->load->view('common/temp_header');
$this->load->view('agent/agentadd', $data);
$this->load->view('common/footer'); 

} else {

// $this->load->view('common/temp_header');
$this->load->view('agent/invalid');  
}




} else {

        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
            $data['agenttype'] = $this->Master_model->getAgenttype();

            //  for member in
        $data['get_member'] = $this->Master_model->agent_member_in();
       // member in ends here


        $data['agent_m'] = $this->Master_model->get_mem();

            $this->load->view('common/header');
            $this->load->view('agent/agentmain');
            $this->load->view('agent/agentadd', $data);
            $this->load->view('common/footer');



        } else {

            $this->load->view('login');
        }
    

}
    }








    public function insert() {
        
if(!empty($_POST['hash']) ) {

$deleted = 1;
$hash = $this->input->post('hash');
 } else {

$deleted = 0;
$hash = '';
 }


        $config = array(
            'upload_path' => 'images/',
            'allowed_types' => 'jpg|gif|png|jpeg',
            'overwrite' => 1,
//            'file_name' =>$newFileName,
        );
        if($_FILES['agentImage']['name']!=''){
        $this->load->library('upload', $config);
//           print_r($_FILES);
        $this->upload->initialize($config);
        if (!$this->upload->do_upload('agentImage')) {
            $error = array('error' => $this->upload->display_errors());
        } else {
            $data1 = $this->upload->data();

            $file_path = $data1['file_path'];
            $file = $data1['full_path'];
            $file_ext = $data1['file_ext'];

            echo $final_file_name = $this->input->post('cName') . '_' . time() . '' . $file_ext;
            $rename = rename($file, $file_path . $final_file_name);
        }
        }else{
            echo $final_file_name=$this->input->post('imageedit');
        }

// here is the renaming functon







if($this->input->post('mem') != null) { $mem = implode(',', $this->input->post('mem')); } else { $mem = ''; }
// print_r($mem); 
        if ($this->input->post('id') == 0) {
            $key = $this->generateRandomString(16);



    //agent mail

            $this->load->library('email');     
          

          




       
          //others  

            $data = array(
                'member_id' => $mem,
                'agent_refno' => $this->input->post('agentrefno'),
                'branch_name' => $this->input->post('bName'),
                'position' => $this->input->post('position'),
                 'display_name' => $this->input->post('displayName'),
                'agent_name' => $this->input->post('cName'),
                // 'vat_reg_no' => $this->input->post('vatregno'),
                // 'vat_reg_country' => $this->input->post('vatregcty'),
                'agent_key' => $this->generateRandomString(8),
                'num_of_branch' => $this->input->post('nobranch'),
                // 'default_currency' => $this->input->post('defcur'),
                // 'invoice_due_day' => $this->input->post('invoicedue'),
                // 'work_start_time' => $this->input->post('workstart'),
                // 'work_end_time' => $this->input->post('workend'),
                'agent_logo' => $final_file_name,
                'dwelling' => $this->input->post('AddressCityName'),
                'number' => $this->input->post('Number'),
                'street' => $this->input->post('AddressStreet'),
                'locality' => $this->input->post('AddressStreet'),
                'town' => $this->input->post('AddressCityName'),
                // 'country' => $this->input->post('AddressCounty'),
                'county' => $this->input->post('County'),
                'post_code' => $this->input->post('AddressZip'),
                // 'title' => $this->input->post('title'),
                'forename' => $this->input->post('Forename'),
                // 'agent_password' => $this->generateRandomString(5),
                // 'surname'=> $this->input->post('Surname'),
                'email_id'=> $this->input->post('Email'),
                'phone_no'=> $this->input->post('Phone'),
                // 'fax'=> $this->input->post('Fax'),
                'website'=> $this->input->post('Website'),
                'agent_type'=> $this->input->post('AgentTypeId'),
                'agent_aboutus'=> $this->input->post('About'),
                'agent_created_on' => date('yy-mm-dd H:i:s'),
                'agent_created_by' => 1,
                'agent_is_deleted' => $deleted,
                'hash' => $hash,
                'agent_key' => $key,
                'agent_lat' => $this->input->post('Latitude'),
                'agent_long' => $this->input->post('Longitude'),
            );
            $result = $this->Agent_model->addAgent($data,$key,$deleted,$hash);

        


      $mydata['name'] = $this->input->post('Name');

            $mydata['link'] = base_url() . 'agent/agent/verify/' . $key;

            $message = $this->load->view('mail_template/agent_welcome', $mydata, true);

            $this->email->from('info@admin.easamove.co.uk', 'Easamove Admin');
            
            $this->email->to($this->input->post('Email'));
            $this->email->subject('Welcome Agent');
            
            $this->email->message($message);    
            
            $this->email->send();


        } else {

            $id = $this->input->post('id');
            $data = array(
                'member_id' => $mem,
                'agent_refno' => $this->input->post('agentrefno'),
                 'branch_name' => $this->input->post('bName'),
                'position' => $this->input->post('position'),
                 'display_name' => $this->input->post('displayName'),
                'agent_name' => $this->input->post('cName'),
                // 'vat_reg_no' => $this->input->post('vatregno'),
                // 'vat_reg_country' => $this->input->post('vatregcty'),
                'agent_key' => $this->generateRandomString(8),
                'num_of_branch' => $this->input->post('nobranch'),
                // 'default_currency' => $this->input->post('defcur'),
                // 'invoice_due_day' => $this->input->post('invoicedue'),
                // 'work_start_time' => $this->input->post('workstart'),
                // 'work_end_time' => $this->input->post('workend'),
                'agent_logo' => $final_file_name,
                'dwelling' => $this->input->post('AddressCityName'),
                'number' => $this->input->post('Number'),
                'street' => $this->input->post('AddressStreet'),
                'locality' => $this->input->post('AddressStreet'),
                'town' => $this->input->post('AddressCityName'),
                // 'country' => $this->input->post('AddressCounty'),
                'county' => $this->input->post('County'),
                'post_code' => $this->input->post('AddressZip'),
                // 'title' => $this->input->post('title'),
                'forename' => $this->input->post('Forename'),
                // 'surname'=> $this->input->post('Surname'),
                'email_id'=> $this->input->post('Email'),
                'phone_no'=> $this->input->post('Phone'),
                // 'fax'=> $this->input->post('Fax'),
                'website'=> $this->input->post('Website'),
                'agent_type'=> $this->input->post('AgentTypeId'),
                'agent_aboutus'=> $this->input->post('About'),
                'agent_created_on' => date('yy-mm-dd H:i:s'),
                'agent_created_by' => 1,
                'agent_key' => $key,
                'agent_lat' => $this->input->post('Latitude'),
                'agent_long' => $this->input->post('Longitude'),
            );
            $result = $this->Agent_model->updateAgent($data, $id);

            

        }
        return $result;
    }

    public function verify($key = '') {
//        $key = $this->input->get('key');
        if ($key != '') {
            $res = $this->Agent_model->verify($key);
            if ($res) {


                $this->load->library('email');

                $this->session->set_flashdata('logincheck', 'Agent verified');
                
                $mydata['name'] = $res[0]->agent_name;
                $mydata['email'] = $res[0]->email_id;
                $mydata['password'] = $res[0]->agentuser_password;

                $message = $this->load->view('mail_template/agent_verify', $mydata, true);
                $this->email->from('info@admin.easamove.co.uk', 'Easamove Admin');

                $this->email->to($res[0]->email_id);

                $this->email->subject('Welcome to Easamove');

                $this->email->message($message);

                $this->email->send();
            }
            
        } else {
            $this->session->set_flashdata('logincheck', '');
        }
        $this->load->view('login');
    }

    public function getAgent() {
        $result = $this->Agent_model->getAgent();

 $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];

        $html = '';
        foreach ($result as $key => $res) {
            // print_r($result);
            $html.='<tr id="' . $res->agent_id . '">

                            <td>' . ($key + 1) . '</td>
                            <td>'.$res->agent_refno.'</td>
                            <td style="text-transform: capitalize;">';

      $html.=  $res->agent_head_office == 0 ? $res->agent_name. "<strong> (H.O)</strong>" :  $res->agent_name. "<strong> (B)<strong>";

                   $html.=          '</td>
                            <td>' .$res->post_code.'</td>
                            <td>' . $res->street . '</td>
                            <td>' . $res->type . '</td>
                            <td>' . $res->email_id . '</td>
                            <td>' . $res->phone_no . '</td>
                            <td align="center"><a title="Edit" href="' . base_url('agent/agent/getBranchDetails') . '/' . $res->agent_id . '"><button type="button" class="btn btn-primary btn-xs" style="">B</button></a><a title="Edit" href="' . base_url('agent/agent/getUsersDetails') . '/' . $res->agent_id . '"><button type="button" class="btn btn-primary btn-xs" style="margin-left:3px">U</button></a></td>';
if ($access == 1 || $access == 2) {

                         $html.=    '<td><a title="Edit" href="' . base_url('agent/agent/editAgent') . '/' . $res->agent_id . '"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a></td>';
                         $html.=   '<td><a  title="Delete" href="javascript:;" onclick="deleteAgent(' . $res->agent_id . ')"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';
                          } 
                          else 

                          {

       $html.='<td>-</td>';  $html.='<td>-</td>'; 
                          }

                        $html.='</tr>';
        }
        echo $html;
    }

    public function deleteAgent() {
        $id = $this->input->post('id');
        $data = array(
            'agent_is_deleted' => 1,
            'agent_modified_on' => date('yy-mm-dd H:i:s'),
            'agent_modified_by' => 1
        );
        $result = $this->Agent_model->deleteAgent($data, $id);
         // if($result == 1) {
        $this->db->query("UPDATE pro_tbl_property SET property_is_deleted = 1 WHERE agent_id='$id'");
        // }
        echo $result;
    }

    public function getAgentall() {

        $result = $this->Agent_model->getAgent();
        echo json_encode($result);
    }

    function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function editAgent($get_mem) {
 $data['agent_m'] = $this->Master_model->get_mem($get_mem);
         //  for member in
        $data['get_member'] = $this->Master_model->agent_member_in();
       // member in ends here
        $site = $this->uri->segment(4);
        if ($site != '') {
            $id = $site;
        } else {
            $id = 0;
        }
        $data['agenttype'] = $this->Master_model->getAgenttype();
        $data['agent'] = $this->Agent_model->getAgentEdit($id);

        $this->load->view('common/header');
        $this->load->view('agent/agentmain');
        $this->load->view('agent/agentadd', $data);
        $this->load->view('common/footer');
    }

    public function export() {
        $this->load->library('PHPExcel');
//        $this->load->library('PHPExcel/IOFactory');

        $objPHPExcel = new PHPExcel();
//        $reader = PHPExcel_IOFactory::createReader('Excel5');
        $objPHPExcel->getProperties()->setTitle("export")->setDescription("none");

        $objPHPExcel->setActiveSheetIndex(0);
        $query = $this->db->query("SELECT * FROM easamoveadmin_tbl_agent where agent_is_deleted=0");
        // Field names in the first row
        $fields = $query->list_fields();
        $col = 0;
        foreach ($fields as $field) {
            $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, 1, $field);
            $col++;
        }

        // Fetching the table data
        $row = 2;
        foreach ($query->result() as $data) {
            $col = 0;
            foreach ($fields as $field) {
                $objPHPExcel->getActiveSheet()->setCellValueByColumnAndRow($col, $row, $data->$field);
                $col++;
            }

            $row++;
        }

        $objPHPExcel->setActiveSheetIndex(0);

        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');

        // Sending headers to force the user to download the file
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="Agents_' . date('Y-m-d') . '.csv"');
        header('Cache-Control: max-age=0');

//     $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, "PDF");
        $objWriter->save('php://output');
    }
    public function checkref(){
          $ref=$this->input->post('ref');
         $query=$this->db->query("select * from  easamoveadmin_tbl_agent where  REPLACE(lower(agent_refno),' ','')=REPLACE(lower('$ref'),' ','') and agent_is_deleted=0");
        if ($query->num_rows() > 0) {
            echo 1;
        }else{
            echo 0;
        }
    }


  public function checkemail(){
         $eid = $this->input->post('email');
         $query=$this->db->query("select * from  pro_tbl_agentuser where agentuser_is_deleted=0 and agentuser_email='$eid'");
        if ($query->num_rows() > 0) {
            echo 1;
        }else{
            echo 0;
        }
    }

    //view agent branch list 
    public function getUsersDetails($id){
        $data['agentId'] = $id;
        $this->load->view('common/header');
        $this->load->view('agent/agentmain');
        $this->load->view('agent/agent_users', $data);
        $this->load->view('common/footer');

    }

    //view agent branch list 
    public function getBranchDetails($id){
        $data['agentId'] = $id;
        $this->load->view('common/header');
        $this->load->view('agent/agentmain');
        $this->load->view('agent/agent_branches', $data);
        $this->load->view('common/footer');

    }

    public function getagentbranch(){
        $result = $this->Agent_model->getAgentBranch();

        $svar = $this->session->userdata['adminuser'];
            $access = $svar['access'];

            $html = '';
            foreach ($result as $key => $res) {
                $html.='<tr id="' . $res->agent_id . '">
                                <td>' . ($key + 1) . '</td>
                                <td style="text-transform: capitalize;">';

          $html.=  $res->agent_head_office == 0 ? $res->agent_name. "<strong> (H.O)</strong>" :  $res->agent_name. "<strong> (B)<strong>";

                       $html.=          '</td>
                                <td>' . $res->street . '</td>
                                <td>' . $res->type . '</td>
                                <td>' . $res->email_id . '</td>
                                <td>' . $res->phone_no . '</td>
                                <td><a title="Edit" href="' . base_url('agent/agent/getUsersDetails') . '/' . $res->agent_id . '"><button type="button" class="btn btn-primary btn-xs" style="padding-right: 12px;">Users List </button></a></td>';
        if ($access == 1 || $access == 2) {

                             $html.=    '<td><a title="Edit" href="' . base_url('agent/agent/editAgent') . '/' . $res->agent_id . '"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a></td>';
                             $html.=   '<td><a  title="Delete" href="javascript:;" onclick="deleteAgent(' . $res->agent_id . ')"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';
                              } 
                              else 

                              {

           $html.='<td>-</td>';  $html.='<td>-</td>'; 
                              }

                            $html.='</tr>';
            }
            echo $html;
    }



    //create pdf and print function
    public function getAgentData() {
         $aid = $this->session->userdata['adminuser']['id'];
         $aname = $this->session->userdata['adminuser']['name'];
         $whr = "";
        $selection = $this->input->post('sel');
        //echo $selection;
        if($selection != ""){
            $whr = "and (a.agent_name like '%$selection%' or a.street like '%$selection%' or b.agent_name like '%$selection%' or email_id like '%$selection%' or phone_no like '%$selection%')";
        }
        $this->load->library('Pdf');
//        
        $pdf = new Pdf(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        $pdf->SetAuthor('Nicola Asuni');
        $pdf->SetTitle('');
        $pdf->SetSubject('TCPDF Tutorial');
        $pdf->SetKeywords('Customer');

        $pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', '8'));
        $pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

        $pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

        $pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
        $pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
        $pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

// set auto page breaks
        $pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

// set image scale factor
        $pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);

// set some language-dependent strings (optional)
        if (@file_exists(dirname(__FILE__) . '/lang/eng.php')) {
            require_once(dirname(__FILE__) . '/lang/eng.php');
            $pdf->setLanguageArray($l);
        }

// ---------------------------------------------------------
// set font
        $pdf->SetFont('helvetica', '', 10);

// add a page
        $pdf->AddPage();


        $query = $this->db->query("SELECT a.*,b.agent_name as type FROM easamoveadmin_tbl_agent a left join easamoveadmin_tbl_agenttype b on (a.agent_type = b.agent_id) where a.agent_is_deleted=0 and a.agent_head_office=0 $whr");
        // echo $this->db->last_query();
        // exit();
        $html = '';
        
        $html.='<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
</head><body style="font-size:8px;font-family:Arial, Helvetica, sans-serif">';
$html .= '<table width="100%" cellpadding="4" cellspacing="1">
                <tr>
                  <td style="text-align:left;font-style:12px;font-size:18px;">Easamove Agent List</td>
                </tr>
              </table>';

$html .= '<table  bgcolor="#E1E1E1"  width="100%" cellpadding="4" cellspacing="1" >
                    <thead>
                        <tr bgcolor="#E1E1E1" style="color: #fff;">
                            <th width="10%" style="color: #000000; font-weight: bold; ">SERIAL NO</th>
                            <th width="10%" style="color: #000000; font-weight: bold; ">AGENT NAME</th>
                            <th width="20%" style="color: #000000; font-weight: bold; ">AGENT EMAIL</th>
                            <th width="20%" style="color: #000000; font-weight: bold; ">AGENT MOBILE</th>
                            <th width="30%" style="color: #000000; font-weight: bold; ">AGENT ADDRESS</th>
                            <th width="10%" style="color: #000000; font-weight: bold; ">AGENT TYPE</th>
                            
                        </tr>
                    </thead></table><table  bgcolor="#F4F2F2"  width="100%" cellpadding="4" cellspacing="1">
                    <tbody class="tb">';
        foreach ($query->result() as $key=>$row) {
            
            
            $html.='<tr >'
                    . '<td width="10%" align="left" bgcolor="#FFFFFF">' . ($key+1) . '</td>'
                    . '<td width="10%" align="left" bgcolor="#FFFFFF">' . ucfirst($row->agent_name) . '</td>'
                    . '<td  width="20%" align="left" bgcolor="#FFFFFF">'. $row->email_id . '</td>'
                    . '<td  width="20%" align="left" bgcolor="#FFFFFF">' . $row->phone_no . '</td>'
                    . '<td  width="30%" align="left" bgcolor="#FFFFFF">' . $row->number.', '.$row->street.', '.$row->post_code . '</td>'
                    . '<td  width="10%" align="left" bgcolor="#FFFFFF">' . $row->type . '</td>'
                    . '</tr>';
        }
        $html.='</tbody></table></body></html>';
        $pdf->writeHTML($html, true, false, true, false, '');

        $filename = 'agentdata'.'_'.$aid.'_'.date('Y-m-d');
//            $filelocation = base_url()."Images/pdfimg"; //Linux
//             $fileNL = $filelocation."/".$filename; //Linux
        ob_clean();
        // $root = realpath($_SERVER["DOCUMENT_ROOT"]);
        $filetosave = '/images/' . $filename . '.pdf';
        $output = $pdf->Output($filetosave, 'FI');
    }




}
