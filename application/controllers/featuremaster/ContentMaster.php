<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ContentMaster extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Featuremaster_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
$svar = $this->session->userdata['adminuser'];
           $data['access'] = $svar['access'];
            $this->load->view('common/header');
            $this->load->view('featuremaster/featuremaster');
            $this->load->view('featuremaster/contentmaster',$data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'content_name' => $this->input->post('content'),
                'content_desc' => $this->input->post('Desc'),
                'content_created_on' => date('yy-mm-dd H:i:s'),
                'content_created_by' => 1
            );
            $result = $this->Featuremaster_model->addContentmaster($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'content_name' => $this->input->post('content'),
                'content_desc' => $this->input->post('Desc'),
                'content_modified_on' => date('yy-mm-dd H:i:s'),
                'content_modified_by' => 1
            );
            $result = $this->Featuremaster_model->updateContentmaster($data, $id);
        }
        return $result;
    }

    public function getContentmaster() {
         $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];
        $result = $this->Featuremaster_model->getContentmaster();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->content_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->content_name . '</td>
                                <td>' . $res->content_desc . '</td>';
            if ($access == 1 || $access == 2) {
                $html.='<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->content_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a></td>';
        $html.='<td><a  title="Delete" href="javascript:;" onclick="deleteContentmaster(' . $res->content_id . ')"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';
            } else {
                $html.='<td>-</td>';
                $html.='<td>-</td>';
            }
            $html.='</tr>';
        }
        echo $html;
    }

    public function deleteContentmaster() {
        $id = $this->input->post('id');
        $data = array(
            'content_is_deleted' => 1,
            'content_modified_on' => date('yy-mm-dd H:i:s'),
            'content_modified_by' => 1
        );
        $result = $this->Featuremaster_model->deleteContentmaster($data, $id);
        echo $result;
    }
   public function checkContentmaster(){
        $type=trim($this->input->post('type'));
        $id = $this->input->post('id');
         
        if($id == 0) {
            $query=$this->db->query("select * from easamoveadmin_tbl_content where  REPLACE(lower(content_name),' ','')=REPLACE(lower('$type'),' ','') and content_is_deleted=0");

        } else {

            $query=$this->db->query("select * from easamoveadmin_tbl_content where  REPLACE(lower(content_name),' ','')=REPLACE(lower('$type'),' ','') and  content_id != $id and content_is_deleted=0");
        }


        if ($query->num_rows() > 0) {
            echo 1;
        }else{
            echo 0;
        }
    }
}
