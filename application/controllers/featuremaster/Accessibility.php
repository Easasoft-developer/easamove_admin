<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Accessibility extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Featuremaster_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
$svar = $this->session->userdata['adminuser'];
           $data['access'] = $svar['access'];
            $this->load->view('common/header');
            $this->load->view('featuremaster/featuremaster');
            $this->load->view('featuremaster/accessibility',$data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'accessibility_name' => $this->input->post('accessibility'),
                'accessibility_desc' => $this->input->post('Desc'),
                'accessibility_created_on' => date('yy-mm-dd H:i:s'),
                'accessibility_created_by' => 1
            );
            $result = $this->Featuremaster_model->addAccessibility($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'accessibility_name' => $this->input->post('accessibility'),
                'accessibility_desc' => $this->input->post('Desc'),
                'accessibility_modified_on' => date('yy-mm-dd H:i:s'),
                'accessibility_modified_by' => 1
            );
            $result = $this->Featuremaster_model->updateAccessibility($data, $id);
        }
        return $result;
    }

    public function getAccessibility() {
         $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];$delc = '';

 $del = $this->db->query("SELECT DISTINCT(property_access)  FROM pro_tbl_property");
   
        foreach ($del->result() as $key1 => $value1) {
            $delc[] .= $value1->property_access;
        }

        $result = $this->Featuremaster_model->getAccessibility();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->accessibility_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->accessibility_name . '</td>
                                <td>' . $res->accessibility_desc . '</td>';
            if ($access == 1 || $access == 2) {
                $html.='<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->accessibility_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a></td>';




 $html.='<td><a  title="Delete" href="javascript:;" onclick="';
                
                $html.=   in_array($res->accessibility_id ,$delc ) ? 'df()' :'deleteAccessibility(' . $res->accessibility_id . ')';

                $html.= '"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';




            // $html.='<td><a  title="Delete" href="javascript:;" onclick="deleteAccessibility(' . $res->accessibility_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-times"></i></button></a></td>';



            } else {
                $html.='<td>-</td>';
                $html.='<td>-</td>';
            }
            $html.='</tr>';
        }
        echo $html;
    }

    public function deleteAccessibility() {
        $id = $this->input->post('id');
        $data = array(
            'accessibility_is_deleted' => 1,
            'accessibility_modified_on' => date('yy-mm-dd H:i:s'),
            'accessibility_modified_by' => 1
        );
        $result = $this->Featuremaster_model->deleteAccessibility($data, $id);
        echo $result;
    }
   public function checkAccessibility(){
        $type=trim($this->input->post('type'));
        $id = $this->input->post('id');

        if($id == 0) {
             $query=$this->db->query("select * from easamoveadmin_tbl_accessibility where  REPLACE(lower(accessibility_name),' ','')=REPLACE(lower('$type'),' ','') and accessibility_is_deleted=0");

        } else {

             $query=$this->db->query("select * from easamoveadmin_tbl_accessibility where  REPLACE(lower(accessibility_name),' ','')=REPLACE(lower('$type'),' ','') and accessibility_id != $id and accessibility_is_deleted=0");

        }


        if ($query->num_rows() > 0) {
            echo 1;
        }else{
            echo 0;
        }
    }
}
