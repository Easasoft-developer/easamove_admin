<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Furnishes extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Featuremaster_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
$svar = $this->session->userdata['adminuser'];
           $data['access'] = $svar['access'];
            $this->load->view('common/header');
            $this->load->view('featuremaster/featuremaster');
            $this->load->view('featuremaster/furnishes',$data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }

    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'furnishes_name' => $this->input->post('FurnishType'),
                'furnishes_desc' => $this->input->post('FurnishDesc'),
                'furnishes_created_on' => date('yy-mm-dd H:i:s'),
                'furnishes_created_by' => 1
            );
            $result = $this->Featuremaster_model->addFurnishes($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'furnishes_name' => $this->input->post('FurnishType'),
                'furnishes_desc' => $this->input->post('FurnishDesc'),
                'furnishes_modified_on' => date('yy-mm-dd H:i:s'),
                'furnishes_modified_by' => 1
            );
            $result = $this->Featuremaster_model->updateFurnishes($data, $id);
        }
        return $result;
    }

    public function getFurnishes() {
         $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];$delc = '';

 $del = $this->db->query("SELECT DISTINCT(property_furnish)  FROM pro_tbl_property");
   
        foreach ($del->result() as $key1 => $value1) {
            $delc[] .= $value1->property_furnish;
        }



        $result = $this->Featuremaster_model->getFurnishes();
        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->furnishes_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->furnishes_name . '</td>
                                <td>' . $res->furnishes_desc . '</td>';
            if ($access == 1 || $access == 2) {
                $html.='<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->furnishes_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a></td>';





 $html.='<td><a  title="Delete" href="javascript:;" onclick="';
                
                $html.=   in_array($res->furnishes_id ,$delc ) ? 'df()' :'deleteFurnishes(' . $res->furnishes_id . ')';

                $html.= '"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';





                
              // $html.='<td><a  title="Delete" href="javascript:;" onclick="deleteFurnishes(' . $res->furnishes_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-times"></i></button></a></td>';
            } else {
                $html.='<td>-</td>';
                $html.='<td>-</td>';
            }
            $html.='</tr>';
        }
        echo $html;
    }

    public function deleteFurnishes() {
        $id = $this->input->post('id');
        $data = array(
            'furnishes_is_deleted' => 1,
            'furnishes_modified_on' => date('yy-mm-dd H:i:s'),
            'furnishes_modified_by' => 1
        );
        $result = $this->Featuremaster_model->deleteFurnishes($data, $id);
        echo $result;
    }
   public function checkFurnishes(){
        $type=trim($this->input->post('type'));
        $id = $this->input->post('id');

        if ($id == 0) {
             $query=$this->db->query("select * from easamoveadmin_tbl_furnishes where  REPLACE(lower(furnishes_name),' ','')=REPLACE(lower('$type'),' ','') and furnishes_is_deleted=0");
        } else {

             $query=$this->db->query("select * from easamoveadmin_tbl_furnishes where  REPLACE(lower(furnishes_name),' ','')=REPLACE(lower('$type'),' ','') and furnishes_id != $id and furnishes_is_deleted=0");
        }

        
        if ($query->num_rows() > 0) {
            echo 1;
        }else{
            echo 0;
        }
    }
}
