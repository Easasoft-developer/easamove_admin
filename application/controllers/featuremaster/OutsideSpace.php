<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class OutsideSpace extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Featuremaster_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
$svar = $this->session->userdata['adminuser'];
           $data['access'] = $svar['access'];
            $this->load->view('common/header');
            $this->load->view('featuremaster/featuremaster');
            $this->load->view('featuremaster/outsidespace',$data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }
    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'outsidespace_name' => $this->input->post('Type'),
                'outsidespace_desc' => $this->input->post('Desc'),
                'outsidespace_created_on' => date('yy-mm-dd H:i:s'),
                'outsidespace_created_by' => 1
            );
            $result = $this->Featuremaster_model->addOutsidespace($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'outsidespace_name' => $this->input->post('Type'),
                'outsidespace_desc' => $this->input->post('Desc'),
                'outsidespace_modified_on' => date('yy-mm-dd H:i:s'),
                'outsidespace_modified_by' => 1
            );
            $result = $this->Featuremaster_model->updateOutsidespace($data, $id);
        }
        return $result;
    }

    public function getOutsidespace() {
        $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];
        $result = $this->Featuremaster_model->getOutsidespace();
        $html = ''; $nall[] = '';

        $this->db->distinct();
        $this->db->select('property_outspace');
        $query = $this->db->get("pro_tbl_property");

        foreach ($query->result() as $nkey => $nvalue) {
            
               if(is_numeric($nvalue->property_outspace)){
        $nall[] = $nvalue->property_outspace;
        } 
        else {
        preg_match_all('!\d+!', $nvalue->property_outspace, $matches);
        $nall = array_merge($nall,array_values($matches[0]));
        
        }
        }

        // print_r(array_unique($nall));



        foreach ($result as $key => $res) {
           
            $html.='<tr id="' . $res->outsidespace_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->outsidespace_name . '</td>
                            <td>' . $res->outsidespace_desc . '</td>';
            if ($access == 1 || $access == 2) {
                $html.='<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->outsidespace_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a></td>';
            } else {
                $html.='<td>-</td>';
            }
            if ($access == 1 || $access == 2) {
                $html.='<td><a  title="Delete" href="javascript:;" onclick="';

               $html.= in_array($res->outsidespace_id ,$nall ) ? 'df()' :'deleteOutsidespace(' . $res->outsidespace_id . ')';


                $html.='"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';
            } else {
                $html.='<td>-</td>';
            }
            $html.='</tr>';
        }
        echo $html;
    }

    public function deleteOutsidespace() {
        $id = $this->input->post('id');
        $data = array(
            'outsidespace_is_deleted' => 1,
            'outsidespace_modified_on' => date('yy-mm-dd H:i:s'),
            'outsidespace_modified_by' => 1
        );
        $result = $this->Featuremaster_model->deleteOutsidespace($data, $id);
        echo $result;
    }
    public function checkOutsidespace(){
        $type=trim($this->input->post('type'));
        $id = $this->input->post('id');

         if($id == 0){

            $query=$this->db->query("select * from easamoveadmin_tbl_outsidespace where  REPLACE(lower(outsidespace_name),' ','')=REPLACE(lower('$type'),' ','') and outsidespace_is_deleted=0");

         } else {

            $query=$this->db->query("select * from easamoveadmin_tbl_outsidespace where  REPLACE(lower(outsidespace_name),' ','')=REPLACE(lower('$type'),' ','') and outsidespace_id != $id and outsidespace_is_deleted=0");

         }


        if ($query->num_rows() > 0) {
            echo 1;
        }else{
            echo 0;
        }
    }

}