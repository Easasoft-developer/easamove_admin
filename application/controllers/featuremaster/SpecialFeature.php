<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class SpecialFeature extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Featuremaster_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
$svar = $this->session->userdata['adminuser'];
           $data['access'] = $svar['access'];
            $this->load->view('common/header');
            $this->load->view('featuremaster/featuremaster');
            $this->load->view('featuremaster/specialfeature',$data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }
    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'specialfeature_name' => $this->input->post('Type'),
                'specialfeature_desc' => $this->input->post('Desc'),
                'specialfeature_created_on' => date('yy-mm-dd H:i:s'),
                'specialfeature_created_by' => 1
            );
            $result = $this->Featuremaster_model->addSpecialfeature($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'specialfeature_name' => $this->input->post('Type'),
                'specialfeature_desc' => $this->input->post('Desc'),
                'specialfeature_modified_on' => date('yy-mm-dd H:i:s'),
                'specialfeature_modified_by' => 1
            );
            $result = $this->Featuremaster_model->updateSpecialfeature($data, $id);
        }
        return $result;
    }

    public function getSpecialfeature() {
        $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];
        $result = $this->Featuremaster_model->getSpecialfeature();
        $html = '';$nall[] = '';

        $this->db->distinct();
        $this->db->select('property_specialfeat');
        $query = $this->db->get("pro_tbl_property");

        foreach ($query->result() as $nkey => $nvalue) {
            
               if(is_numeric($nvalue->property_specialfeat)){
        $nall[] = $nvalue->property_specialfeat;
        } 
        else {
        preg_match_all('!\d+!', $nvalue->property_specialfeat, $matches);
        $nall = array_merge($nall,array_values($matches[0]));
        
        }
        }












        foreach ($result as $key => $res) {
           
            $html.='<tr id="' . $res->specialfeature_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->specialfeature_name . '</td>
                            <td>' . $res->specialfeature_desc . '</td>';
            if ($access == 1 || $access == 2) {
                $html.='<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->specialfeature_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></i></td>';
            } else {
                $html.='<td>-</td>';
            }
            if ($access == 1 || $access == 2) {



$html.='<td><a  title="Delete" href="javascript:;" onclick="';

               $html.= in_array($res->specialfeature_id ,$nall ) ? 'df()' :'deleteSpecialfeature(' . $res->specialfeature_id . ')';


                $html.='"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';








                // $html.='<td><a  title="Delete" href="javascript:;" onclick="deleteSpecialfeature(' . $res->specialfeature_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-times"></i></button></a></td>';




            } else {
                $html.='<td>-</td>';
            }
            $html.='</tr>';
        }
        echo $html;
    }

    public function deleteSpecialfeature() {
        $id = $this->input->post('id');
        $data = array(
            'specialfeature_is_deleted' => 1,
            'specialfeature_modified_on' => date('yy-mm-dd H:i:s'),
            'specialfeature_modified_by' => 1
        );
        $result = $this->Featuremaster_model->deleteSpecialfeature($data, $id);
        echo $result;
    }
    public function checkSpecialfeature(){
        $type=trim($this->input->post('type'));
        $id  = $this->input->post('id');

        if($id == 0) {
         $query=$this->db->query("select * from easamoveadmin_tbl_specialfeature where  REPLACE(lower(specialfeature_name),' ','')=REPLACE(lower('$type'),' ','') and specialfeature_is_deleted=0");
        } else {
            $query=$this->db->query("select * from easamoveadmin_tbl_specialfeature where  REPLACE(lower(specialfeature_name),' ','')=REPLACE(lower('$type'),' ','') and specialfeature_id != $id and specialfeature_is_deleted=0");

        }



        if ($query->num_rows() > 0) {
            echo 1;
        }else{
            echo 0;
        }
    }

}