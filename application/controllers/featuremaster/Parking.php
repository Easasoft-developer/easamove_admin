<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Parking extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Featuremaster_model');
        $this->load->library('session');
        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
$svar = $this->session->userdata['adminuser'];
           $data['access'] = $svar['access'];
            $this->load->view('common/header');
            $this->load->view('featuremaster/featuremaster');
            $this->load->view('featuremaster/parking',$data);
            $this->load->view('common/footer');
        } else {

            $this->load->view('login');
        }
    }
    public function insert() {

        if ($this->input->post('id') == 0) {
            $data = array(
                'parking_name' => $this->input->post('Type'),
                'parking_desc' => $this->input->post('Desc'),
                'parking_created_on' => date('yy-mm-dd H:i:s'),
                'parking_created_by' => 1
            );
            $result = $this->Featuremaster_model->addParking($data);
        } else {
            $id = $this->input->post('id');
            $data = array(
                'parking_name' => $this->input->post('Type'),
                'parking_desc' => $this->input->post('Desc'),
                'parking_modified_on' => date('yy-mm-dd H:i:s'),
                'parking_modified_by' => 1
            );
            $result = $this->Featuremaster_model->updateParking($data, $id);
        }
        return $result;
    }

    public function getParking() {
        $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];
        $result = $this->Featuremaster_model->getParking();
        $html = '';$nall[] = '';

        $this->db->distinct();
        $this->db->select('property_parking');
        $query = $this->db->get("pro_tbl_property");

        foreach ($query->result() as $nkey => $nvalue) {
            
               if(is_numeric($nvalue->property_parking)){
        $nall[] = $nvalue->property_parking;
        } 
        else {
        preg_match_all('!\d+!', $nvalue->property_parking, $matches);
        $nall = array_merge($nall,array_values($matches[0]));
        
        }
        }




        foreach ($result as $key => $res) {
           
            $html.='<tr id="' . $res->parking_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->parking_name . '</td>
                            <td>' . $res->parking_desc . '</td>';
            if ($access == 1 || $access == 2) {
                $html.='<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->parking_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></a></td>';
            } else {
                $html.='<td>-</td>';
            }
            if ($access == 1 || $access == 2) {



  $html.='<td><a  title="Delete" href="javascript:;" onclick="';

               $html.= in_array($res->parking_id ,$nall ) ? 'df()' :'deleteParking(' . $res->parking_id . ')';


                $html.='"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';




                // $html.='<td><a  title="Delete" href="javascript:;" onclick="deleteParking(' . $res->parking_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-times"></i></button></a></td>';



            } else {
                $html.='<td>-</td>';
            }
            $html.='</tr>';
        }
        echo $html;
    }

    public function deleteParking() {
        $id = $this->input->post('id');
        $data = array(
            'parking_is_deleted' => 1,
            'parking_modified_on' => date('yy-mm-dd H:i:s'),
            'parking_modified_by' => 1
        );
        $result = $this->Featuremaster_model->deleteParking($data, $id);
        echo $result;
    }
    public function checkParking(){
        $type=trim($this->input->post('type'));
        $id = $this->input->post('id');
         
         if ($id == 0) {

            $query=$this->db->query("select * from easamoveadmin_tbl_parking where  REPLACE(lower(parking_name),' ','')=REPLACE(lower('$type'),' ','') and parking_is_deleted=0");
             
         } else {

            $query=$this->db->query("select * from easamoveadmin_tbl_parking where  REPLACE(lower(parking_name),' ','')=REPLACE(lower('$type'),' ','') and parking_id != $id and parking_is_deleted=0");

         }


        if ($query->num_rows() > 0) {
            echo 1;
        }else{
            echo 0;
        }
    }

}