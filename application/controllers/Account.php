<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Account extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Admin_model');
        $this->load->library('session');
        $this->load->library('form_validation');

        // Your own constructor code
    }

    public function index() {
        if (isset($this->session->userdata['adminuser']['logged_in']) && $this->session->userdata['adminuser']['logged_in'] == TRUE) {
//            $this->load->view('dashboard');
            redirect('Dashboard');
        } else {

            $this->load->view('login');
        }
    }

    public function login() {
        $data = array(
            'admin_email' => $this->input->post('UserName'),
            'admin_password' => $this->input->post('Password'),            
            'admin_is_deleted'=>0
        );
        $res = $this->Admin_model->login($data);
        if($res == 1){
            $query = $this->db->query("SELECT admin_verifykey FROM easamoveadmin_tbl_admin WHERE admin_email='".$data['admin_email']."' AND admin_password='".$data['admin_password']."' AND admin_is_deleted=0");
            $queryres = $query->row();
            $var = strval($queryres->admin_verifykey);
            $emailver = $data['admin_email'];
            $link = "Account was not verified, please click following <a href='javascript:void(0)' id='getKeyElement' data-email='$emailver' data-key='$var' onclick='resendVerify()' class='color: #333;font-weight: bold;text-decoration: underline;margin: 0;'>Link</a>";

            $this->session->set_flashdata('logincheckAccount', $link);

            redirect('Account');
        }else{

            if (!empty($res)) {
                $session_array = array(
                    'id' => $res[0]->admin_id,
                    'name' => $res[0]->admin_name,
                    'email' => $res[0]->admin_email,
                    'access' => $res[0]->admin_access,
                    'logged_in' => TRUE
                );
                $this->session->set_userdata('adminuser', $session_array);
                redirect('Dashboard');
            } else {
                $this->session->set_flashdata('logincheck', 'Invalid Username (or) Password');

                redirect('Account');
            }
        }
    }

    public function changePassword() {
    
        $this->load->view('common/header');
       $this->load->view('changepassword');
        $this->load->view('common/footer');
    }
    public function myprofile(){
        $svar = $this->session->userdata['adminuser'];
         $res['data'] = $this->Admin_model->mgetUser($svar['id']);
        $this->load->view('common/header');
       $this->load->view('myprofile',$res);
        $this->load->view('common/footer');
    }

        public function forgotpassword() {



        $email = $this->input->post('UserEmail');

        $verifiedemail = $this->Admin_model->getUser($email);   

        if(!empty($verifiedemail)) { 



            $this->load->library('email');        
            $mydata['name'] = $verifiedemail[0]->admin_name;
            $mydata['email'] =  $email;
            $mydata['password'] = $verifiedemail[0]->admin_password;

            //property detail by passing property id     
            $message = $this->load->view('mail_template/forgot_password', $mydata, true);
            // $message = "hellhl";
            $this->email->from('info@admin.easamove.co.uk', 'Easamove Admin');
            
            $this->email->to($email);
            $this->email->subject('Forgot your password');
            
            $this->email->message($message);    
            
            $this->email->send();


}
        if (!empty($verifiedemail)) {
            $this->session->set_flashdata('forgot', 'Email has been sent successfully');
        } 
        else if($email == ''){
            $this->session->set_flashdata('forgot', '');

        }
        else  {
            $this->session->set_flashdata('forgot', 'Email not registered');

        }
        $this->load->view('forgotpassword');



    }

    public function verify($key = '') {
//        $key = $this->input->get('key');
        if ($key != '') {
            $res = $this->Admin_model->verify($key);
            if ($res != FALSE) {
                $this->load->library('email');
                $mydata['name'] = $res[0]->admin_name;
                $mydata['email'] = $res[0]->admin_email;
                $mydata['password'] = $res[0]->admin_password;

                $message = $this->load->view('mail_template/verifymail', $mydata, true);
                $this->email->from('info@admin.easamove.co.uk', 'Easamove Admin');

                $this->email->to($res[0]->admin_email);

                $this->email->subject('Welcome to Easamove');

                $this->email->message($message);

                $this->email->send();
               
               $this->session->set_flashdata('logincheck', 'Your Account verified');
            } else {
                $this->session->set_flashdata('logincheck', 'Your account has been already verified, please login with your login details');
            }
             
        } else {
            $this->session->set_flashdata('logincheck', 'There is some problem, please try again');
        }
         $this->load->view('login');
    }

    public function verifyagain() {
        // $res = $this->Admin_model->verify($key);
        $key = $this->input->post('verfiykey');
        $email = $this->input->post('email');
        $this->load->library('email');

        $query =  $this->db->get_where("easamoveadmin_tbl_admin",array("admin_email" => $email, "admin_verifykey" => $key));
         $result = $query->row();
                 $mydata['name'] =  $result->admin_name;

        $mydata['link'] = base_url() . 'account/verify/' . $key;

        $message = $this->load->view('mail_template/verify_again', $mydata, true);
        
        $this->email->from('info@admin.easamove.co.uk', 'Easamove Admin');

        $this->email->to($email);

        $this->email->subject('Verification email');

        $this->email->message($message);

        $this->email->send();
       
        // $this->session->set_flashdata('logincheck', 'Your Account verified');
        echo 1;
    }

    public function logout() {
        $session_array = array(
            'name' => '',
            'email' => '',
            'access' => '',
            'logged_in' => FALSE
        );

        $this->session->unset_userdata('adminuser');

        redirect();
    }

}
