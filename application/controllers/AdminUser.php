<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class AdminUser extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->model('Admin_model');
        $this->load->library('session');

// Your own constructor code
    }

    public function index() {
        $svar = $this->session->userdata['adminuser'];
        if ($svar) {
            $access = $svar['access'];
            $data['access'] = $access;
            $this->load->view('common/header');
            $this->load->view('adminuser', $data);
            $this->load->view('common/footer');
        } else {
            redirect();
        }
    }



public function selfupdateAdminuser() {


   $id = $this->input->post('id');

            $data = array(
                'admin_name' => $this->input->post('name'),
                'admin_email' => $this->input->post('Email'),
                'admin_role' => $this->input->post('role'),
                'admin_access' => $this->input->post('permission'),
                'admin_modified_on' => date('Y-m-d H:i:s'),
                'admin_modified_by' => 1
            );

$_SESSION['adminuser']['name'] = $this->input->post('name');
            $_SESSION['adminuser']['email'] = $this->input->post('Email');

$result = $this->Admin_model->selfupdateAdminuser($data, $id);

}


    public function insert() {
        $this->load->library('email');
$svar = $this->session->userdata['adminuser'];
        if ($this->input->post('id') == 0) {
            $key = $this->generateRandomString(16);
//mail function

            

          

            
            $data = array(
                'admin_name' => $this->input->post('name'),
                'admin_email' => $this->input->post('Email'),
                'admin_password' => $this->input->post('Password'),
                'admin_role' => $this->input->post('role'),
                'admin_access' => $this->input->post('permission'),
                'admin_created_on' => date('yy-mm-dd H:i:s'),
                'admin_created_by' => 1,
                'admin_verified' => 0,
                'admin_verifykey' => $key
            );
            $result = $this->Admin_model->addAdminuser($data);

            if($result == 1) {

           $mydata['name'] = $this->input->post('name');

            $mydata['link'] = base_url() . 'account/verify/' . $key;

            $message = $this->load->view('mail_template/welcome_message', $mydata, true);
            
            $this->email->from('info@admin.easamove.co.uk', 'Easamove Admin');

            $this->email->to($this->input->post('Email'));

            $this->email->subject('Welcome to Easamove');

            $this->email->message($message);

            $this->email->send();



            }


        } else {
            $id = $this->input->post('id');
            $data = array(
                'admin_name' => $this->input->post('name'),
                'admin_email' => $this->input->post('Email'),
                'admin_password' => $this->input->post('Password'),
                'admin_role' => $this->input->post('role'),
                'admin_access' => $this->input->post('permission'),
                'admin_modified_on' => date('Y-m-d H:i:s'),
                'admin_modified_by' => 1
            );


$result = $this->Admin_model->updateAdminuser($data, $id);
        if($svar['access'] == 1) { 

             $mydata['name'] = $this->input->post('name');
             $mydata['email'] =   $this->input->post('Email');
             $mydata['password'] =  $this->input->post('Password');
            

            $message = $this->load->view('mail_template/notify', $mydata, true);
            
            $this->email->from('info@admin.easamove.co.uk', 'Easamove Admin');

            $this->email->to($this->input->post('Email'));

            $this->email->subject('Your account credentials have been changed');

            $this->email->message($message);

            $this->email->send();
          

            // $_SESSION['adminuser']['name'] = $this->input->post('name');
            // $_SESSION['adminuser']['email'] = $this->input->post('Email');

            $svar = $this->session->userdata['adminuser'];

        
}
            
          
  


           


        }
        return $result;
    }

    // public function updateUsers(){
    //     $id = $this->input->post('id');
    //         $data = array(
    //             'admin_name' => $this->input->post('name'),
    //             'admin_email' => $this->input->post('Email'),
    //             'admin_role' => $this->input->post('role'),
    //             'admin_modified_on' => date('Y-m-d H:i:s'),
    //             'admin_modified_by' => 1
    //         );


    //         $result = $this->Admin_model->updateUsersAdmin($data, $id);
    //         if($result == 1 ) {
    //             $_SESSION['adminuser']['name'] = $this->input->post('name');
    //         }
    //         echo $result;

    // }



    public function updateUsers(){
        $this->load->library('email');
        $id = $this->input->post('id');


if($_SESSION['adminuser']['access'] == 1) {

    $data_email = $this->db->get_where('easamoveadmin_tbl_admin',array("admin_is_deleted" => 0,"admin_id"=> $id));
            foreach ($data_email->result() as $key => $value) {
            $d_email = $value->admin_email;
            }
 }


        if($_SESSION['adminuser']['access'] == 1 && $this->input->post('Email') != $d_email)  {

                  $pass = $this->generateRandomString(8);
                  $verifykey = $this->generateRandomString(16);
               
               $data = array(
                'admin_name' => $this->input->post('name'),
                'admin_email' => $this->input->post('Email'),
                'admin_password' => $pass,
                'admin_role' => $this->input->post('role'),
                'admin_verifykey' => $verifykey,
                'admin_verified' => 0,
                'admin_modified_on' => date('Y-m-d H:i:s'),
                'admin_modified_by' => 1
            );


               $result = $this->Admin_model->anotherupdateAdminuser($data,$id);

        
                 if($result) {

                
             $mydata['name'] = $this->input->post('name');
             $mydata['email'] =  $this->input->post('Email');
             $mydata['password'] =  $pass;
             $mydata['link'] = base_url()."Account/verify/".$verifykey;
            
             $message = $this->load->view('mail_template/admin_verify', $mydata, true);
            // $message = $this->load->view('mail_template/notify', $mydata, true);
            
            $this->email->from('info@admin.easamove.co.uk', 'Easamove Admin');

            $this->email->to($d_email);

            $this->email->subject('Your account credentials have been changed');

            $this->email->message($message);

            $this->email->send();
            }

echo 2;
        } else  {
            $data = array(
                'admin_name' => $this->input->post('name'),
                'admin_email' => $this->input->post('Email'),
                'admin_role' => $this->input->post('role'),
                'admin_modified_on' => date('Y-m-d H:i:s'),
                'admin_modified_by' => 1
            );

            $result = $this->Admin_model->updateUsersAdmin($data, $id);
            if($result == 1 ) {
                $_SESSION['adminuser']['name'] = $this->input->post('name');
            }
            echo $result;


        }

           
    }












    public function getAdminuser() {

        $svar = $this->session->userdata['adminuser'];
        $access = $svar['access'];
        $result = $this->Admin_model->getAdminuser();

        $html = '';
        foreach ($result as $key => $res) {
            $html.='<tr id="' . $res->admin_id . '">
                            <td>' . ($key + 1) . '</td>
                            <td>' . $res->admin_name . '</td>
                              <td>' . $res->admin_email . '</td><td>' . $res->admin_role . '</td>';
            if ($access == 1) {
                $html.='<td>' . $res->admin_password . '</td>';
            } else {
                $html.='<td>********</td>';
            }
            $acc='';
            if ($res->admin_access == 1) {
                $acc='Admin';
            } else if ($res->admin_access == 2) {
                $acc='Write';
            } else if ($res->admin_access == 3) {
                $acc='Read';
            }
            $html.='<td>' .$acc . '</td>';
            if ($access == 1 ) {

                
                $html.='<td><a title="Edit" href="javascript:;" onclick="edit(' . $res->admin_id . ')"><button type="button" class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i></button></td>';
            } else {
                $html.='<td>No Access</td>';
            }
            if ($access == 1) {
                $html.='<td><a  title="Delete" href="javascript:;" onclick="deleteAdminuser(' . $res->admin_id . ')"><button type="button" class="btn btn-danger btn-xs"><i class="fa fa-times"></i></button></a></td>';
            } else {
                $html.='<td>No Access</td>';
            }
            $html.='</tr>';
        }
        echo $html;
    }

    public function deleteAdminuser() {

        $id = $this->input->post('id');
        $data = array(
            'admin_is_deleted' => 1,
            'admin_modified_on' => date('yy-mm-dd H:i:s'),
            'admin_modified_by' => 1
        );
        $result = $this->Admin_model->deleteAdminuser($data, $id);
        echo $result;
    }

    public function resetPassword() {
        $svar = $this->session->userdata['adminuser'];
        $this->load->library('email');
        $email = $svar['email'];
        $data = array(
            'admin_password' => $this->input->post('newpassword'),
            'admin_modified_on' => date('yy-mm-dd H:i:s'),
            'admin_modified_by' => 1
        );
        $result = $this->Admin_model->resetPassword($data, $email);

        

        if($result == 1 ) {

        
        $mydata['name'] = $svar['name'];
        $mydata['email'] = $svar['email'];
        $mydata['password'] = $this->input->post('newpassword');

        $message = $this->load->view('mail_template/resetpassword', $mydata, true);
        $this->email->from('info@admin.easamove.co.uk', 'Easasoft');

        $this->email->to($email);

        $this->email->subject('Welcome to Easamove');

        $this->email->message($message);

        $this->email->send();
    }
        echo $result;
    }

    function generateRandomString($length) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public function checkemail() {

        $eid = $this->input->post('email');
        $id = $this->input->post('id');
        if($id == 0) {
        $query = $this->db->query("SELECT * from easamoveadmin_tbl_admin where admin_is_deleted=0 and admin_email='$eid'");
    } else {


        $query = $this->db->query("SELECT * from easamoveadmin_tbl_admin where admin_is_deleted=0 and admin_email='$eid' and admin_id != $id");

// print_r("SELECT * from easamoveadmin_tbl_admin where admin_is_deleted=0 and admin_email='$eid' and admin_id != $id");

    }


        if ($query->num_rows() > 0) {
            echo 1;
        } else {
            echo "0";
        }
    }

}
